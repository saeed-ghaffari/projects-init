﻿using System.Data;
using System.Data.SqlClient;
using Cerberus.Domain.Interface.Repositories;
using Microsoft.Extensions.Options;

namespace TestConnection
{
    public class SqlConnectionProvider : ISqlConnectionProvider
    {
        //private readonly SqlConnection _sqlConnection;
        public SqlConnectionProvider()
        {
            //_sqlConnection = new SqlConnection("Data Source=172.16.170.45;initial catalog=Cerberus;user ID=sa;password=123456;Connection Timeout=6;");
           // _sqlConnection.Open();
        }

        public IDbConnection GetConnection()
        {
            //return _sqlConnection;
            return new SqlConnection("Data Source=172.16.170.45;initial catalog=Cerberus;user ID=sa;password=123456;Connection Timeout=6;");
        }

        public IDbConnection GetReadConnection()
        {
            throw new System.NotImplementedException();
        }

        public IDbConnection GetRequestLogConnection()
        {
            throw new System.NotImplementedException();
        }

        public IDbConnection GetApiRequestLogConnection()
        {
            throw new System.NotImplementedException();
        }

        public IDbConnection GetStatusReportDbConnection()
        {
            throw new System.NotImplementedException();
        }
    }
}