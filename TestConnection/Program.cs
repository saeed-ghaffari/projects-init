﻿using System;
using System.IO;
using Cerberus.Domain.Interface.Repositories;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace TestConnection
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder();
                //.SetBasePath(Directory.GetCurrentDirectory())
                //.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            IConfigurationRoot configuration = builder.Build();
            var services = new ServiceCollection();
            services.AddTransient<ISqlConnectionProvider, SqlConnectionProvider>();
            services.AddTransient<IUserRepository, UserRepository>();

            Console.WriteLine($"{TimeSpan.FromDays(2).ToString()}");
            Console.WriteLine($"{TimeSpan.FromHours(48).ToString()}");
            Console.WriteLine($"{TimeSpan.FromHours(48) == TimeSpan.FromDays(2)}");




            var serviceProvider = services.BuildServiceProvider();


            var uService = serviceProvider.GetService<IUserRepository>();

            Console.WriteLine("---------- GetList 1:");
            uService.GetList();

            Console.WriteLine("---------- GetList 2:");
            uService.GetList();

            Console.WriteLine("---------- GetList 3:");
            uService.GetList();

            Console.ReadLine();

        }
    }
}
