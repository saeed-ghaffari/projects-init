﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using ProtoBuf;

namespace GateKeeper.Auth.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("Resource", Schema = "auth")]
    public class Resource : BaseModel
    {
        public string Title { get; set; }

        public string Controller { get; set; }

        public string Action { get; set; }

        [ProtoIgnore]
        public IEnumerable<RoleResource> RoleResources { get; set; }
    }
}