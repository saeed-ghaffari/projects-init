﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using ProtoBuf;

namespace GateKeeper.Auth.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("Role", Schema = "auth")]
    public class Role : BaseModel
    {
        public string Title { get; set; }

        public IEnumerable<RoleResource> RoleResources { get; set; }

        [ProtoIgnore]
        public IEnumerable<UserRole> Roles { get; set; }
    }
}
