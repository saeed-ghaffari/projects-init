﻿using System;

namespace GateKeeper.Auth
{
    public class GateKeeperAuthConfiguration
    {
        public string MsSqlConnection { get; set; }
        public bool LockOutOnFailure { get; set; }
        public int MaxFailedAccessAttempts { get; set; }
        public TimeSpan DefaultLockoutTimeSpan { get; set; }


    }
}