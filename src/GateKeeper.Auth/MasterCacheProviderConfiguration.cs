﻿using System;

namespace GateKeeper.Auth
{
    public class MasterCacheProviderConfiguration
    {
        public bool IsActive { get; set; }
        public TimeSpan ExpirationTime { get; set; }
        public TimeSpan UserExpirationTime { get; set; }

    }
}
