﻿using System;
using System.Threading.Tasks;
using Cerberus.Utility;
using GateKeeper.Auth.Enum;
using GateKeeper.Auth.Interface;
using GateKeeper.Auth.Model;
using Microsoft.Extensions.Options;

namespace GateKeeper.Auth.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly GateKeeperAuthConfiguration _configuration;
        private readonly IMasterCacheProvider _masterCacheProvider;
        private readonly MasterCacheProviderConfiguration _masterCacheProviderConfiguration;
        public UserService(IUserRepository userRepository,IOptions<GateKeeperAuthConfiguration> configuration,
            IMasterCacheProvider masterCacheProvider,IOptions<MasterCacheProviderConfiguration> masterCacheProviderConfiguration)
        {
            _userRepository = userRepository;
            _configuration = configuration.Value;
            _masterCacheProvider = masterCacheProvider;
            _masterCacheProviderConfiguration = masterCacheProviderConfiguration.Value;
        }

        public Task CreateAsync(User user)
        {
            user.Password = HashHelper.GenerateMd5String(user.Password);

            return _userRepository.CreateAsync(user);
        }

        public async Task<User> GetAsync(long id, bool includeAllRelatedData)
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(UserService)}--key:{id}:{includeAllRelatedData}",
                async () => await _userRepository.GetAsync(id, includeAllRelatedData),
                _masterCacheProviderConfiguration.UserExpirationTime);
            
        }

        public async Task<User> GetAsync(string userName)
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(UserService)}:key:UserName:{userName}",
                async () => await _userRepository.GetAsync(userName),
                _masterCacheProviderConfiguration.UserExpirationTime);
            
        }

        public async Task<User> GetAsync(string userName, string password)
        {
            var user = await _userRepository.GetAsync(userName);

            if (user != null)
            {
                if (user.LockedUntil.HasValue && DateTime.Now < user.LockedUntil)
                    throw new GateKeeperAuthException(ErrorCode.Locked);

                if (HashHelper.GenerateMd5String(password).Equals(user.Password))
                {
                    //user.LastLogIn = DateTime.Now;
                    //user.LockedUntil = null;
                    //user.Attempts = 0;
                    await _userRepository.UpdateAsync(user.Id,DateTime.Now);

                    return user;
                }

                user.Attempts += 1;

                if (_configuration.LockOutOnFailure && _configuration.MaxFailedAccessAttempts <= user.Attempts)
                    user.LockedUntil = DateTime.Now.Add(_configuration.DefaultLockoutTimeSpan);

                await _userRepository.UpdateAsync(user.Id,user.Attempts,user.LockedUntil);
            }

            throw new GateKeeperAuthException(ErrorCode.InCorrectUsernameOrPassword);

        }
    }
}