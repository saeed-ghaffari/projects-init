﻿using System;
using System.Threading.Tasks;
using Komodo.Caching.Abstractions;
using Microsoft.Extensions.Logging;

namespace GateKeeper.Auth.Service
{
    public class BaseService
    {
        private readonly ICacheProvider _cacheProvider;
        private readonly ILogger _logger;
        private readonly TimeSpan? _reconstructTime;
        private readonly TimeSpan? _cacheExpiration;
        private readonly TimeSpan? _ttl;

        public BaseService(ICacheProvider cacheProvider, ILogger logger, TimeSpan? reconstructTime, TimeSpan? cacheExpiration, TimeSpan? ttl)
        {
            _cacheProvider = cacheProvider;
            this._reconstructTime = reconstructTime;
            this._cacheExpiration = cacheExpiration;
            this._ttl = ttl;
            _logger = logger;
        }

        public async Task<T> FetchAsync<T>(string cacheKey, Func<Task<T>> getObjectFromStorageFunc, TimeSpan? reconstructTime, TimeSpan? cacheExpiration, TimeSpan? ttl)
        {
            CacheItem<T> cacheObject = null;

            try
            {
                cacheObject = await _cacheProvider.FetchAsync<T>(cacheKey, reconstructTime);


                if (cacheObject == null || cacheObject.Data == null || cacheObject.IsExpired)
                {
                    var result = await getObjectFromStorageFunc();
                    if (result == null)
                        return default(T);

                    await _cacheProvider.StoreAsync(cacheKey, result,
                        cacheExpiration, ttl);

                    return result;
                }

                return cacheObject.Data;

            }
            catch (Exception e)
            {
                _logger.LogCritical(e, e.Message);
            }

            return cacheObject != null ? cacheObject.Data : await getObjectFromStorageFunc();
        }

        public Task<T> FetchAsync<T>(string cacheKey, Func<Task<T>> getObjectFromStorageFunc)
        {
            return FetchAsync(
                cacheKey,
                getObjectFromStorageFunc,
                _reconstructTime,
                _cacheExpiration,
                _ttl);
        }
    }
}