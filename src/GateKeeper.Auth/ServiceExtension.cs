﻿using GateKeeper.Auth.Interface;
using GateKeeper.Auth.Repository;
using GateKeeper.Auth.Service;
using Komodo.Caching.Abstractions;
using Komodo.Caching.Redis;
using Komodo.Redis.StackExchange;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using StackExchange.Redis;

namespace GateKeeper.Auth
{
    public static class ServiceExtension
    {
        /// <summary>
        /// before register 'GateKeeper Auth Services', IDatabase and ISerializer must be registered,
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public static void AddGateKeeperAuthServices(this IServiceCollection services, IConfiguration configuration)
        {
            var section = configuration.GetSection("GateKeeperAuthConfiguration");
            var connectionConfiguration = new GateKeeperAuthConfiguration();

            section.Bind(connectionConfiguration);
            services.Configure<GateKeeperCacheConfiguration>(configuration.GetSection("GateKeeperCacheConfiguration"));
            services.Configure<MasterCacheProviderConfiguration>(configuration.GetSection("MasterCacheProviderConfiguration"));

            services.AddDbContextPool<AuthDbContext>(options =>
            {
                options.UseSqlServer(connectionConfiguration.MsSqlConnection);
            });


            //repositories
            services.AddTransient<IUserRepository, UserRepository>();

            //services
            services.AddTransient<IUserService, UserService>();
            services.AddScoped<IMasterCacheProvider, MasterCacheProvider>();

        }
    }
}