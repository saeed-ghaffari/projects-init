﻿using GateKeeper.Auth.Model;
using Microsoft.EntityFrameworkCore;

namespace GateKeeper.Auth.Repository
{
    public class AuthDbContext : DbContext
    {
        public AuthDbContext(DbContextOptions<AuthDbContext> options) : base(options)
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Resource> Resources { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<RoleResource> RoleResources { get; set; }
    }
}