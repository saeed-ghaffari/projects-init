﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class AddAlleyAndPlaqueToAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Alley",
                table: "Address",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Plaque",
                table: "Address",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Alley",
                table: "Address");

            migrationBuilder.DropColumn(
                name: "Plaque",
                table: "Address");
        }
    }
}
