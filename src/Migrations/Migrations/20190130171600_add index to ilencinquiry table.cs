﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addindextoilencinquirytable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Iban_SuccessInquiry",
                table: "IbanInquiry");

            migrationBuilder.AlterColumn<string>(
                name: "NationalCode",
                table: "IlencInquiry",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Iban",
                table: "IbanInquiry",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_NationalCode_SuccessInquiry_CreationDate",
                table: "IlencInquiry",
                columns: new[] { "NationalCode", "SuccessInquiry", "CreationDate" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_NationalCode_SuccessInquiry_CreationDate",
                table: "IlencInquiry");

            migrationBuilder.AlterColumn<string>(
                name: "NationalCode",
                table: "IlencInquiry",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Iban",
                table: "IbanInquiry",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Iban_SuccessInquiry",
                table: "IbanInquiry",
                columns: new[] { "Iban", "SuccessInquiry", "CreationDate" });
        }
    }
}
