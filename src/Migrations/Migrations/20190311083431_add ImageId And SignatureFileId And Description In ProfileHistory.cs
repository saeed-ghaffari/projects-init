﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addImageIdAndSignatureFileIdAndDescriptionInProfileHistory : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "ProfileHistory",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ImageId",
                table: "ProfileHistory",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "SignatureFileId",
                table: "ProfileHistory",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProfileHistory_ImageId",
                table: "ProfileHistory",
                column: "ImageId");

            migrationBuilder.CreateIndex(
                name: "IX_ProfileHistory_SignatureFileId",
                table: "ProfileHistory",
                column: "SignatureFileId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProfileHistory_File_ImageId",
                table: "ProfileHistory",
                column: "ImageId",
                principalTable: "File",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);

            migrationBuilder.AddForeignKey(
                name: "FK_ProfileHistory_File_SignatureFileId",
                table: "ProfileHistory",
                column: "SignatureFileId",
                principalTable: "File",
                principalColumn: "Id",
                onDelete: ReferentialAction.NoAction);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProfileHistory_File_ImageId",
                table: "ProfileHistory");

            migrationBuilder.DropForeignKey(
                name: "FK_ProfileHistory_File_SignatureFileId",
                table: "ProfileHistory");

            migrationBuilder.DropIndex(
                name: "IX_ProfileHistory_ImageId",
                table: "ProfileHistory");

            migrationBuilder.DropIndex(
                name: "IX_ProfileHistory_SignatureFileId",
                table: "ProfileHistory");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "ProfileHistory");

            migrationBuilder.DropColumn(
                name: "ImageId",
                table: "ProfileHistory");

            migrationBuilder.DropColumn(
                name: "SignatureFileId",
                table: "ProfileHistory");
        }
    }
}
