﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addbankIdtoauthenticationofficeentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Telephone",
                table: "AuthenticationOffices",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Address",
                table: "AuthenticationOffices",
                maxLength: 1000,
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "BankId",
                table: "AuthenticationOffices",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AuthenticationOffices_BankId",
                table: "AuthenticationOffices",
                column: "BankId");

            migrationBuilder.AddForeignKey(
                name: "FK_AuthenticationOffices_Bank_BankId",
                table: "AuthenticationOffices",
                column: "BankId",
                principalTable: "Bank",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AuthenticationOffices_Bank_BankId",
                table: "AuthenticationOffices");

            migrationBuilder.DropIndex(
                name: "IX_AuthenticationOffices_BankId",
                table: "AuthenticationOffices");

            migrationBuilder.DropColumn(
                name: "BankId",
                table: "AuthenticationOffices");

            migrationBuilder.AlterColumn<string>(
                name: "Telephone",
                table: "AuthenticationOffices",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Address",
                table: "AuthenticationOffices",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 1000,
                oldNullable: true);
        }
    }
}
