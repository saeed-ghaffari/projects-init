﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class nullfeildprivatepersonmembership : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte>(
                name: "Gender",
                schema: "mem",
                table: "PrivatePersonMembership",
                nullable: true,
                oldClrType: typeof(byte));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<byte>(
                name: "Gender",
                schema: "mem",
                table: "PrivatePersonMembership",
                nullable: false,
                oldClrType: typeof(byte),
                oldNullable: true);
        }
    }
}
