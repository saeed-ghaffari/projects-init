﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addindextonocrinquirytable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_NationalCode_SuccessInquiry_CreationDate",
                table: "IlencInquiry");

            migrationBuilder.AlterColumn<string>(
                name: "Ssn",
                table: "NocrInquiry",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NationalCode",
                table: "IlencInquiry",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Ssn_SuccessInquiry_CreationDate",
                table: "NocrInquiry",
                columns: new[] { "Ssn", "SuccessInquiry", "CreationDate" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Ssn_SuccessInquiry_CreationDate",
                table: "NocrInquiry");

            migrationBuilder.AlterColumn<string>(
                name: "Ssn",
                table: "NocrInquiry",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "NationalCode",
                table: "IlencInquiry",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_NationalCode_SuccessInquiry_CreationDate",
                table: "IlencInquiry",
                columns: new[] { "NationalCode", "SuccessInquiry", "CreationDate" });
        }
    }
}
