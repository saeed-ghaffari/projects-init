﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addcontrollerandactioninProfilingRequestLog : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Action",
                schema: "log",
                table: "ProfilingRequestLog",
                maxLength: 200,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Controller",
                schema: "log",
                table: "ProfilingRequestLog",
                maxLength: 200,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Action",
                schema: "log",
                table: "ProfilingRequestLog");

            migrationBuilder.DropColumn(
                name: "Controller",
                schema: "log",
                table: "ProfilingRequestLog");
        }
    }
}
