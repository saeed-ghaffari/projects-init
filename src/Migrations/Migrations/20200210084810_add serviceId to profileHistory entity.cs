﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addserviceIdtoprofileHistoryentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ServiceId",
                table: "ProfileHistory",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProfileHistory_ServiceId",
                table: "ProfileHistory",
                column: "ServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProfileHistory_ThirdPartyService_ServiceId",
                table: "ProfileHistory",
                column: "ServiceId",
                principalTable: "ThirdPartyService",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProfileHistory_ThirdPartyService_ServiceId",
                table: "ProfileHistory");

            migrationBuilder.DropIndex(
                name: "IX_ProfileHistory_ServiceId",
                table: "ProfileHistory");

            migrationBuilder.DropColumn(
                name: "ServiceId",
                table: "ProfileHistory");
        }
    }
}
