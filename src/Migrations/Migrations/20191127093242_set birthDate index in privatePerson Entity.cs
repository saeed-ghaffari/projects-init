﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class setbirthDateindexinprivatePersonEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_PrivatePerson_BirthDate",
                table: "PrivatePerson",
                column: "BirthDate");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PrivatePerson_BirthDate",
                table: "PrivatePerson");
        }
    }
}
