﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addhasFactorpropertytoThirdPartyServiceentity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                  name: "HasFactor",
                  table: "ThirdPartyService",
                  nullable: false,
                  defaultValue: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HasFactor",
                table: "ThirdPartyService");

        }
    }
}
