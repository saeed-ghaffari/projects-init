﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class addindextoibaninquirytable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Iban",
                table: "IbanInquiry",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Iban_SuccessInquiry",
                table: "IbanInquiry",
                columns: new[] { "Iban", "SuccessInquiry" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Iban_SuccessInquiry",
                table: "IbanInquiry");

            migrationBuilder.AlterColumn<string>(
                name: "Iban",
                table: "IbanInquiry",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
