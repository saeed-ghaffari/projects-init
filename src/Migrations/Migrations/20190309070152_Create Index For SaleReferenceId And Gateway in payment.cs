﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cerberus.Migrations.Migrations
{
    public partial class CreateIndexForSaleReferenceIdAndGatewayinpayment : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "SaleReferenceId",
                table: "Payment",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Payment_SaleReferenceId_Gateway",
                table: "Payment",
                columns: new[] { "SaleReferenceId", "Gateway" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Payment_SaleReferenceId_Gateway",
                table: "Payment");

            migrationBuilder.AlterColumn<string>(
                name: "SaleReferenceId",
                table: "Payment",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
