﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Cerberus.Domain;
using Cerberus.Domain.Enum;
using Gatekeeper.Core;
using Gatekeeper.Core.Interface;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using Cerberus.Api.Model;
using Gatekeeper.AspModules;
using GateKeeper.Auth.Interface;
using GateKeeper.Auth.Model;
using Microsoft.AspNetCore.Mvc;

namespace Cerberus.Api.Filters
{
    public class AuthorizeUserAttribute : IAsyncActionFilter, IOrderedFilter
    {

        private readonly IJwtTokenService<IssuerAccessTokenPayload> _jwtTokenService;
        private readonly IUserService _userService;
        private readonly RequestLogConfiguration _requestLogConfiguration;
        public int Order { get; set; }

        public AuthorizeUserAttribute(
            IJwtTokenService<IssuerAccessTokenPayload> sessionService,
            IUserService userService,
            IOptions<RequestLogConfiguration> requestLogConfiguration)
        {
            _jwtTokenService = sessionService;
            _userService = userService;
            _requestLogConfiguration = requestLogConfiguration.Value;
        }


        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (!context.HttpContext.Request.Headers.TryGetValue("Authorization", out var tokenWithSchema))
                throw new CerberusException(ErrorCode.Unauthorized);

            var parts = tokenWithSchema.ToString().Split(' ');
            if (parts.Length != 2)
                throw new CerberusException(ErrorCode.Unauthorized);

            if (parts[0].ToLower() != "bearer")
                throw new CerberusException(ErrorCode.Unauthorized);

            try
            {
                //validate token and retrieve payload
                var tokenPayload = await _jwtTokenService.RetrievePayloadFromTokenAsync(parts[1], true);

                var user = await _userService.GetAsync(tokenPayload.UserId, true);
                if (user == null)
                    throw new CerberusException(ErrorCode.Unauthorized);


                var actionName = context.ActionDescriptor.RouteValues["Action"];
                var controllerName = context.ActionDescriptor.RouteValues["Controller"];

                var allResources = new List<Resource>();

                foreach (var items in user.UserRoles.Select(x => x.Role))
                {
                    allResources.AddRange(items.RoleResources.Select(x => x.Resource).ToList());
                }

                if (allResources.Count == 0)
                    throw new CerberusException(ErrorCode.Forbidden);

                if (allResources.FirstOrDefault(x =>
                        (x.Controller == "*" || x.Controller.ToLower().Equals(controllerName.ToLower())) &&
                        (x.Action == "*" || x.Action.ToLower().Equals(actionName.ToLower()))) == null)
                    throw new CerberusException(ErrorCode.Forbidden);


                //set payload in request items
                context.HttpContext.Items.Add("CurrentUser", tokenPayload);
                context.HttpContext.Items.Add("ServiceId", tokenPayload.ServiceId);

                //invoke next step
                await next.Invoke();
            }
            catch (GateKeeperException)
            {
                throw new CerberusException(ErrorCode.Unauthorized);
            }
        }


    }
}