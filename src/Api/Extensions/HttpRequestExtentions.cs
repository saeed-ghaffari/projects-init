﻿using System.Linq;
using System.Net;
using Cerberus.Api.Model;
using Microsoft.AspNetCore.Http;

namespace Cerberus.Api.Extensions
{
    public static class HttpRequestExtensions
    {
        public static string GetUserAgent(this HttpRequest request)
        {
            return request.Headers["User-Agent"].ToString();
        }

   
        public static IssuerAccessTokenPayload GetCurrentServiceAccessTokenPayload(this HttpRequest request)
        {
            return request.HttpContext.Items["CurrentUser"] as IssuerAccessTokenPayload;
        }
    }
}