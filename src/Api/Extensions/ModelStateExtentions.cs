﻿using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Cerberus.Api.Extensions
{
    public static class ModelStateExtensions
    {
        public static object ToViewModel(this ModelStateDictionary state)
        {
            return new
            {
                ValidationErrors = state.Select(x => new
                {
                    PropertyName = x.Key,
                    Issues = x.Value.Errors.Select(v => v.ErrorMessage)
                })
            };
        }
    }
}
