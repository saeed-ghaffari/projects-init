﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using AutoMapper;
using Cerberus.Api.ApiLogic;
using Cerberus.Api.Configuration;
using Cerberus.Api.Filters;
using Cerberus.Api.MappingProfile;
using Cerberus.Api.Model;
using Cerberus.ApiService;
using Cerberus.ApiService.Implementations;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Repositories.Log;
using Cerberus.Domain.Interface.Repositories.Membership;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Interface.Services.FGM;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Domain.ViewModel.Inquiries;
using Cerberus.MockService;
using Cerberus.Repository.MsSql;
using Cerberus.Repository.MsSql.Log;
using Cerberus.Repository.MsSql.Membership;
using Cerberus.Service;
using Cerberus.Service.BankService.Configuration;
using Cerberus.Service.FGM;
using Cerberus.Service.Implementations;
using Cerberus.Service.Implementations.Inquiries;
using Cerberus.TasksManager.Core;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Repository;
using Gatekeeper.AspModules;
using Gatekeeper.AspModules.ActionFilter;
using Gatekeeper.AspModules.Interface;
using Gatekeeper.AspModules.Repository;
using Gatekeeper.AspModules.Service;
using GateKeeper.Auth;
using Gatekeeper.Core.Configurations;
using Gatekeeper.Core.Implementation;
using Gatekeeper.Core.Interface;
using Komodo.Caching.Abstractions;
using Komodo.Caching.Redis;
using Komodo.Redis.StackExchange;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using Swashbuckle.AspNetCore.Swagger;
using File = Cerberus.Domain.Model.File;
using MasterCacheProviderConfiguration = Cerberus.Service.MasterCacheProviderConfiguration;
using Profile = AutoMapper.Profile;
using Cerberus.Repository;

namespace Cerberus.Api.Extensions
{
    public static class ServicesExtensions
    {
        public static void AddCerberusServices(this IServiceCollection services, IConfiguration configuration)
        {
            //Configurations

            var connectionSection = configuration.GetSection("ConnectionStrings");
            var connectionConfiguration = new ConnectionStringsConfiguration();
            connectionSection.Bind(connectionConfiguration);
            services.Configure<ConnectionStringsConfiguration>(connectionSection);
            Connection.SetConnectionString(connectionConfiguration.ReadConnection);
            Connection.SetConnectionString(connectionConfiguration.MasterDbConnection);
            services.Configure<MessagingConfiguration>(configuration.GetSection("MessagingConfiguration"));
            services.Configure<OtpConfiguration>(configuration.GetSection("OtpConfiguration"));
            services.Configure<JwtTokenConfiguration>(configuration.GetSection("JwtTokenConfiguration"));
            services.Configure<RequestLogConfiguration>(configuration.GetSection("RequestLogConfiguration"));
            services.Configure<ApiConfiguration>(configuration.GetSection("ApiConfiguration"));
            services.Configure<FileStorageConfiguration>(configuration.GetSection("FileStorageConfiguration"));
            services.Configure<ObjectCacheServicesConfiguration>(configuration.GetSection("ObjectCacheServicesConfiguration"));
            services.Configure<RateLimitConfiguration>(configuration.GetSection("RateLimitConfiguration"));
            services.Configure<PermanentOtpConfiguration>(configuration.GetSection("PermanentOtpConfiguration"));
            services.Configure<AuthenticatorConfiguration>(configuration.GetSection("AuthenticatorConfiguration"));
            services.Configure<IxServiceConfiguration>(configuration.GetSection("IxServiceConfiguration"));
            services.Configure<IssuanceStockCodeConfiguration>(configuration.GetSection("IssuanceStockCodeConfiguration"));
            services.Configure<FgmConfiguration>(configuration.GetSection("FgmConfiguration"));
            services.Configure<FgmConfiguration>(configuration.GetSection("FgmConfiguration"));
            services.Configure<LegalPersonsWhiteListConfiguration>(configuration.GetSection("LegalPersonsWhiteListConfiguration"));
            services.Configure<MasterCacheProviderConfiguration>(configuration.GetSection("MasterCacheProviderConfiguration"));
            services.Configure<GateKeeperAuthConfiguration>(configuration.GetSection("GateKeeperAuthConfiguration"));
            services.Configure<EntryNodeConfiguration>(configuration.GetSection("EntryNodeConfiguration"));
            Connection.SetConnectionString(connectionConfiguration.TaskManagerConnection);//for offline task


            services.AddSingleton<ISqlConnectionProvider, SqlConnectionProvider>();

            //dbcontext

            services.AddDbContextPool<CerberusDbContext>(options =>
            {
                options.UseSqlServer(connectionConfiguration.MasterDbConnection);
                options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);

            }, 4096);

            services.AddDbContextPool<CerberusLogDbContext>(options =>
            {
                options.UseSqlServer(connectionConfiguration.LogDbConnection);
                options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);

            }, 4096);

            #region Redis
            //Redis
            var redisConfiguration = new ConfigurationOptions();
            foreach (var endpoint in connectionConfiguration.RedisWriteDbHost.Split(';'))
            {
                redisConfiguration.EndPoints.Add(endpoint);
            }
            redisConfiguration.Password = connectionConfiguration.RedisWriteDbPass;
            redisConfiguration.ConnectTimeout = 400;
            services.AddSingleton<ConfigurationOptions>(redisConfiguration);

            services.AddSingleton<IConnectionMultiplexerProvider>(new ConnectionMultiplexerProvider(redisConfiguration, Environment.ProcessorCount * 2));

            services.AddSingleton<IDataBaseProvider, DatabaseProvider>();

            services.AddTransient<IDatabase>(provider => provider.GetService<IDataBaseProvider>().GetWriteDatabase());

            services.AddSingleton<ISerializer, BinarySerializer>();

            services.AddTransient<ICacheProvider>(provider =>
            {
                if (!provider.GetService<IOptions<ApiConfiguration>>().Value.EnableCache)
                    return new NullCacheProvider();

                return new RedisCacheProvider(
                    provider.GetService<IDatabase>(),
                    provider.GetService<IDatabase>(),
                    provider.GetService<ISerializer>());
            });
            #endregion

            //Repositories

            //---gatekeeper 
            services.AddSingleton<IRequestLogRepository, LogRequestRepository>();
            services.AddScoped<IThirdPartyServiceRepository, ThirdPartyServiceRepository>();
            services.AddScoped<IProfileRepository, ProfileRepository>();
            services.AddScoped<IAgentRepository, AgentRepository>();
            services.AddScoped<IAgentDocumentRepository, AgentDocumentRepository>();
            services.AddScoped<IFileRepository, FileRepository>();
            services.AddScoped<IPrivatePersonRepository, PrivatePersonRepository>();
            services.AddScoped<IAddressRepository, AddressRepository>();
            services.AddScoped<IBankingAccountRepository, BankingAccountRepository>();
            services.AddScoped<IBrokerRepository, BrokerRepository>();
            services.AddScoped<ICountryRepository, CountryRepository>();
            services.AddScoped<IFinancialInfoRepository, FinancialInfoRepository>();
            services.AddScoped<IJobInfoRepository, JobInfoRepository>();
            services.AddScoped<IJobRepository, JobRepository>();
            services.AddScoped<IPrivatePersonRepository, PrivatePersonRepository>();
            services.AddScoped<ITradingCodeRepository, TradingCodeRepository>();
            services.AddScoped<IProvinceRepository, ProvinceRepository>();
            services.AddScoped<ICityRepository, CityRepository>();
            services.AddScoped<IAddressSectionRepository, AddressSectionRepository>();
            services.AddScoped<IBankRepository, BankRepository>();
            services.AddScoped<IPaymentRepository, PaymentRepository>();
            services.AddScoped<ILegalPersonShareholderRepository, LegalPersonShareholderRepository>();
            services.AddScoped<ILegalPersonStakeholderRepository, LegalPersonStakeholderRepository>();
            services.AddScoped<ILegalPersonRepository, LegalPersonRepository>();
            services.AddScoped<IProfileHistoryRepository, ProfileHistoryRepository>();
            services.AddScoped<IAuthenticationOfficesRepository, AuthenticationOfficesRepository>();
            services.AddScoped<IPermanentOtpRepository, PermanentOtpRepository>();
            services.AddSingleton<IApiRequestLogRepository, ApiRequestLogRepository>();
            services.AddSingleton<ISmsLogRepository, SmsLogRepository>();
            services.AddScoped<IAgentArchiveRepository, AgentArchiveRepository>();
            services.AddScoped<IAgentDocumentArchiveRepository, AgentDocumentArchiveRepository>();
            services.AddScoped<IIssuanceStockCodeRepository, IssuanceStockCodeRepository>();
            services.AddScoped<IIssuanceStockCodeResponseRepository, IssuanceStockCodeResponseRepository>();
            services.AddScoped<IProfilingRequestLogRepository, ProfilingRequestLogRepository>();
            services.AddScoped<IEtfMembershipRepository, EtfMembershipRepository>();
            services.AddScoped<IMsisdnRepository, MsisdnRepository>();
            
           
            services.AddScoped<ITaskRepository, SqlTaskRepository>();


            #region ReadRepositories
            //read repositories
            services.AddCerberusReadRepositories(configuration);
            #endregion

            //services
            //----banking gateway
            services.AddScoped<MockGatewayService>();
            services.AddScoped<MockMelliGatewayService>();
            //----Api Services
            services.AddScoped<IAddressApiService, AddressApiService>();
            services.AddScoped<IProfileApiService, ProfileApiService>();
            services.AddScoped<IBankingAccountApiService, BankingAccountApiService>();
            services.AddScoped<IFinancialInfoApiService, FinancialInfoApiService>();
            services.AddScoped<IJobInfoApiService, JobInfoApiService>();
            services.AddScoped<ILegalPersonApiService, LegalPersonApiService>();
            services.AddScoped<IPrivatePersonApiService, PrivatePersonApiService>();
            services.AddScoped<ITradingCodeApiService, TradingCodesApiService>();

            services.AddScoped<IThirdPartyServiceService, ThirdPartyServiceService>();
            services.AddScoped<IMessagingService, MessagingService>();
            services.AddScoped<IProfileService, ProfileService>();
            services.AddScoped<IAgentService, AgentService>();
            services.AddScoped<IAgentDocumentService, AgentDocumentService>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IPrivatePersonService, PrivatePersonService>();
            services.AddScoped<IAddressService, AddressService>();
            services.AddScoped<IBankingAccountService, BankingAccountService>();
            services.AddScoped<IBrokerService, BrokerService>();
            services.AddScoped<ICountryService, CountryService>();
            services.AddScoped<IFinancialInfoService, FinancialInfoService>();
            services.AddScoped<IJobInfoService, JobInfoService>();
            services.AddScoped<IJobService, JobService>();
            services.AddScoped<IPrivatePersonService, PrivatePersonService>();
            services.AddScoped<ITradingCodeService, TradingCodeService>();
            services.AddScoped<IBankService, BankService>();
            services.AddScoped<IProvinceService, ProvinceService>();
            services.AddScoped<ICityService, CityService>();
            services.AddScoped<IAddressSectionService, AddressSectionService>();
            services.AddScoped<IBankingPaymentGateway, MockGatewayService>();
            services.AddScoped<IBankMelliPaymentGateway, MockMelliGatewayService>();
            services.AddScoped<IPaymentService, PaymentService>();
            services.AddScoped<IBankService, BankService>();
            services.AddScoped<ILegalPersonShareholderService, LegalPersonShareholderService>();
            services.AddScoped<ILegalPersonStakeholderService, LegalPersonStakeholderService>();
            services.AddScoped<ILegalPersonService, LegalPersonService>();
            services.AddScoped<IOfflineTaskService, OfflineTaskService>();
            services.AddScoped<ITaskRepository, SqlTaskRepository>();
            services.AddScoped<IRecurringTaskRepository, SqlRecurringTaskRepository>();
            services.AddScoped<IProfileHistoryService, ProfileHistoryService>();
            services.AddScoped<IAuthenticationOfficesService, AuthenticationOfficesService>();
            services.AddScoped<IPermanentOtpService, PermanentOtpService>();
            services.AddScoped<IPermanentOtpApiService, PermanentOtpApiService>();
            services.AddScoped<ILegacyCodeInquiryService, LegacyCodeInquiryService>();
            services.AddScoped<IIxService, IxService>();
            services.AddScoped<IFgmService, FgmService>();
            services.AddScoped<IMasterCacheProvider, MasterCacheProvider>();
            services.AddScoped<IMsisdnService, MsisdnService>();

            // gatekeeper
            services.AddScoped<IOtpService, OtpService>();
            services.AddScoped<IJwtTokenService<IssuerAccessTokenPayload>, JwtTokenService<IssuerAccessTokenPayload>>();
            services.AddScoped<IRateLimitService, RateLimitService>();

            //action filters
            services.AddScoped<AuthorizeUserAttribute>();
            services.AddScoped<LogRequestAttribute>();
            services.AddScoped<RateLimitAttribute>();
            services.AddScoped<ProfilingLogRequestAttribute>();

            //http clients
            services.AddCerberusHttpClients(configuration);

        }

        public static void AddMapper(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton(provider =>
            {
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<Profile, ProfileViewModel>();
                    cfg.CreateMap<PrivatePerson, PrivatePersonViewModel>();
                    cfg.CreateMap<LegalPerson, LegalPersonViewModel>();
                    cfg.CreateMap<Address, AddressViewModel>();
                    cfg.CreateMap<TradingCode, TradingCodeViewModel>();
                    cfg.CreateMap<Agent, AgentViewModel>();
                    cfg.CreateMap<BankingAccount, BankingAccountViewModel>();
                    cfg.CreateMap<JobInfo, JobInfoViewModel>();
                    cfg.CreateMap<FinancialInfo, FinancialInfoViewModel>();
                    cfg.CreateMap<LegalPersonShareholder, LegalPersonShareholderViewModel>();
                    cfg.CreateMap<LegalPersonStakeholder, LegalPersonStakeholderViewModel>();
                    cfg.CreateMap<IssuanceStockCode, IssuanceStockCodeViewModel>();
                    cfg.CreateMap<IssuanceStockCodeResponse, IssuanceStokeCodeResponseViewModel>();
                    cfg.CreateMap<Country, CountryViewModel>();
                    cfg.CreateMap<Province, ProvinceViewModel>();
                    cfg.CreateMap<City, CityViewModel>();
                    cfg.CreateMap<AddressSection, AddressSection>();
                    cfg.CreateMap<Broker, BrokerViewModel>();
                    cfg.CreateMap<FinancialBroker, FinancialBrokerViewModel>();
                    cfg.CreateMap<Bank, BankViewModel>();
                    cfg.CreateMap<Job, JobViewModel>();

                    cfg.AddProfile<AddressProfile>();
                    cfg.AddProfile<FinancialInfoProfile>();
                    cfg.AddProfile<JobInfoProfile>();
                    cfg.AddProfile<PrivatePersonProfile>();
                    cfg.AddProfile<AgentProfile>();
                    cfg.AddProfile<BankingAccountProfile>();
                    cfg.AddProfile<AccountProfile>();
                    cfg.AddProfile<TradingCodeProfile>();
                    cfg.AddProfile<AuthenticateOfficeProfile>();
                    cfg.AddProfile<PaymentProfile>();

                    cfg.CreateMap<CreateBankingAccountRequest, BankingAccount>();
                    cfg.CreateMap<CreateBankingAccountRequest, BankingAccount>();
                    cfg.CreateMap<CreatePaymentRequest, Payment>();
                    cfg.CreateMap<CreateAddressRequest, Address>();
                    cfg.CreateMap<CreateFinancialInfoRequest, FinancialInfo>();
                    cfg.CreateMap<CreateJobInfoRequest, JobInfo>();
                    cfg.CreateMap<CreatePrivatePersonRequest, PrivatePerson>();
                    cfg.CreateMap<CreateTradingCode, TradingCode>();

                    cfg.CreateMap<ProfileHistory, ProfileHistoryViewModel>();
                    cfg.CreateMap<Domain.Model.Profile, ProfileCompactViewModel>();

                    cfg.CreateMap<File, FileViewModel>()
                        .ForMember(file => file.FileName,
                        exp => exp.ResolveUsing(fileViewModel =>
                            fileViewModel.FileName
                                    = $"{configuration.GetValue<string>("FileStorageConfiguration:SejamServeFileUrl")}/{fileViewModel.FileName}"));

                    cfg.CreateMap<string, FileViewModel>()
                        .ForMember(file => file.FileName,
                            exp => exp.ResolveUsing(fileString=>$"{configuration.GetValue<string>("FileStorageConfiguration:SejamServeFileUrl")}/{fileString}"));

                    cfg.CreateMap<AuthenticationOffices, AuthenticateOfficeViewModel>();
                });
                return config.CreateMapper();
            });
        }

        public static void AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.UseReferencedDefinitionsForEnums();
                c.IgnoreObsoleteActions();
                c.SwaggerDoc("v0.1", new Info
                {
                    Title = "Sejam",
                    Version = "v0.1",
                    Description = @"
## Introduction
This document is a developer guide to use the Sejam API that allows you to interact with our system programmatically from your own application. 
The API supports JSON data formats, client should provide Authorization header.
We do our best to have all our URLs be RESTful. Every endpoint (URL) may support one of four different http verbs. GET requests fetch information about an object, POST requests create objects, PUT requests update objects, and finally DELETE requests will delete objects.
"
                });

                c.DescribeAllEnumsAsStrings();
                c.DescribeStringEnumsInCamelCase();

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme()
                {

                    Description = "API Key Authentication",
                    In = "header",
                    Name = "Authorization"
                });

                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    { "Bearer", new string[] { } }
                });

            });
        }

        public static void AddCerberusHttpClients(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHttpClient<IIssuanceStockCodeService, IssuanceStockCodeService>(client => { client.BaseAddress = new Uri(configuration.GetSection("IssuanceStockCodeConfiguration:Url").Value); })
                .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler { ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true });

            services.AddHttpClient("IxService", client => { client.BaseAddress = new Uri(configuration.GetSection("IxServiceConfiguration:IxAwaGetTradeCodeUrl").Value); })
                .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler { ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true });

            services.AddHttpClient("IxServiceAccessToken", client => { client.BaseAddress = new Uri(configuration.GetSection("IxServiceConfiguration:IxAwaGetAccessTokenUrl").Value); })
                .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler { ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true });

            services.AddHttpClient("IxServiceRefreshAccessToken", client => { client.BaseAddress = new Uri(configuration.GetSection("IxServiceConfiguration:IxAwaRefreshAccessTokenUrl").Value); })
                .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler { ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true });
        }
    }
}