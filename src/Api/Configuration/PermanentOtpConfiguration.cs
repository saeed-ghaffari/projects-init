﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Api.Configuration
{
    public class PermanentOtpConfiguration
    {
        public long ExpirationDays { get; set; }
    }
}
