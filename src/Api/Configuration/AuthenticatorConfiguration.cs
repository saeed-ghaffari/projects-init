﻿using System.Collections.Generic;

namespace Cerberus.Api.Configuration
{
    public class AuthenticatorConfiguration
    {
        public long PishkhanServiceId { get; set; }
        public bool IgnoreValidateOtp { get; set; }
        public List<long>LongsTermIssuerOtp{ get; set; }
    }
}
