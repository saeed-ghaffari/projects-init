﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Utility;
using Profile = AutoMapper.Profile;

namespace Cerberus.Api.MappingProfile
{
    public class TradingCodeProfile:Profile
    {
        public TradingCodeProfile()
        {
            CreateMap<TradingCode, AuthenticationOfficeTradingCodeViewModel>()
                .ForMember(t => t.BurseDescription, m => m.MapFrom(t => t.Type.GetEnumDescription()))
                .ForMember(t => t.BurseCode1, m => m.MapFrom(t => t.FirstPart))
                .ForMember(t => t.BurseCode2, m => m.MapFrom(t => t.SecondPart))
                .ForMember(t => t.BurseCode3, m => m.MapFrom(t => t.ThirdPart));

        }
    }
}
