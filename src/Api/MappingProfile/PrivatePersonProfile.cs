﻿using Cerberus.Api.ViewModel;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using Cerberus.Utility;
using Profile = AutoMapper.Profile;

namespace Cerberus.Api.MappingProfile
{
    public class PrivatePersonProfile:Profile
    {
        public PrivatePersonProfile()
        {
            CreateMap<PrivatePerson, AuthenticationOfficePrivatePersonViewModel>()
                .ForMember(p => p.BirthDate, a => a.MapFrom(p => p.BirthDate.ToPersianDate("yyyy/MM/dd")))
                .ForMember(p => p.SeriChar, a => a.MapFrom(p => p.SeriShChar))
                .ForMember(p => p.Seri, a => a.MapFrom(p => p.SeriSh))
                .ForMember(p => p.GenderDescription, a => a.MapFrom(p => p.Gender == Gender.Male ? "مرد" : "زن"))
                .ForMember(p=>p.ShenasnameNumber , a=>a.MapFrom(p=>p.ShNumber));
        }
        
    }
}
