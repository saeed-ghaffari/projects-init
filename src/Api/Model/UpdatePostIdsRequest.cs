﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cerberus.Domain.CustomValidation;

namespace Cerberus.Api.Model
{
    public class UpdatePostIdsRequest
    {
       
        [Required]
        public long ReferenceNumber { get; set; }

        [MaxLength(10)]
        [Required]
        [Utility.UniqueIdentifierValidation(ErrorMessage = " invalid uniqueIdentifier")]
        public string UniqueIdentifier { get; set; }
        [Required]
        [MaxLength(100)]
        public string PostId { get; set; }
    }
}