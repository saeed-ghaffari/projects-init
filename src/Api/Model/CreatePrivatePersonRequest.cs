﻿using System;
using System.ComponentModel.DataAnnotations;
using Cerberus.Domain.Enum;

namespace Cerberus.Api.Model
{
    public class CreatePrivatePersonRequest
    {

        [Display(Name = "کد ملی")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [StringLength(10, ErrorMessage = " {0} باید حداقل ده رقمی باشد .", MinimumLength = 10)]
        [Utility.UniqueIdentifierValidation(ErrorMessage = "{0} اشتباه است")]
        public string NationalCode { get; set; }
        [Display(Name = "شماره تلفن همراه")]
        [Required(ErrorMessage = "{0} اجباری است ")]
        [RegularExpression(@"9(0[1-5]|1[0-9]|3[0-9]|2[0-9]|9[0-9])-?[0-9]{3}-?[0-9]{4}", ErrorMessage = "لطفا شماره تلفن همراه خود را صحیح وارد نمایید")]
        [MinLength(10, ErrorMessage = "{0}  معتبر نیست")]
        [MaxLength(10, ErrorMessage = "{0}  معتبر نیست")]
        public string Mobile { get; set; }

        [Display(Name = "نام")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(40, ErrorMessage = "{0} نمی تواند بیشتر از 40 کارکتر باشد")]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(50, ErrorMessage = "{0} نمی تواند بیشتر از 50 کارکتر باشد")]
        public string LastName { get; set; }


        [Display(Name = "نام پدر")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(40, ErrorMessage = "{0} نمی تواند بیشتر از 40 کارکتر باشد")]
        public string FatherName { get; set; }

        [Display(Name = "جنسیت")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [Range((byte)(Domain.Enum.Gender.Male), ((byte)(Domain.Enum.Gender.Female)), ErrorMessage = "مقدار {0} نامعتبر است")]
        public Gender Gender { get; set; }

        [Display(Name = "حرف سری شناسنامه")]
        [MaxLength(3, ErrorMessage = "حداکثر 3 رقم مجاز است")]
        [RegularExpression(@"^\s*[الفبلدر1234910\s]+\s*$", ErrorMessage = "معتبر نیست")]
        public string SeriShChar { get; set; }

        [Display(Name = "سری شناسنامه")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MaxLength(2, ErrorMessage = "حداکثر 2 رقم مجاز است")]
        public string SeriSh { get; set; }

        [Display(Name = "سریال شناسنامه")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MaxLength(6, ErrorMessage = "حداکثر 6 رقم مجاز است")]
        public string Serial { get; set; }


        [Display(Name = "شماره شناسنامه")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MaxLength(10, ErrorMessage = "حداکثر 10 رقم مجاز است")]
        public string ShNumber { get; set; }

        [Display(Name = "تاریخ تولد")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public DateTime BirthDate { get; set; }

        [Display(Name = "محل صدور")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(30, ErrorMessage = "{0} نمی تواند بیشتر از 30 کارکتر باشد")]
        public string PlaceOfIssue { get; set; }

        [Display(Name = "محل تولد")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(30, ErrorMessage = "{0} نمی تواند بیشتر از 30 کارکتر باشد")]
        public string PlaceOfBirth { get; set; }
    }
}