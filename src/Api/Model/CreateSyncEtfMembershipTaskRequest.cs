﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cerberus.Domain.ViewModel.Membership;

namespace Cerberus.Api.Model
{
    public class CreateSyncEtfMembershipTaskRequest
    {
        [Required]
        public IEnumerable<EtfMemberShipViewModel> Members { get; set; }
    }
}