﻿using System.ComponentModel.DataAnnotations;
using Cerberus.Utility;

namespace Cerberus.Api.Model
{
    public class GenerateOtpRequest
    {
        [Required]
        [UniqueIdentifierValidation(ErrorMessage = "invalid uniqueIdentifier")]
        public string UniqueIdentifier { get; set; }
    }
}