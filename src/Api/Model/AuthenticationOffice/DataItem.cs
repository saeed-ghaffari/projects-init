﻿using System.Collections.Generic;
using System.ComponentModel;
using Cerberus.Api.ViewModel;
using Cerberus.Domain.ViewModel.Api;

namespace Cerberus.Api.Model.AuthenticationOffice
{
    public class DataItem
    {
        public object Real_Person_Natives { get; set; }
        public object Addressies { get; set; }
        public object JobTitles { get; set; }
        public object FinancialInformations { get; set; }
        public object Agents { get; set; }
        public object TradCodes { get; set; }
        public object Acounts { get; set; }
        public object Real_Person_Foreigns { get; set; }
        public AuthenticationOfficeProfileViewModel Main { get; set; }


    }
}
