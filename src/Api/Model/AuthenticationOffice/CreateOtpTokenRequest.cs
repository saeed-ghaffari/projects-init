﻿using System.ComponentModel.DataAnnotations;

namespace Cerberus.Api.Model.AuthenticationOffice
{
    public class CreateOtpTokenRequest
    {
        [MaxLength(10)]
        public string TraceCode { get; set; }
        [MaxLength(10)]
        public string  NationalCode { get; set; }
    }
}
