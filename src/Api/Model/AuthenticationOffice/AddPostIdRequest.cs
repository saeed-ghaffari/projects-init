﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Cerberus.Api.Model.AuthenticationOffice
{
    public class AddPostIdRequest
    {
        [JsonProperty(PropertyName = "Pishkhanpost")]
        public List<PostIdForPishkhan> PishkhanPost { get; set; }
    }

    public class PostIdForPishkhan
    {
        [MaxLength(11)]
        //[RegularExpression("")]
        [JsonProperty(PropertyName = "nationalCode")]
        public string NationalCode { get; set; }
        [MaxLength(50)]
        //[RegularExpression("")]
        [JsonProperty(PropertyName = "postid")]
        public string Postid { get; set; }

        [JsonProperty(PropertyName = "id")]
        public long Id { get; set; }
    }
}
