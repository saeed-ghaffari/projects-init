﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Api.Model.AuthenticationOffice
{
    public class AddPostIdResponse:BaseAuthenticationOfficeModel
    {
        public AddPostIdResponse()
        {
            Data = new List<object>();
        }
        public List<object> Data { get; set; }
        public string DataItem { get; set; }
    }
}
