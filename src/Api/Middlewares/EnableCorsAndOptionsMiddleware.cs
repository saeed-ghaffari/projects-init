﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Cerberus.Api.Middlewares
{
    public class EnableCorsAndOptionsMiddleware
    {
        private readonly RequestDelegate _next;

        public EnableCorsAndOptionsMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public Task InvokeAsync(HttpContext context)
        {

            context.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            context.Response.Headers.Add("Access-Control-Allow-Headers", "Content-Type,ServerTime,X-CSRF-TOKEN,Authorization");
            context.Response.Headers.Add("Access-Control-Expose-Headers", "Content-Type,ServerTime,X-CSRF-TOKEN,Authorization");

            if (context.Request.Method.ToLower() == "options")
            {
                context.Response.StatusCode = 200;
                return Task.CompletedTask;
            }
            else
                return _next.Invoke(context);

        }
    }
}