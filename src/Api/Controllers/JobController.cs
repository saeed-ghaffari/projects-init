﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Api.ApiLogic;
using Cerberus.Api.Filters;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Gatekeeper.AspModules.ActionFilter;
using Microsoft.AspNetCore.Mvc;

namespace Cerberus.Api.Controllers
{
    [ApiVersion("0.1")]
    [ApiVersion("1.1")]
    [Produces("application/json")]
    [ApiController]
    [AuthorizeUserFilterFactory(Order = 1)]
    [Route("v{version:apiVersion}/jobs")]
    [ProfilingLogRequestFilterFactory(Order = 3)]
    public class JobController : BaseController
    {
        private readonly IJobService _jobService;
        private readonly IMapper _mapper;
        public JobController(IJobService jobService, IMapper mapper)
        {
            _jobService = jobService;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("")]
        [RateLimitFilterFactory(Order = 2, Limit = 30, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<IEnumerable<JobViewModel>> GetJobsAsync()
        {
            return _mapper.Map<IEnumerable<Job>, IEnumerable<JobViewModel>>(await _jobService.GetListAsync());
        }
    }
}