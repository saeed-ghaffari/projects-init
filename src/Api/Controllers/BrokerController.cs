﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Api.ApiLogic;
using Cerberus.Api.Filters;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Gatekeeper.AspModules.ActionFilter;
using Microsoft.AspNetCore.Mvc;

namespace Cerberus.Api.Controllers
{
    [ApiVersion("0.1")]
    [ApiVersion("1.1")]
    [Produces("application/json")]
    [ApiController]
    [AuthorizeUserFilterFactory(Order = 1)]
    [ProfilingLogRequestFilterFactory(Order = 3)]
    public class BrokerController : BaseController
    {
        private readonly IBrokerService _brokerService;
        private readonly IMapper _mapper;

        public BrokerController(IBrokerService brokerService, IMapper mapper)
        {
            _brokerService = brokerService;
            _mapper = mapper;
        }

        [HttpGet]
        [Route("v{version:apiVersion}/brokers")]
        [Route("v{version:apiVersion}/financialBrokers")]
        [RateLimitFilterFactory(Order = 2, Limit = 30, PeriodInSec = 300, UserIdentifier = "ServiceId")]
        public async Task<IEnumerable<BrokerViewModel>> GetBrokersListAsync()
        {
            return _mapper.Map<IEnumerable<Broker>, IEnumerable<BrokerViewModel>>(await _brokerService.GetListAsync());
        }
    }
}