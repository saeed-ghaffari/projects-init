﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NLog.Common;
using NLog.Config;
using SharpRaven;
using SharpRaven.Data;

// ReSharper disable CheckNamespace
namespace NLog.Targets
// ReSharper restore CheckNamespace
{
    [Target("Sentry")]
    public class SentryTarget : TargetWithLayout
    {
        private Dsn _dsn;
        private static Lazy<IRavenClient> _client;
        private ConcurrentQueue<SentryEvent> _queue;
        private List<Task> _tasks;
        private Semaphore _semaphore;
        private int _concurrencyLevel;

        /// <summary>
        /// Map of NLog log levels to Raven/Sentry log levels
        /// </summary>
        protected static readonly IDictionary<LogLevel, ErrorLevel> LoggingLevelMap = new Dictionary<LogLevel, ErrorLevel>
        {
            {LogLevel.Debug, ErrorLevel.Debug},
            {LogLevel.Error, ErrorLevel.Error},
            {LogLevel.Fatal, ErrorLevel.Fatal},
            {LogLevel.Info, ErrorLevel.Info},
            {LogLevel.Trace, ErrorLevel.Debug},
            {LogLevel.Warn, ErrorLevel.Warning},
        };

        [RequiredParameter]
        public TimeSpan Timeout { get; set; }

        /// <summary>
        /// The DSN for the Sentry host
        /// </summary>
        [RequiredParameter]
        public string Dsn
        {
            get => _dsn?.ToString();
            set
            {
                _dsn = new Dsn(value);
                _client = new Lazy<IRavenClient>(() => new RavenClient(_dsn) { Timeout = Timeout });
            }
        }

        [RequiredParameter]
        public int ConcurrencyLevel
        {
            get => _concurrencyLevel;
            set
            {
                _concurrencyLevel = value;
                _semaphore = new Semaphore(0, value);
                InitBackgroundProcess();
            }
        }

        /// <summary>
        /// Determines whether events with no exceptions will be send to Sentry or not
        /// </summary>
        public bool IgnoreEventsWithNoException { get; set; }

        /// <summary>
        /// Determines whether event properties will be sent to sentry as Tags or not
        /// </summary>
        public bool SendLogEventInfoPropertiesAsTags { get; set; }

        private void InitBackgroundProcess()
        {
            _queue = new ConcurrentQueue<SentryEvent>();
            _tasks = new List<Task>();
            for (int i = 0; i < ConcurrencyLevel; i++)
            {
                var newTask = new Task(() =>
                  {
                      while (true)
                      {
                          _semaphore.WaitOne();
                          if (_queue.TryDequeue(out var sentryEvent))
                          {
                              try
                              {
                                  _client.Value.Capture(sentryEvent);
                              }
                              catch (Exception e)
                              {
                                  InternalLogger.Error("Unable to send Sentry request: {0}", e.Message);
                              }
                          }
                      }
                  });
                _tasks.Add(newTask);
                newTask.Start();
            }
        }


        /// <summary>
        /// Writes logging event to the log target.
        /// </summary>
        /// <param name="logEvent">Logging event to be written out.</param>
        protected override void Write(LogEventInfo logEvent)
        {
            try
            {
                var tags = SendLogEventInfoPropertiesAsTags
                    ? logEvent.Properties.ToDictionary(x => x.Key.ToString(), x => x.Value.ToString())
                    : null;

                var extras = SendLogEventInfoPropertiesAsTags
                    ? null
                    : logEvent.Properties.ToDictionary(x => x.Key.ToString(), x => x.Value.ToString());

                _client.Value.Logger = logEvent.LoggerName;

                // If the log event did not contain an exception and we're not ignoring
                // those kinds of events then we'll send a "Message" to Sentry
                if (logEvent.Exception == null && !IgnoreEventsWithNoException)
                {
                    var sentryMessage = new SentryMessage(Layout.Render(logEvent));
                    var sentryEvent = new SentryEvent(sentryMessage)
                    {
                        Tags = tags,
                        Extra = extras,
                        Message = sentryMessage
                    };

                    _queue.Enqueue(sentryEvent);
                    _semaphore.Release();
                }
                else if (logEvent.Exception != null)
                {
                    var sentryEvent = new SentryEvent(logEvent.Exception)
                    {
                        Tags = tags,
                        Extra = extras,

                    };

                    _queue.Enqueue(sentryEvent);
                    _semaphore.Release();
                }
            }
            catch (Exception e)
            {
                InternalLogger.Error("Unable to send Sentry request: {0}", e.Message);
            }
        }
    }
}

