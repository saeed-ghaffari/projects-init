﻿#region Using

using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;

#endregion

namespace Cerberus.Utility
{
    public static class TypeExtension
    {
        #region Methods

        #region To Int & Double & Long & Float

        public static int ToInt(this object value)
        {
            if (value == null)
                return 0;
            int.TryParse(value.ToString(), out var result);
            return result;
        }
        public static decimal ToDecimal(this object value)
        {
            if (value == null)
                return 0;
            decimal.TryParse(value.ToString(), out var result);
            return result;
        }
        public static double ToDouble(this object value)
        {
            if (value == null)
                return 0;
            double.TryParse(value.ToString(), out var result);
            return result;
        }
        public static long ToLong(this object value)
        {
            if (value == null)
                return 0;
            long.TryParse(Math.Round(value.ToDouble(), 0).ToString(CultureInfo.InvariantCulture), out var result);
            return result;
        }
        public static ulong ToULong(this object value)
        {
            if (value == null)
                return 0;
            ulong.TryParse(Math.Round(value.ToDouble(), 0).ToString(CultureInfo.InvariantCulture), out var result);
            return result;
        }
        public static short ToShort(this object value)
        {
            if (value == null)
                return 0;
            short.TryParse(Math.Round(value.ToDouble(), 0).ToString(CultureInfo.InvariantCulture), out var result);
            return result;
        }

        public static float ToFloat(this object value)
        {
            if (value == null)
                return 0;
            float.TryParse(value.ToString(), out var result);
            return result;
        }

        #endregion

        #region Get String

        public static string GetDefultStringValue(this object value) => value?.ToString() ?? "-";

        #endregion

        #region To Boolean

        public static bool ToBoolean(this object value)
        {
            if (value == null)
                return false;
            bool.TryParse(value.ToString(), out var result);
            return result;
        }

        #endregion

        #region To Guid

        public static Guid ToGuid(this object value)
        {
            if (value == null)
                return Guid.Empty;
            Guid.TryParse(value.ToString(), out var result);
            return result;
        }

        #endregion

        #region ToCurrency

        public static string ToCurrency(this decimal value) => value.ToString("N0", CultureInfo.InvariantCulture);

        #endregion

        #region Convert Date Time ToString

        public static string DateToString(this DateTime date) => date.ToString("yyyyMMdd");

        public static string TimeToString(this DateTime date) => date.ToString("HHMMSS");
 public static string DateTimeToString(this DateTime date) => date.ToString("yyyyMMdd HHMMss");

        #endregion


        public static bool IsValidNationalCode(this string uniqueIdentifier)
        {
            if (!System.Text.RegularExpressions.Regex.IsMatch(uniqueIdentifier, @"^(?!(\d)\1{9})\d{10}$"))
                return false;

            var check = Convert.ToInt32(uniqueIdentifier.Substring(9, 1));
            var sum = Enumerable.Range(0, 9)
                          .Select(x => Convert.ToInt32(uniqueIdentifier.Substring(x, 1)) * (10 - x))
                          .Sum() % 11;

            return sum < 2 && check == sum || sum >= 2 && check + sum == 11;
        }
       #endregion
    }
}
