﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Utility.Model;
using Newtonsoft.Json;

namespace Cerberus.Utility
{
    public static class HttpHepler
    {
        [Obsolete("This method is deprecated, please use PostAsync instead.")]
        public static async Task<T> PostRequestAsync<T>(string url, object param) where T : class
        {
            return (await PostAsync<T>(url, param, "application/json", null, null)).Body;
        }
        public static async Task<T> GetContentAsync<T>(string url, AuthenticationHeaderValue authorization = null)
        {
            var httpClient = new HttpClient();

            if (authorization != null)
                httpClient.DefaultRequestHeaders.Authorization = authorization;

            using (var response = await httpClient.GetAsync(url))
            {
                var content = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(content);
            }
        }

        public static async Task<string> GetContentAsync(string url, AuthenticationHeaderValue authorization = null)
        {
            var httpClient = new HttpClient();

            if (authorization != null)
                httpClient.DefaultRequestHeaders.Authorization = authorization;

            using (var response = await httpClient.GetAsync(url))
            {
                return await response.Content.ReadAsStringAsync();
            }
        }
        public static async Task<HttpResponseResult<string>> GetHttpResponseResultAsync(string url, AuthenticationHeaderValue authorization = null)
        {
            var httpClient = new HttpClient();

            if (authorization != null)
                httpClient.DefaultRequestHeaders.Authorization = authorization;

            using (var response = await httpClient.GetAsync(url))
            {
                return new HttpResponseResult<string>()
                {
                    StatusCode = response.StatusCode,
                    Body = await response.Content.ReadAsStringAsync(),
                    ContentType = response.Content.Headers.ContentType?.MediaType
                };
            }
        }

        public static async Task<HttpResponseResult<string>> PostAsync(string url, string body, string contentType, TimeSpan? timeout = null, AuthenticationHeaderValue authorization = null, Dictionary<string, string> headers = null,bool useSsl=false)
        {
            ServicePointManager.ServerCertificateValidationCallback += (senderX, certificate, chain, sslPolicyErrors) => true;

            var httpClient = new HttpClient();
            if (useSsl)
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;
                httpClient = new HttpClient(httpClientHandler);
            }

            var httpContent = new StringContent(body, Encoding.UTF8, contentType);

            if (authorization != null)
                httpClient.DefaultRequestHeaders.Authorization = authorization;

            if (timeout.HasValue)
                httpClient.Timeout = timeout.Value;
            if (headers != null)
            {
                foreach (var header in headers)
                {
                    httpClient.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }

            using (var response = await httpClient.PostAsync(url, httpContent))
            {
                return new HttpResponseResult<string>()
                {
                    StatusCode = response.StatusCode,
                    Body = await response.Content.ReadAsStringAsync(),
                    ContentType = response.Content.Headers.ContentType?.MediaType
                };
            }
        }

        public static async Task<HttpResponseResult<T>> PostAsync<T>(string url, string body, string contentType, TimeSpan? timeout = null, AuthenticationHeaderValue authorization = null, Dictionary<string, string> headers = null, bool useSsl = false)
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => true;

            var httpClient = new HttpClient();
            if (useSsl)
            {
                var httpClientHandler = new HttpClientHandler();
                httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;
                httpClient = new HttpClient(httpClientHandler);
            }


            var httpContent = new StringContent(body, Encoding.UTF8, contentType);

            if (authorization != null)
                httpClient.DefaultRequestHeaders.Authorization = authorization;

            if (timeout.HasValue)
                httpClient.Timeout = timeout.Value;
            if (headers != null)
            {
                foreach (var header in headers)
                {
                    httpClient.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }

            using (var response = await httpClient.PostAsync(url, httpContent))
            {
                
                return new HttpResponseResult<T>()
                {
                    StatusCode = response.StatusCode,
                    Body = JsonConvert.DeserializeObject<T>(await response.Content.ReadAsStringAsync()),
                    ContentType = response.Content.Headers.ContentType?.MediaType
                };
            }
        }

        public static async Task<HttpResponseResult<T>> PostAsync<T>(HttpClient httpClient, string body, string contentType, TimeSpan? timeout = null, AuthenticationHeaderValue authorization = null, Dictionary<string, string> headers = null, bool useSsl = false)
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = (senderX, certificate, chain, sslPolicyErrors) => true;


            var httpContent = new StringContent(body, Encoding.UTF8, contentType);

            if (authorization != null)
                httpClient.DefaultRequestHeaders.Authorization = authorization;

            
            if (timeout.HasValue && httpClient.Timeout!=timeout.Value)
                httpClient.Timeout = timeout.Value;

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    httpClient.DefaultRequestHeaders.Remove(header.Key);
                    httpClient.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }

            using (var response = await httpClient.PostAsync(httpClient.BaseAddress, httpContent))
            {
                var bodyString =await response.Content.ReadAsStringAsync();
                return new HttpResponseResult<T>()
                {
                    StatusCode = response.StatusCode,
                    Body = JsonConvert.DeserializeObject<T>(bodyString),
                    ContentType = response.Content.Headers.ContentType?.MediaType
                };
            }
        }

        public static Task<HttpResponseResult<T>> PostAsync<T>(string url, object body, string contentType, TimeSpan? timeout=null, AuthenticationHeaderValue authorization = null)
        {
            return PostAsync<T>(url, JsonConvert.SerializeObject(body), contentType, timeout, authorization);
        }


        public static async Task<long> GetContentLengthAsync(string url)
        {
            var req = WebRequest.Create(url);
            req.Method = "HEAD";
            using (var resp = await req.GetResponseAsync())
            {
                return long.Parse(resp.Headers.Get("Content-Length"));
            }
        }
    }
}
