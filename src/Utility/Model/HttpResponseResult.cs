﻿using System.Net;

namespace Cerberus.Utility.Model
{
    public class HttpResponseResult<T>
    {
        public T Body { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string ContentType { get; set; }
    }
}