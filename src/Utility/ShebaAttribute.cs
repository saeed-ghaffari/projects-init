﻿using System.ComponentModel.DataAnnotations;
using System.Numerics;

namespace Cerberus.Web.WebLogic.CustomAttribute
{
    public class ShebaAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            if (value==null)
                return true;

            var inputValue = value.ToString();
            var substring1 = inputValue.Substring(0, 4);
            var substring2 = inputValue.Substring(4, 22);
            var total = substring2 + substring1;
            var r = total.Replace("IR", "1827");
            BigInteger.TryParse(r,out  var res);
            if (res == 0) return false;

            return (res % 97) == 1;
        }
    }
}
