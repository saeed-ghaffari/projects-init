﻿namespace Cerberus.Utility.AsanPardakht
{
  public  class AsanPardakhtResultDescriptor
    {
        public AsanPardakhtResultDescriptor(string amount,string preInvoiceId,string token,string resCode,string messageText,string payGateTranId,string rrn,string lastFourDigitOfPan)
        {
            this.Amount = amount;
            this.PreInvoiceId = preInvoiceId;
            this.Token = token;
            this.ResCode = resCode;
            this.MessageText = messageText;
            this.PayGateTranId = payGateTranId;
            this.Rrn = rrn;
            this.LastFourDigitOfPan = lastFourDigitOfPan;
        }

        public static AsanPardakhtResultDescriptor AsanPardakhtTrxResultDescriptorFromString(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return null;
            }

            string[] resArray = str.Split(',');
            if (resArray.Length != 8)
            {
                return null;
            }

            return new AsanPardakhtResultDescriptor(resArray[0], resArray[1], resArray[2], resArray[3], resArray[4], resArray[5], resArray[6], resArray[7]);
        }

        #region Public Properties

        public string Amount { get; }

        public string PreInvoiceId { get; }

        public string Token { get; }

        public string ResCode { get; }

        public string MessageText { get; }

        public string PayGateTranId { get; }

        public string Rrn { get; }

        public string LastFourDigitOfPan { get; }

        #endregion
    }
}

