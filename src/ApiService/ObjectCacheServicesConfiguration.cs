﻿using System;

namespace Cerberus.ApiService
{
    public class ObjectCacheServicesConfiguration
    {
        public TimeSpan ReconstructTime { get; set; }
        public TimeSpan Ttl { get; set; }
        public TimeSpan AddressesExpiration { get; set; }
        public TimeSpan BankingAccountsExpiration { get; set; }
        public TimeSpan FinancialInfoExpiration { get; set; }
        public TimeSpan JobInfoExpiration { get; set; }
        public TimeSpan LegalPersonExpiration { get; set; }
        public TimeSpan PrivatePersonExpiration { get; set; }
        public TimeSpan ProfileExpiration { get; set; }
        public TimeSpan TradingCodesExpiration { get; set; }
        public TimeSpan PermanentOtpExpiration { get; set; }

    }
}