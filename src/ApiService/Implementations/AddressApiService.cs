﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Service.Implementations;
using Cerberus.Utility;
using Komodo.Caching.Abstractions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Cerberus.ApiService.Implementations
{
    public class AddressApiService : BaseListObjectCacheService<Address,AddressViewModel>, IAddressApiService
    {

        private readonly IAddressService _addressService;

        public AddressApiService(
            ICacheProvider cacheProvider,
            ILogger<IAddressApiService> logger,
            IOptions<ObjectCacheServicesConfiguration> options,
            IProfileApiService profileApiService,
            IMapper mapper, IAddressService addressService)
            : base(cacheProvider, logger, options.Value.ReconstructTime, options.Value.AddressesExpiration, options.Value.Ttl, profileApiService, mapper)
        {
            _addressService = addressService;
        }

        protected override ProfileViewModel UpdateCurrentProfileChildAsync(ProfileViewModel profile, IEnumerable<AddressViewModel> dataToStore)
        {
            profile.Addresses = dataToStore;

            return profile;
        }

        protected override string GenerateCacheKey(string uniqueIdentifier) =>
            CacheKeyHelper.Addresses.GetKey(uniqueIdentifier);

        protected override Task<IEnumerable<Address>> GetObjectFromMasterDatabaseAsync(string uniqueIdentifier)
        {
            return _addressService.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);
        }
    }
}