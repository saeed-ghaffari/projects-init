﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Service.Implementations;
using Komodo.Caching.Abstractions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Cerberus.ApiService.Implementations
{
    public class JobInfoApiService : BaseObjectCacheService<JobInfo, JobInfoViewModel>, IJobInfoApiService
    {
        private readonly IJobInfoService _jobInfoService;

        public JobInfoApiService(
            ICacheProvider cacheProvider,
            ILogger<JobInfoApiService> logger,
            IOptions<ObjectCacheServicesConfiguration> options,
            IProfileApiService profileApiService,
            IMapper mapper, IJobInfoService jobInfoService)
            : base(cacheProvider, logger, options.Value.ReconstructTime, options.Value.JobInfoExpiration, options.Value.Ttl, profileApiService, mapper)
        {
            _jobInfoService = jobInfoService;
        }

        protected override ProfileViewModel UpdateCurrentProfileChildAsync(ProfileViewModel profile, JobInfoViewModel dataToStore)
        {
            profile.JobInfo = dataToStore;

            return profile;
        }

        protected override string GenerateCacheKey(string uniqueIdentifier) =>
            CacheKeyHelper.JobInfo.GetKey(uniqueIdentifier);

        protected override Task<JobInfo> GetObjectFromMasterDatabaseAsync(string uniqueIdentifier)
        {
            return _jobInfoService.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);
        }
    }
}