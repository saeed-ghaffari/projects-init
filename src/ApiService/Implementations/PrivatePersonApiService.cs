﻿using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Service.Implementations;
using Komodo.Caching.Abstractions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Cerberus.ApiService.Implementations
{
    public class PrivatePersonApiService : BaseObjectCacheService<PrivatePerson, PrivatePersonViewModel>, IPrivatePersonApiService
    {
        private readonly IPrivatePersonService _privatePersonService;

        public PrivatePersonApiService(
            ICacheProvider cacheProvider,
            ILogger<PrivatePersonApiService> logger,
            IOptions<ObjectCacheServicesConfiguration> options,
            IProfileApiService profileApiService,
            IMapper mapper, IPrivatePersonService privatePersonService)
            : base(cacheProvider, logger, options.Value.ReconstructTime, options.Value.PrivatePersonExpiration, options.Value.Ttl, profileApiService, mapper)
        {
            _privatePersonService = privatePersonService;
        }

        
        protected override ProfileViewModel UpdateCurrentProfileChildAsync(ProfileViewModel profile, PrivatePersonViewModel dataToStore)
        {
            profile.PrivatePerson = dataToStore;

            return profile;
        }

        protected override string GenerateCacheKey(string uniqueIdentifier) =>
            CacheKeyHelper.PrivatePerson.GetKey(uniqueIdentifier);

        protected override Task<PrivatePerson> GetObjectFromMasterDatabaseAsync(string uniqueIdentifier)
        {
            return _privatePersonService.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);
        }

        public Task<PrivatePerson> GetIncludeImageAsync(string uniqueIdentifier)
        {
            return _privatePersonService.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);
        }
    }
}