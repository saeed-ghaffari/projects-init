﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Service.Implementations;
using Komodo.Caching.Abstractions;
using Microsoft.Extensions.Localization.Internal;
using Microsoft.Extensions.Logging;
using Profile = AutoMapper.Profile;

namespace Cerberus.ApiService.Implementations
{
    public abstract class BaseListObjectCacheService<TModel, TViewModel> : BaseService, IListObjectCacheService<TModel, TViewModel>
        where TViewModel : new()
        where TModel : BaseModel
    {
        private readonly IMapper _mapper;
        private readonly IProfileApiService _profileApiService;
        private readonly ICacheProvider _cacheProvider;
        private readonly TimeSpan? _cacheExpiration;
        private readonly TimeSpan? _ttl;

        protected BaseListObjectCacheService(
            ICacheProvider cacheProvider,
            ILogger logger,
            TimeSpan? reconstructTime,
            TimeSpan? cacheExpiration,
            TimeSpan? ttl,
            IProfileApiService profileApiService, IMapper mapper)
            : base(cacheProvider, logger, reconstructTime, cacheExpiration, ttl)
        {
            _cacheExpiration = cacheExpiration;
            _ttl = ttl;
            _cacheProvider = cacheProvider;
            _profileApiService = profileApiService;
            _mapper = mapper;
        }


        public async Task StoreAsync(string uniqueIdentifier, ProfileOwnerType? type = null)
        {
            IEnumerable<TModel> objectToStore = await GetObjectFromMasterDatabaseAsync(uniqueIdentifier);


            var mappedObject = _mapper.Map<IEnumerable<TModel>, IEnumerable<TViewModel>>(objectToStore)?.ToList();


            //store current child object
            await _cacheProvider.StoreAsync(GenerateCacheKey(uniqueIdentifier), mappedObject,
                _cacheExpiration, _ttl);


            //update profile object

            await UpdateProfileChildObjectAsync(uniqueIdentifier, mappedObject, type);
        }

        public async Task RemoveAsync(string uniqueIdentifier)
        {
            await _cacheProvider.RemoveAsync(GenerateCacheKey(uniqueIdentifier));
        }

        public Task<IEnumerable<TViewModel>> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier)
        {
            return FetchAsync(GenerateCacheKey(uniqueIdentifier),
                async () =>
                {
                    var result = await GetObjectFromMasterDatabaseAsync(uniqueIdentifier);
                    return _mapper.Map<IEnumerable<TModel>, IEnumerable<TViewModel>>(result);
                });
        }


        public async Task UpdateProfileChildObjectAsync(string uniqueIdentifier, IEnumerable<TViewModel> childObjects, ProfileOwnerType? type)
        {

            ProfileViewModel profile = await _profileApiService.GetByUniqueIdentifierAsync(uniqueIdentifier, true, type);

            UpdateCurrentProfileChildAsync(profile, childObjects);

            await _profileApiService.StoreAsync(profile);
        }


        protected abstract ProfileViewModel UpdateCurrentProfileChildAsync(ProfileViewModel profile, IEnumerable<TViewModel> dataToStore);

        protected abstract string GenerateCacheKey(string uniqueIdentifier);

        protected abstract Task<IEnumerable<TModel>> GetObjectFromMasterDatabaseAsync(string uniqueIdentifier);


    }
}