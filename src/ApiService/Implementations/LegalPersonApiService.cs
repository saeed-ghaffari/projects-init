﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Service.Implementations;
using Komodo.Caching.Abstractions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Cerberus.ApiService.Implementations
{
    public class LegalPersonApiService : BaseObjectCacheService<LegalPerson, LegalPersonViewModel>, ILegalPersonApiService
    {
        private readonly ILegalPersonService _legalPersonService;

        public LegalPersonApiService(
            ICacheProvider cacheProvider,
            ILogger<LegalPersonApiService> logger,
            IOptions<ObjectCacheServicesConfiguration> options,
            IProfileApiService profileApiService,
            IMapper mapper, ILegalPersonService legalPersonService)
            : base(cacheProvider, logger, options.Value.ReconstructTime, options.Value.LegalPersonExpiration, options.Value.Ttl, profileApiService, mapper)
        {
            _legalPersonService = legalPersonService;
        }


        protected override ProfileViewModel UpdateCurrentProfileChildAsync(ProfileViewModel profile, LegalPersonViewModel dataToStore)
        {
            profile.LegalPerson = dataToStore;

            return profile;
        }

        protected override string GenerateCacheKey(string uniqueIdentifier) =>
            CacheKeyHelper.LegalPerson.GetKey(uniqueIdentifier);

        protected override Task<LegalPerson> GetObjectFromMasterDatabaseAsync(string uniqueIdentifier)
        {
            return _legalPersonService.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);
        }
    }
}