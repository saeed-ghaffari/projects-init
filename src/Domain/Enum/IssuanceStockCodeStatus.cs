﻿namespace Cerberus.Domain.Enum
{
    public enum IssuanceStockCodeStatus
    {
        None = 0,
        InProcess = 1,
        Done = 2,
        InvalidData = 3,
        Exception = 4
    }
}