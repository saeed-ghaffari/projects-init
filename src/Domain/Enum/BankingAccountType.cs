﻿using System.ComponentModel;

namespace Cerberus.Domain.Enum
{
    public enum BankingAccountType : byte
    {
        [Description("بلند مدت")]
        LongTermAccount = 1,

        [Description("کوتاه مدت")]
        ShortTermAccount = 2,

        [Description("جاری")]
        CurrentAccount = 3,

        [Description("قرض الحسنه")]
        SavingAccount = 4
    }
}