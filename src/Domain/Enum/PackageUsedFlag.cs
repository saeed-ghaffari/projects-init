﻿using System;
using System.ComponentModel;

namespace Cerberus.Domain.Enum
{
    [Flags]
    public enum PackageUsedFlag : byte
    {
        None = 0,

        [Description("ثبت نام")]
        Registration = 1,


        [Description("ویرایش تلفن همراه")]
        EditMobileNumber = 2,


        [Description("ویرایش اطلاعات هویتی")]
        EditPrivatePersonInfo = 4,


        [Description("ویرایش اطلاعات حساب بانکی")]
        EditBankingAccount = 8
    }
}