﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Cerberus.Domain.Enum
{
    public enum StatusReasonType : byte
    {
        [Description("ویرایش اطلاعات هویتی")]
        EditPrivatePerson = 1,

        [Description("ویرایش اطلاعات بانکی")]
        EditBankingAccount = 2,

        [Description("حذف نماینده")]
        DeleteAgent = 3,

        [Description("حذف  ولی زیر 18 سال")]
        DeleteAgentEighteen = 4,

        [Description("ویرایش نماینده")]
        EditAgent = 5,

        [Description("ادمین")]
        Admin = 6,

        [Description("حذف ولی زیر 18 سال در حالت ویرایش اطلاعات هویتی")]
        DeleteAgentEighteenPrivatePersonSemiSejami = 7,

        [Description("حذف ولی زیر 18 سال در حالت ویرایش اطلاعات بانکی")]
        DeleteAgentEighteenBankingAccountSemiSejami = 8,

        [Description("استعلام افرادی که در قید حیات نیستند")]
        DeadInquiry =9



    }
}