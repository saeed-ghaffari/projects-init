﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Domain.Enum
{
    public enum ExceptionType:byte
    {
        Exception=1,
        CerberusException
    }
}
