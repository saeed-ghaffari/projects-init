﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Cerberus.Domain.Enum
{
    public enum AuthenticationOfficesType : byte
    {
        [Description("دفتر پیشخوان")]
        CounterOffice = 1,

        [Description("کارگزاری")]
        Broker=2,

        [Description("بانک")]
        Bank=3,
        [Description("احراز هویت غیر حضوری")]
        NotInPresence = 4,
        [Description("احراز هویت حضوری")]
        Presence = 5,
        [Description("دفتر پیشخوان - شرکت آتی ساز")]
        AtisazCounterOffice = 6
        //[Description("پست")]
        //Post = 7
    }
}
