﻿namespace Cerberus.Domain.Enum
{
    public enum ImageType
    {
        PersonImage=1,
        SignatureImage=2
    }
}
