﻿namespace Cerberus.Domain.Enum
{
    public enum AgentDocumentStatus : byte
    {
        NotSet = 1,
        Confirm = 2,
        Reject = 3
    }
}
