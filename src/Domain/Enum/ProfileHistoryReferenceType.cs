﻿namespace Cerberus.Domain.Enum
{
    public enum ProfileHistoryReferenceType
    {
        ChangePrivatePerson = 1,
        UpdatePrivatePerson = 2,
        ChangeBankingAccount = 3,
        UpdateBankingAccount = 4,
        DeleteAgent = 5,
        RemoveAgentEighteen = 6,
        Admin = 7,
        EditAgent = 8


    }
}