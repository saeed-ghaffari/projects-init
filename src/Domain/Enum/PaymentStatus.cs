﻿namespace Cerberus.Domain.Enum
{
    public enum PaymentStatus : byte
    {
        Pending = 1,
        Init = 2,
        Verify = 3,
        Settle = 4,
        VerifyFailed = 5,
        SettleFailed = 6,
        Unsuccessful = 7,
        Cancel=8
    }
}