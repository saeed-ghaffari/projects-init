﻿namespace Cerberus.Domain.Enum
{
    public enum OperationType
    {
        Create = 1,
        Update = 2
    }
}
