﻿using System.ComponentModel;

namespace Cerberus.Domain.Enum
{
    public enum RejectType : byte
    {
        [Description("مشکل عکس")]
        Image=1,

        [Description("مشکل امضا")]
        Signature = 2,

        [Description("مشکل امضا و عکس")]
        ImageAndSignature = 3,

        [Description("چاپ مجدد فرم اطلاعات مشتری")]
        ReprintInformationForm = 4

    }
}