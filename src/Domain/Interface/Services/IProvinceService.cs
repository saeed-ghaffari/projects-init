﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface IProvinceService
    {
        Task<IEnumerable<Province>> GetListAsync(long countryId);
        Task<IEnumerable<Province>> GetListAsync();
    }
}