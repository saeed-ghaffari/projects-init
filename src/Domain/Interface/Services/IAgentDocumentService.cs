﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface IAgentDocumentService
    {
        Task CreateAsync(long profileId,File file);

        Task DeleteAsync(AgentDocument entity);
        Task<List<AgentDocument>> GetListDocumentByAgentId(long id);
        Task DeleteAsync(long id);
    }
}