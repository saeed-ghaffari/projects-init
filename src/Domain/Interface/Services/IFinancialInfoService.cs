﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface IFinancialInfoService
    {
        Task CreateAsync(FinancialInfo financialInfo);
        Task UpdateAsync(FinancialInfo financialInfo);

        Task<FinancialInfo> GetAsync(long id);
        Task<FinancialInfo> GetByProfileIdAsync(long profileId, bool includeBrokers = false);
        Task UpdateAsync(FinancialInfo financialInfoModel, IEnumerable<Broker> brokers);
        Task CreateAsync(FinancialInfo financialInfoModel, IEnumerable<Broker> brokers);
        Task<FinancialInfo> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
    }
}