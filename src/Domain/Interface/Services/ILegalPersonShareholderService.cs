﻿using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cerberus.Domain.Interface.Services
{
    public interface ILegalPersonShareholderService
    {
        Task<List<LegalPersonShareholder>> GetListAsync(long profileId);

        Task<LegalPersonShareholder> GetAsync(long id);

        Task CreateAsync(LegalPersonShareholder model);

        Task DeleteAsync(LegalPersonShareholder model);

    }
}