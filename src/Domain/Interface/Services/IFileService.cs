﻿using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface IFileService
    {
        Task CreateAsync(File file,byte[] arrayOfFile,string path);
        Task DeleteAsync(long id);
    }
}