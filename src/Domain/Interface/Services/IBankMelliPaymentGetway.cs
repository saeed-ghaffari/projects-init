﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.ViewModel;

namespace Cerberus.Domain.Interface.Services
{
    public interface IBankMelliPaymentGateway
    {
        Task<string> InitialPaymentAsync(long localInvoiceId, long amount);
        Task<MelliVerifyResult> MelliVerifyPaymentAsync(string orderId, string token);
    }
}
