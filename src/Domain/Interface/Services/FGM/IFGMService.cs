﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.ViewModel;
using Cerberus.Domain.ViewModel.FGM;
using Cerberus.Utility.Model;

namespace Cerberus.Domain.Interface.Services.FGM
{
    public interface IFgmService
    {
        Task<AuthenticationHeaderValue> GetAuthenticationHeaderValueAsync();
        Task<string> GetPaymentToken(string serviceId, long orderId,int amount);
        Task<Envelop<VerifyPaymentResponse>> VerifyPayment(string token);
        Task<Envelop<CallbackResponseViewModel>> CheckPaymentStatus(string orderId,Guid serviceId);
        Task<Envelop<CallbackResponseViewModel>> CheckOtherPaymentStatus(string orderId,Guid serviceId);
        Task<Envelop<FactorResponseModel>> SetFactorNumber(FactorRequestModel model);
        Task<Envelop<FactorResponseModel>> SetFactorForOtherPayment(FactorRequestModel model);
        Task<Envelop<HttpStatusCode>> CreatePayment(PaymentRequest paymentRequest);
    }
}
