﻿using System;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    /// <summary>
    /// for cache all objects --> Be careful that expiration time should not be very high
    /// </summary>
    public interface IMasterCacheProvider
    {
        /// <summary>
        /// Fetch data from cache if not exist fetch data from given func and save to cache then return it
        /// </summary>
        /// <typeparam name="T"> object type</typeparam>
        /// <param name="key">key to get data from cache</param>
        /// <param name="getFromDatabase">get data from database when data does not exist in cache</param>
        /// <param name="expirationTime">cache expiration time</param>
        /// <param> cache time to live
        ///     <name>ttl</name>
        /// </param>
        /// <returns></returns>
        Task<T> FetchAsync<T>(string key, Func<Task<T>> getFromDatabase, TimeSpan expirationTime);

        /// <summary>
        /// Fetch data from cache 
        /// </summary>
        /// <typeparam name="T"> object type</typeparam>
        /// <param name="key">key to get data from cache</param>
        /// <param name="throwException">consider when cache is disable throw exception or not</param>
        /// <returns></returns>
        Task<T> FetchAsync<T>(string key, bool throwException = true);
        Task<T> FetchFromCacheAsync<T>(string key);

        Task<PermanentOtp> FetchPermanentOtp(string key, Func<Task<PermanentOtp>> getFromDatabase, TimeSpan expirationTime);

        /// <summary>
        /// Store data in cache
        /// </summary>
        /// <typeparam name="T"> object type</typeparam>
        /// <param name="key">cache key</param>
        /// <param name="value">data</param>
        /// <param name="expirationTime">cache expiration time</param>
        /// <returns></returns>
        Task StoreAsync<T>(string key, T value, TimeSpan expirationTime);

        /// <summary>
        /// remove value from cache based on given key
        /// </summary>
        /// <param name="key">cache key</param>
        /// <param name="removeAt"></param>
        /// <returns></returns>
        Task RemoveAsync(string key, TimeSpan? removeAt = null);



    }
}