﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.StatusData;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.Domain.ViewModel.StatusData;

namespace Cerberus.Domain.Interface.Services
{
    public interface IProfileService
    {
        Task<Profile> GetAsync(long id, bool includeAllRelations = false);
        Task<Profile> GetWithNoCacheAsync(long id, bool includeAllRelations = false);
        Task<Profile> GetByUniqueIdentifierAsync(string uniqueIdentifier, bool includeAllRelations = false, bool includeAllChildRelations = false, ProfileOwnerType? type = null);

        Task<Profile> GetByUniqueIdentifierWithNoCacheAsync(string uniqueIdentifier,
            bool includeAllRelations = false,
            bool includeAllChildRelations = false,
            ProfileOwnerType? type = null);
        Task<Profile> CreateAsync(Profile profile);
        Task<Profile> UpdateAsync(Profile profile);
        Task UpdateStatusAsync(Profile profile);
        Task UpdateStatusAndReasonAsync(Profile profile);
        Task CreateInTransactionAsync(Profile profile);
        Task<IEnumerable<Profile>> GetGenericAsync(Expression<Func<Profile, bool>> predicate,
             Func<IQueryable<Profile>, IQueryable<Profile>> include = null, Func<IQueryable<Profile>, IOrderedQueryable<Profile>> order = null);
        Task<bool> IsExistMobileStatus(long mobile, ProfileStatus status);
        Task<bool> RegisterMobileCount(long mobile, ProfileStatus status, int count);

        Task<IEnumerable<Profile>> GetProfileListAsync(long fromRow, long toRow, string uniqueIdentifier = null, ProfileOwnerType? type = null, ProfileStatus status = ProfileStatus.SuccessPayment);

        Task<int> CountAsync(ProfileOwnerType? type = null, ProfileStatus? status = null);
        Task DeleteAsync(Profile entity);
        Task<Profile> GetDataForFactorAsync(long profileId);
        Task<Profile> GetRelatedDataWithEntity(long? profileId, string uniqueIdentifier, EntityType entity);
        Task<Profile> GetRelatedDataForDashboard(long profileId, ProfileOwnerType type);
        Task<Profile> GetRelatedDataForAgentAsync(long profileId);

        Task<Profile> GetRelatedDataForScopeAsync(long profileId, ProfileOwnerType type);

        Task<Profile> GetSignatureFileByUniqueIdentifierAsync(string uniqueIdentifier);


        Task<List<Profile>> GetForDeadAsync(string uniqueIdentifier, DateTime birthDate, ProfileStatus status = ProfileStatus.PolicyAccepted);

        Task<string> GetFullName(long profileId);
        Task<Profile> GetDataForFgmFactorAsync(long profileId);


        Task<Profile> GetWithNoLockAsync(string uniqueIdentifier);
        Task<IEnumerable<BatchSejamStatusResultViewModel>> GetSejamStatus(string referenceId, List<StatusData> validUniqueIdentifiers);

        Task UpdateCacheProfileAsync(Profile profile);

        Task<Profile> GetByUniqueIdentifierFromCacheAsync(string uniqueIdentifier);
        Task UpdateIsConfirmedEntryNodeAsync(Profile profile, long paymentId);

        Task<IEnumerable<BatchPrivatePersonResultViewModel>> GetBatchPrivatePerson(string referenceId, List<StatusData> validUniqueIdentifiers);

        Task<Profile> GetAuthenticatorProfileByUniqueIdentifierAsync(string uniqueIdentifier);

    }
}