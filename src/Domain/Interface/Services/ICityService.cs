﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface ICityService
    {
        Task<List<City>> GetListAsync(long provinceId);
        Task<List<City>> GetListAsync();
        Task<City> GetAsync(long id);
    }
}