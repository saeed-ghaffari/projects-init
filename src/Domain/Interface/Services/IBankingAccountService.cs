﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface IBankingAccountService
    {
        Task CreateAsync(BankingAccount bankingAccount);
        Task UpdateAsync(BankingAccount bankingAccount);
        Task DeleteAsync(BankingAccount bankingAccount);

        Task<BankingAccount> GetAsync(long id);

        Task<IEnumerable<BankingAccount>> GetListAsync(long profileId, bool includeRelatedData = false);

        Task<IEnumerable<BankingAccount>> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
        


    }
}