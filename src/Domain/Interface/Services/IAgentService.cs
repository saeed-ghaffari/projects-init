﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Services
{
    public interface IAgentService
    {
        Task CreateAsync(Agent agent);
        Task UpdateAsync(Agent agent);

        Task<Agent> GetAsync(long id);
      
        Task<Agent> GetByProfileIdAsync(long profileId);
        Task<Agent> GetByProfileIdWithNoLockAsync(long profileId);
        Task<Agent> GetByAgentProfileIdAsync(long agentProfileId);
        Task DeleteAsync(Agent entity);
        Task DeleteAsync(long id);
        Task MoveToArchiveAsync(Agent agent);
        Task AddToArchive(Agent oldAgent, DateTime fromDate);

        Task<List<Agent>> GetListByAgentProfileIdAsync(long agentProfileId);
    }
}