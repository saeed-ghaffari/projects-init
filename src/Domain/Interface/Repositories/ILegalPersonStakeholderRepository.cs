﻿using System;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface ILegalPersonStakeholderRepository : IBaseRepository<LegalPersonStakeholder>
    {
        Task<List<LegalPersonStakeholder>> GetListAsync(long profileId, StakeHolderType type);
       

    }
}