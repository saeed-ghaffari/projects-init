﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IProfilePackageRepository : IBaseRepository<ProfilePackage>
    {
        Task<ProfilePackage> GetUnusedPackageAsync(long profileId, PackageType type,PackageUsedFlag usePackage);
        Task<IEnumerable<ProfilePackage>> AllPackagesByProfileId(long profileId, PackageType type);
    }
}
