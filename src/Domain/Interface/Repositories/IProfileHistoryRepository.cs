﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IProfileHistoryRepository : IBaseRepository<ProfileHistory>
    {
        Task<ProfileHistory> GetByUniqueIdentifierAsync(string uniqueIdentifier);
        Task<IEnumerable<ProfileHistory>> GetByReferenceIdAsync(string referenceId, DateTime from, DateTime to);
        Task<ProfileHistory> GetTheLastHistoryAsync(long profileId);
    }
}