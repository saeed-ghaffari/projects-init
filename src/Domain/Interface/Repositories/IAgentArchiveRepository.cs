﻿using System;
using System.Collections.Generic;
using System.Text;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IAgentArchiveRepository : IBaseRepository<AgentArchive>
    {
    }
}
