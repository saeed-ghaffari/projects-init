﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IAgentRepository : IBaseRepository<Agent>
    {
        Task<Agent> GetByProfileIdAsync(long profileId);
        Task<Agent> GetByAgentProfileIdAsync(long agentProfileId);
        Task DeleteAsync(long id);

        Task<List<Agent>> GetListByAgentProfileIdAsync(long agentProfileId);
    }
}