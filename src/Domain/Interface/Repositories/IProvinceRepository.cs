﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IProvinceRepository : IBaseRepository<Province>
    {
      Task<IEnumerable<Province>> GetListAsync(long countryId);
        Task<IEnumerable<Province>> GetListAsync();
    }
}