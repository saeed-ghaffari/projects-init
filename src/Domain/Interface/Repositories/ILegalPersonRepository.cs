﻿using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface ILegalPersonRepository : IBaseRepository<LegalPerson>
    {
        Task<LegalPerson> GetByProfileIdAsync(long profileId);
        Task<LegalPerson> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
    }
}