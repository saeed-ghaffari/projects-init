﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IJobInfoRepository : IBaseRepository<JobInfo>
    {
        Task<JobInfo> GetByProfileIdAsync(long profileId, bool getAllRelated);
        Task<JobInfo> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
    }
}