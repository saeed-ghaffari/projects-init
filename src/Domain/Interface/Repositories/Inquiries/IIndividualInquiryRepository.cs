﻿using Cerberus.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IIndividualInquiryRepository : IBaseRepository<IndividualInquiry>
    {
    }
}
