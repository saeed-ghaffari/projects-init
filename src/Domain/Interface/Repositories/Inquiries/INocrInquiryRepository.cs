﻿using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface INocrInquiryRepository : IBaseRepository<NocrInquiry>
    {
    }
}
