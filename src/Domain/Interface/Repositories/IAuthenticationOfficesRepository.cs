﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IAuthenticationOfficesRepository : IBaseRepository<AuthenticationOffices>
    {
       Task<(List<AuthenticationOffices> authenticationOfficeses, long recordCount)> GetListAsync(AuthenticationOffices offices, int skip, int take);
       Task<List<AuthenticationOffices>>GetAllAsync();
    }
}
