﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IAddressSectionRepository : IBaseRepository<AddressSection>
    {
    }
}