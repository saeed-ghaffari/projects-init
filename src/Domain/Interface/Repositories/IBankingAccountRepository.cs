﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IBankingAccountRepository : IBaseRepository<BankingAccount>
    {
        Task<IEnumerable<BankingAccount>> GetListAsync(long profileId, bool includeRelatedData = false);
     
        Task<IEnumerable<BankingAccount>> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
        
       
    }
}