﻿using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IMessageRepository : IBaseRepository<Message>
    {

    }
}