﻿using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cerberus.Domain.Interface.Repositories
{
    public interface IBankRepository : IBaseRepository<Bank>
    {
      Task<List<Bank>> GetListAsync();
      Task<List<Bank>> GetListForAuthenticationOfficeAsync();
    }
}