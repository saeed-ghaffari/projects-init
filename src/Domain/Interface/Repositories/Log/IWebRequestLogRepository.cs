﻿using System.Threading.Tasks;
using Cerberus.Domain.Model.Log;

namespace Cerberus.Domain.Interface.Repositories.Log
{
    public interface IWebRequestLogRepository
    {
        Task AddAsync(WebRequestLog log);
    }
}
