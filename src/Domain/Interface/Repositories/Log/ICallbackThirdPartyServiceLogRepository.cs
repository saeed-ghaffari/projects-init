﻿using System.Threading.Tasks;
using Cerberus.Domain.Model.Log;

namespace Cerberus.Domain.Interface.Repositories.Log
{
    public interface ICallbackThirdPartyServiceLogRepository
    {
        Task AddAsync(CallbackThirdPartyServicesLog log);
    }
}