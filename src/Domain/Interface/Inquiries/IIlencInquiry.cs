﻿using System.Threading.Tasks;
using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Domain.Interface.Inquiries
{
    public interface IIlencInquiryService
    {
        Task<IlencInquiry> GetAsync(string nationalCode);
        Task UpdateAsync(IlencInquiry ilencInquiry);
    }
}
