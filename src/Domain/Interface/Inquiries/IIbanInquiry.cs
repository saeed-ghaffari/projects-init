﻿using System.Threading.Tasks;
using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Domain.Interface.Inquiries
{
    public interface IIbanInquiryService
    {
        Task<IbanInquiry> GetAsync(string iban);
        Task UpdateAsync(IbanInquiry ibanInquiry);
    }
}
