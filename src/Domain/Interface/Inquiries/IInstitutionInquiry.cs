﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel;
namespace Cerberus.Domain.Interface.Inquiries
{
    public interface IInstitutionInquiryService
    {
        Task<InstitutionInquiry> GetAsync(string privateCode);
    }
}
