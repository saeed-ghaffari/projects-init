﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.Inquiries;

namespace Cerberus.Domain.Interface.ReadRepositories.ReadInquiries
{
    public interface IIssuanceStockCodeReadRepository : IBaseReadRepository<IssuanceStockCode>
    {
        Task<IssuanceStockCode> GetByProfileIdAsync(long profileId, bool successful);
        Task<IssuanceStockCode> GetByUniqueIdentifierAsync(string uniqueIdentifier, bool successful);

        Task<IEnumerable<IssuanceStokeCodeModel>> GetBatchAsync(bool getCurrentDay, byte mode, byte divisor);
        Task<IssuanceStokeCodeModel> GetAllInfoAsync(string uniqueIdentifier);

        Task<List<IssuanceStockCode>> GetListWithListIdAsync(List<long> id);

    }

 
}