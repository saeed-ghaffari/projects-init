﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IBaseReadRepository<T> where T : BaseModel
    {
        Task<T> GetAsync(long id);
        Task<IEnumerable<T>> GetListAsync(Expression<Func<T, bool>> predicate);
        Task<IEnumerable<T>> GetConditionalListAsync(Expression<Func<T, bool>> predicate);
        Task<IEnumerable<T>> GetGenericAsync(Expression<Func<T, bool>> predicate,
            Func<IQueryable<T>, IQueryable<T>> include, Func<IQueryable<T>, IOrderedQueryable<T>> order);
    }
}