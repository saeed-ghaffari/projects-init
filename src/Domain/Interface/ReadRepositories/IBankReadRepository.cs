﻿using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IBankReadRepository : IBaseReadRepository<Bank>
    {
      Task<List<Bank>> GetListAsync();
      Task<List<Bank>> GetListForAuthenticationOfficeAsync();
    }
}