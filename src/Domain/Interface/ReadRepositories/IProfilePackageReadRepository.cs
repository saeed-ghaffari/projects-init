﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IProfilePackageReadRepository : IBaseReadRepository<ProfilePackage>
    {
        Task<ProfilePackage> GetUnusedPackageAsync(long profileId, PackageType type,PackageUsedFlag usePackage);
        Task<ProfilePackage> GetSuspiciousPackageAsync(long profileId, PackageType type);
        Task<IEnumerable<ProfilePackage>> AllPackagesByProfileId(long profileId, PackageType type);
    }
}
