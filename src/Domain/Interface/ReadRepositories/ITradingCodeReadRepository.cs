﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface ITradingCodeReadRepository : IBaseReadRepository<TradingCode>
    {
        Task<List<TradingCode>> GetListAsync(long profileId);
        Task<IEnumerable<TradingCode>> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
    }
}