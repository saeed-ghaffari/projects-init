﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IPrivatePersonReadRepository : IBaseReadRepository<PrivatePerson>
    {
        Task<PrivatePerson> GetByProfileIdAsync(long profileId);
        Task<PrivatePerson> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier);
        Task<IEnumerable<PrivatePerson>> GetByDaysLeftToEighteenAsync(int days,int status);
        Task<PrivatePerson> GetByProfileIdWithNoLockAsync(long profileId);
        Task<string> GetIncludeImageAsync(long profileId, ImageType imageType);
    }
}