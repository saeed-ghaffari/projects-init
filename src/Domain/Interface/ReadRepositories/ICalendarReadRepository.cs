﻿using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface ICalendarReadRepository : IBaseReadRepository<Calendar>
    {
        Task<bool> TodayIsHoliday();
    }
}