﻿using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.Interface.ReadRepositories
{
    public interface IThirdPartyServiceReadRepository : IBaseReadRepository<ThirdPartyService>
    {
        Task<ThirdPartyService> GetByLinkAsync(string link);

        Task<string> GetThirdPartyName(long? id);
        Task<ThirdPartyService> GetAsync(string title);
    }
}