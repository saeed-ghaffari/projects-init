﻿using Cerberus.Domain.ViewModel.FGM;

namespace Cerberus.Domain.ViewModel
{
    public class Envelop<T>
    {
        public T Data { get; set; }
        public Meta Meta { get; set; }
        public Error Error { get; set; }

    }
}
