﻿using Cerberus.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Domain.ViewModel
{
    public class IlencInquiryVM: BaseInquiryParams
    {
        public string NationalCode { get; set; }
    }
}
