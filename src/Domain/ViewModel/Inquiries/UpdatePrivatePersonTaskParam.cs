﻿using Cerberus.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Domain.ViewModel
{
   public class UpdatePrivatePersonTaskParam : BaseInquiryParams
    {
        public string Ssn { get; set; }
        public string BirthDate { get; set; }
    }
}
