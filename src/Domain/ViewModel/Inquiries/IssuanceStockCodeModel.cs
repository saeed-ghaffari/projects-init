﻿namespace Cerberus.Domain.ViewModel.Inquiries
{
    public class IssuanceStockCodeModel
    {
        public string NIN { get; set; }
        public string LegacyCode { get; set; }
        public string InvestorCode { get; set; }
        public string Message { get; set; }
    }
}