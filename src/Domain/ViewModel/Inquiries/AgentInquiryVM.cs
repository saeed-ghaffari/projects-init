﻿using System;
using System.Collections.Generic;
using System.Text;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.ViewModel
{
    public class AgentInquiryVm:BaseInquiryParams
    {
        public long? AgentProfileId { get; set; }
        public bool SendSmsVerificationAgent { get; set; }
    }
}
