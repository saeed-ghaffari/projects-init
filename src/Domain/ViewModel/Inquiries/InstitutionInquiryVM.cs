﻿using Cerberus.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Domain.ViewModel
{
    public class InstitutionInquiryVM: BaseInquiryParams
    {
        public string PrivateCode { get; set; }
    }
}
