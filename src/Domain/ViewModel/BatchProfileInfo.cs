﻿using System;

namespace Cerberus.Domain.ViewModel
{
    public class BatchProfileInfo
    {
        public string UniqueIdentifier { get; set; }
        public DateTime BirthDate { get; set; }

    }
}