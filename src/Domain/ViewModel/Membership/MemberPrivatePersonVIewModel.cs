﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.CustomValidation;
using Cerberus.Domain.Enum;
using Newtonsoft.Json;

namespace Cerberus.Domain.ViewModel.Membership
{
    public class MemberPrivatePerson
    {


        [Required(ErrorMessage = "{0} is Required")]
        [Range(0, 100000, ErrorMessage = "The amount must be between 100 USD and 100 thousand")]
        public long Discount { get; set; }


        [Required(ErrorMessage = "{0} is Required")]
        [RegularExpression(@"989(0[1-5]|1[0-9]|3[0-9]|2[0-9]|9[0-9])-?[0-9]{3}-?[0-9]{4}", ErrorMessage = "Invalid {0}")]
        public string Mobile { get; set; }



        [Required(ErrorMessage = "{0} is Required")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ,\s]+\s*$", ErrorMessage = "Only the Persian Character is allowed in {0}")]
        public string FirstName { get; set; }



        [Required(ErrorMessage = "{0} is Required")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ,\s]+\s*$", ErrorMessage = "Only the Persian Character is allowed in {0}")]
        public string LastName { get; set; }


        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ,\s]+\s*$", ErrorMessage = "Only the Persian Character is allowed in {0}")]
        public string FatherName { get; set; }


        [Required(ErrorMessage = "{0} is Required")]
        public Gender Gender { get; set; }


        [MaxLength(3, ErrorMessage = "Maximum of 3 Character is allowed in {0}")]
        [RegularExpression(@"^\s*[الفبلدر1234910\s]+\s*$", ErrorMessage = "{0} Invalid")]
        public string SeriShChar { get; set; }


        [RegularExpression("^[0-9]*$", ErrorMessage = "Only the number is allowed in {0}")]
        [MaxLength(2, ErrorMessage = "Maximum of 2 Character is allowed in {0}")]
        public string SeriSh { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Only the number is allowed in {0}")]
        [MaxLength(6, ErrorMessage = "Maximum of 6 digits is allowed in {0}")]
        public string Serial { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Only the number is allowed in {0}")]
        [MaxLength(10, ErrorMessage = "Maximum of 10 digits is allowed in {0}")]
        public string ShNumber { get; set; }

        [DateTimeValidation]
        public string BirthDate { get; set; }


        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ,\s]+\s*$", ErrorMessage = "Only the Persian Character is allowed in {0}")]
        [MaxLength(30, ErrorMessage = "Maximum of 30 Character is allowed in {0}")]
        public string PlaceOfIssue { get; set; }

        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ,\s]+\s*$", ErrorMessage = "Only the Persian Character is allowed in {0}")]
        [MaxLength(30, ErrorMessage = "Maximum of 30 Character is allowed in {0}")]
        public string PlaceOfBirth { get; set; }

        //[DisplayName("AccountLock")]
        //[Display(Name = "AccountLock")]
        [Required(ErrorMessage = "{0} Required")]

        public bool CanEditAccounts { get; set; }
    }
}