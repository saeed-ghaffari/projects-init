﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;

namespace Cerberus.Domain.ViewModel.Membership
{
    public class MemberBankingAccountViewModel
    {
       [RegularExpression("^[0-9]*$", ErrorMessage = "Only the number is allowed in {0}")]
        public string AccountNumber { get; set; }

        [Range((byte)(BankingAccountType.LongTermAccount), ((byte)BankingAccountType.SavingAccount), ErrorMessage = "{0} Invalid")]
        public BankingAccountType? Type { get; set; }

        [Required(ErrorMessage = "{0} can not be null")]
        [MaxLength(26, ErrorMessage = "{0} Invalid")]
        [MinLength(26, ErrorMessage = "{0} Invalid")]
        public string Sheba { get; set; }

        [Required(ErrorMessage = "{0} can not be null")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only the number is allowed in {0}")]
        public long? BankId { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Only the number is allowed in {0}")]
        public string BranchCode { get; set; }

        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ,\s]+\s*$", ErrorMessage = "Only the Persian Character is allowed in {0}")]
        public string BranchName { get; set; }

        
        [RegularExpression("^[0-9]*$", ErrorMessage = "Only the number is allowed in {0}")]
        public long? BranchCityId { get; set; }

        [Required(ErrorMessage = "{0} can not be null")]
        public bool IsDefault { get; set; }

    }
}