﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Domain.ViewModel.OfflineTasks.TaskParams
{
  public  class CreateInquiryParams
    {
        public long ProfileId { get; set; }
        public long? Mobile { get; set; }
    }
}
