﻿using System.Collections.Generic;

namespace Cerberus.Domain.ViewModel.OfflineTasks.TaskParams
{
    public class GetProfileInfoTaskParam
    {
        public long ServiceId { get; set; }
        public string CallbackUrl { get; set; }
        public IEnumerable<BatchProfileInfo> ProfileInfos { get; set; }
    }
}