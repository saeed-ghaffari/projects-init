﻿using System;
using System.Collections.Generic;
using System.Text;
using Cerberus.Domain.Enum;

namespace Cerberus.Domain.ViewModel.OfflineTasks.TaskParams
{
    public class SynchronizeTaskParams
    {
        public SynchronizerTarget Target { get; set; }
        public long ProfileId { get; set; }
        public long RefId { get; set; }
        public bool IsDeleted { get; set; }
        public string UniqueIdentifier { get; set; }
        public long ServiceId { get; set; }
        public byte[] DataSerialized { get; set; }
    }
}
