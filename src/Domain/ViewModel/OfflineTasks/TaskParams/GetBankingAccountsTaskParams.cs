﻿using System.Collections.Generic;

namespace Cerberus.Domain.ViewModel.OfflineTasks.TaskParams
{
    public class GetBankingAccountsTaskParams
    {
        public long ServiceId { get; set; }
        public string CallbackUrl { get; set; }
        public IEnumerable<string> UniqueIdentifiers { get; set; }
    }
}