﻿using Cerberus.Domain.Enum;

namespace Cerberus.Domain.ViewModel.OfflineTasks.TaskParams
{
    public class ChangeProfileStatusTaskParam
    {
        public long ProfileId { get; set; }
        public ProfileStatus ChangeToStatus { get; set; }
        public string Description { get; set; }
        public string ReferenceId { get; set; }
    }
}