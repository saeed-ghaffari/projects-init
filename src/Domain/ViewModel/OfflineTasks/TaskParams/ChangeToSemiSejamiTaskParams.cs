﻿using Cerberus.Domain.Enum;

namespace Cerberus.Domain.ViewModel.OfflineTasks.TaskParams
{
    public class ChangeToSemiSejamiTaskParams
    {
        public long ProfileId { get; set; }
        public short Tier { get; set; }
        public string Description { get; set; }
        public RejectType? RejectType { get; set; }

    }
}