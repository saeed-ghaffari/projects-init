﻿namespace Cerberus.Domain.ViewModel
{
    public class CompactLegalPerson
    {
        public string CompanyName { get; set; }
    }
}