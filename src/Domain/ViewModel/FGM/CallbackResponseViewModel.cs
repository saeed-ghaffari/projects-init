﻿using System;

namespace Cerberus.Domain.ViewModel.FGM
{
    public class CallbackResponseViewModel
    {
        public string Description { get; set; }
        public Guid ReferenceId { get; set; }
        public string SaleReferenceId { get; set; }
        public string SaleReferenceNumber { get; set; }
        public string OrderId { get; set; }
        public string Amount { get; set; }

    }
}