﻿namespace Cerberus.Domain.ViewModel
{
    public class CompactPrivatePerson
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}