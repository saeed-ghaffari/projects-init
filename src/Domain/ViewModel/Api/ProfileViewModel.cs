﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cerberus.Domain.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class ProfileViewModel
    {
        [JsonIgnore]
        public long Id { get; set; }

        public long Mobile { get; set; }

        public string Email { get; set; }

        public string UniqueIdentifier { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public ProfileOwnerType Type { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public ProfileStatus Status { get; set; }

        [JsonIgnore]
        public string TraceCode { get; set; }

        public PrivatePersonViewModel PrivatePerson { get; set; }

        public LegalPersonViewModel LegalPerson { get; set; }

        public IEnumerable<AddressViewModel> Addresses { get; set; }

        public IEnumerable<TradingCodeViewModel> TradingCodes { get; set; }

        public AgentViewModel Agent { get; set; }

        public IEnumerable<BankingAccountViewModel> Accounts { get; set; }

        public JobInfoViewModel JobInfo { get; set; }

        public FinancialInfoViewModel FinancialInfo { get; set; }

        public IEnumerable<LegalPersonShareholderViewModel> LegalPersonShareholders { get; set; }
        public IList<LegalPersonStakeholderViewModel> LegalPersonStakeholders { get; set; }

        [JsonIgnore]
        public IList<IssuanceStockCodeViewModel> IssuanceStockCodes{ get; set; }



        public CompactProfile ToCompactViewModel()
        {
            var result = new CompactProfile()
            {
                Type = Type,
                Iban = Accounts?.FirstOrDefault(x => x.IsDefault)?.Sheba,
            };

            if (Type == ProfileOwnerType.IranianPrivatePerson)
                result.PrivatePerson = new CompactPrivatePerson()
                {
                    FirstName = PrivatePerson.FirstName,
                    LastName = PrivatePerson.LastName
                };
            else if (Type == ProfileOwnerType.IranianLegalPerson)
            {
                result.LegalPerson = new CompactLegalPerson()
                {
                    CompanyName = LegalPerson.CompanyName
                };
            }

            return result;
        }
    }
}