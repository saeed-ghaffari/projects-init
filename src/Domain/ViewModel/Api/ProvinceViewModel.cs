﻿using System;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class ProvinceViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}