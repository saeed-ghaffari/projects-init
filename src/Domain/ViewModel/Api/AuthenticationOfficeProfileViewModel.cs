﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Domain.ViewModel.Api
{
    public class AuthenticationOfficeProfileViewModel
    {
        public List<object> results { get; set; } = new List<object>();
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string CountryCode { get; set; }
        public string TraceCode { get; set; }
        public string PersonTypeStr { get; set; }
        public DateTime? insertDateTime { get; set; }
        public string InsertDate { get; set; }
        public string NationalCode { get; set; }

        public string LegalOrReal { get; set; }

        public string IranOrNot { get; set; }
        public string IsAceptRealTerms { get; set; }
        public string IsAceptLegalTerms { get; set; }

        public string CurrentStep { get; set; }

        public string IsSejam { get; set; }

        public string IsLegal { get; set; }
        public string IsReal { get; set; }

        public string IsNative { get; set; }

        public string IsForeign { get; set; }
        public string IranOrForeignStr { get; set; }

        public int Id { get; set; }
        public string InsertIp { get; set; }

        public string IsDelete { get; set; }
        public string ResultData { get; set; }

        public string SecretCode2 { get; set; }

        public string SessionID { get; set; }

        public string MyConfirmToken { get; set; }

    }
}
