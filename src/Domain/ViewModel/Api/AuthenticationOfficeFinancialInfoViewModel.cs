﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Cerberus.Domain.Enum;

namespace Cerberus.Domain.ViewModel.Api
{
    [DisplayName("اطلاعات مالی")]
  public  class AuthenticationOfficeFinancialInfoViewModel
    {
        [DisplayName("ارزش دارایی")]
        public long? AssesstValue { get; set; }

        [DisplayName("متوسط درآمد")]
        public long? InComingAverage { get; set; }

        [DisplayName("بورس اوراق بهادار و فرابورس")]
        public long? SExchange_Transactin { get; set; }

        [DisplayName("بورس کالایی")]
        public long? CExchange_Transactin { get; set; }

        [DisplayName("بورس های خارج از کشور")]
        public long? outExchange_Transactin { get; set; }

        [DisplayName("نام شرکت")]
        public string WorkingCompany { get; set; }

        [DisplayName("پیش بینی سطح معاملات")]
        public string FtransactionLevelDescription { get; set; }

        [DisplayName("میزان آشنایی با مفاهیم مالی و بورس")]
        public string HowFamilyWithBourceDescription { get; set; }
    }
}
