﻿using System;
using Cerberus.Domain.Enum;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class BankingAccountViewModel
    {
        public string AccountNumber { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public BankingAccountType Type { get; set; }

        public string Sheba { get; set; }

        public BankViewModel Bank { get; set; }

        public string BranchCode { get; set; }

        public string BranchName { get; set; }

        public CityViewModel BranchCity { get; set; }

        public bool IsDefault { get; set; }

        [JsonIgnore]
        public bool? IsConfirmed { get; set; }
    }
}