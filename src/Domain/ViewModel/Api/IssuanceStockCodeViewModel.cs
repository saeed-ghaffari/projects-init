﻿using System;
using System.Collections.Generic;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{

    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class IssuanceStockCodeViewModel
    {
        public long ProfileId { get; set; }
        public string UniqueIdentifier { get; set; }
        public IList<IssuanceStokeCodeResponseViewModel> IssuanceStockCodeResponses { get; set; }
    }


}
