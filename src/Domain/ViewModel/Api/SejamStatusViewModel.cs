﻿namespace Cerberus.Domain.ViewModel.Api
{
    public class SejamStatusViewModel
    {
        public string ProfileStatus { get; set; }
    }
}