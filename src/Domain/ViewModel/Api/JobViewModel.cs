﻿using System;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class JobViewModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
    }
}