﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using ProtoBuf;

namespace Cerberus.Domain.ViewModel.Api
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class LegalPersonStakeholderViewModel
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public StakeHolderType Type { get; set; }

        [JsonConverter(typeof(StringEnumConverter))]
        public StakHolderPositionType PositionType { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? EndAt { get; set; }

        public bool IsOwnerSignature { get; set; }

        public string UniqueIdentifier { get; set; }
    }
}