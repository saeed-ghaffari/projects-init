﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [Table("Broker")]
    public class Broker : BaseModel
    {
        public string Title { get; set; }
        public string Code { get; set; }
        public IEnumerable<FinancialBroker> FinancialBrokers { get; set; }
    }
}
