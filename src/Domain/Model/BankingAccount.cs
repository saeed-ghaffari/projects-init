﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("BankingAccount")]
    public class BankingAccount : InquiryDataModel
    {
        [ForeignKey(nameof(ProfileOwner))]
        public long ProfileId { get; set; }

        public Profile ProfileOwner { get; set; }

        public string AccountNumber { get; set; }

        public BankingAccountType? Type { get; set; }

        public string Sheba { get; set; }

        [ForeignKey(nameof(Bank))]
        public long BankId { get; set; }

        public Bank Bank { get; set; }

        public string BranchCode { get; set; }

        public string BranchName { get; set; }

        [ForeignKey(nameof(BranchCity))]
        public long? BranchCityId { get; set; }

        public City BranchCity { get; set; }

        public bool IsDefault { get; set; }

    }
}