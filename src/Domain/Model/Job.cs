﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("Job")]
    public class Job : BaseModel
    {
        public string Title { get; set; }

    }
}
