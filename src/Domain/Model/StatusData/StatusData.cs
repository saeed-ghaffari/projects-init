﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Domain.Model.StatusData
{
    public class StatusData
    {
        public string RefrenceNumber { get; set; }
        public string NationalCode { get; set; }
    }
}
