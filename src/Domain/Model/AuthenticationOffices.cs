﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Cerberus.Domain.Enum;
using ProtoBuf;

namespace Cerberus.Domain.Model
{

    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("AuthenticationOffices")]

    public class AuthenticationOffices : BaseModel
    {
        [ForeignKey(nameof(Province))]
        public long? ProvinceId { get; set; }
        public Province Province { get; set; }

        [ForeignKey(nameof(City))]
        public long? CityId { get; set; }
        public City City { get; set; }

        [MaxLength(150)]
        public string Telephone { get; set; }

        [MaxLength(1000)]
        public string  Address { get; set; }
        public string ResponsibleName { get; set; }
        public AuthenticationOfficesType? Type { get; set; }

        [MaxLength(500)]
        public string OfficeName { get; set; }

        [MaxLength(50)]
        public string OfficeCode { get; set; }

        [ForeignKey(nameof(Bank))]
        public long? BankId { get; set; }

        public Bank Bank { get; set; }

    }
}
