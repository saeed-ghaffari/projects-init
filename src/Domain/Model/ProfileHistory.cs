﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;

namespace Cerberus.Domain.Model
{
    [Table("ProfileHistory")]
    public class ProfileHistory : BaseModel
    {
        [ForeignKey(nameof(ProfileHistory))]
        public long ProfileId { get; set; }

        public string ReferenceId { get; set; }

        public string PostId { get; set; }

        public Profile Profile { get; set; }


        [ForeignKey(nameof(ImageFile))]
        public long? ImageId { get; set; }

        public File ImageFile { get; set; }

        [ForeignKey(nameof(SignatureFile))]
        public long? SignatureFileId { get; set; }

        public File SignatureFile { get; set; }

        public ProfileStatus? ChangeFrom { get; set; }

        public ProfileStatus ChangedTo { get; set; }

        public string Description { get; set; }

        public RejectType? RejectType { get; set; }

        [ForeignKey(nameof(Service))]
        public long? ServiceId { get; set; }

        public ThirdPartyService Service { get; set; }
    }
}