﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("LegalPersonStakeholder")]
    public class LegalPersonStakeholder : BaseModel
    {
        //
        [ForeignKey(nameof(ProfileOwner))]
        public long ProfileId { get; set; }

        public Profile ProfileOwner { get; set; }

        [ForeignKey(nameof(StakeholderProfile))]
        public long StakeholderProfileId { get; set; }

        public Profile StakeholderProfile { get; set; }

        public StakeHolderType Type { get; set; }

        public StakHolderPositionType PositionType { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? EndAt { get; set; }

        public bool IsOwnerSignature { get; set; }

        [ForeignKey(nameof(SignatureFile))]
        public long? SignatureFileId { get; set; }

        public File SignatureFile { get; set; }

        [NotMapped]
        public string UniqueIdentifier { get; set; }
    }
}