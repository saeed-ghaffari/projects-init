﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cerberus.Domain.Model.Log
{
    [Table("CallbackThirdPartyServicesLog", Schema = "log")]
    public class CallbackThirdPartyServicesLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long ServiceId { get; set; }
        public string Url { get; set; }
        public string Payload { get; set; }
        public int HttpStatusCode { get; set; }
        public string Response { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime CreationDate { get; set; }
    }
}