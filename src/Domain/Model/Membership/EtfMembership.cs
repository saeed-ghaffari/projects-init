﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;
using Cerberus.Utility.Enum;
using ProtoBuf;

namespace Cerberus.Domain.Model.Membership
{
    [Table("EtfMembership", Schema = "mem")]
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class EtfMembership:BaseModel
    {
        [MaxLength(10)]
        public string UniqueIdentifier { get; set; }

        [MaxLength(12)]
        public string Mobile { get; set; }

        public Carrier Carrier { get; set; }

        [MaxLength(100)]
        public string FirstName { get; set; }

        [MaxLength(150)]
        public string LastName { get; set; }

        [MaxLength(100)]
        public string FatherName { get; set; }
        
        public DateTime? BirthDate { get; set; }

        [MaxLength(26)]
        public string Sheba { get; set; }

        [MaxLength(10)]
        public string PostalCode { get; set; }

        public long Amount { get; set; }

        public DateTime PaymentDate { get; set; }

        public long ServiceId { get; set; }

    }
}
