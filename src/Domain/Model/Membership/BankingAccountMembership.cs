﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;
using ProtoBuf;

namespace Cerberus.Domain.Model.Membership
{
    [Table("BankingAccountMembership", Schema = "mem")]
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class BankingAccountMembership : BaseModel
    {
        [ForeignKey(nameof(ThirdPartyService))]
        public long ServiceId { get; set; }

        public string UniqueIdentifier { get; set; }

        public string AccountNumber { get; set; }

        public BankingAccountType? Type { get; set; }

        public string Sheba { get; set; }

        [ForeignKey(nameof(Bank))]
        public long? BankId { get; set; }

        public Bank Bank { get; set; }

        public string BranchCode { get; set; }

        public string BranchName { get; set; }

        [ForeignKey(nameof(BranchCity))]
        public long? BranchCityId { get; set; }

        public City BranchCity { get; set; }

        public bool IsDefault { get; set; }

        public ThirdPartyService ThirdPartyService { get; set; }

        public MembershipFlag Flag { get; set; }
    }
}