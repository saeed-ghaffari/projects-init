﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Cerberus.Domain.Model
{
    [Table("IndividualInquiry")]
    public class IndividualInquiry:BaseModel
    {
        //[ForeignKey(nameof(Profile))]
        //public long ProfileId { get; set; }

        //public Profile Profile { get; set; }

        //[ForeignKey(nameof(PrivatePerson))]
        //public long PrivatePersonId { get; set; }

        //public PrivatePerson PrivatePerson { get; set; }
        public string PrivateCode { get; set; }
        public string BirthDate { get; set; }

        public string BirthPlaceCountry { get; set; }

        public int BirthPlaceCountryId { get; set; }

        public string Nationality { get; set; }

        public int NationalityId { get; set; }

        public bool Gender { get; set; }

        public string IdDocType { get; set; }

        public int IdDocTypeId { get; set; }

        public string IdDocExpirationDate { get; set; }

        public string AncestorName { get; set; }

        public string IdDocDescription { get; set; }

        public string IdDocNumber { get; set; }

        public string LatinFatherName { get; set; }

        public string LatinFirstName { get; set; }

        public string LatinLastName { get; set; }

        public string PersianFirstName { get; set; }

        public string PersianLastName { get; set; }

        public string PersianFatherName { get; set; }

        public string Photo { get; set; }

        public string Message { get; set; }

        public bool Successful { get; set; }
        /// <summary>
        /// For avoid get more inquiry if it's true
        /// </summary>
        public bool SuccessInquiry { get; set; }


        public bool HasException { get; set; }


    }
}
