﻿using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Cerberus.Domain.Model.Inquiries
{
    [Table("IbanInquiry")]
    public class IbanInquiry : BaseModel
    {

        [JsonProperty(PropertyName = "Iban")]
        public string Iban { get; set; }

        [JsonProperty(PropertyName = "FirstName")]
        public string FirstName { get; set; }

        [JsonProperty(PropertyName = "LastName")]
        public string LastName { get; set; }

        [JsonProperty(PropertyName = "Successful")]
        public bool Successful { get; set; }
        /// <summary>
        /// For avoid get more inquiry if it's true
        /// </summary>
        [JsonProperty(PropertyName = "SuccessInquiry")]
        public bool SuccessInquiry { get; set; }

        [JsonProperty(PropertyName = "Message")]
        public string Message { get; set; }

        [JsonProperty(PropertyName = "HasException")]
        public bool HasException { get; set; }

        

    }
}
