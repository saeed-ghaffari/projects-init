﻿using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace Cerberus.Domain.Model.Inquiries
{
    [Table("LegacyCodeResponse")]
    public class LegacyCodeResponse:BaseModel
    {
        [ForeignKey(nameof(LegacyCodeInquiry))]
        [JsonProperty(PropertyName = "LegacyCodeInquiryId")]
        public long? LegacyCodeInquiryId { get; set; }

        public LegacyCodeInquiry LegacyCodeInquiry { get; set; }

        [JsonProperty(PropertyName = "Nin")]
        public string Nin { get; set; }

        [JsonProperty(PropertyName = "LegacyCode")]
        public string LegacyCode { get; set; }

        [JsonProperty(PropertyName = "IsIndividual")]
        public bool? IsIndividual { get; set; }

        [JsonProperty(PropertyName = "IsActive")]
        public bool? IsActive { get; set; }

        [JsonProperty(PropertyName = "LegacyCodeOrginal")]
        public string LegacyCodeOrginal { get; set; }

        [JsonProperty(PropertyName = "LagecyCodeUnicode")]
        public string LagecyCodeUnicode { get; set; }

    }
}
