﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Domain.Model
{
  public  class BaseInquiryParams
    {
        
        public long ReferenceId { get; set; }
        public string Mobile { get; set; }
    }
}
