﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [Table("City")]
    public class City : BaseModel
    {
        public string Name { get; set; }

        [ForeignKey(nameof(Province))]
        public long ProvinceId { get; set; }

        public Province Province { get; set; }

        public string Prefix { get; set; }

        public IEnumerable<AddressSection> Sections { get; set; }

        public IEnumerable<Address> Addresses { get; set; }
        public IEnumerable<AuthenticationOffices> AuthenticationOfficeses { get; set; }
    }
}