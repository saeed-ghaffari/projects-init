﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    [Table("ThirdPartyService")]
    public class ThirdPartyService : BaseModel
    {
        public string Title { get; set; }
        public string LinkRefId { get; set; }
        public bool Enabled { get; set; }
        public bool EnabledLink { get; set; }
        public bool EnabledDiscount { get; set; }
        public bool HasFactor { get; set; }
    }
}
