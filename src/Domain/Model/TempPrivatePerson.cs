﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.Enum;
using ProtoBuf;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class TempPrivatePerson
    {
        [ForeignKey(nameof(ThirdPartyService))]
        public long ServiceId { get; set; }

        public string UniqueIdentifier { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FatherName { get; set; }

        public Gender Gender { get; set; }

        public string SeriShChar { get; set; }

        public string SeriSh { get; set; }

        public string Serial { get; set; }

        public string ShNumber { get; set; }

        public DateTime BirthDate { get; set; }

        public string PlaceOfIssue { get; set; }

        public string PlaceOfBirth { get; set; }

        public long Discount { get; set; }

        public ThirdPartyService ThirdPartyService { get; set; }



    }
}