﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cerberus.Domain.Model
{
    [Table("Message")]
    public class Message : BaseModel
    {
        public long ProfileId { get; set; }

        [Required]
        [MaxLength(1024)]
        public string Body { get; set; }

        public bool SendBySms { get; set; }

        [ForeignKey(nameof(ProfileId))]
        public Profile Profile { get; set; }
    }
}