﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cerberus.Domain.Model
{
    [Serializable]
    [Table("Province")]
    public class Province : BaseModel
    {
        public string Name { get; set; }

        [ForeignKey(nameof(Country))]
        public long CountryId { get; set; }

        public Country Country { get; set; }

        public IEnumerable<City> Cities { get; set; }

        public IEnumerable<Address> Addresses { get; set; }
        public IEnumerable<AuthenticationOffices> AuthenticationOfficeses { get; set; }
    }
}