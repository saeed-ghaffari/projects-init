﻿using System;
using System.ComponentModel.DataAnnotations;
using Cerberus.Utility;

namespace Cerberus.Domain.CustomValidation
{
    public class PersianMaxDatetimeValidation : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            var str = value != null ? value.ToString() : string.Empty;

            if (string.IsNullOrWhiteSpace(str)) return true;
            DateTime t;
            try
            {
                t = str.ToGeorgianDate();
            }
            catch (Exception)
            {
                return false;
            }

            return t.Year >= 1280 && t > DateTime.Now.Date;

        }
    }
}