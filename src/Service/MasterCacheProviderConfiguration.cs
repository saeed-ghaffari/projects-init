﻿using System;

namespace Cerberus.Service
{
    public class MasterCacheProviderConfiguration
    {
        public bool IsActive { get; set; }
        public TimeSpan ExpirationTime { get; set; }
        public TimeSpan UserExpirationTime { get; set; }
        public TimeSpan ProfileExpirationTime { get; set; }
        public TimeSpan PermanentOtpExpirationTime { get; set; }
    }
}
