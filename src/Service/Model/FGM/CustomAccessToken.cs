﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Service.Model.FGM
{
   public class CustomAccessToken
    {
        public string AccessToken { get; set; }

        public DateTime ExpiredAt { get; set; }
    }
}
