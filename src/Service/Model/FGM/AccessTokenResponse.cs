﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Service.Model.FGM
{
   public class AccessTokenResponse
    {
        public string AccessToken { get; set; }
        public TimeSpan Ttl { get; set; }
    }
}
