﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Service.Model
{
    public class FileServiceResponse
    {
        public string Reference { get; set; }
    }
}
