﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Service.Model.Inquiries
{
    public class DeadInquiryRequest
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
}
