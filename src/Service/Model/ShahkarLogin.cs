﻿namespace Cerberus.Service.Model
{
    public class ShahkarLogin
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ProjectCode { get; set; }

    }
}