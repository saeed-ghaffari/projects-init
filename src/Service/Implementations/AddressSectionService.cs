﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Microsoft.Extensions.Options;

namespace Cerberus.Service.Implementations
{
    public class AddressSectionService : IAddressSectionService
    {
        private readonly IAddressSectionReadRepository _addressSectionReadRepository;
        private readonly IMasterCacheProvider _masterCacheProvider;
        private readonly MasterCacheProviderConfiguration _masterCacheProviderConfiguration;
        public AddressSectionService(IAddressSectionReadRepository addressSectionReadRepository, IMasterCacheProvider masterCacheProvider,IOptions<MasterCacheProviderConfiguration> masterCacheProviderConfiguration)
        {
            _addressSectionReadRepository = addressSectionReadRepository;
            _masterCacheProvider = masterCacheProvider;
            _masterCacheProviderConfiguration = masterCacheProviderConfiguration.Value;
        }

        public async Task<IEnumerable<AddressSection>> GetListAsync(long cityId)
        {
            return await _masterCacheProvider.FetchAsync($"{nameof(AddressSectionService)}:City:{cityId}:key",
                async () => await  _addressSectionReadRepository.GetListAsync(x => x.CityId == cityId),
                _masterCacheProviderConfiguration.ExpirationTime);
            
        }
    }
}