﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;

namespace Cerberus.Service.Implementations
{
    public class FinancialInfoService : IFinancialInfoService
    {
        private readonly IFinancialInfoRepository _financialInfoRepository;
        private readonly IFinancialInfoReadRepository _financialInfoReadRepository;
        public FinancialInfoService(IFinancialInfoRepository financialInfoRepository, IFinancialInfoReadRepository financialInfoReadRepository)
        {
            _financialInfoRepository = financialInfoRepository;
            _financialInfoReadRepository = financialInfoReadRepository;
        }

        public Task CreateAsync(FinancialInfo financialInfo)
        {
            return _financialInfoRepository.CreateAsync(financialInfo);
        }

        public Task UpdateAsync(FinancialInfo financialInfo)
        {
            return _financialInfoRepository.UpdateAsync(financialInfo);
        }

        public Task<FinancialInfo> GetAsync(long id)
        {
            return _financialInfoReadRepository.GetAsync(id);
        }

        public Task<FinancialInfo> GetByProfileIdAsync(long profileId, bool includeBrokers = false)
        {
            return _financialInfoRepository.GetByProfileIdAsync(profileId, includeBrokers);
        }

        public Task UpdateAsync(FinancialInfo financialInfoModel, IEnumerable<Broker> brokers)
        {
            return _financialInfoRepository.UpdateAsync(financialInfoModel, brokers);
        }

        public Task CreateAsync(FinancialInfo financialInfoModel, IEnumerable<Broker> brokers)
        {
            return _financialInfoRepository.CreateAsync(financialInfoModel, brokers);
        }

        public Task<FinancialInfo> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier)
        {
            return _financialInfoReadRepository.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);
        }
    }
}