﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;

namespace Cerberus.Service.Implementations
{
    public class AddressService : IAddressService
    {
        private readonly IAddressRepository _addressRepository;
        private readonly IAddressReadRepository _addressReadRepository;
        public AddressService(IAddressRepository addressRepository, IAddressReadRepository aaAddressReadRepository)
        {
            _addressRepository = addressRepository;
            _addressReadRepository = aaAddressReadRepository;
        }

        public Task CreateAsync(Address address)
        {
            return _addressRepository.CreateAsync(address);
        }

        public Task UpdateAsync(Address address)
        {
            return _addressRepository.UpdateAsync(address);
        }

        public Task<Address> GetByProfileIdAsync(long profileId, bool getRelatedData = false)
        {
            return _addressReadRepository.GetByProfileIdAsync(profileId, getRelatedData);
        }

        public async Task<List<Address>> GetListByProfileIdAsync(long profileId)
        {
            return await _addressReadRepository.GetListByProfileIdAsync(profileId);
        }

        public async Task<IEnumerable<Address>> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier)
        {
            return await _addressReadRepository.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);
        }
    }
}