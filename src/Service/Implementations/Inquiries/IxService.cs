﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.Utility;
using Cerberus.Utility.Model;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace Cerberus.Service.Implementations.Inquiries
{
    public class IxService : IIxService
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly IDatabase _database;
        private readonly IxServiceConfiguration _ixServiceConfiguration;
        public IxService(IHttpClientFactory clientFactory, IDatabase database, IOptions<IxServiceConfiguration> ixServiceConfiguration)
        {
            _clientFactory = clientFactory;
            _database = database;
            _ixServiceConfiguration = ixServiceConfiguration.Value;
        }

        public async Task<HttpResponseResult<IxReturnType>> GetAsync(string uniqueIdentifier, TimeSpan ixAccessTokenTtl)
        {
            var accessToken = await GetTokenAsync(ixAccessTokenTtl);

            AwaRequestObject root = new AwaRequestObject();
            root.filter._operator = "OR";
            root.fromDate = DateTime.Now.AddYears(-10).ToPersianDate(format: "yyyyMMdd");//value isn't important just fill it
            root.toDate = DateTime.Now.ToPersianDate(format: "yyyyMMdd"); //value isn't important just fill it
            root.filter.items.Add(new Item() { attribute = "CISNAM", @operator = "==", value = uniqueIdentifier });
            root.awaUserCredentials.userID = uniqueIdentifier;
            root.awaUserCredentials.userIP = "127.0.0.1";
            root.awaUserCredentials.applicationName = "sejam";
            root.awaUserCredentials.context = "business optional desc.";
            var header = new Dictionary<string, string>
            {
                {"Accept", "application/json" }
                //,{ "Authorization", "Bearer " + accessToken }
            };

            var client = _clientFactory.CreateClient("IxService");

            var response = await HttpHepler.PostAsync<IxReturnType>(client, JsonConvert.SerializeObject(root),
                "application/json", TimeSpan.FromSeconds(2), new AuthenticationHeaderValue("Bearer", accessToken), header, true);

            if (response.Body.Fault != null)
            {
                Console.WriteLine(response.Body.Fault.Description);
                accessToken = await GetTokenAsync(ixAccessTokenTtl, true);
                header = new Dictionary<string, string>
                {
                    { "Accept", "application/json" }
                    //,{ "Authorization", "Bearer " + accessToken }
                };
                response = await HttpHepler.PostAsync<IxReturnType>(client, JsonConvert.SerializeObject(root), "application/json", TimeSpan.FromSeconds(2), new AuthenticationHeaderValue("Bearer", accessToken), header, true);

            }

            return response;
        }

        public async Task<LegacyCodeInquiry> GetLegacyCodeAsync(string uniqueIdentifier)
        {
            if (!_ixServiceConfiguration.EnableIxService)
                return new LegacyCodeInquiry() { HasException = true };

            Console.WriteLine($"Legacy Code for : {uniqueIdentifier}");
            var response = await GetAsync(uniqueIdentifier, _ixServiceConfiguration.IxAccessTokenTtl);

            LegacyCodeInquiry legacyCodeInquiry = new LegacyCodeInquiry();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                legacyCodeInquiry.Successful = true;
                legacyCodeInquiry.SuccessInquiry = true;
            }
            else if (response.StatusCode == HttpStatusCode.InternalServerError || response.StatusCode == HttpStatusCode.BadRequest)
            {
                legacyCodeInquiry.HasException = true;
            }

            legacyCodeInquiry.NinOrSsn = uniqueIdentifier;
            legacyCodeInquiry.ModifiedDate = DateTime.Now;

            if (legacyCodeInquiry.HasException)
                return legacyCodeInquiry;

            legacyCodeInquiry.LegacyCodeResponseList = new List<LegacyCodeResponse>();
            var responseList = new List<LegacyCodeResponse>();

            var data = response.Body;
            if (data.records.Any())
            {
                foreach (var record in data.records)
                {
                    LegacyCodeResponse legacyCodeResponse = new LegacyCodeResponse();
                    legacyCodeResponse.IsIndividual =
                        record.properties.CIITYP.Trim() == "1";
                    if (record.properties.CIACTV == "Y")
                        legacyCodeResponse.IsActive = true;
                    else if (record.properties.CIACTV == "N")
                        legacyCodeResponse.IsActive = false;
                    else
                    {
                        legacyCodeResponse.IsActive = null;
                    }


                    legacyCodeResponse.Nin = uniqueIdentifier;
                    legacyCodeResponse.LegacyCodeInquiry = legacyCodeInquiry;
                    legacyCodeResponse.LegacyCode = record.properties.CIINDV;
                    legacyCodeResponse.LegacyCodeOrginal = record.properties.CIINDV_Original;
                    legacyCodeResponse.LagecyCodeUnicode = record.properties.CIINDVUnicode;
                    legacyCodeResponse.ModifiedDate = DateTime.Now;

                    responseList.Add(legacyCodeResponse);
                }
            }

            legacyCodeInquiry.LegacyCodeResponseList = responseList;
            return legacyCodeInquiry;
        }


        public async Task<string> GetTokenAsync(TimeSpan accessTokenTtl, bool getNewToken = false)
        {
            try
            {
                var authorizeHeader = new AuthenticationHeaderValue("Basic", "UTVRVGZGcGF3dldNRDJpZEtPV01naThCZjlvYToxZ2J1SkVkN0VfQjU0VGdEVWV4UF9GRGMxWHNh");
                var accessTokenKey = $"Cerberus-{nameof(LegacyCodeInquiryService)}-AccessToken-IX-Service";
                if (getNewToken)
                {
                    var client = _clientFactory.CreateClient("IxServiceAccessToken");
                    var response = await HttpHepler.PostAsync<RefreshTokenVM>(client, "", "application/x-www-form-urlencoded", TimeSpan.FromSeconds(2), authorizeHeader, null, true);
                    var ixToken = new SaveAccessToken()
                    {
                        TokenModel = response.Body,
                        Ttl = DateTime.Now.Add(accessTokenTtl)
                    };

                    await _database.StringSetAsync(accessTokenKey, JsonConvert.SerializeObject(ixToken),
                        accessTokenTtl);
                    Console.WriteLine($"Get Force New Token : {response.Body.access_token}");
                    return response.Body.access_token;
                }
                var token = await _database.StringGetAsync(accessTokenKey);
                if (token.IsNullOrEmpty || !token.HasValue)
                {
                    var client = _clientFactory.CreateClient("IxServiceAccessToken");
                    var response = await HttpHepler.PostAsync<RefreshTokenVM>(client, "", "application/x-www-form-urlencoded", TimeSpan.FromSeconds(2), authorizeHeader, null, true);
                    var ixToken = new SaveAccessToken()
                    {
                        TokenModel = response.Body,
                        Ttl = DateTime.Now.Add(accessTokenTtl)
                    };

                    await _database.StringSetAsync(accessTokenKey, JsonConvert.SerializeObject(ixToken),
                        accessTokenTtl);

                    Console.WriteLine($"Get New Token : {response.Body.access_token}");

                    return response.Body.access_token;
                }
                else
                {
                    var ixTokenVm = JsonConvert.DeserializeObject<SaveAccessToken>(token);
                    if (DateTime.Now.AddMinutes(1) > ixTokenVm.Ttl)
                    {
                        var client = _clientFactory.CreateClient("IxServiceRefreshAccessToken");
                        client.BaseAddress = new Uri(string.Format(client.BaseAddress.ToString(), ixTokenVm.TokenModel.refresh_token));
                        var response = await HttpHepler.PostAsync<RefreshTokenVM>(client, "", "application/x-www-form-urlencoded", TimeSpan.FromSeconds(2), authorizeHeader, null, true);
                        var ixToken = new SaveAccessToken()
                        {
                            TokenModel = response.Body,
                            Ttl = DateTime.Now.Add(accessTokenTtl)
                        };

                        await _database.StringSetAsync(accessTokenKey, JsonConvert.SerializeObject(ixToken),
                            accessTokenTtl);

                        Console.WriteLine($"Get Refresh Token : {response.Body.access_token}");

                        return response.Body.access_token;
                    }

                    Console.WriteLine($"Get Cached Token : {ixTokenVm.TokenModel.access_token}");

                    return ixTokenVm.TokenModel.access_token;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine($"ERROR: {e.Message}");
                Console.WriteLine(e);
                throw e;
            }
        }
    }
}