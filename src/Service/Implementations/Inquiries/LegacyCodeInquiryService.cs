﻿using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.Domain.ViewModel;
using Cerberus.Utility;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;

namespace Cerberus.Service.Implementations.Inquiries
{
    public class LegacyCodeInquiryService : ILegacyCodeInquiryService
    {
        private readonly ILegacyCodeInquiryRepository _iLegacyCodeInquiryRepository;
        private readonly ILegacyCodeResponseRepository _legacyCodeResponseRepository;
        private readonly IIxService _ixService;
        private readonly IDatabase _database;
        private readonly InquiryConfiguration _config;
        private readonly IxServiceConfiguration _ixConfig;
        public LegacyCodeInquiryService(ILegacyCodeInquiryRepository iLegacyCodeInquiryRepository, IOptions<InquiryConfiguration> config,
            ILegacyCodeResponseRepository legacyCodeResponseRepository, IDatabase database, IIxService ixService, IOptions<IxServiceConfiguration> ixConfig)
        {
            _config = config.Value;
            _iLegacyCodeInquiryRepository = iLegacyCodeInquiryRepository;
            _legacyCodeResponseRepository = legacyCodeResponseRepository;
            _database = database;
            _ixService = ixService;
            _ixConfig = ixConfig.Value;
        }
        public async Task<LegacyCodeInquiry> GetAsync(string ninOrSsn)
        {
            var legacyCodeInquiry = await _ixService.GetLegacyCodeAsync(ninOrSsn);
            legacyCodeInquiry.NinOrSsn = ninOrSsn;
            await _iLegacyCodeInquiryRepository.CreateAsync(legacyCodeInquiry);
           
            return legacyCodeInquiry;

        }
        
    }
}
