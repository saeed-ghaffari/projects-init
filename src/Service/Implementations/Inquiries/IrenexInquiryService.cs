﻿using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Utility;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Model.Inquiries;

namespace Cerberus.Service.Implementations.Inquiries
{
    public class IrenexInquiryService : IIrenexInquiryService
    {
        private readonly ILegacyCodeInquiryRepository _iLegacyCodeInquiryRepository;
        private readonly InquiryConfiguration _config;
        public IrenexInquiryService(ILegacyCodeInquiryRepository iLegacyCodeInquiryRepository, IOptions<InquiryConfiguration> config)
        {
            _config = config.Value;
            _iLegacyCodeInquiryRepository = iLegacyCodeInquiryRepository;
        }
        public async Task<LegacyCodeInquiry> GetAsync(string ninOrSsn)
        {

            string baseApiUrl = _config.BaseApiUrl;
            string inquiryUrl = _config.IrenexInquiryUrl;
            string url = baseApiUrl + inquiryUrl;
            var existItem = await _iLegacyCodeInquiryRepository.GetConditionalListAsync(x => x.SuccessInquiry == true && (DateTime.Now - x.CreationDate).TotalHours < _config.LegacyInquiryTtl && x.NinOrSsn==ninOrSsn);
            var legacyCodeInquiries = existItem.ToList();
            if (!legacyCodeInquiries.Any())
            {
                var resultString = await HttpHepler.PostAsync(url, Newtonsoft.Json.JsonConvert.SerializeObject(new { NinOrSsn = ninOrSsn }), "application/json");

                var result = Newtonsoft.Json.JsonConvert.DeserializeObject<LegacyCodeInquiry>(resultString.Body);
                result.NinOrSsn = ninOrSsn;
                result.SuccessInquiry = result.Successful;
                await _iLegacyCodeInquiryRepository.CreateAsync(result);
                return result;
            }
            var inquiry = legacyCodeInquiries.FirstOrDefault();
            if (inquiry != null)
            {
                inquiry.Successful = inquiry.SuccessInquiry;
                await _iLegacyCodeInquiryRepository.UpdateAsync(inquiry);
                return inquiry;
            }

            return null;

        }
    }
}
