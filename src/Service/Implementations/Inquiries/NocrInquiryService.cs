﻿using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Utility;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories.ReadInquiries;
using Cerberus.Domain.Model.Inquiries;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Cerberus.Service.Implementations.Inquiries
{
    public class NocrInquiryService : INocrInquiryService
    {
        private readonly INocrInquiryRepository _iNocrInquiryRepository;
        private readonly INocrInquiryReadRepository _iNocrInquiryReadRepository;
        private readonly InquiryConfiguration _config;
        private readonly ILogger<NocrInquiryService> _logger;
        public NocrInquiryService(INocrInquiryRepository iNocrInquiryRepository, IOptions<InquiryConfiguration> config, ILogger<NocrInquiryService> logger, INocrInquiryReadRepository iNocrInquiryReadRepository)
        {
            _config = config.Value;
            _iNocrInquiryRepository = iNocrInquiryRepository;
            _logger = logger;
            _iNocrInquiryReadRepository = iNocrInquiryReadRepository;
        }
        public async Task<NocrInquiry> GetAsync(string ssn, string birthDate)
        {
            var resultBody = string.Empty;//just for log
            try
            {
                Console.WriteLine("NocrInquiry service");
                string baseApiUrl = _config.BaseApiUrl;
                string inquiryUrl = _config.NocrInquiryUrl;
                string url = baseApiUrl + inquiryUrl;
                var existItem = await _iNocrInquiryReadRepository.GetConditionalListAsync(x => x.SuccessInquiry && x.CreationDate.AddHours(_config.NocrInquiryTtl) > DateTime.Now && x.Ssn == ssn);
                var nocrInquiries = existItem.ToList();
                if (!nocrInquiries.Any())
                {
                    var resultString = await HttpHepler.PostAsync(url, JsonConvert.SerializeObject(new { Ssn = ssn, BirthDate = birthDate }), "application/json");
                    resultBody = resultString.Body;
                    var result = JsonConvert.DeserializeObject<NocrInquiry>(resultString.Body);
                    result.Ssn = ssn;
                    result.SuccessInquiry = result.Successful;
                    await _iNocrInquiryRepository.CreateAsync(result);

                    return result;
                }
                var inquiry = nocrInquiries.FirstOrDefault();
                if (inquiry != null)
                {
                    inquiry.Successful = inquiry.SuccessInquiry;
                    await _iNocrInquiryRepository.UpdateAsync(inquiry);
                    return inquiry;
                }

                return null;
            }
            catch (Exception e)
            {
                _logger.LogCritical($"{nameof(NocrInquiryService)} Error:{resultBody} --- {e.Message}---{e.InnerException?.Message}");
                return null;
            }
        }

        public async Task<NocrInquiry> GetExistInquiryAsync(string ssn, string birthDate)
        {
            var existItem = await _iNocrInquiryReadRepository.GetListAsync(x => x.SuccessInquiry && x.Ssn == ssn);

            var inquiry = existItem.LastOrDefault();

            if (inquiry == null)
            {
                return await GetAsync(ssn, birthDate);
            }

            return inquiry;
        }
        public Task UpdateAsync(NocrInquiry nocrInquiry)
        {
            return _iNocrInquiryRepository.UpdateAsync(nocrInquiry);
        }
    }
}
