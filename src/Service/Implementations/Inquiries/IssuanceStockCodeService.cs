﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.ViewModel.Inquiries;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Cerberus.Service.Implementations.Inquiries
{
    
    public class IssuanceStockCodeService : IIssuanceStockCodeService
    {
        private readonly HttpClient _httpClient;
        private readonly IssuanceStockCodeConfiguration _config;

        public IssuanceStockCodeService(HttpClient httpClient, IOptions<IssuanceStockCodeConfiguration> config)
        {
            _httpClient = httpClient;
            _config = config.Value;
        }

        public async Task<(IEnumerable<IssuanceStockCodeModel> codeResponseModel, HttpStatusCode StatusCode)>
            GetStockCodeAsync(IEnumerable<IssuanceStokeCodeModel> model)
        {

            var response = await Utility.HttpHepler.PostAsync<IEnumerable<IssuanceStockCodeModel>>(
                _httpClient, JsonConvert.SerializeObject(model), "application/json",
                TimeSpan.FromMinutes(_config.RequestTimeOut),null,new Dictionary<string, string>(){{"Accept","application/json"}});

            return (response.Body,response.StatusCode);
        }
    }
}