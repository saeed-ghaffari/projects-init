﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Repositories.Inquiries;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.Domain.ViewModel.Inquiries;
using Cerberus.Service.Model.Inquiries;
using Cerberus.Utility;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace Cerberus.Service.Implementations.Inquiries
{
    public class DeadInquiryService : IDeadInquiryService
    {
        private readonly InquiryConfiguration _config;
        private readonly ILogger<DeadInquiryService> _logger;
        private readonly IDeadInquiryRepository _deadInquiryRepository;
        public DeadInquiryService(IOptions<InquiryConfiguration> config, ILogger<DeadInquiryService> logger, IDeadInquiryRepository deadInquiryRepository)
        {
            _config = config.Value;
            _logger = logger;
            _deadInquiryRepository = deadInquiryRepository;
        }

        public async Task<DeadListInquiryVm> SendAsync(string fromDate, string toDate)
        {

            var baseApiUrl = _config.BaseApiUrl;
            var inquiryUrl = _config.DeadInquiryRequestUrl;
            var url = baseApiUrl + inquiryUrl;
            var request = new DeadInquiryRequest
            {
                FromDate = fromDate,
                ToDate = toDate
            };
            try
            {
                var resultString = await HttpHepler.PostAsync(url, JsonConvert.SerializeObject(request), "application/json");

                var result = JsonConvert.DeserializeObject<DeadListInquiryVm>(resultString.Body);
                return result;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, "unable to dead inquiry request executer service");
                throw;
            }
        }

        public async Task<DeadListResponseVm> GetAsync(Guid id)
        {
            var resultBody = string.Empty;//just for log
            var baseApiUrl = _config.BaseApiUrl;
            var inquiryUrl = _config.DeadInquiryDownloadUrl;
            var url = baseApiUrl + inquiryUrl;
            var request = new { Id = id };
            string[] stringSeparators = { "\r\n" };
            try
            {
                var resultString = await HttpHepler.PostAsync(url, JsonConvert.SerializeObject(request), "application/json");
                resultBody = resultString.Body;

                var result = JsonConvert.DeserializeObject<DeadListResponseVm>(resultBody);
                if (result.FileName != null)
                {

                    var deadList = result.Data.Split(stringSeparators, StringSplitOptions.None).ToList();


                    var inquiry = new List<DeadInquiry>();
                    foreach (var item in deadList.Take(deadList.Count - 1))
                    {
                        var birthDate = item.Split(',')[3].Trim();
                        var deadDate = item.Split(',')[1].Trim();
                        inquiry.Add(new DeadInquiry
                        {
                            FromDate = item.Split(',')[0].ConcatDate().ToGeorgianDate(),
                            ToDate = item.Split(',')[0].ConcatSecondDate().ToGeorgianDate(),
                            DeadDate = deadDate.Length > 7 ? deadDate.ConcatDate().ToGeorgianDate() : ("1300/01/01").ToGeorgianDate(),
                            UniqueIdentifier = item.Split(',')[2],
                            BirthDate = birthDate.Length > 7 ? birthDate.ConcatDate().ToGeorgianDate() : ("1300/01/01").ToGeorgianDate(),
                            PersianBirthDate = birthDate.Length > 7 ? birthDate.ConcatDate() : "1300/01/01",
                            LastName = item.Split(',')[4],
                            FileName = result.FileName.GetValueOrDefault(),
                            HasException = false,
                            IsOk = birthDate.Length>7 && deadDate.Length>7,
                            SuccessInquiry = true,

                        });


                    }
                    await _deadInquiryRepository.CreateRangeAsync(inquiry);
                    result.DeadInquiries = inquiry;
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.LogCritical(e, $"unable to dead inquiry response executer service: {resultBody}");
                throw;
            }
        }
    }
}
