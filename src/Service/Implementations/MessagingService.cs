﻿using Cerberus.Domain.Interface.Services;
using System;
using System.Threading.Tasks;
using Aspina;
using Cerberus.Domain.Interface.Repositories.Log;
using Cerberus.Domain.Model.Log;
using Newtonsoft.Json;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Domain.ViewModel.OfflineTasks;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.Utility.Enum;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;

namespace Cerberus.Service.Implementations
{
    public class MessagingService : IMessagingService
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IBackgroundTaskQueue _backgroundTaskQueue;
        private readonly ISmsLogRepository _smsLogRepository;
        private readonly ILogger<MessagingService> _logger;
        private readonly MessagingConfiguration _messagingConfiguration;
        public MessagingService(ITaskRepository taskRepository, IBackgroundTaskQueue backgroundTaskQueue,
            ISmsLogRepository smsLogRepository, ILogger<MessagingService> logger, IOptions<MessagingConfiguration> messagingConfiguration)
        {
            _taskRepository = taskRepository;
            _backgroundTaskQueue = backgroundTaskQueue;
            _smsLogRepository = smsLogRepository;
            _logger = logger;
            _messagingConfiguration = messagingConfiguration.Value;
        }

        public async Task SendSmsAsync(long? profileId, string msisdn, Carrier? carrier, string message, DateTime? dueTime)
        {
            var param = new SendSmsTaskParam() { Msisdn = msisdn, ProfileId = profileId.GetValueOrDefault(), SmsText = message };
            await _taskRepository.AddAsync(new ScheduledTask
            {
                Type = CerberusTasksIdentifierHelper.SendSms.Value,
                DueTime = dueTime ?? DateTime.Now,
                GroupId = CerberusTasksIdentifierHelper.SendSms.Key,
                Params = JsonConvert.SerializeObject(param),
                RefId = profileId,
                Status = TaskStatus.Pending
            });

        }

        public async Task SendOtpSmsAsync(long? profileId, string msisdn, Carrier? carrier, string message, DateTime? dueTime = null)
        {
            
            if (!_messagingConfiguration.UseRahyab)
                await SendSmsAsync(profileId, msisdn, carrier, message, DateTime.Now);
            else
            {
                var insertData = "insert into outgoing_message(from_mobile_number,dest_mobile_number,message_body,due_date,creation_date," +
                                 "Priority) values (@from_mobile, @dest_mobile_number, @message_body, @due_date, @creation_date, @Priority)";
                using (var connection = new MySqlConnection(_messagingConfiguration.RahyabSmsConectionString))
                {
                    try
                    {
                        var command = new MySqlCommand(insertData, connection);
                        command.Parameters.AddWithValue("@from_mobile", _messagingConfiguration.RahyabSmsFromMobile);
                        command.Parameters.AddWithValue("@dest_mobile_number", msisdn);
                        command.Parameters.AddWithValue("@message_body", message);
                        command.Parameters.AddWithValue("@due_date", DateTime.Now);
                        command.Parameters.AddWithValue("@creation_date", DateTime.Now);
                        command.Parameters.AddWithValue("@Priority", 1);
                        connection.Open();
                        command.CommandTimeout = 5;
                        await command.ExecuteNonQueryAsync();
                        connection.Close();
                    }
                    catch (Exception e)
                    {
                        _logger.LogCritical(e.Message, "rahyabError:");
                        return;
                    }
                }
                var smsLog = new SmsLog() { ProfileId = profileId.GetValueOrDefault(), SmsText = message, Msisdn = msisdn };

                _backgroundTaskQueue.QueueBackgroundWorkItem(sms => _smsLogRepository.AddAsync(smsLog));

            }
        }
        //private (long key, string value) GetHelper()
        //{
        //    var rnd = new Random();
        //    var dice = rnd.Next(1, 4);
        //    switch (dice)
        //    {
        //        case 1:
        //            return (CerberusTasksIdentifierHelper.SendSms1.Key, CerberusTasksIdentifierHelper.SendSms1.Value);

        //        case 2:
        //            return (CerberusTasksIdentifierHelper.SendSms2.Key, CerberusTasksIdentifierHelper.SendSms2.Value);
        //        default:
        //            return (CerberusTasksIdentifierHelper.SendSms3.Key, CerberusTasksIdentifierHelper.SendSms3.Value);
        //    }
        //}
    }
}