﻿using System;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Services;
using Cerberus.Service;
using Cerberus.Utility;
using Cerberus.Web.Configuration;
using Cerberus.Web.Enum;
using Cerberus.Web.Extensions;
using Cerberus.Web.Filters;
using Cerberus.Web.Model;
using Cerberus.Web.ViewModel.Mobile;
using Cerberus.Web.ViewModel.Profile;
using Cerberus.Web.ViewModel.ProfilePackageViewModel;
using Cerberus.Web.WebLogic;
using Gatekeeper.AspModules.ActionFilter;
using Gatekeeper.AspModules.Interface;
using Gatekeeper.Core.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace Cerberus.Web.Controllers
{
    public class ProfileController : BaseController
    {
        private readonly MessagingConfiguration _messagingConfiguration;
        private readonly IMessagingService _messagingService;
        private readonly WebConfiguration _webConfiguration;
        private readonly IProfileService _profileService;
        private readonly IOtpService _otpService;
        private readonly IProfilePackageService _profilePackageService;
        private readonly IRateLimitService _rateLimitService;
        private readonly CookieHelper _cookieHelper;
        private readonly IAgentService _agentService;
        private readonly IOfflineTaskService _offlineTaskService;
        private readonly IMsisdnService _msisdnService;
        private readonly IPrivatePersonService _privatepPersonService;
        public ProfileController(
            IOptions<MessagingConfiguration> messagingConfiguration,
            IMessagingService messagingService,
            IOptions<WebConfiguration> webConfiguration,
            IProfileService profileService,
            IOtpService otpService,
            IProfilePackageService profilePackageService,
            IRateLimitService rateLimitService,
            CookieHelper cookieHelper,
            IAgentService agentService,
            IOfflineTaskService offlineTaskService,
            IMsisdnService msisdnService,
            IPrivatePersonService privatepPersonService)
        {
            _messagingConfiguration = messagingConfiguration.Value;
            _messagingService = messagingService;
            _webConfiguration = webConfiguration.Value;
            _profileService = profileService;
            _otpService = otpService;
            _profilePackageService = profilePackageService;
            _rateLimitService = rateLimitService;
            _cookieHelper = cookieHelper;
            _agentService = agentService;
            _offlineTaskService = offlineTaskService;
            _msisdnService = msisdnService;
            _privatepPersonService = privatepPersonService;
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("/editMsisdn/confirm")]
        [AuthorizeUserFilterFactory()]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> ConfirmAsync(EditMobileViewModel model)
        {
            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                return View("EditMobileNumber");
            }

            var profile = await _profileService.GetAsync(User.ProfileId);
            var newMobile = model.NewMobile;
            if (!ValidationHelper.IsMsisdnValid(ref newMobile, out var carrier) || carrier == null)
            {
                Alert("شماره تلفن وارد شده صحیح نمی باشد", NotificationType.Warning);
                return View("EditMobileNumber");
            }

            if (profile.Mobile == newMobile.ToLong())
            {
                Alert("شماره تلفن وارد شده با شماره تلفن همراه جدید نمی تواند برابر باشد.", NotificationType.Warning);
                return View("EditMobileNumber");
            }

            var validateOtpStatus = await _otpService.IsValidAsync(ConventionalHelper.CreateOtpKey(profile.Mobile.ToString(), OtpType.EditMobile), model.Otp);

            if (!validateOtpStatus)
            {
                Alert("کد تایید وارد شده صحیح نمی باشد", NotificationType.Error);
                return View("EditMobileNumber");
            }
            var key = ConventionalHelper.CreateOtpKey(newMobile, OtpType.EditMobile);
            var otpCode = await _otpService.GetAsync(key, _webConfiguration.OtpTtl);
            //send otp to user using SMS
            await _messagingService.SendOtpSmsAsync(
                null,
               newMobile, null
                ,
                string.Format(_messagingConfiguration.ChangeMobileOtpMessage, otpCode, Environment.NewLine));
            Alert("کد تایید با موفقیت به شماره تلفن همراه جدید ارسال شد", NotificationType.Success);
            model.Type = PrimitivePackageType.EditMobileNumber;
            return View("VerifyMobileNumber", model);
        }

        public IActionResult VerifyAsync(EditMobileViewModel model)
        {
            return View("VerifyMobileNumber", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("/editMsisdn/Final")]
        [AuthorizeUserFilterFactory()]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> UpdateAsync(EditMobileViewModel model)
        {
            var newMobile = User.Mobile.HasValue ? User.Mobile.Value.ToString() : model.NewMobile;
            if (!ValidationHelper.IsMsisdnValid(ref newMobile, out var carrier) || carrier == null)
            {
                Alert("شماره تلفن وارد شده صحیح نمی باشد", NotificationType.Warning);
                return View("VerifyMobileNumber", model);
            }

            var validateOtpStatus = await _otpService.IsValidAsync(
                ConventionalHelper.CreateOtpKey(newMobile,
                    User.Mobile.HasValue ? OtpType.EditMobileWithShahkarService : OtpType.EditMobile), model.Otp);

            if (!validateOtpStatus)
            {
                Alert("کد تایید وارد شده صحیح نمی باشد", NotificationType.Warning);
                return View("VerifyMobileNumber", model);
            }

            var profilePackage = await _profilePackageService.GetUnusedPackagesAsync(User.ProfileId,
                PackageType.EditAllData, PackageUsedFlag.EditMobileNumber);

            if (profilePackage == null)
            {
                Alert("برای تغییر شماره موبایل باید قبلا عملیات پرداخت را انجام دهید.", NotificationType.Warning);
                return RedirectToAction("Index");
            }

            if (!await HasAccess("UpdateAsync", User.ProfileId.ToString(), 1 * 60 * 24, 3))
            {
                return File("~/429.html", "text/html");
            }

            profilePackage.UsedFlag |= PackageUsedFlag.EditMobileNumber;
            await _profilePackageService.UpdateAsync(profilePackage);

            var profile = await _profileService.GetWithNoCacheAsync(User.ProfileId);

            if (profile.Status == ProfileStatus.SuccessPayment || profile.Status >= ProfileStatus.TraceCode)
            {
                await _offlineTaskService.ScheduleOfflineTasksShahkarAsync(User.ProfileId, profile.Mobile.ToLong(), newMobile.ToLong(), profile.UniqueIdentifier, DateTime.Now, User.ProfileStatus);
            }
            else
                await _offlineTaskService.ScheduleOfflineTasksAsync(User.ProfileId, newMobile.ToLong(), DateTime.Now.AddSeconds(10));
            var final = new FinalProfileViewModel
            {
                PackageType = profilePackage.Type,
                ProfilePackageId = profilePackage.Id

            };

            return View("Final", final);
        }


        [HttpPost]
        [Route("api/resendOtpEdit/{mobile}/{type}")]
        [RateLimitFilterFactory(Order = 1, Limit = 2, PeriodInSec = 1 * 60 * 10)]
        public async Task<JsonResult> ResendOtpAsync(string mobile, OtpType type)
        {
            if (!await HasAccess("ResendOtpAsync", mobile + type, 120, 3))
            {
                return Json(new { IsSuccess = false, Message = "تعداد درخواست شما بیش از حد مجاز می باشد" });
            }
            if (!ValidationHelper.IsMsisdnValid(ref mobile, out var carrier) || carrier == null)
            {
                return Json(new { IsSuccess = false, Message = "شماره تلفن وارد شده صحیح نمی باشد" });
            }

            var key = ConventionalHelper.CreateOtpKey(mobile, type);
            var otpCode = await _otpService.GetAsync(key, _webConfiguration.OtpTtl);

            //send otp to user using SMS
            await _messagingService.SendOtpSmsAsync(
                    null,
                    mobile,
                    carrier.Value,
                    string.Format(_messagingConfiguration.ChangeMobileOtpMessage, otpCode, Environment.NewLine));

            return Json(new { IsSuccess = true, Message = "کد تایید با موفقیت برای شما ارسال شد " });

        }



        [Route("msisdn")]
        public IActionResult Index()
        {
            Alert("از آنجایی که ویرایش شماره تلفن همراه مستلزم پرداخت هزینه می باشد، در نظر داشته باشید شماره تلفن همراه جدید بایستی تحت مالکیت کد ملی ثبت نام شده باشد", NotificationType.Info);
            return View();
        }



        [ValidateAntiForgeryToken]
        [HttpPost("msisdn")]
        [ServiceFilter(typeof(ValidateCaptchaAttribute))]
        [RateLimitFilterFactory(Order = 1, Limit = 100, PeriodInSec = 1 * 60 * 1)]
        public async Task<IActionResult> CheckProfileExist(CheckMobileViewModel model)
        {
            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                return View("Index");
            }

            var mobile = model.Msisdn;
            if (!ValidationHelper.IsMsisdnValid(ref mobile, out var carrier) || carrier == null)
            {
                Alert("فرمت شماره تلفن وارد شده صحیح نمی باشد", NotificationType.Warning);
                return View("Index");
            }


            var profile = await _profileService.GetByUniqueIdentifierAsync(model.UniqueIdentifier);
            if (profile == null || profile.Status == ProfileStatus.Init)
            {
                Alert("اطلاعات ارسالی اشتباه می باشد.", NotificationType.Warning);
                return View("Index");
            }

            if (profile.Status==ProfileStatus.PendingValidation)
            {
                Alert("تا قبل از مشخص شدن  نتیجه استعلام، نمی توانید شماره تلفن همراه خود را ویرایش نمایید.", NotificationType.Warning);
                return View("Index");
            }
            var privatePerson = await _privatepPersonService.GetByProfileIdAsync(profile.Id);
            if (Tools.IsUnder18Years(privatePerson?.BirthDate))
            {
                Alert("به دلیل نرسیدن به سن قانونی، شما نمی توانید شماره موبایل خود را ویرایش کنید .", NotificationType.Warning);
                return View("Index");
            }

            //if (profile.Status >= ProfileStatus.PolicyAccepted && profile.Status < ProfileStatus.TraceCode)
            //{
            //    Alert("تا قبل از مشخص شدن  نتیجه استعلام، نمی توانید شماره تلفن همراه خود را ویرایش نمایید", NotificationType.Warning);
            //    return View("Index");
            //}

            var msisdn = await _msisdnService.GetByProfileIdAsync(profile.Id);
            if (msisdn != null && msisdn.IsConfirmed == null)
            {
                Alert("تا قبل از مشخص شدن  نتیجه استعلام، نمی توانید شماره تلفن همراه خود را ویرایش نمایید", NotificationType.Warning);
                return View("Index");
            }
            if (profile.Mobile == mobile.ToLong())
            {
                Alert("شما قبلا با این شماره تلفن در سجام ثبت نام کرده اید", NotificationType.Warning);
                return View("Index");
            }

            var viewModel = new ChangeMobileViewModel();
            var package = await _profilePackageService.GetUnusedPackagesAsync(profile.Id, PackageType.EditAllData, PackageUsedFlag.EditMobileNumber);

            if (package != null)
            {

                await _cookieHelper.CreateCookie(new UserSessionInfo
                {
                    ProfileId = profile.Id,
                    ProfileOwnerType = profile.Type,
                    ProfileStatus = profile.Status,
                    Mobile = mobile.ToLong()
                });
                viewModel.Mobile = profile.Mobile.NormalizeMobile();
                viewModel.NewMobile = mobile.NormalizeMobile();
                return View("EditMobile", viewModel);
            }

            await _cookieHelper.CreateCookie(new UserSessionInfo
            {
                ProfileId = profile.Id,
                ProfileOwnerType = profile.Type,
                ProfileStatus = profile.Status,
                Mobile = mobile.ToLong()
            });

            viewModel.PrimitivePackageType = PrimitivePackageType.EditMobileNumberWithShahkar;
            return View("Rtp", viewModel);


        }

        [HttpGet]
        [Route("/updateMsisdn")]
        [AuthorizeUserFilterFactory()]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> EditMobile()
        {
            var profile = await _profileService.GetAsync(User.ProfileId);
            if (profile == null || profile?.Status == ProfileStatus.Init)
            {
                Alert("شما اجازه تغییر شماره تلفن همراه خود را ندارید", NotificationType.Error);

                return View("Index");
            }



            var mobile = User.Mobile.GetDefultStringValue();


            var key = ConventionalHelper.CreateOtpKey(mobile, OtpType.EditMobileWithShahkarService);
            var otpCode = await _otpService.GetAsync(key, _webConfiguration.OtpTtl);
            //send otp to user using SMS
            await _messagingService.SendOtpSmsAsync(
                null,
                mobile, null
                ,
                string.Format(_messagingConfiguration.ChangeMobileOtpMessage, otpCode, Environment.NewLine));

            Alert("کد تایید با موفقیت ارسال شد", NotificationType.Success);
            EditMobileViewModel editMobileViewModel = new EditMobileViewModel
            {
                NewMobile = mobile,
                Type = User.Mobile.HasValue ? PrimitivePackageType.EditMobileNumberWithShahkar : PrimitivePackageType.EditMobileNumber
            };
            return View("VerifyMobileNumber", editMobileViewModel);



            //todo: redirect to separate view 


        }



        [Route("/checkMsisdn")]
        [AuthorizeUserFilterFactory()]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> CheckMobile()
        {
            var viewModel = new ChangeMobileViewModel();
            var profile = await _profileService.GetAsync(User.ProfileId);

            var package = await _profilePackageService.GetUnusedPackagesAsync(User.ProfileId, PackageType.EditAllData, PackageUsedFlag.EditMobileNumber);

            if (package != null)
            {

                viewModel.Mobile = profile.Mobile.NormalizeMobile();
                viewModel.NewMobile = User.Mobile.GetDefultStringValue().NormalizeMobile();
                return View("EditMobile", viewModel);
            }
            return View("Index");
        }


        [HttpPost("/clientMsisdn")]
        [AuthorizeUserFilterFactory()]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> UpdateClientMsisdn(string uniqueIdentifier, long id, string uProfile)
        {

            var profile = await _profileService.GetWithNoCacheAsync(id);
            var clientAgent = await _agentService.GetByProfileIdAsync(id);
            var userProfile = await _profileService.GetAsync(User.ProfileId);
            if (profile.UniqueIdentifier != uniqueIdentifier || clientAgent.AgentProfileId != User.ProfileId || (clientAgent.Type != AgentType.Province && clientAgent.Type != AgentType.Conspiracy))
                return Json(new { isSuccess = false, message = "اطلاعات ارسالی  اشتباه است" });
            if (profile.Mobile == userProfile.Mobile)
                return Json(new { isSuccess = false, message = "شما قبلا این شماره تلفن همراه را تغییر داده اید" });

            //var key = HashHelper.GenerateMd5String(uProfile);
            //var oldMobile = await _writeDatabase.StringGetAsync(key);
            //if(oldMobile!=profile.Mobile)
            //    return Json(new { isSuccess = false, message = "اطلاعات ارسالی  اشتباه است" });
            profile.Mobile = userProfile.Mobile;
            await _profileService.UpdateAsync(profile);
            return Json(new { isSuccess = true });
        }

        async Task<bool> HasAccess(string action, string profileId, int periodInSec, int limit)
        {
            return await _rateLimitService.HasAccessAsync($"ProfileController:{action}:{profileId}", periodInSec, limit);
        }
    }
}