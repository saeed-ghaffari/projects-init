﻿using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Utility;
using Cerberus.Web.Enum;
using Cerberus.Web.Filters;
using Cerberus.Web.ViewModel.Factor;
using Cerberus.Web.WebLogic;
using Gatekeeper.AspModules.ActionFilter;
using Microsoft.AspNetCore.Mvc;


namespace Cerberus.Web.Controllers
{

    public class FactorController : BaseController
    {
        private readonly IThirdPartyServiceService _thirdPartyIssure;
        private readonly IProfileService _profileService;
        private readonly IAddressService _addressService;
        public FactorController(IThirdPartyServiceService thirdPartyIssure,
               IProfileService profileService, IAddressService addressService)
        {
            _thirdPartyIssure = thirdPartyIssure;
            _profileService = profileService;
            _addressService = addressService;
        }


        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.Factor)]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> Factor()
        {

            var profile = await _profileService.GetDataForFactorAsync(User.ProfileId);

            var payment = profile?.Payments.FirstOrDefault(x => x.Status == PaymentStatus.Settle && x.ProfilePackageId == null);
            var address = profile?.Addresses.First();

            if (payment == null) return View(nameof(Checkout));


            var amountOfDiscount = payment.Amount == payment.Discount ? 0 : payment.Amount;
            var discountIssuer = await _thirdPartyIssure.GetThirdPartyName(payment.DiscountIssuer);
            if (amountOfDiscount == 0)
            {
                if (discountIssuer != null) Alert("به دلیل تخفیف کامل از " + discountIssuer + "، امکان نمایش فاکتور مقدور نمی باشد.", NotificationType.Warning);

                Alert("به دلیل تخفیف کامل، امکان نمایش فاکتور مقدور نمی باشد.", NotificationType.Warning);
                return RedirectToAction("Checkout");
            }



            if (payment.FactorId == null || string.IsNullOrWhiteSpace(payment.SerialNumber))
            {
                Alert(" شماره فاکتور شما در حال صدور است لطفا کمی صبر نمایید ", NotificationType.Info);
                return RedirectToAction("Checkout");
            }


            {
                var factorVm = new FactorViewModel
                {
                    Address = address?.RemnantAddress,
                    Alley = address?.Alley,
                    Plaque = address?.Plaque,
                    Amount = amountOfDiscount,
                    Date = payment?.ModifiedDate.ToPersianDate(),
                    Discount = payment.Discount,
                    UniqueIdentifier = profile.UniqueIdentifier,
                    InvoiceNumber = payment.SerialNumber,
                    SaleReferenceId = payment?.SaleReferenceId,
                    Name = profile.PrivatePerson?.FirstName ?? profile.LegalPerson?.CompanyName,
                    LastName = profile.PrivatePerson?.LastName ?? string.Empty,
                    City = profile.Addresses.FirstOrDefault()?.City?.Name,
                    Mobile = profile.Mobile.NormalizeMobile(),
                    PostalCode = address?.PostalCode,
                    Province = profile.Addresses.FirstOrDefault()?.Province?.Name,
                };
                if (payment.Discount != 0)
                {
                    factorVm.ThirdPartyTitle = await _thirdPartyIssure?.GetThirdPartyName(payment?.DiscountIssuer);
                }

                return View(factorVm);
            }
        }

        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.Factor)]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> Checkout()
        {
            var address = await _addressService.GetByProfileIdAsync(User.ProfileId);
            if (address.IsConfirmPostalCode == false)
                return View(new PostalCodeViewModel
                {
                    NotConfirm = true
                });
            return View();


        }


        [HttpPost]
        [AuthorizeUserFilterFactory]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> EditProfileFactor(PackageType packageType, long profilePackageId)
        {

            var profile = await _profileService.GetDataForFactorAsync(User.ProfileId);

            var payment = profile?.Payments.FirstOrDefault(x => x.Status == PaymentStatus.Settle && x.ProfilePackageId == profilePackageId && x.ProfileId == User.ProfileId);
            if (payment == null) return View("FactorError");



            if (payment.FactorId == null)
            {
                Alert("شماره فاکتور شما در حال صدور است لطفا کمی صبر نمایید ", NotificationType.Warning);
                return View("FactorError");
            }
            var address = profile.Addresses.FirstOrDefault();
            var privatePerson = profile.PrivatePerson != null ? profile.PrivatePerson?.FirstName : "فاقد نام";

            var factorVm = new FactorViewModel
            {
                Address = address?.RemnantAddress,
                Alley = address?.Alley,
                Plaque = address?.Plaque,
                Amount = payment.Amount,
                Date = payment?.ModifiedDate.ToPersianDate(),
                Discount = 0,
                UniqueIdentifier = profile.UniqueIdentifier,
                InvoiceNumber = payment.SerialNumber,
                SaleReferenceId = payment?.SaleReferenceId,
                Name = privatePerson ?? profile.LegalPerson?.CompanyName,
                LastName = profile.PrivatePerson?.LastName ?? string.Empty,
                City = profile.Addresses.FirstOrDefault()?.City?.Name,
                Mobile = profile.Mobile.NormalizeMobile(),
                PostalCode = address?.PostalCode,
                Province = profile.Addresses.FirstOrDefault()?.Province?.Name,

            };
            if (profilePackageId > 0)
            {
                factorVm.ProfilePackage = new ProfilePackage
                {
                    Id = profilePackageId,
                    Type = packageType
                };
            }

            return View("Factor", factorVm);
        }


    }
}