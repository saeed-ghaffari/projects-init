﻿using System;
using AutoMapper;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Web.Enum;
using Cerberus.Web.Extensions;
using Cerberus.Web.Filters;
using Cerberus.Web.ViewModel.FinancialInfo;
using Cerberus.Web.WebLogic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Utility;
using Gatekeeper.AspModules.ActionFilter;

namespace Cerberus.Web.Controllers
{
    [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.FinancialInfo)]
    public class FinancialInfoController : BaseController
    {
        private readonly IFinancialInfoService _financialInfoService;
        private readonly IMapper _mapper;
        private readonly IBrokerService _brokerService;

        public FinancialInfoController(IFinancialInfoService financialInfoService, IMapper mapper, IBrokerService brokerService)
        {
            _financialInfoService = financialInfoService;
            _mapper = mapper;
            _brokerService = brokerService;
        }

        [HttpGet]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> Index()
        {
            var financialInfoModel = await _financialInfoService.GetByProfileIdAsync(User.ProfileId, true);

            var createFinancialInfoViewModel = _mapper.Map<FinancialInfo, CreateFinancialInfoViewModel>(financialInfoModel) ?? new CreateFinancialInfoViewModel();
            createFinancialInfoViewModel.ProfileOwnerType = User.ProfileOwnerType;
            ViewBag.Brokers = (await _brokerService.GetListAsync()).OrderBy(x => x.Title).Select(x => new SelectListItem(x.Title, x.Id.ToString()));

            if (financialInfoModel?.FinancialBrokers != null)
                createFinancialInfoViewModel.BrokerIds = financialInfoModel.FinancialBrokers.Select(x => x.BrokerId);

            return View(createFinancialInfoViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> CreateAsync(CreateFinancialInfoViewModel financialInfoViewModel)
        {

            if (!ModelState.IsValid)
            {
                Alert("مقادیر ورودی نامعتبر می باشد", NotificationType.Error);
                return RedirectToAction(nameof(Index));
            }

            if ((User.ProfileOwnerType == ProfileOwnerType.ForeignPrivatePerson || User.ProfileOwnerType == ProfileOwnerType.IranianPrivatePerson) && financialInfoViewModel.TradingKnowledgeLevel == null)
            {
                Alert("میزان آشنایی شما با مفاهیم مالی و سرمایه گذاری اجباری است.", NotificationType.Error);
                return RedirectToAction(nameof(Index));
            }
            if ((User.ProfileOwnerType == ProfileOwnerType.ForeignLegalPerson || User.ProfileOwnerType == ProfileOwnerType.IranianLegalPerson) && financialInfoViewModel.CheckRate == 1)
            {
                if (financialInfoViewModel.Rate == null ||
                    string.IsNullOrWhiteSpace(financialInfoViewModel.ReferenceRateCompany) ||
                    string.IsNullOrWhiteSpace(financialInfoViewModel.RateDate))
                {
                    Alert(
                        "در صورتی که مشتری از طرف مرجعی مورد رتبه بندی قرار گرفته باشد، نام مرجع رتبه بندی، تاریخ رتبه بندی و رتبه اخذ شده اجباری می باشد",
                        NotificationType.Error);
                    return RedirectToAction(nameof(Index));
                }
            }

            var brokers = new List<Broker>();

            if (financialInfoViewModel.BrokerIds != null && financialInfoViewModel.BrokerIds.Count() != 0)
            {
                foreach (var brokerId in financialInfoViewModel.BrokerIds)
                {
                    var broker = await _brokerService.GetAsync(brokerId);
                    if (broker != null)
                        brokers.Add(broker);
                    else
                    {
                        ModelState.AddModelError(nameof(financialInfoViewModel.BrokerIds), "کارگزار انتخاب شده در سجام تعریف نشده است");
                        return View("Index", financialInfoViewModel);
                    }
                }
            }

            var financialInfoModel = _mapper.Map<FinancialInfo>(financialInfoViewModel);

            var dbObject = await _financialInfoService.GetByProfileIdAsync(User.ProfileId);

            financialInfoModel.ProfileId = User.ProfileId;

            if (dbObject != null)
            {
                financialInfoModel.Id = dbObject.Id;
                financialInfoModel.CreationDate = dbObject.CreationDate;
                await _financialInfoService.UpdateAsync(financialInfoModel, brokers);
            }
            else
            {
                await _financialInfoService.CreateAsync(financialInfoModel, brokers);
            }


            return RedirectToNextStep(CurrentStep.FinancialInfo);
        }
    }
}