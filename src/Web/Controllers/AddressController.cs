﻿using AutoMapper;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Web.Enum;
using Cerberus.Web.Filters;
using Cerberus.Web.ViewModel.Address;
using Cerberus.Web.WebLogic;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Services.Membership;
using Cerberus.Utility;
using Gatekeeper.AspModules.ActionFilter;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Cerberus.Web.Controllers
{
    [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.Address)]
 
    public class AddressController : BaseController
    {
        private readonly IProfileService _profileService;
        private readonly IAddressService _addressService;
        private readonly ICountryService _countryService;
        private readonly IProvinceService _provinceService;
        private readonly ICityService _cityService;
        private readonly IAddressSectionService _addressSectionService;
        private readonly IMapper _mapper;
        private readonly IMembershipService _membershipService;
        private readonly IPostInquiryService _postInquiryService;
        private readonly IPrivatePersonService _privatePersonService;

        public AddressController(IProfileService profileService, IAddressService addressService,
            ICountryService countryService, IProvinceService provinceService, ICityService cityService,
            IAddressSectionService addressSectionService, IMapper mapper, IMembershipService membershipService,
            IPostInquiryService postInquiryService, IPrivatePersonService privatePersonService)
        {
            _profileService = profileService;
            _addressService = addressService;
            _countryService = countryService;
            _provinceService = provinceService;
            _cityService = cityService;
            _addressSectionService = addressSectionService;
            _mapper = mapper;
            _membershipService = membershipService;
            _postInquiryService = postInquiryService;
            _privatePersonService = privatePersonService;
        }

        [HttpGet]
        [Route("address")]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> Index()
        {
            var counties = await _countryService.GetListAsync();
            ViewBag.Countries = counties.Select(x => new SelectListItem(x.Name, x.Id.ToString()));
            ViewBag.legalCountries = counties.Select(x => new SelectListItem(x.Name, x.Id.ToString()));
            // get user address from  user table
            var address = await _addressService.GetListByProfileIdAsync(User.ProfileId);
            var profile = await _profileService.GetAsync(User.ProfileId);


            if (address.Count > 0)
            {
                var firstAddress = address.First();

                if (firstAddress.CountryId > 0)
                    ViewBag.Provinces =
                        (await _provinceService.GetListAsync(firstAddress.CountryId.Value)).Select(x => new SelectListItem(x.Name, x.Id.ToString()));

                if (firstAddress.ProvinceId > 0)
                    ViewBag.Cities =
                        (await _cityService.GetListAsync(firstAddress.ProvinceId.Value)).Select(x =>
                            new SelectListItem(x.Name, x.Id.ToString()));

                if (firstAddress.CityId > 0)
                    ViewBag.Sections =
                        (await _addressSectionService.GetListAsync(firstAddress.CityId.Value)).Select(x =>
                            new SelectListItem(x.Name, x.Id.ToString()));

                //show original address in view 
                var createAddressViewModel = _mapper.Map<CreateAddressViewModel>(address.First());

                if (address.Count() > 1 && User.ProfileOwnerType == ProfileOwnerType.IranianLegalPerson)
                {
                    var legalAddress = address.Skip(1).First();
                    if (legalAddress.CountryId > 0)
                        ViewBag.legalProvinces =
                            (await _provinceService.GetListAsync(legalAddress.CountryId.Value)).Select(x =>
                                new SelectListItem(x.Name, x.Id.ToString()));


                    if (legalAddress.ProvinceId > 0)
                        ViewBag.legalCities =
                            (await _cityService.GetListAsync(legalAddress.ProvinceId.Value)).Select(x =>
                                new SelectListItem(x.Name, x.Id.ToString()));

                    if (legalAddress.CityId > 0)
                    {
                        ViewBag.legalSections =
                            (await _addressSectionService.GetListAsync(legalAddress.CityId.Value)).Select(x =>
                                new SelectListItem(x.Name, x.Id.ToString()));
                    }


                    createAddressViewModel.LegalRemnantAddress = legalAddress.RemnantAddress;
                    createAddressViewModel.LegalPostalCode = legalAddress.PostalCode;
                    createAddressViewModel.LegalTel = legalAddress.Tel;
                    createAddressViewModel.LegalCityPrefix = legalAddress.CityPrefix;
                    createAddressViewModel.LegalCountryId = legalAddress.CountryId.ToString();
                    createAddressViewModel.LegalCityId = legalAddress.CityId.ToString();
                    createAddressViewModel.LegalProvinceId = legalAddress.ProvinceId.ToString();
                    createAddressViewModel.LegalSectionId = legalAddress.SectionId.ToString();
                    createAddressViewModel.LegalRemnantAddress = legalAddress.RemnantAddress;
                    createAddressViewModel.LegalAlley = legalAddress.Alley;
                    createAddressViewModel.LegalPlaque = legalAddress.Plaque;

                }


                return View(createAddressViewModel);
            }


            if (User.ServiceId.HasValue && User.ServiceId.Value != 0)
            {
                var checkUser = await _membershipService.GetPrivatePersonAsync(profile.UniqueIdentifier,
                    profile.Mobile.ToString(), User.ServiceId.Value);
                if (checkUser != null)
                {
                    var personTmp =
                        await _membershipService.GetAddressAsync(profile.UniqueIdentifier, User.ServiceId.Value);
                    if (personTmp != null)
                    {
                        if (personTmp.CountryId.HasValue)
                            ViewBag.Provinces =
                                (await _provinceService.GetListAsync(personTmp.CountryId.Value)).Select(x =>
                                    new SelectListItem(x.Name, x.Id.ToString()));

                        if (personTmp.ProvinceId.HasValue)
                            ViewBag.Cities =
                                (await _cityService.GetListAsync(personTmp.ProvinceId.Value)).Select(x =>
                                    new SelectListItem(x.Name, x.Id.ToString()));

                        if (personTmp.CityId.HasValue)
                            ViewBag.Sections =
                                (await _addressSectionService.GetListAsync(personTmp.CityId.Value)).Select(x =>
                                    new SelectListItem(x.Name, x.Id.ToString()));

                        var createAddressViewModel = _mapper.Map<CreateAddressViewModel>(personTmp);


                        return View(createAddressViewModel);

                    }


                }


            }

            return View();
        }




        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Address")]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> CreateAsync(CreateAddressViewModel model)
        {
            if (!ModelState.IsValid)
            {

                ViewBag.Countries = (await _countryService.GetListAsync())
                    .Select(x => new SelectListItem(x.Name, x.Id.ToString()));

                if (long.TryParse(model.CountryId, out var countryId))
                    ViewBag.Provinces = (await _provinceService.GetListAsync(countryId))
                        .Select(x => new SelectListItem(x.Name, x.Id.ToString()));

                if (long.TryParse(model.ProvinceId, out var provinceId))
                    ViewBag.Cities = (await _cityService.GetListAsync(provinceId))
                    .Select(x => new SelectListItem(x.Name, x.Id.ToString()));

                if (long.TryParse(model.CityId, out var cityId))
                    ViewBag.Sections = (await _addressSectionService.GetListAsync(cityId))
                    .Select(x => new SelectListItem(x.Name, x.Id.ToString()));


                return View("Index", model);
            }

            var province = await _provinceService.GetListAsync(model.CountryId.ToLong());

            var provinces = province as Province[] ?? province.ToArray();
            if (provinces.All(x => x.Id != model.ProvinceId.ToLong()))
            {
                Alert("اطلاعات ارسالی نامعتبر می باشد", NotificationType.Error);
                return RedirectToAction(nameof(Index));
            }

            var city = await _cityService.GetListAsync(model.ProvinceId.ToLong());
            if (city.All(x => x.Id != model.CityId.ToLong()))
            {
                Alert("اطلاعات ارسالی نامعتبر می باشد", NotificationType.Error);
                return RedirectToAction(nameof(Index));
            }

            var section = await _addressSectionService.GetListAsync(model.CityId.ToLong());
            if (section.All(x => x.Id != model.SectionId.ToLong()))
            {
                Alert("اطلاعات ارسالی نامعتبر می باشد", NotificationType.Error);
                return RedirectToAction(nameof(Index));
            }

            var mappedObject = _mapper.Map<CreateAddressViewModel, Address>(model);

            if (User.ProfileOwnerType == ProfileOwnerType.IranianPrivatePerson)
            {
                var profile = await _profileService.GetRelatedDataWithEntity(User.ProfileId, null, EntityType.PrivatePerson);
                if (profile.PrivatePerson == null)
                {
                    Alert("لطفا اطلاعات هویتی خود را وارد نمایبد", NotificationType.Warning);
                    return RedirectToAction("Index", "BaseInfo");
                }

                mappedObject = await CheckPostalCodeAsync(mappedObject, profile.UniqueIdentifier,
                    profile.PrivatePerson.BirthDate.ToPersianDate());
            }


            mappedObject.ProfileId = User.ProfileId;

            var dbObject = await _addressService.GetListByProfileIdAsync(User.ProfileId);
            var firstObject = dbObject.FirstOrDefault();

            if (firstObject != null)
            {

                mappedObject.Id = dbObject.First().Id;
                await _addressService.UpdateAsync(mappedObject);
            }
            else
            {
                mappedObject.ProfileId = User.ProfileId;
                await _addressService.CreateAsync(mappedObject);
            }


            if (User.ProfileOwnerType == ProfileOwnerType.IranianLegalPerson)
            {

                if (!string.IsNullOrWhiteSpace(model.LegalCountryId))
                {
                    var legalProvince = await _provinceService.GetListAsync(model.LegalCountryId.ToLong());
                    if (legalProvince.All(x => x.Id != model.LegalProvinceId.ToLong()))
                    {
                        Alert("اطلاعات ارسالی نامعتبر می باشد", NotificationType.Error);
                        return RedirectToAction(nameof(Index));
                    }
                }

                if (!string.IsNullOrWhiteSpace(model.LegalProvinceId))
                {
                    var legalCity = await _cityService.GetListAsync(model.LegalProvinceId.ToLong());
                    if (legalCity.All(x => x.Id != model.LegalCityId.ToLong()))
                    {
                        Alert("اطلاعات ارسالی نامعتبر می باشد", NotificationType.Error);
                        return RedirectToAction(nameof(Index));
                    }
                }

                if (!string.IsNullOrWhiteSpace(model.LegalCityId))
                {
                    var legalSection = await _addressSectionService.GetListAsync(model.LegalCityId.ToLong());
                    if (legalSection.All(x => x.Id != model.LegalSectionId.ToLong()))
                    {
                        Alert("اطلاعات ارسالی نامعتبر می باشد", NotificationType.Error);
                        return RedirectToAction(nameof(Index));
                    }
                }

                Address address = new Address
                {
                    CountryId = model.LegalCountryId?.ToLong(),
                    ProvinceId = model.LegalProvinceId?.ToLong(),
                    CityId = model.LegalCityId?.ToLong(),
                    SectionId = model.LegalSectionId?.ToLong(),
                    RemnantAddress = model.LegalRemnantAddress,
                    Alley = model.LegalAlley,
                    Plaque = model.LegalPlaque,
                    PostalCode = model.LegalPostalCode,
                    CityPrefix = model.LegalCityPrefix,
                    Tel = model.LegalTel,
                    ProfileId = User.ProfileId
                };
                if (dbObject.Count > 1 && dbObject.Skip(1).First() != null)
                {
                    address.Id = dbObject.Skip(1).First().Id;
                    await _addressService.UpdateAsync(address);
                }
                else
                {
                    await _addressService.CreateAsync(address);
                }
            }

            return RedirectToNextStep(CurrentStep.AddressInfo);
        }

        private async Task<Address> CheckPostalCodeAsync(Address model, string uniqueIdentifier, string birthDate)
        {
            var postService = await _postInquiryService.GetAsync(uniqueIdentifier, birthDate);

            if (!string.IsNullOrWhiteSpace(postService?.PostalCode))
                model.IsConfirmPostalCode = model.PostalCode == postService.PostalCode;
            else
                model.IsConfirmPostalCode = null;

            return model;
        }

    }
}