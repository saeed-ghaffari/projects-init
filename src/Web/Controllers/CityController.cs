﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Services;
using Cerberus.Web.Filters;
using Cerberus.Web.WebLogic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Cerberus.Web.Controllers
{
   
    public class CityController : BaseController
    {
        private readonly ICityService _cityService;

        public CityController(ICityService cityService)
        {
            _cityService = cityService;
        }

        [Route("api/cities/{provinceId}")]
        [HttpGet]
        public async Task<IEnumerable<SelectListItem>> GetSelectListAsync(long provinceId)
        {
            return (await _cityService.GetListAsync(provinceId)).Select(x => new SelectListItem(x.Name, x.Id.ToString()));
        }
    }
}