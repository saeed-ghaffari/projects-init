﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Web.ViewModel.AuthenticationOffices;
using Cerberus.Web.WebLogic;
using Gatekeeper.AspModules.ActionFilter;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Cerberus.Web.Controllers
{
    public class AuthenticationOfficesController : BaseController
    {

        private readonly IAuthenticationOfficesService _authenticationOfficesService;
        private readonly IMapper _mapper;
        private readonly IProvinceService _provinceService;
        private readonly IBankService _bankService;

        public AuthenticationOfficesController(IAuthenticationOfficesService authenticationOfficesService,
            IMapper mapper,
           IProvinceService provinceService, IBankService bankService)
        {
            _authenticationOfficesService = authenticationOfficesService;
            _mapper = mapper;
            _provinceService = provinceService;
            _bankService = bankService;
        }


        [RateLimitFilterFactory(Order = 1, Limit = 500, PeriodInSec = 1 * 60 * 5)]
        public async Task<IActionResult> Index()
        {

            ViewBag.Provinces = (await _provinceService.GetListAsync()).Where(x => x.CountryId == 1).OrderBy(x => x.Name).Select(x => new SelectListItem(x.Name, x.Id.ToString()));
            ViewBag.Banks = (await _bankService.GetListForAuthenticationOfficeAsync()).OrderBy(x => x.Name).Select(x => new SelectListItem(x.Name, x.Id.ToString()));
            return View();
        }


        [RateLimitFilterFactory(Order = 1, Limit = 500, PeriodInSec = 1 * 60 * 5)]
        public async Task<PartialViewResult> Search(AuthenticationOfficesViewModel model)
        {
            if (!ModelState.IsValid)
               return PartialView(new List<AuthenticationOfficesViewModel>());

            var authenticationOffice = _mapper.Map<AuthenticationOffices>(model);
            var offices = await _authenticationOfficesService.GetListAsync(authenticationOffice, model.PageNumber * 20, 20);
            ViewBag.paging = DivideRoundingUp(offices.recordCount, 20);
            ViewBag.page = model.PageNumber;
            ViewBag.recordCount = offices.recordCount;
            var viewModel = _mapper.Map<List<AuthenticationOffices>, List<AuthenticationOfficesViewModel>>(offices.authenticationOfficeses);
            return PartialView(viewModel);
        }

        [RateLimitFilterFactory(Order = 1, Limit = 500, PeriodInSec = 1 * 60 * 5)]
        public async Task<PartialViewResult> NotInPresenceSearch(AuthenticationOfficesViewModel model)
        {
            if (!ModelState.IsValid)
                return PartialView(new List<AuthenticationOfficesViewModel>());

            var authenticationOffice = _mapper.Map<AuthenticationOffices>(model);
            var offices = await _authenticationOfficesService.GetListAsync(authenticationOffice, 0, 20);
            var viewModel = _mapper.Map<List<AuthenticationOffices>, List<AuthenticationOfficesViewModel>>(offices.authenticationOfficeses);
            return PartialView(viewModel);
        }
        public long DivideRoundingUp(long x, long y) => Math.DivRem(x, y, out var remainder);
    }
}