﻿using AutoMapper;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Web.Enum;
using Cerberus.Web.Extensions;
using Cerberus.Web.Filters;
using Cerberus.Web.ViewModel.LegalPerson;
using Cerberus.Web.ViewModel.PrivatePerson;
using Cerberus.Web.WebLogic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain;
using Cerberus.Domain.Extensions;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Services.Membership;
using Cerberus.Domain.Model.Membership;
using Cerberus.Utility;
using Cerberus.Web.MappingProfile;
using Gatekeeper.AspModules.ActionFilter;
using Profile = Cerberus.Domain.Model.Profile;
using ProfileExtensions = Cerberus.Domain.Extensions.ProfileExtensions;

namespace Cerberus.Web.Controllers
{

    public class BaseInfoController : BaseController
    {
        private readonly IProfileService _profileService;
        private readonly IPrivatePersonService _privatePersonService;
        private readonly ILegalPersonService _legalPersonService;
        private readonly ICountryService _countryService;
        private readonly IMapper _mapper;
        private readonly IMembershipService _membershipService;
        private readonly IProfilePackageService _profilePackageService;
        private readonly IOfflineTaskService _offlineTaskService;
        private readonly IProfileHistoryService _profileHistoryService;
        public BaseInfoController(IProfileService profileService, IPrivatePersonService privatePersonService, ILegalPersonService legalPersonService, ICountryService countryService, IMapper mapper, IMembershipService membershipService, IProfilePackageService profilePackageService, IOfflineTaskService offlineTaskService, IProfileHistoryService profileHistoryService)
        {
            _profileService = profileService;
            _privatePersonService = privatePersonService;
            _legalPersonService = legalPersonService;
            _countryService = countryService;
            _mapper = mapper;
            _membershipService = membershipService;
            _profilePackageService = profilePackageService;
            _offlineTaskService = offlineTaskService;
            _profileHistoryService = profileHistoryService;
        }

        [HttpGet]
        [Route("baseInfo")]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.BaseInfo)]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> Index()
        {
            //show uniqueIdentifier as readonly field 
            var profile = await _profileService.GetAsync(User.ProfileId);
            if (User.ProfileOwnerType == ProfileOwnerType.IranianPrivatePerson)
            {
                var privatePersonModel = await _privatePersonService.GetByProfileIdAsync(User.ProfileId);
                if (privatePersonModel != null)
                {
                    var privatePersonNativeViewModel = _mapper.Map<CreatePrivatePersonNativeViewModel>(privatePersonModel);
                    privatePersonNativeViewModel.UniqueIdentifier = profile.UniqueIdentifier;
                    return View("Native", privatePersonNativeViewModel);
                }
                else
                {
                    if (User.ServiceId.HasValue && User.ServiceId.Value > 0)
                    {
                        var personTmp = await _membershipService.GetPrivatePersonAsync(profile.UniqueIdentifier, profile.Mobile.ToString(), User.ServiceId.Value);
                        if (personTmp != null)
                        {
                            var privatePersonNativeViewModel = _mapper.Map<CreatePrivatePersonNativeViewModel>(personTmp);
                            privatePersonNativeViewModel.UniqueIdentifier = profile.UniqueIdentifier;
                            return View("Native", privatePersonNativeViewModel);
                        }

                        return View("Native", new CreatePrivatePersonNativeViewModel()
                        {
                            UniqueIdentifier = profile.UniqueIdentifier
                        });

                    }
                    return View("Native", new CreatePrivatePersonNativeViewModel()
                    {
                        UniqueIdentifier = profile.UniqueIdentifier
                    });
                }
            }
            if (User.ProfileOwnerType == ProfileOwnerType.IranianLegalPerson)
            {
                var legalPersonModel = await _legalPersonService.GetByProfileIdAsync(User.ProfileId);
                ViewBag.Countries = new SelectList(await _countryService.GetListAsync(), "Id", "Name");
                if (legalPersonModel != null)
                {
                    var createLegalPersonViewModel = _mapper.Map<CreateLegalPersonViewModel>(legalPersonModel);
                    createLegalPersonViewModel.UniqueIdentifier = profile.UniqueIdentifier;
                    return View("Legal", createLegalPersonViewModel);
                }
                return View("Legal", new CreateLegalPersonViewModel()
                {
                    UniqueIdentifier = profile.UniqueIdentifier
                });

            }
            if (User.ProfileOwnerType == ProfileOwnerType.ForeignPrivatePerson)
            {
                var privatePersonModel = await _privatePersonService.GetByProfileIdAsync(User.ProfileId);

                CreatePrivatePersonForeignViewModel createPrivatePersonForeignViewModel = _mapper.Map<CreatePrivatePersonForeignViewModel>(privatePersonModel);

                ViewBag.Countries = new SelectList(await _countryService.GetListAsync(), "Id", "Name");

                return View("Foreign", createPrivatePersonForeignViewModel);
            }


            throw new NotImplementedException();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("baseInfo/privatePerson/foreign")]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.BaseInfo)]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> CreateForeignAsync(CreatePrivatePersonForeignViewModel model)
        {
            if (User.ProfileOwnerType != ProfileOwnerType.ForeignPrivatePerson)
            {
                Alert("اطلاعات شما معتبر نمی باشد", NotificationType.Error);
                return RedirectToAction("SignOut", "Session");
            }
            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                return View("Foreign", model);
            }

            var mappedObject = _mapper.Map<CreatePrivatePersonForeignViewModel, PrivatePerson>(model);

            var dbObject = await _privatePersonService.GetByProfileIdAsync(User.ProfileId);

            if (dbObject != null)
            {
                mappedObject.Id = dbObject.Id;
                mappedObject.ImageId = dbObject.ImageId;
                mappedObject.IsConfirmed = null;
                mappedObject.ProfileId = User.ProfileId;
                mappedObject.CreationDate = DateTime.Now;

                await _privatePersonService.UpdateAsync(mappedObject);
            }
            else
            {
                mappedObject.ProfileId = User.ProfileId;

                await _privatePersonService.CreateAsync(mappedObject);
            }

            return RedirectToAction("Index", "Address");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("baseInfo/privatePerson/iranian")]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.BaseInfo)]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> CreateNativeAsync(CreatePrivatePersonNativeViewModel model)
        {
       
            if (User.ProfileOwnerType != ProfileOwnerType.IranianPrivatePerson)
            {
                Alert("اطلاعات شما معتبر نمی باشد", NotificationType.Error);
                return RedirectToAction("SignOut", "Session");
            }

            var profile = await _profileService.GetAsync(User.ProfileId);
            //show uniqueIdentifier as readonly field 
            model.UniqueIdentifier = profile.UniqueIdentifier;
            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);

                return View("Native", model);
            }



            var mappedObject = _mapper.Map<CreatePrivatePersonNativeViewModel, PrivatePerson>(model);

            var oldPerson = await _privatePersonService.GetByProfileIdAsync(User.ProfileId);
            var currentStep = oldPerson?.IsConfirmed == false ? CurrentStep.FinancialInfo : CurrentStep.BaseInfo;

            if (oldPerson != null)
            {
                if (oldPerson.Locked)
                {
                    Alert("اطلاعات هویتی شما قابل ویرایش نمی باشد", NotificationType.Error);
                    return View("Native", model);
                }

                mappedObject.Id = oldPerson.Id;
                mappedObject.ImageId = oldPerson.ImageId;
                mappedObject.IsConfirmed = null;
                mappedObject.ProfileId = User.ProfileId;
                mappedObject.CreationDate = DateTime.Now;

                await _privatePersonService.UpdateAsync(mappedObject);

            }
            else
            {
                mappedObject.ProfileId = User.ProfileId;

                await _privatePersonService.CreateAsync(mappedObject);

            }

            return RedirectToNextStep(currentStep);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("baseInfo/legalPerson/iranian")]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.BaseInfo)]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> CreateLegalAsync(CreateLegalPersonViewModel model)
        {
            if (User.ProfileOwnerType != ProfileOwnerType.IranianLegalPerson)
            {
                Alert("اطلاعات شما معتبر نمی باشد", NotificationType.Error);
                return RedirectToAction("SignOut", "Session");
            }
            //show uniqueIdentifier as readonly field 
            var profile = await _profileService.GetAsync(User.ProfileId);
            ViewBag.Countries = new SelectList(await _countryService.GetListAsync(), "Id", "Name");
            model.UniqueIdentifier = profile.UniqueIdentifier;


            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                return View("Legal", model);
            }
            var mappedObject = _mapper.Map<CreateLegalPersonViewModel, LegalPerson>(model);

            var oldLegalPerson = await _legalPersonService.GetByProfileIdAsync(User.ProfileId);
            var currentStep = oldLegalPerson?.IsConfirmed == false ? CurrentStep.FinancialInfo : CurrentStep.BaseInfo;
            if (oldLegalPerson != null)
            {
                if (oldLegalPerson.Locked)
                {
                    Alert("اطلاعات هویتی شرکت/موسسه شما قابل ویرایش نمی باشد.", NotificationType.Error);
                    return RedirectToAction(nameof(Index));
                }
                mappedObject.Id = oldLegalPerson.Id;
                mappedObject.IsConfirmed = null;
                mappedObject.ProfileId = User.ProfileId;
                mappedObject.CreationDate = DateTime.Now;

                await _legalPersonService.UpdateAsync(mappedObject);
            }
            else
            {
                mappedObject.ProfileId = User.ProfileId;

                await _legalPersonService.CreateAsync(mappedObject);

            }

            return RedirectToNextStep(currentStep);
        }


        public IActionResult PlaceOfIssue()
        {
            return PartialView();
        }
        public IActionResult PlaceOfBirth()
        {
            return PartialView();
        }

        [AuthorizeUserFilterFactory]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<JsonResult> UpdateIssueAsync(PlaceOfIssueViewModel vm)
        {
            if (!ModelState.IsValid) return Json(ModelState.ToErrorMessage());
            var privatePerson = await _privatePersonService.GetByProfileIdAsync(User.ProfileId);
            if (privatePerson == null) return Json("false");
            privatePerson.PlaceOfIssue = vm.PlaceOfIssue;
            await _privatePersonService.UpdateAsync(privatePerson);
            return Json("true");
        }

        [AuthorizeUserFilterFactory]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<JsonResult> UpdatePlaceOfBirthAsync(PlaceOfBirthViewModel vm)
        {
            if (!ModelState.IsValid) return Json(ModelState.ToErrorMessage());
            var privatePerson = await _privatePersonService.GetByProfileIdAsync(User.ProfileId);
            if (privatePerson == null) return Json("false");
            privatePerson.PlaceOfBirth = vm.PlaceOfBirth;
            await _privatePersonService.UpdateAsync(privatePerson);
            return Json("true");
        }



        [Route("baseInfo/privatePerson/edit")]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.EditPrivatePerson)]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> ChangePrivatePersonAsync()
        {
            ViewBag.package = "access";

            Alert("در صورت تغییر اطلاعات هویتی در سازمان ثبت احوال، توصیه میگردد قبل از بروز رسانی اطلاعات در سجام، تغییرات اطلاعات هویتی خود را  به بانک حساب  ثبت شده خود در سجام نیز اطلاع داده تا اطلاعات هویتی شما در بانک نیز بروز گردد. در غیر این صورت ویرایش حساب بانکی بعد از 30 روز مستلزم هزینه خواهد بود", NotificationType.Info);

            var profile = await _profileService.GetAsync(User.ProfileId);
            if (!profile.IsEditPrivatePersonSemiSejami() && !profile.IsEditBankingAccountSemiSejami() && !profile.IsEditPrivatePersonAndDeleteAgentEighteen() && !profile.IsEditBankingAccountAndDeleteAgentEighteen())
            {
                Alert("شما اجازه بروزرسانی اطلاعات هویتی خود را ندارید", NotificationType.Warning);
                return RedirectToAction("Index", "Dashboard");
            }

            var package = await _profilePackageService.GetUnusedPackagesAsync(User.ProfileId, PackageType.EditAllData, PackageUsedFlag.EditPrivatePersonInfo);
            if (package == null)
            {
                ViewBag.package = "payment";
            }

            var privatePerson = await _privatePersonService.GetByProfileIdAsync(User.ProfileId);
            if (privatePerson == null)  return RedirectToAction("Index", "Dashboard");

            var privatePersonNativeViewModel = _mapper.Map<CreatePrivatePersonNativeViewModel>(privatePerson);

            return View("EditPrivatePerson", privatePersonNativeViewModel);

        }



        [Route("baseInfo/privatePerson/changeData")]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.EditPrivatePerson)]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> UpdatePrivatePerson()
        {
            var package = await _profilePackageService.GetUnusedPackagesAsync(User.ProfileId, PackageType.EditAllData, PackageUsedFlag.EditPrivatePersonInfo);
            if (package == null)
                return RedirectToAction(nameof(ChangePrivatePersonAsync));



            var privatePerson = await _privatePersonService.GetByProfileIdAsync(User.ProfileId);

            var profile = await _profileService.GetAsync(User.ProfileId);
            if (!profile.IsEditPrivatePersonSemiSejami() && !profile.IsEditBankingAccountSemiSejami() && !profile.IsEditPrivatePersonAndDeleteAgentEighteen() && !profile.IsEditBankingAccountAndDeleteAgentEighteen())
            {
                Alert("شما نمی توانید اطلاعات هویتی خود را بروز رسانی کنید", NotificationType.Warning);
                return RedirectToAction("Index", "Dashboard");
            }
            if (profile.Status == ProfileStatus.Sejami && privatePerson.IsConfirmed == null)
            {
                Alert("تا زمان مشخص نشدن نتیجه استعلام ، شما نمی توانید اطلاعات  هویتی خود را بروز رسانی کنید.", NotificationType.Warning);
                return RedirectToAction(nameof(ChangePrivatePersonAsync));
            }

            profile.Status = ProfileStatus.SemiSejami;
            profile.StatusReasonType = StatusReasonType.EditPrivatePerson;
            await _profileService.UpdateStatusAndReasonAsync(profile);
            await _profileHistoryService.CreateAsync(new ProfileHistory
            {
                ReferenceId = ProfileHistoryReferenceType.ChangePrivatePerson.ToString(),
                ProfileId = User.ProfileId,
            }, ProfileStatus.SemiSejami);

            privatePerson.IsConfirmed = null;
            await _privatePersonService.UpdateAsync(privatePerson);


            await _offlineTaskService.ScheduleEditAfterSejamiTasksAsync(User.ProfileId);
            return View("FinalEdit", package);
        }
    }
}