﻿using AutoMapper;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Utility;
using Cerberus.Web.Enum;
using Cerberus.Web.Extensions;
using Cerberus.Web.Filters;
using Cerberus.Web.ViewModel.LegalPersonShareholder;
using Cerberus.Web.ViewModel.LegalPersonStakeholder;
using Cerberus.Web.WebLogic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain;
using Cerberus.Service;
using Gatekeeper.AspModules.ActionFilter;
using Microsoft.EntityFrameworkCore;
using Gatekeeper.AspModules.Interface;
using File = Cerberus.Domain.Model.File;
using Profile = Cerberus.Domain.Model.Profile;

namespace Cerberus.Web.Controllers
{
    public class StakeholderController : BaseController
    {
        private readonly ILegalPersonStakeholderService _legalPersonStakeholderService;
        private readonly ILegalPersonShareholderService _legalPersonShareholderService;
        private readonly IProfileService _profileService;
        private readonly IFileService _fileService;
        private readonly IMapper _mapper;
        private readonly FileStorageConfiguration _fileStorageConfiguration;
        public StakeholderController(ILegalPersonStakeholderService legalPersonStakeholderService, ILegalPersonShareholderService legalPersonShareholderService, IProfileService profileService, IFileService fileService, IOptions<FileStorageConfiguration> fileStorageConfiguration, IMapper mapper)

        {
            _legalPersonStakeholderService = legalPersonStakeholderService;
            _legalPersonShareholderService = legalPersonShareholderService;
            _profileService = profileService;
            _fileService = fileService;
            _fileStorageConfiguration = fileStorageConfiguration.Value;
            _mapper = mapper;

        }

        [HttpPost]
        [Route("api/checkstatus/{UniqueIdentifier}/{BirthDate}/{type}")]
        [AuthorizeUserFilterFactory]
        [RateLimitFilterFactory(Order = 1, Limit = 30, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<JsonResult> CheckState(CheckStatusViewModel model)
        {
            //TODO:Capcha Check Validation

            var profile = await GetProfile(model.UniqueIdentifier);

            if (profile == null || profile.Status < ProfileStatus.TraceCode)
            {
                return Json(new { isSuccess = false, message = "اطلاعات وارد شده صحیح نمی باشد" });
            }


            if (profile.PrivatePerson.BirthDate.Date != model.BirthDate.ToGeorgianDate())

            {
                return Json(new { isSuccess = false, message = $"{model.Type} باید قبلا در سجام ثبت نام کرده باشند و کد پیگیری دریافت کنند" });
            }

            return Json(new { isSuccess = true, firstName = profile.PrivatePerson.FirstName, lastName = profile.PrivatePerson.LastName });

        }

        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.StakeholderTakeAccess)]
        [RateLimitFilterFactory(Order = 1, Limit = 40, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> TakeAccess()
        {
            //TODO:Check Profile Is Legal In All Actions
            List<LegalPersonStakeholder> model = await _legalPersonStakeholderService.GetListAsync(User.ProfileId, StakeHolderType.TakeAccess);

            CreateStakeholderTakeAccessViewModel createStakeholderTakeAccessViewModel = new CreateStakeholderTakeAccessViewModel()
            {
                Stakeholders = _mapper.Map<List<StakeholderTakeAccessViewModel>>(model),
            };

            return View(createStakeholderTakeAccessViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.StakeholderTakeAccess)]
        [RateLimitFilterFactory(Order = 1, Limit = 30, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [Route("Stakeholder/TakeAccess")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> CreateTakeAccessAsync(CreateStakeholderTakeAccessViewModel model)
        {
            var stackholder = await _legalPersonStakeholderService.GetListAsync(User.ProfileId, StakeHolderType.TakeAccess);
            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                model.Stakeholders = _mapper.Map<List<StakeholderTakeAccessViewModel>>(stackholder);
                return View("TakeAccess", model);
            }

            if (model.SignatureFile.Length > 2100000 || model.SignatureFile.Length < 10000)
            {
                Alert("حجم فایل مربوط به امضا معتبر نیست،حجم فایل باید بیشتر از 10 کیلوبایت و کمتر از 2 مگابایت باشد", NotificationType.Warning);
                model.Stakeholders = _mapper.Map<List<StakeholderTakeAccessViewModel>>(stackholder);
                return View("TakeAccess", model);
            }
            var userProfile = await GetProfile(model.UniqueIdentifier);

            if (userProfile.PrivatePerson.BirthDate.Date != model.BirthDate.ToGeorgianDate())
            {
                Alert("دارندگان حق برداشت باید قبلا در سجام ثبت نام کرده باشند و کد پیگیری دریافت کنند", NotificationType.Error);
                model.Stakeholders = _mapper.Map<List<StakeholderTakeAccessViewModel>>(stackholder);
                return View("TakeAccess", model);
            }


            if (stackholder.Any(x => x.StakeholderProfileId == userProfile.Id))
            {
                Alert("این شخص  به عنوان شخص دارای حق برداشت  در این  شرکت قبلا ثبت شده است", NotificationType.Info);
                model.Stakeholders = _mapper.Map<List<StakeholderTakeAccessViewModel>>(stackholder);
                return View("TakeAccess", model);
            }

            var cdnfileName = $"{Guid.NewGuid()}{Path.GetExtension(model.SignatureFile.FileName)}";

            var file = new File
            {
                FileName = cdnfileName,
                Title = cdnfileName,
                Type = FileType.Image,
            };
            try
            {
                await _fileService.CreateAsync(file, model.SignatureFile.ToByte(), _fileStorageConfiguration.CdnEndPoint);
            }
            catch (CerberusException e) when (e.ErrorCode == ErrorCode.UnsupportedMediaType)
            {
                Alert("فرمت فایل مربوط به امضاء معتبر نیست، فایل بارگذاری شده باید دارای یکی از فرمت های jpg،png,jpeg و یا pdf باشد. ", NotificationType.Warning);
                model.Stakeholders = _mapper.Map<List<StakeholderTakeAccessViewModel>>(stackholder);
                return View("TakeAccess", model);
            }
            catch (CerberusException e) when (e.ErrorCode == ErrorCode.ImageTooLarge)
            {
                Alert("حجم هر فایل ارسالی باید بیشتر از 10 کیلوبایت و  کمتر از 2 مگابایت باشد،لطفا حجم فایل های خود را بررسی نمایید", NotificationType.Warning);
                model.Stakeholders = _mapper.Map<List<StakeholderTakeAccessViewModel>>(stackholder);
                return View("TakeAccess", model);
            }
            catch (CerberusException e) when (e.ErrorCode == ErrorCode.UnknownServerError)
            {
                Alert("در ذخیره فایل مشکلی بوجود آمده لطفا با پشتیبانی تماس بگیرید.", NotificationType.Warning);
                return View("TakeAccess", model);
            }
            catch (CerberusException)
            {
                Alert("خطا در ذخیره اطلاعات", NotificationType.Warning);
                return View("TakeAccess", model);
            }
            var legalPersonStakeholderModel = new LegalPersonStakeholder
            {
                ProfileId = User.ProfileId,
                StakeholderProfileId = userProfile.Id,
                PositionType = model.PositionType,
                Type = StakeHolderType.TakeAccess,
                SignatureFileId = file.Id
            };

            await _legalPersonStakeholderService.CreateAsync(legalPersonStakeholderModel);

            Alert("اطلاعات با موفقیت ذخیره شد", NotificationType.Success);
            return RedirectToAction("TakeAccess");
        }

        [HttpPost]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.StakeholderTakeAccess)]
        [Route("takeAccessAsync/{id}")]
        [ValidateAntiForgeryToken]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> DeleteTakeAccessAsync(DeleteStakeHolderViewModel model)
        {
            var deleteModel = new DeleteStakeHolderViewModel();
            var legalPersonStakeholder = await _legalPersonStakeholderService.GetAsync(model.Id);
            if (legalPersonStakeholder == null || legalPersonStakeholder.ProfileId != User.ProfileId)
            {
                deleteModel.IsSuccess = false;
                deleteModel.Message = "خطا در حذف اطلاعات";
                return Json(deleteModel);
            }
            await _legalPersonStakeholderService.DeleteAsync(legalPersonStakeholder);
            deleteModel.IsSuccess = true;
            return Json(deleteModel);
        }

        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.StakeholderTakeOrder)]
        [RateLimitFilterFactory(Order = 1, Limit = 50, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> OrderAccess()
        {
            List<LegalPersonStakeholder> data = await _legalPersonStakeholderService.GetListAsync(User.ProfileId, StakeHolderType.OrderAccess);
            CreateStakeholderOrderAccessViewModel createStakeholderOrderAccessViewModel = new CreateStakeholderOrderAccessViewModel
            {
                Stakeholders = _mapper.Map<List<StakeholderOrderAccessViewModel>>(data)
            };
            return View(createStakeholderOrderAccessViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.StakeholderTakeOrder)]
        [Route("Stakeholder/OrderAccess")]
        [RateLimitFilterFactory(Order = 1, Limit = 50, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> CreateOrderAccessAsync(CreateStakeholderOrderAccessViewModel model)
        {

            var orderAccessModel =
                await _legalPersonStakeholderService.GetListAsync(User.ProfileId, StakeHolderType.OrderAccess);
            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                model.Stakeholders = _mapper.Map<List<StakeholderOrderAccessViewModel>>(orderAccessModel);
                return View("OrderAccess", model);
            }
            var userProfile = await GetProfile(model.UniqueIdentifier);
            if (userProfile.PrivatePerson.BirthDate.Date != model.BirthDate.ToGeorgianDate())
            {
                Alert("اشخاص مجاز به سفارش باید قبلا در سجام ثبت نام کرده باشند و کد پیگیری دریافت کنند", NotificationType.Error);
                model.Stakeholders = _mapper.Map<List<StakeholderOrderAccessViewModel>>(orderAccessModel);
                return View("OrderAccess", model);
            }

            if (int.Parse(model.StartAt.Substring(0, 4).Replace("/", "")) < 1275 || int.Parse(model.EndAt.Substring(0, 4).Replace("/", "")) < 1275 ||
                model.StartAt.ToGeorgianDate() >= model.EndAt.ToGeorgianDate())
            {
                Alert("لطفا تاریخ  شروع و پایان دوره تصدی را اصلاح نمایید.", NotificationType.Warning);
                model.Stakeholders = _mapper.Map<List<StakeholderOrderAccessViewModel>>(await _legalPersonStakeholderService.GetListAsync(User.ProfileId, StakeHolderType.OrderAccess));
                return View("OrderAccess", model);
            }
            if (model.SignatureFile.Length > 2100000 || model.SignatureFile.Length < 10000)
            {
                Alert("حجم فایل مربوط به امضا معتبر نیست،حجم فایل باید بیشتر از 10 کیلوبایت و کمتر از 2 مگابایت باشد", NotificationType.Warning);
                model.Stakeholders = _mapper.Map<List<StakeholderOrderAccessViewModel>>(await _legalPersonStakeholderService.GetListAsync(User.ProfileId, StakeHolderType.OrderAccess));
                return View("OrderAccess", model);
            }
            if (orderAccessModel.Any(x => x.StakeholderProfileId == userProfile.Id))
            {
                Alert("این شخص  به عنوان یکی از اشخاص مجاز به سفارش در این  شرکت قبلا ثبت شده است", NotificationType.Info);
                model.Stakeholders = _mapper.Map<List<StakeholderOrderAccessViewModel>>(orderAccessModel);
                return View("OrderAccess", model);
            }

            var cdnFileName = $"{Guid.NewGuid()}{Path.GetExtension(model.SignatureFile.FileName)}";

            var file = new File
            {
                FileName = cdnFileName,
                Title = cdnFileName,
                Type = FileType.Image,
            };

            try
            {
                await _fileService.CreateAsync(file, model.SignatureFile.ToByte(), _fileStorageConfiguration.CdnEndPoint);
            }
            catch (CerberusException e) when (e.ErrorCode == ErrorCode.UnsupportedMediaType)
            {
                Alert("فرمت فایل مربوط به امضاء معتبر نیست، فایل بارگذاری شده باید دارای یکی از فرمت های jpg،png,jpeg و یا pdf باشد. ", NotificationType.Warning);
                model.Stakeholders = _mapper.Map<List<StakeholderOrderAccessViewModel>>(await _legalPersonStakeholderService.GetListAsync(User.ProfileId, StakeHolderType.OrderAccess));
                return View("OrderAccess", model);
            }
            catch (CerberusException e) when (e.ErrorCode == ErrorCode.ImageTooLarge)
            {
                Alert("حجم هر فایل ارسالی باید بیشتر از 10 کیلوبایت و  کمتر از 2 مگابایت باشد،لطفا حجم فایل های خود را بررسی نمایید", NotificationType.Warning);
                model.Stakeholders = _mapper.Map<List<StakeholderOrderAccessViewModel>>(await _legalPersonStakeholderService.GetListAsync(User.ProfileId, StakeHolderType.OrderAccess));
                return View("OrderAccess", model);
            }
            catch (CerberusException e) when (e.ErrorCode == ErrorCode.UnknownServerError)
            {
                Alert("در ذخیره فایل مشکلی بوجود آمده لطفا با پشتیبانی تماس بگیرید.", NotificationType.Warning);
                return View("OrderAccess", model);
            }
            catch (CerberusException)
            {
                Alert("خطا در ذخیره اطلاعات", NotificationType.Warning);
                return View("OrderAccess", model);
            }
            var legalPersonStakeholderModel = new LegalPersonStakeholder
            {
                ProfileId = User.ProfileId,
                StakeholderProfileId = userProfile.Id,
                StartAt = model.StartAt.ToGeorgianDate(),
                EndAt = model.EndAt.ToGeorgianDate(),
                PositionType = model.PositionType,
                Type = StakeHolderType.OrderAccess,
                SignatureFileId = file.Id
            };

            await _legalPersonStakeholderService.CreateAsync(legalPersonStakeholderModel);
            Alert("اطلاعات با موفقیت ذخیره شد", NotificationType.Success);
            return RedirectToAction("OrderAccess");

        }

        [HttpPost]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.StakeholderTakeOrder)]
        [Route("orderAccessAsync/{id}")]
        [ValidateAntiForgeryToken]
        [RateLimitFilterFactory(Order = 1, Limit = 40, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> DeleteOrderAccessAsync(DeleteStakeHolderViewModel model)
        {
            var deleteModel = new DeleteStakeHolderViewModel();
            var legalPersonStakeholder = await _legalPersonStakeholderService.GetAsync(model.Id);
            if (legalPersonStakeholder == null || legalPersonStakeholder.ProfileId != User.ProfileId)
            {
                deleteModel.IsSuccess = false;
                deleteModel.Message = "خطا در حذف اطلاعات";
                return Json(deleteModel);
            }
            await _legalPersonStakeholderService.DeleteAsync(legalPersonStakeholder);
            deleteModel.IsSuccess = true;
            return Json(deleteModel);
        }

        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.StakeholderTakeManager)]
        [RateLimitFilterFactory(Order = 1, Limit = 50, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> Manager()
        {
            List<LegalPersonStakeholder> stakeholders = await _legalPersonStakeholderService.GetListAsync(User.ProfileId, StakeHolderType.Manager);
            var createStakeholderManagerViewModel = new CreateStakeholderManagerViewModel()
            {
                Stakeholders = _mapper.Map<List<StakeholderManagerViewModel>>(stakeholders),
            };
            return View(createStakeholderManagerViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.StakeholderTakeManager)]
        [Route("Stakeholder/Manager")]
        [RateLimitFilterFactory(Order = 1, Limit = 50, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> CreateManagerAsync(CreateStakeholderManagerViewModel model)
        {
            var managerModel = await _legalPersonStakeholderService.GetListAsync(User.ProfileId, StakeHolderType.Manager);

            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                model.Stakeholders = _mapper.Map<List<StakeholderManagerViewModel>>(managerModel);
                return View("Manager", model);
            }
            var userProfile = await GetProfile(model.UniqueIdentifier);

            if (userProfile.PrivatePerson.BirthDate.Date != model.BirthDate.ToGeorgianDate())
            {
                Alert("اعضای هیئت مدیره باید قبلا در سجام ثبت نام کرده باشند و کد پیگیری دریافت کنند", NotificationType.Error);
                model.Stakeholders = _mapper.Map<List<StakeholderManagerViewModel>>(managerModel);
                return View("Manager", model);
            }
            if (managerModel.Any(x => x.StakeholderProfileId == userProfile.Id))
            {
                Alert("این شخص  به عنوان یکی از اعضای هیئت مدیره  در این  شرکت قبلا ثبت شده است", NotificationType.Warning);
                model.Stakeholders = _mapper.Map<List<StakeholderManagerViewModel>>(managerModel);
                return View("Manager", model);
            }

            if (model.IsOwnerSignature && model.SignatureFile == null)
            {
                Alert("در صورت انتخاب صاحب امضا،تصویر فایل امضاء اجباری است.", NotificationType.Warning);
                model.Stakeholders = _mapper.Map<List<StakeholderManagerViewModel>>(managerModel);
                return View("Manager", model);
            }
            long imageId = 0;
            if (model.IsOwnerSignature)
            {
                var cdnfileName = $"{Guid.NewGuid()}{Path.GetExtension(model.SignatureFile.FileName)}";

                var file = new File
                {
                    FileName = cdnfileName,
                    Title = cdnfileName,
                    Type = FileType.Image,
                };

                try
                {
                    await _fileService.CreateAsync(file, model.SignatureFile.ToByte(),
                        _fileStorageConfiguration.CdnEndPoint);

                }
                catch (CerberusException e) when (e.ErrorCode == ErrorCode.UnsupportedMediaType)
                {
                    Alert(
                        "فرمت فایل مربوط به امضاء معتبر نیست، فایل بارگذاری شده باید دارای یکی از فرمت های jpg،png,jpeg و یا pdf باشد. ",
                        NotificationType.Warning);
                    model.Stakeholders = _mapper.Map<List<StakeholderManagerViewModel>>(
                        await _legalPersonStakeholderService.GetListAsync(User.ProfileId, StakeHolderType.Manager));
                    return View("Manager", model);
                }
                catch (CerberusException e) when (e.ErrorCode == ErrorCode.ImageTooLarge)
                {
                    Alert("حجم هر فایل ارسالی باید بیشتر از 10 کیلوبایت و  کمتر از 2 مگابایت باشد،لطفا حجم فایل های خود را بررسی نمایید", NotificationType.Warning);
                    model.Stakeholders = _mapper.Map<List<StakeholderManagerViewModel>>(
                        await _legalPersonStakeholderService.GetListAsync(User.ProfileId, StakeHolderType.Manager));
                    return View("Manager", model);
                }
                catch (CerberusException e) when (e.ErrorCode == ErrorCode.UnknownServerError)
                {
                    Alert("در ذخیره فایل مشکلی بوجود آمده لطفا با پشتیبانی تماس بگیرید.", NotificationType.Warning);
                    return View("Manager", model);
                }
                catch (CerberusException)
                {
                    Alert("خطا در ذخیره اطلاعات", NotificationType.Warning);
                    return View("Manager", model);
                }

                imageId = file.Id;
            }

            var legalPersonStakeholderModel = new LegalPersonStakeholder
            {
                ProfileId = User.ProfileId,
                StakeholderProfileId = userProfile.Id,
                PositionType = model.PositionType,
                Type = StakeHolderType.Manager,
                IsOwnerSignature = model.IsOwnerSignature
            };
            if (model.IsOwnerSignature)
            {
                legalPersonStakeholderModel.SignatureFileId = imageId;
            }

            await _legalPersonStakeholderService.CreateAsync(legalPersonStakeholderModel);

            //todo:refresh token
            Alert("اطلاعات با موفقیت ذخیره شد", NotificationType.Success);
            return RedirectToAction("Manager");
        }

        [HttpPost]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.StakeholderTakeManager)]
        [Route("managerAsync/{id}")]
        [ValidateAntiForgeryToken]
        [RateLimitFilterFactory(Order = 1, Limit = 30, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> DeleteManagerAsync(DeleteStakeHolderViewModel model)
        {
            var deleteModel = new DeleteStakeHolderViewModel();
            var legalPersonStakeholder = await _legalPersonStakeholderService.GetAsync(model.Id);
            if (legalPersonStakeholder == null || legalPersonStakeholder.ProfileId != User.ProfileId)
            {
                deleteModel.IsSuccess = false;
                deleteModel.Message = "خطا در حذف اطلاعات";
                return Json(deleteModel);
            }
            await _legalPersonStakeholderService.DeleteAsync(legalPersonStakeholder);
            deleteModel.IsSuccess = true;
            return Json(deleteModel);
        }


        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.StakeholderShareholder)]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 1, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> Shareholder()
        {
            var createShareholderViewModel = new CreateShareholderViewModel()
            {
                Shareholders = _mapper.Map<List<ShareholderViewModel>>(await _legalPersonShareholderService.GetListAsync(User.ProfileId)),
            };
            return View(createShareholderViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.StakeholderShareholder)]
        [Route("Stakeholder/Shareholder")]
        [RateLimitFilterFactory(Order = 1, Limit = 40, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> CreateShareholderAsync(CreateShareholderViewModel model)
        {

            var shareholderModel = await _legalPersonShareholderService.GetListAsync(User.ProfileId);
            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                model.Shareholders =
                    _mapper.Map<List<ShareholderViewModel>>(shareholderModel);
                return View("Shareholder", model);
            }

            if (shareholderModel.Any(x => x.UniqueIdentifier == model.UniqueIdentifier))
            {
                Alert("شخصی با این کد ملی قبلا اضافه شده است", NotificationType.Error);
                model.Shareholders =
              _mapper.Map<List<ShareholderViewModel>>(shareholderModel);
                return View("Shareholder", model);
            }

            var legalPersonShareholder = _mapper.Map<LegalPersonShareholder>(model);
            legalPersonShareholder.ProfileId = User.ProfileId;

            await _legalPersonShareholderService.CreateAsync(legalPersonShareholder);

            Alert("اطلاعات با موفقیت ذخیره شد", NotificationType.Success);
            return RedirectToAction("Shareholder");
        }

        [HttpPost]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.StakeholderShareholder)]
        [Route("shareholderAsync/{id}")]
        [ValidateAntiForgeryToken]
        [RateLimitFilterFactory(Order = 1, Limit = 40, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> DeleteShareholderAsync(DeleteStakeHolderViewModel model)
        {
            var deleteModel = new DeleteStakeHolderViewModel();
            var legalPersonShareholderModel = await _legalPersonShareholderService.GetAsync(model.Id);
            if (legalPersonShareholderModel == null || legalPersonShareholderModel.ProfileId != User.ProfileId)
            {
                deleteModel.IsSuccess = false;
                deleteModel.Message = "خطا در حذف اطلاعات";
                return Json(deleteModel);
            }
            await _legalPersonShareholderService.DeleteAsync(legalPersonShareholderModel);
            deleteModel.IsSuccess = true;
            return Json(deleteModel);
        }

        [AuthorizeUserFilterFactory]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 1, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> Redirect(CurrentStep step)
        {

            if (step == CurrentStep.AllowTake)
            {
                var takeAccess = await _legalPersonStakeholderService.GetListAsync(User.ProfileId, StakeHolderType.TakeAccess);
                var model = new CreateStakeholderTakeAccessViewModel();
                if (takeAccess.Count != 0) return RedirectToNextStep(step);
                Alert("وارد کردن اطلاعات افراد دارای حق برداشت الزامی است", NotificationType.Info);
                model.Stakeholders = _mapper.Map<List<StakeholderTakeAccessViewModel>>(takeAccess);
                return View("TakeAccess", model);
            }
            else if (step == CurrentStep.Manager)
            {
                var model = new CreateStakeholderManagerViewModel();
                var manager = await _legalPersonStakeholderService.GetListAsync(User.ProfileId, StakeHolderType.Manager);
                if (manager.Count >= 2) return RedirectToNextStep(step);
                Alert("لطفا تمامی اعضای هیئت مدیره را ثبت نمایید", NotificationType.Info);
                model.Stakeholders = _mapper.Map<List<StakeholderManagerViewModel>>(manager);
                return View("Manager", model);
            }
            else if (step == CurrentStep.AllowOrder)
            {
                var model = new CreateStakeholderOrderAccessViewModel();
                var orderAccess = await _legalPersonStakeholderService.GetListAsync(User.ProfileId, StakeHolderType.OrderAccess);
                if (orderAccess.Count != 0) return RedirectToNextStep(step);
                Alert("وارد کردن اطلاعات اشخاص مجاز به سفارش الزامی است", NotificationType.Info);

                model.Stakeholders = _mapper.Map<List<StakeholderOrderAccessViewModel>>(orderAccess);
                return View("OrderAccess", model);
            }
            else if (step == CurrentStep.Shareholder)
            {
                var model = new CreateShareholderViewModel();
                var shareHolder = await _legalPersonShareholderService.GetListAsync(User.ProfileId);
                if (shareHolder.Count != 0) return RedirectToNextStep(step);
                Alert("وارد کردن اطلاعات سهامداران الزامی است", NotificationType.Info);
                model.Shareholders = _mapper.Map<List<ShareholderViewModel>>(shareHolder);
                return View("Shareholder", model);

            }

            return RedirectToNextStep(step);
        }


        public async Task<Profile> GetProfile(string uniqueIdentifier)
        {
            return (await _profileService.GetGenericAsync(x => x.UniqueIdentifier == uniqueIdentifier,
                inc =>
                {
                    return inc
                    .Include(x => x.PrivatePerson);
                })).FirstOrDefault();

        }


    }
}