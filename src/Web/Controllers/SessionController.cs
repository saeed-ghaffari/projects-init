﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Interface.Services.Membership;
using Cerberus.Service;
using Cerberus.Utility;
using Cerberus.Utility.Enum;
using Cerberus.Web.Configuration;
using Cerberus.Web.Enum;
using Cerberus.Web.Extensions;
using Cerberus.Web.Filters;
using Cerberus.Web.Model;
using Cerberus.Web.ViewModel.Profile;
using Cerberus.Web.WebLogic;
using Gatekeeper.AspModules.ActionFilter;
using Gatekeeper.AspModules.Interface;
using Gatekeeper.Core.Interface;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using Profile = Cerberus.Domain.Model.Profile;

namespace Cerberus.Web.Controllers
{
    public class SessionController : BaseController
    {
        private readonly IOtpService _otpService;
        private readonly IMessagingService _messagingService;
        private readonly IProfileService _profileService;
        private readonly WebConfiguration _webConfiguration;
        private readonly MessagingConfiguration _messagingConfiguration;
        private readonly IThirdPartyServiceService _thirdPartyService;
        private readonly CookieHelper _cookieHelper;
        private readonly IDatabase _readDatabase;
        private readonly IDatabase _writeDatabase;
        private readonly CaptchaConfiguration _captchaConfiguration;
        private readonly IMembershipService _membershipService;
        private readonly IRateLimitService _rateLimitService;

        public SessionController(
                IOtpService otpService, IOptions<WebConfiguration> webConfiguration, IMessagingService messagingService,
                IOptions<MessagingConfiguration> messagingConfiguration, IProfileService profileService,
               IThirdPartyServiceService thirdPartyService, IDatabase readDatabase, IDatabase writeDatabase,
                IOptions<CaptchaConfiguration> captchaConfiguration, CookieHelper cookieHelper,
                IMembershipService membershipService, IRateLimitService rateLimitService)

        {
            _otpService = otpService;
            _messagingService = messagingService;
            _profileService = profileService;
            _thirdPartyService = thirdPartyService;
            _readDatabase = readDatabase;
            _writeDatabase = writeDatabase;
            _cookieHelper = cookieHelper;
            _membershipService = membershipService;
            _rateLimitService = rateLimitService;
            _captchaConfiguration = captchaConfiguration.Value;
            _messagingConfiguration = messagingConfiguration.Value;
            _webConfiguration = webConfiguration.Value;
        }

        [HttpGet]
        [Route("session")]
        [RateLimitFilterFactory(Order = 1, Limit = 100, PeriodInSec = 1 * 60 * 2)]

        public IActionResult CreateOtpAsync()
        {
            return View("index");
        }

        [HttpPost]
        [Route("session")]
        [ValidateAntiForgeryToken]
        [RateLimitFilterFactory(Order = 1, Limit = 100, PeriodInSec = 1 * 60 * 2)]//30 min
        [ServiceFilter(typeof(ValidateCaptchaAttribute))]
        public async Task<IActionResult> CreateOtpAsync(RegisterViewModel registerViewModel)
        {
            //validate data model
            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                return View("Index", registerViewModel);
            }

            var mobile = registerViewModel.Mobile;

            //validate mobile
            if (!ValidationHelper.IsMsisdnValid(ref mobile, out var carrier) || carrier == null)
            {
                Alert("شماره تلفن وارد شده صحیح نمی باشد", NotificationType.Warning);
                return View("Index", registerViewModel);
            }

            if (!await HasAccess("CreateOtpAsync", mobile, 3))
            {
                Response.StatusCode = 429;
                return Redirect("~/429.html");
            }
            //create otp
            var key = ConventionalHelper.CreateOtpKey(mobile, OtpType.Registration);
            var otpCode = await _otpService.GetAsync(key, _webConfiguration.OtpTtl);
            //send otp to user using SMS
            await _messagingService.SendOtpSmsAsync(
                      null,
                      mobile,
                      carrier.Value,
                      string.Format(_messagingConfiguration.RegisterOtpMessage, otpCode, Environment.NewLine));

            ModelState.Clear();

            return View("Confirm", new CreateProfileViewModel()
            {
                Msisdn = mobile
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("confirmOtp")]
        [RateLimitFilterFactory(Order = 1, Limit = 100, PeriodInSec = 1 * 60 * 2)]
        [ServiceFilter(typeof(ValidateCaptchaAttribute))]
        public async Task<IActionResult> ConfirmAsync(CreateProfileViewModel model)
        {
            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                return View("Confirm", model);
            }

            if (!await HasAccess("ConfirmAsync", model.Msisdn, 3))
            {
                Response.StatusCode = 429;
                return Redirect("~/429.html");
            }
            ModelState.Clear();

            if (Tools.CheckUniqueIdentifier(model.UniqueIdentifier, (int)model.Type) == false)
            {
                Alert("کد ملی وارد شده نامعتبر می باشد", NotificationType.Warning);
                return View("Confirm", model);
            }
            //validate otp
            var validateOtpStatus = await _otpService.IsValidAsync(ConventionalHelper.CreateOtpKey(model.Msisdn, OtpType.Registration), model.Otp);

            if (!validateOtpStatus)
            {
                Alert("کد تایید وارد شده صحیح نمی باشد", NotificationType.Warning);
                return View("Confirm", model);
            }

            string mobile = model.Msisdn;

            if (!ValidationHelper.IsMsisdnValid(ref mobile, out Carrier? carrier) || carrier == null)
            {
                Alert("شماره تلفن وارد شده صحیح نمی باشد", NotificationType.Warning);
                return View("Confirm", model);
            }
            var mobileLong = long.Parse(mobile);

            //Get userdata from profile tab

            var profile = await _profileService.GetByUniqueIdentifierWithNoCacheAsync(model.UniqueIdentifier);

            long serviceId = 0;
            // check user exists in profile 
            if (profile == null)
            {
                var partyServiceResult = await CheckThirdPartyService(model.UniqueIdentifier, mobile);
                serviceId = partyServiceResult.ServiceId;
                if (!partyServiceResult.HasMembership)
                {
                    var existMobile = await _profileService.RegisterMobileCount(mobile.ToLong(), ProfileStatus.SuccessPayment, _webConfiguration.RegisterMobileCount);
                    if (existMobile)
                    {
                        Alert("شخصی با این شماره موبایل قبلا در سامانه ثبت نام کرده است", NotificationType.Error);
                        return RedirectToAction("CreateOtpAsync");
                    }
                }


                profile = await _profileService.CreateAsync(new Profile
                {
                    Carrier = carrier.Value,
                    UniqueIdentifier = model.UniqueIdentifier,
                    Mobile = mobileLong,
                    Type = model.Type,
                    Status = ProfileStatus.Init
                });
            }
            else
            {
                profile = await _profileService.GetRelatedDataForScopeAsync(profile.Id, model.Type);

                if (profile.Status < ProfileStatus.SuccessPayment)
                {
                    var settlePayment = profile.Payments?.FirstOrDefault(x => x.Status == PaymentStatus.Settle);
                    if (settlePayment?.ReferenceServiceId != null && settlePayment.IsConfirmedReference == false)
                    {
                        Alert("متاسفانه در ورود شما به سامانه خطایی رخ داده است،لطفا با پشتیبانی تماس بگیرید",
                            NotificationType.Warning);
                        return RedirectToAction("CreateOtpAsync");
                    }

                    var partyServiceResult = await CheckThirdPartyService(model.UniqueIdentifier, mobile);
                    serviceId = partyServiceResult.ServiceId;
                    if (!partyServiceResult.HasMembership)
                    {
                        var existMobile = await _profileService.RegisterMobileCount(mobile.ToLong(),
                            ProfileStatus.SuccessPayment, _webConfiguration.RegisterMobileCount);
                        if (existMobile)
                        {
                            Alert("شخصی با این شماره موبایل قبلا در سامانه ثبت نام کرده است", NotificationType.Error);
                            return RedirectToAction("CreateOtpAsync");
                        }
                    }

                    if (profile.Mobile != mobileLong)
                    {
                        if (profile.CreationDate.AddMinutes(40) >= DateTime.Now)
                        {
                            Alert("شخصی با این کد ملی  در حال ورود اطلاعات در سامانه است", NotificationType.Warning);
                            return View("Confirm", model);
                        }

                        profile.Mobile = mobileLong;
                        profile.Carrier = carrier.Value;
                        await _profileService.UpdateAsync(profile);
                    }

                }

                else if (profile.Status >= ProfileStatus.SuccessPayment)
                {
                    if (profile.Mobile != mobileLong)
                    {
                        Alert("کد یا شناسه ملی وارد شده قبلا با شماره موبایل دیگری ثبت شده است.",
                            NotificationType.Warning);
                        return View("Confirm", model);
                    }

                    if (profile.Status == ProfileStatus.PendingValidation)
                    {
                        Alert("تا مشخص نشدن نتیجه استعلام، نمی توانید وارد سامانه شوید",
                            NotificationType.Warning);
                        return RedirectToAction("CreateOtpAsync");
                    }

                    var settlePayment = profile.Payments?.FirstOrDefault(x => x.Status == PaymentStatus.Settle);


                    serviceId = settlePayment?.DiscountIssuer ?? 0;
                }
            }

            //create access token
            var tokenPayload = new UserSessionInfo
            {
                ProfileId = profile.Id,
                ProfileOwnerType = profile.Type,
                ProfileStatus = profile.Status,
                AvailableScope = profile.GetAvailableScope(),
                CurrentStep = profile.GetCurrentStep()
            };
            if (serviceId != 0) tokenPayload.ServiceId = serviceId;
            await _cookieHelper.CreateCookie(tokenPayload);

            if (profile.Status == ProfileStatus.SuccessPayment)
            {
                return RedirectToNextStep(tokenPayload.CurrentStep);
            }

            if (profile.Status <= ProfileStatus.SuccessPayment) return RedirectToNextStep(CurrentStep.Init);
            if (profile.Type == ProfileOwnerType.IranianPrivatePerson || profile.Type == ProfileOwnerType.IranianLegalPerson)
            {
                return RedirectToAction("Index", "Dashboard");
            }
            return RedirectToNextStep(CurrentStep.Init);
        }



        [HttpPost]
        [Route("api/resendOtp/{mobile}")]
        [RateLimitFilterFactory(Order = 1, Limit = 10, PeriodInSec = 1 * 60 * 10)]
        public async Task<JsonResult> ResendOtpAsync(string mobile)
        {
            if (!await HasAccess("ResendOtpAsync", mobile, 2))
            {
                Response.StatusCode = 429;
                return Json(new { IsSuccess = false, Message = "تعداد درخواست شما بیشتر از حد مجاز می باشد" });
            }
            if (!ValidationHelper.IsMsisdnValid(ref mobile, out var carrier) || carrier == null)
            {
                return Json(new { IsSuccess = false, Message = "شماره تلفن وارد شده صحیح نمی باشد" });
            }
            var key = ConventionalHelper.CreateOtpKey(mobile, OtpType.Registration);
            var otpCode = await _otpService.GetAsync(key, _webConfiguration.OtpTtl);

            //send otp to user using SMS
            await _messagingService.SendOtpSmsAsync(
                     null,
                     mobile,
                     carrier.Value,
                     string.Format(_messagingConfiguration.RegisterOtpMessage, otpCode, Environment.NewLine));

            return Json(new { IsSuccess = true, Message = "کد تایید با موفقیت برای شما ارسال شد " });

        }

        [Route("signOut")]
        [RateLimitFilterFactory(Order = 1, Limit = 200, PeriodInSec = 1 * 60 * 10)]
        public async Task<IActionResult> SignOut()
        {
            await _cookieHelper.KillCookie();
            return RedirectToAction("Index", "Home");
        }




        [Route("session/getCaptchaImage")]
        public async Task<IActionResult> GetCaptchaImage()
        {
            var captchaCode = await GenerateCaptchaCode();
            var result = CaptchaHelper.GenerateCaptchaImage(_captchaConfiguration.CaptchaImageWidth, _captchaConfiguration.CaptchaImageHeight, captchaCode);
            Stream s = new MemoryStream(result.CaptchaByteData);
            return new FileStreamResult(s, "image/png");
        }
        public async Task<string> GenerateCaptchaCode()
        {
            HttpContext.Request.Cookies.TryGetValue("_ck", out var clientsKey);

            if (clientsKey != null)
            {
                var key = HashHelper.GenerateMd5String(_captchaConfiguration.KeySalt + clientsKey);
                await _readDatabase.KeyDeleteAsync($"{_captchaConfiguration.CaptchaPrefix}:{key}");
            }
            string clientKey = Guid.NewGuid().ToString("N");
            var serverKey = HashHelper.GenerateMd5String(_captchaConfiguration.KeySalt + clientKey);

            HttpContext.Response.Cookies.Append("_ck", clientKey);

            var captchaCode = Tools.GenerateRandomNumber(_captchaConfiguration.CaptchaLength);

            var writeStatus = await _writeDatabase.StringSetAsync($"{_captchaConfiguration.CaptchaPrefix}:{serverKey}", captchaCode, _captchaConfiguration.CaptchaTtl);
            if (!writeStatus)
                throw new Exception("unable to store captcha key into write DB");

            return captchaCode;
        }

        public async Task<ThirdPartyServiceResult> CheckThirdPartyService(string uniqueIdentifier, string mobile)
        {
            var th = new ThirdPartyServiceResult();
            if (Request.Cookies["ServiceLink"] != null)
            {
                var serviceLink = Request.Cookies["ServiceLink"].ToString();
                var thirdPartyServices = await _thirdPartyService.GetByLinkAsync(serviceLink);
                if (thirdPartyServices != null && thirdPartyServices.EnabledLink)
                {
                    th.ServiceId = thirdPartyServices.Id;
                    var privatePersonTmp = await _membershipService.GetPrivatePersonAsync(uniqueIdentifier,
                       mobile, thirdPartyServices.Id);
                    if (privatePersonTmp != null)
                    {
                        th.HasMembership = true;
                    }
                }
            }

            return th;
        }

        async Task<bool> HasAccess(string action, string mobile, int rate)
        {
            return await _rateLimitService.HasAccessAsync($"SessionController:{action}:{mobile}", 1 * 60 * 15, rate);
        }

    }
}
