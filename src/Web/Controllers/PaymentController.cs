﻿
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Interface.Services.FGM;
using Cerberus.Domain.Interface.Services.Membership;
using Cerberus.Domain.Model;
using Cerberus.Service.BankService.Configuration;
using Cerberus.Utility;
using Cerberus.Web.Configuration;
using Cerberus.Web.Enum;
using Cerberus.Web.Extensions;
using Cerberus.Web.Filters;
using Cerberus.Web.Model;
using Cerberus.Web.ViewModel.Payment;
using Cerberus.Web.WebLogic;
using Gatekeeper.AspModules.ActionFilter;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Profile = Cerberus.Domain.Model.Profile;

namespace Cerberus.Web.Controllers
{

    public class PaymentController : BaseController
    {
        private readonly AmountConfiguration _amountConfiguration;
        private readonly ILogger<PaymentController> _logger;
        private readonly IMembershipService _membershipService;
        private readonly IPaymentService _paymentService;
        private readonly IProfileService _profileService;
        private readonly IThirdPartyServiceService _thirdPartyServiceService;
        private readonly IProfilePackageService _profilePackageService;
        private readonly CookieHelper _cookieHelper;
        private readonly FgmConfiguration _fgmConfiguration;
        private readonly IFgmService _fgmService;
        private readonly IOfflineTaskService _offlineTaskService;
        public PaymentController(
            IProfileService profileService,
            IPaymentService paymentService,
        IOptions<AmountConfiguration> amountConfiguration,
            ILogger<PaymentController> logger,
         IThirdPartyServiceService thirdPartyService,
           IMembershipService membershipService, IProfilePackageService profilePackageService,
            CookieHelper cookieHelper, IOptions<FgmConfiguration> fgmConfiguration, IFgmService fgmService, IOfflineTaskService offlineTaskService)
        {
            _profileService = profileService;
            _paymentService = paymentService;
            _logger = logger;
            _thirdPartyServiceService = thirdPartyService;
            _membershipService = membershipService;
            _profilePackageService = profilePackageService;
            _cookieHelper = cookieHelper;
            _fgmService = fgmService;
            _offlineTaskService = offlineTaskService;
            _fgmConfiguration = fgmConfiguration.Value;
            _amountConfiguration = amountConfiguration.Value;
        }

        [HttpGet]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.Payment)]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<ActionResult> Index()
        {
            ViewBag.ProfileType = User.ProfileOwnerType;
            //_membershipService.GetUserProfileService
            var profile = await _profileService.GetRelatedDataWithEntity(User.ProfileId, null, EntityType.Payments);

            if (profile == null) return RedirectToNextStep(CurrentStep.Init);

            if (profile.Payments.FirstOrDefault(x => x.Status == PaymentStatus.Settle) != null)
            {
                Alert("شما قبلا عملیات پرداخت را انجام داده اید، لطفا چند دقیقه دیگر وارد سامانه شوید.", NotificationType.Warning);
                return RedirectToAction("SignOut", "Session");
            }
            if (profile.Payments.Any(x => x.Gateway == PaymentGateway.Fgm && (x.Status == PaymentStatus.Init || x.Status == PaymentStatus.Verify)))
            {
                Alert("در حال حاضر شما نمی توانید عملیات پرداخت را انجام دهید، لطفا با پشتیبانی تماس بگیرید", NotificationType.Warning);
                return RedirectToAction("SignOut", "Session");
            }

            var checkSejamMobile = await _profileService.IsExistMobileStatus(profile.Mobile, ProfileStatus.SuccessPayment);
            if (checkSejamMobile) Alert($"کاربر گرامی، شماره موبایل {profile.Mobile.NormalizeMobile()} ،برای ثبت نام شخص دیگری نیز در سامانه ثبت گردیده است،توجه فرمایید پس از ورود به مراحل ثبت نام در صورت نیاز به تغییر شماره موبایل خود، مبلغ پنجاه هزار ریال قابل پرداخت می باشد.", NotificationType.Warning);


            if (profile.Status < ProfileStatus.SuccessPayment)
            {
                CreatePaymentViewModel payment = await CalcDiscount(profile);
                payment.UniqueIdentifier = profile.UniqueIdentifier;
                if (payment.Amount <= 0)
                {

                    var tokenPayload = new UserSessionInfo
                    {
                        ProfileId = profile.Id,
                        ProfileOwnerType = profile.Type,
                        ProfileStatus = profile.Status,
                        AvailableScope = profile.GetAvailableScope(),
                        CurrentStep = profile.GetCurrentStep()
                    };
                    if (payment.ServiceId != 0)
                        tokenPayload.ServiceId = payment.ServiceId;

                    await _cookieHelper.CreateCookie(tokenPayload);

                    Alert(payment.Message, NotificationType.Success);
                    return RedirectToNextStep(CurrentStep.Payment);
                }
                else
                {
                    return View(payment);
                }

            }
            else
            {
                return RedirectToNextStep(CurrentStep.Init);
            }

        }


        [HttpPost("pay")]
        [ValidateAntiForgeryToken]
        [AuthorizeUserFilterFactory]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public IActionResult EditAsync(PrimitivePackageType type)
        {
            if (User.ProfileStatus < ProfileStatus.SuccessPayment)
            {
                Alert("اطلاعات ارسالی اشتباه است", NotificationType.Error);
                return RedirectToAction("SignOut", "Session");
            }
            return View("Edit", new CreatePaymentViewModel
            {
                Amount = _amountConfiguration.GetEditAmountByType(type),
                PrimitivePackageType = type
            });
        }


        [AuthorizeUserFilterFactory]
        [ValidateAntiForgeryToken]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> InitEditAsync(CreatePaymentViewModel model)
        {
            if (!ModelState.IsValid)
            {
                Alert(ModelState.ToErrorMessage(), NotificationType.Error);
                return RedirectToAction("SignOut", "Session");
            }
            if (User.ProfileStatus < ProfileStatus.SuccessPayment)
            {
                Alert("اطلاعات ارسالی اشتباه است", NotificationType.Error);
                return RedirectToAction("SignOut", "Session");
            }
            var defaultValuePay = _amountConfiguration.GetEditAmountByType(model.PrimitivePackageType.GetValueOrDefault());

            var package =
                await _profilePackageService.GetSuspiciousPackageAsync(User.ProfileId, PackageType.EditAllData);
            if (package != null)
            {
                Alert("در حال حاضر شما نمی توانید عملیات پرداخت را انجام دهید، لطفا با پشتیبانی تماس بگیرید", NotificationType.Warning);
                return RedirectToAction("SignOut", "Session");
            }
            ProfilePackage p = new ProfilePackage
            {
                ProfileId = User.ProfileId,
                PrimitivePackageType = model.PrimitivePackageType.GetValueOrDefault(),
                Type = PackageType.EditAllData
            };

            await _profilePackageService.CreateAsync(p);

            var paymentRequest = await Gateway(PaymentGateway.Fgm, defaultValuePay, 0, 0, p.Id);

            return View("Getway", paymentRequest);

        }

        private async Task<CreatePaymentViewModel> CalcDiscount(Profile profile)
        {
            var defaultValuePay = _amountConfiguration.GetAmountByType(User.ProfileOwnerType);
            var createPaymentViewModel = new CreatePaymentViewModel { Amount = defaultValuePay };

            //if user have service id and not equal zero
            var msg = string.Empty;
            if (!User.ServiceId.HasValue || User.ServiceId.Value == 0) return createPaymentViewModel;
            //if  thirdPartyServices exists with Id
            var thirdPartyService = await _thirdPartyServiceService.GetAsync(User.ServiceId.Value);
            if (thirdPartyService == null || !thirdPartyService.EnabledDiscount) return createPaymentViewModel;
            createPaymentViewModel.ServiceId = thirdPartyService.Id;
            // get user from temp table
            var privatePersonTmp = await _membershipService.GetPrivatePersonAsync(profile.UniqueIdentifier, profile.Mobile.ToString(), User.ServiceId.Value);
            if (privatePersonTmp == null) return createPaymentViewModel;
            // calc discount  and get money for pay
            var resultDiscount = defaultValuePay - privatePersonTmp.Discount;
            if (resultDiscount > 0 && privatePersonTmp.Discount > 0)
            {
                msg = $"  شما مبلغ {privatePersonTmp.Discount} ریال  از   {thirdPartyService.Title}، تخفیف دریافت کردید";
            }
            else if (resultDiscount <= 0)
            {
                Payment payment = await _paymentService.CreatePendingAsync(User.ProfileId, PaymentGateway.CompleteDiscount, 0, thirdPartyService.Id, privatePersonTmp.Discount, 0);
                payment.SaleReferenceId = "0";
                payment.ReferenceNumber = "0";
                payment.Status = PaymentStatus.Settle;
                await _paymentService.UpdateAsync(payment);

                //get full discount and register as successful payment
                profile.Status = ProfileStatus.SuccessPayment;
                await _profileService.UpdateAsync(profile);
                createPaymentViewModel.Gateway = PaymentGateway.CompleteDiscount;
                msg = $" شما کل مبلغ   {privatePersonTmp.Discount} ریال را از {thirdPartyService.Title}. تخفیف دریافت کردید";
            }
            createPaymentViewModel.Amount = resultDiscount;
            createPaymentViewModel.Message = msg;
            return createPaymentViewModel;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("payment")]
        [AuthorizeUserFilterFactory(Scope = (int)AvailableScope.Payment)]
        [RateLimitFilterFactory(Order = 1, Limit = 20, PeriodInSec = 1 * 60 * 5, UserIdentifier = "ProfileId")]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> InitAsync(CreatePaymentViewModel model)
        {

            var defaultValuePay = _amountConfiguration.GetAmountByType(User.ProfileOwnerType);
            var user = await _profileService.GetRelatedDataWithEntity(User.ProfileId, null, EntityType.Payments);// await _profileService.GetAsync(User.ProfileId, true);

            if (user.Payments.FirstOrDefault(x => x.Status == PaymentStatus.Settle) != null)
            {
                Alert("شما قبلا عملیات پرداخت را انجام داده اید.", NotificationType.Warning);
                return RedirectToAction("SignOut", "Session");
            }
            if (user.Payments.Any(x => x.Gateway == PaymentGateway.Fgm && (x.Status == PaymentStatus.Init || x.Status == PaymentStatus.Verify)))
            {
                Alert("در حال حاضر شما نمی توانید عملیات پرداخت را انجام دهید، لطفا با پشتیبانی تماس بگیرید", NotificationType.Warning);
                return RedirectToAction("SignOut", "Session");
            }

            var pay = user?.Payments?.FirstOrDefault(x => x.Status == PaymentStatus.Verify);
            if (pay != null && (pay.Gateway == PaymentGateway.Mellat || pay.Gateway == PaymentGateway.AsanPardakht))
            {
                Alert("پدلیل مشخص نشدن نتیجه پرداخت شما از سوی بانک  مربوطه، شما نمیتوانید دوباره عملیات پرداخت را انجام دهید؛ لطفا با پشتیبانی تماس بگیرید", NotificationType.Warning);
                return RedirectToAction("Index");
            }
            //if user have service id and not equal zero
            try
            {
                if (User.ServiceId.HasValue && User.ServiceId > 0)
                {
                    var thirdPartyService = await _thirdPartyServiceService.GetAsync(User.ServiceId.Value);
                    if (thirdPartyService != null)
                    {
                        var privatePersonTmp = await _membershipService.GetPrivatePersonAsync(user?.UniqueIdentifier, user?.Mobile.ToString(), User.ServiceId.Value);
                        if (privatePersonTmp != null && thirdPartyService.EnabledDiscount)
                        {
                            long amount = 0;
                            amount = defaultValuePay - privatePersonTmp.Discount;
                            if (amount > 0)
                            {
                                var payment = await Gateway(PaymentGateway.Fgm, amount, User.ServiceId.Value,
                                    privatePersonTmp.Discount, 0);
                                return View("Getway", payment);
                            }

                        }
                        else
                        {
                            //register as free user
                            var paymentRequest = await Gateway(PaymentGateway.Fgm, defaultValuePay, User.ServiceId.Value, 0, 0);
                            return View("Getway", paymentRequest);

                        }
                    }
                    else
                    {
                        //register as free user
                        var paymentRequest = await Gateway(PaymentGateway.Fgm, defaultValuePay, User.ServiceId.Value, 0, 0);
                        return View("Getway", paymentRequest);

                    }
                }
                else
                {
                    //register as free user
                    var paymentRequest = await Gateway(PaymentGateway.Fgm, defaultValuePay, 0, 0, 0);
                    return View("Getway", paymentRequest);
                }
            }
            catch (Exception e)
            {

                _logger.LogCritical(e, $"Unable to init payment.Gateway:{model.Gateway}");
                Alert("خطا در پرداخت، لطفا چند دقیقه دیگر امتحان نمایید", NotificationType.Error);
                model.Amount = _amountConfiguration.GetAmountByType(User.ProfileOwnerType);
                model.UniqueIdentifier = user?.UniqueIdentifier;
                ViewBag.ProfileType = User.ProfileOwnerType;
                return View("Index", model);

            }


            Alert("خطا رخ داده است ، لطفا با پشتیبانی تماس بگیرید", NotificationType.Warning);
            model.Amount = _amountConfiguration.GetAmountByType(User.ProfileOwnerType);
            model.UniqueIdentifier = user?.UniqueIdentifier;
            ViewBag.ProfileType = User.ProfileOwnerType;
            return View("Index", model);
        }



        private async Task<PaymentRequest> Gateway(PaymentGateway gateway, long defaultValuePay, long issuer, long discount, long profilePackageId)
        {

            var payment = await _paymentService.CreatePendingAsync(User.ProfileId, gateway, defaultValuePay, issuer, discount, profilePackageId);
            return new PaymentRequest
            {
                Url = _fgmConfiguration.WebAddress,
                Token = await _fgmService.GetPaymentToken(profilePackageId > 0 ? _fgmConfiguration.EditServiceId : _fgmConfiguration.RegisterServiceId, payment.Id,
                    defaultValuePay.ToInt())

            };

        }


        [HttpPost("payments/paymentCallback")]
        public async Task<IActionResult> PaymentCallback(PaymentResponse response)
        {
            var model = new BankResponseViewModel();
            var payment = await _paymentService.GetAsync(response.OrderId.ToLong());
            if (payment == null)
            {
                model.Message =
                    "خطا در پرداخت در صورتی که مبلغی از حساب شما کسر شده باشد ظرف 72 ساعت آینده به حساب شما برگشت داده خواهد شد";
                return View("FinalPayment", model);
            }
            if (payment.Status == PaymentStatus.Settle)
            {
                model.Message = "شما قبلا عملیات پرداخت را انجام داده اید";
                return View("FinalPayment", model);
            }
            var lastProfile = await _profileService.GetWithNoCacheAsync(payment.ProfileId);

            if (lastProfile.Status >= ProfileStatus.SuccessPayment && payment.ProfilePackageId == null)
            {
                model.Message = "شما قبلا عملیات پرداخت را انجام داده اید";
                return View("FinalPayment", model);
            }

            if (response.ResultCode == PaymentResultCode.Failed)
            {
                payment.Status = PaymentStatus.Unsuccessful;
                await _paymentService.UpdateAsync(payment);
                model.Message = "خطا در پرداخت";
                return View("FinalPayment", model);
            }
            if (response.ResultCode == PaymentResultCode.Canceled)
            {
                payment.Status = PaymentStatus.Cancel;
                await _paymentService.UpdateAsync(payment);
                model.Message = "پرداخت لغو شد";
                return View("FinalPayment", model);
            }
            if (response.ResultCode == PaymentResultCode.Duplicate)
            {
                var statusResult = await _fgmService.CheckPaymentStatus(response.OrderId, response.ServiceId);
                if (statusResult.Error == null)
                    return await UpdateToken(payment, statusResult.Data.SaleReferenceId, statusResult.Data.SaleReferenceNumber, lastProfile);

                switch (statusResult.Error.ErrorCode)
                {
                    case (int)ErrorCode.FailedPayment:
                        model.Message = "خطا در پرداخت لطفا دوباره تلاش کنید";
                        return View("FinalPayment", model);
                    case (int)HttpStatusCode.RequestTimeout:
                        model.Message = "خطا در پرداخت در صورتی که مبلغی از حساب شما کسر شده باشد ظرف 72 ساعت آینده به حساب شما برگشت داده خواهد شد";
                        return View("FinalPayment", model);
                }
            }
            else if (response.ResultCode == PaymentResultCode.Verify)
            {
                payment.Status = PaymentStatus.Init;
                await _paymentService.UpdateAsync(payment);

                var verifyResult = await _fgmService.VerifyPayment(response.Token);
                if (verifyResult.Error != null)
                {
                    if (verifyResult.Error.ErrorCode == (int)ErrorCode.Conflicts)
                    {
                        model.Message = "شما قبلا عملیات پرداخت را انجام داده اید";
                        return View("FinalPayment", model);
                    }
                    if (verifyResult.Error.ErrorCode == (int)HttpStatusCode.RequestTimeout)
                    {
                        model.Message = "خطا در پرداخت در صورتی که مبلغی از حساب شما کسر شده باشد ظرف 72 ساعت آینده به حساب شما برگشت داده خواهد شد";
                        return View("FinalPayment", model);
                    }
                }

                if (!verifyResult.Data.Successful.Equals(true))
                {
                    model.Message = "خطا در پرداخت در صورتی که مبلغی از حساب شما کسر شده باشد ظرف 72 ساعت آینده به حساب شما برگشت داده خواهد شد";
                    return View("FinalPayment", model);
                }

                payment.Status = PaymentStatus.Verify;
                await _paymentService.UpdateAsync(payment);
                var statusResult = await _fgmService.CheckPaymentStatus(response.OrderId, response.ServiceId);
                if (statusResult.Error == null)
                    return await UpdateToken(payment, statusResult?.Data.SaleReferenceId, statusResult?.Data.SaleReferenceNumber, lastProfile);

                switch (statusResult.Error.ErrorCode)
                {
                    case (int)ErrorCode.FailedPayment:
                        model.Message = "خطا در پرداخت لطفا دوباره تلاش کنید";
                        return View("FinalPayment", model);
                    case (int)HttpStatusCode.RequestTimeout:
                        model.Message = "خطا در پرداخت در صورتی که مبلغی از حساب شما کسر شده باشد ظرف 72 ساعت آینده به حساب شما برگشت داده خواهد شد";
                        return View("FinalPayment", model);
                }

            }
            else if (response.ResultCode == PaymentResultCode.Success)
            {
                var statusResult = await _fgmService.CheckPaymentStatus(response.OrderId, response.ServiceId);
                if (statusResult.Error == null)
                    return await UpdateToken(payment, statusResult.Data.SaleReferenceId, statusResult.Data.SaleReferenceNumber, lastProfile);

                switch (statusResult.Error.ErrorCode)
                {
                    case (int)ErrorCode.FailedPayment:
                        model.Message = "خطا در پرداخت لطفا دوباره تلاش کنید";
                        return View("FinalPayment", model);
                    case (int)HttpStatusCode.RequestTimeout:
                        model.Message = "خطا در پرداخت در صورتی که مبلغی از حساب شما کسر شده باشد ظرف 72 ساعت آینده به حساب شما برگشت داده خواهد شد";
                        return View("FinalPayment", model);
                }
            }
            model.Message = "خطا در پرداخت لطفا دوباره وارد شوید";
            return View("FinalPayment", model);
        }




        public async Task<IActionResult> UpdateToken(Payment payment, string saleReferenceId, string referenceNumber, Profile p)
        {
            var bankResponseViewModel = new BankResponseViewModel();
            payment.Status = PaymentStatus.Settle;
            payment.SaleReferenceId = saleReferenceId;
            payment.ReferenceNumber = referenceNumber;
            await _paymentService.UpdateAsync(payment);

            if (payment.ProfilePackageId > 0) return await FinalEditPayment(payment);
            //update use status and settle payment
            p.Status = ProfileStatus.SuccessPayment;
            await _profileService.UpdateStatusAsync(p);

            var profile = await _profileService.GetRelatedDataForScopeAsync(payment.ProfileId, p.Type);
            //create access token
            if (profile != null)
            {
                var tokenPayload = new UserSessionInfo
                {
                    ProfileId = profile.Id,
                    ProfileOwnerType = profile.Type,
                    ProfileStatus = profile.Status,
                    AvailableScope = profile.GetAvailableScope(),
                    CurrentStep = profile.GetCurrentStep()
                };
                if (payment.DiscountIssuer != 0) tokenPayload.ServiceId = payment.DiscountIssuer;

                await _cookieHelper.CreateCookie(tokenPayload);

                bankResponseViewModel.IsSuccess = true;
                bankResponseViewModel.Message = "عملیات پرداخت با موفقیت انجام شد، شما می توانید با کلیک بر روی ادامه مراحل ثبت نام، وارد مرحله بعد شوید";
                return View("FinalPayment", bankResponseViewModel);
            }

            bankResponseViewModel.IsSuccess = false;
            bankResponseViewModel.Message = "خطا در پرداخت، با پشتیبانی تماس بگیرید";
            return View("FinalPayment", bankResponseViewModel);
        }

        [HttpGet]
        public IActionResult FinalPayment()
        {
            return View();
        }

        [AuthorizeUserFilterFactory]
        [LogWebRequestFilterFactory(Order = 2)]
        public async Task<IActionResult> FinalEditPayment(Payment payment)
        {
            if (payment.FactorId == null)
                await _offlineTaskService.ScheduleSetFactorAsync(payment.Id, DateTime.Now.AddSeconds(5));
            var p = await _profilePackageService.GetAsync(payment.ProfilePackageId.GetValueOrDefault());
            payment.Package = p;
            return View("FinalEditPayment", payment);
        }
        [HttpPost]
        public IActionResult ReturnToAction(PrimitivePackageType primitivePackageType)
        {

            switch (primitivePackageType)
            {
                case PrimitivePackageType.EditMobileNumberWithShahkar:
                    return RedirectToAction("CheckMobile", "Profile");

                case PrimitivePackageType.EditPrivatePersonInfo:
                    return RedirectToAction("ChangePrivatePersonAsync", "BaseInfo");

                case PrimitivePackageType.EditBankingAccount:
                    return RedirectToAction("Edit", "BankingAccount");
            }


            return RedirectToAction("Index", "Dashboard");

        }


    }
}