﻿using System.Linq;
using System.Net;
using Cerberus.Web.Model;
using Microsoft.AspNetCore.Http;

namespace Cerberus.Web.Extensions
{
    public static class HttpRequestExtensions
    {
        public static string GetUserAgent(this HttpRequest request)
        {
            return request.Headers["User-Agent"].ToString();
        }

        public static IPAddress GetUserIp(this HttpRequest request)
        {
            if (request.Headers["X-Forwarded-For"].FirstOrDefault() != null && IPAddress.TryParse(request.Headers["X-Forwarded-For"].ToString(), out var ip))
                return ip;

            return request.HttpContext.Connection.RemoteIpAddress.MapToIPv4();
        }

        public static UserSessionInfo GetCurrentUserData(this HttpRequest request)
        {
            return request.HttpContext.Items["CurrentUser"] as UserSessionInfo;
        }
    }
}