﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;

namespace Cerberus.Web.Extensions
{
    public static class ProfileStatusExtention
    {
        public static string GetStatus(this ProfileStatus input)
        {
            switch (input)
            {
                case ProfileStatus.Init:
                    return "اولیه";
                case ProfileStatus.SuccessPayment:
                    return "پرداخت موفق ";
                case ProfileStatus.PolicyAccepted:
                    return "در حال تعیین وضعیت";
                case ProfileStatus.PendingValidation:
                    return "در انتظار تایید";
                case ProfileStatus.InvalidInformation:
                    return "اطلاعات ناقص";
                case ProfileStatus.TraceCode:
                    return "سجامی";
                case ProfileStatus.Sejami:
                    return "سجامی";
                    
                default:
                    return "نامشخص";
            }
        }
    }
}
