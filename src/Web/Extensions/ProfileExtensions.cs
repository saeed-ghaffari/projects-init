﻿using System.Linq;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Extensions;
using Cerberus.Domain.Model;
using Cerberus.Utility;
using Cerberus.Web.Enum;

namespace Cerberus.Web.Extensions
{
    public static class ProfileExtensions
    {
        public static CurrentStep GetCurrentStep(this Profile profile)
        {
            if (profile.Payments != null)
            {
                if (profile.Payments?.FirstOrDefault(x => x.Status == PaymentStatus.Settle) == null) return CurrentStep.Init;
            }

            switch (profile.Type)
            {
                case ProfileOwnerType.ForeignPrivatePerson:
                case ProfileOwnerType.IranianPrivatePerson:
                    if (profile.PrivatePerson == null)
                        return CurrentStep.Payment;
                    if (profile.Agent == null)
                        return CurrentStep.BaseInfo;
                    if (!profile.Addresses.Any())
                        return CurrentStep.Agent;
                    if (!profile.TradingCodes.Any())
                        return CurrentStep.AddressInfo;
                    if (profile.JobInfo == null)
                        return CurrentStep.TradingCode;
                    if (profile.FinancialInfo == null)
                        return CurrentStep.JobInfo;
                    if (!profile.Accounts.Any())
                        return CurrentStep.FinancialInfo;

                    break;


                case ProfileOwnerType.IranianLegalPerson:
                    if (profile.LegalPerson == null)
                        return CurrentStep.Payment;
                    if (profile.Addresses == null)
                        return CurrentStep.BaseInfo;
                    if (!profile.TradingCodes.Any())
                        return CurrentStep.AddressInfo;
                    if (!profile.LegalPersonStakeholders.Any())
                        return CurrentStep.TradingCode;
                    if (profile.LegalPersonStakeholders?.FirstOrDefault(x => x.Type == StakeHolderType.TakeAccess) == null)
                        return CurrentStep.TradingCode;
                    if (profile.LegalPersonStakeholders?.FirstOrDefault(x => x.Type == StakeHolderType.Manager) == null)
                        return CurrentStep.AllowTake;
                    if (profile.LegalPersonStakeholders?.FirstOrDefault(x => x.Type == StakeHolderType.OrderAccess) == null)
                        return CurrentStep.Manager;

                    if (!profile.LegalPersonShareholders.Any())
                        return CurrentStep.AllowOrder;
                    if (profile.FinancialInfo == null)
                        return CurrentStep.Shareholder;
                    if (!profile.Accounts.Any())
                        return CurrentStep.FinancialInfo;
                    break;
            }

            return CurrentStep.BankingAccount;
        }

        public static AvailableScope GetAvailableScope(this Profile profile)
        {
            var availableScope = AvailableScope.Non;
            if (profile.Payments?.FirstOrDefault(x => x.Status == PaymentStatus.Settle) == null || profile.Status < ProfileStatus.SuccessPayment)
            {

                availableScope |= AvailableScope.Payment;
                return availableScope;

            }


            //available for all type of profile in all situation
            availableScope = AvailableScope.Address | AvailableScope.FinancialInfo | AvailableScope.Confirm | AvailableScope.TradingCode | AvailableScope.Dashboard;

            //available for all type of profile when related data is not locked

            if ((profile.Accounts == null || !profile.Accounts.Any() || !profile.Accounts.All(x => x.Locked) || !profile.Accounts.Any(x => x.IsDefault)) && profile.Status < ProfileStatus.TraceCode)
                availableScope |= AvailableScope.BankingAccount;

            //available scopes order by profile type
            switch (profile.Type)
            {
                case ProfileOwnerType.ForeignPrivatePerson:
                case ProfileOwnerType.IranianPrivatePerson:
                    availableScope |= AvailableScope.JobInfo;

                    if (profile.PrivatePerson == null || !profile.PrivatePerson.Locked)
                        availableScope |= AvailableScope.BaseInfo;
                    if ((profile.Status < ProfileStatus.Sejami && profile.Agent == null) || (profile.Agent?.IsConfirmed != null))
                        availableScope |= AvailableScope.Agent;

                    if (profile.IsEditBankingAccountSemiSejami() || profile.IsEditPrivatePersonSemiSejami() || profile.IsEditPrivatePersonAndDeleteAgentEighteen() || profile.IsEditBankingAccountAndDeleteAgentEighteen())
                    {
                        availableScope |= AvailableScope.EditBankingAccount;
                        availableScope |= AvailableScope.EditPrivatePerson;
                    }
                    break;


                case ProfileOwnerType.IranianLegalPerson:

                    if (profile.LegalPerson == null || !profile.LegalPerson.Locked)
                        availableScope |= AvailableScope.BaseInfo;

                    availableScope |= AvailableScope.StakeholderTakeAccess;

                    availableScope |= AvailableScope.StakeholderTakeManager;

                    availableScope |= AvailableScope.StakeholderTakeOrder;

                    availableScope |= AvailableScope.StakeholderShareholder;

                    break;
            }

            return availableScope;
        }

        public static (AvailableScope availableScope, string missingDataScopeTxt) GetAvailableScopeForConfirm(this Profile profile, ProfileOwnerType type)
        {
            var availableScope = AvailableScope.Non;

            string missingDataScopeTxt = "";

            switch (type)
            {
                case ProfileOwnerType.IranianPrivatePerson:

                    if (profile.PrivatePerson == null)
                    {
                        availableScope |= AvailableScope.BaseInfo;
                        missingDataScopeTxt += "هویتی ، ";
                    }
                    if (profile.PrivatePerson != null && profile.PrivatePerson.IsConfirmed == true && Tools.IsUnder18Years(profile.PrivatePerson.BirthDate) && profile.Agent == null)
                    {
                        availableScope |= AvailableScope.Agent;
                        missingDataScopeTxt += "نماینده قانونی ، ";
                    }

                    if (!profile.Addresses.Any())
                    {
                        missingDataScopeTxt += "ارتباطی ، ";
                        availableScope |= AvailableScope.Address;
                    }

                    if (profile.FinancialInfo == null)
                    {
                        missingDataScopeTxt += "مالی ، ";
                        availableScope |= AvailableScope.FinancialInfo;
                    }

                    if (profile.JobInfo == null)
                    {
                        missingDataScopeTxt += "شغلی ، ";
                        availableScope |= AvailableScope.JobInfo;
                    }

                    if (!profile.Accounts.Any() || !profile.Accounts.Any(x => x.IsDefault))
                    {
                        missingDataScopeTxt += "بانکی ، ";
                        availableScope |= AvailableScope.BankingAccount;
                    }
                    break;
                case ProfileOwnerType.IranianLegalPerson:
                    if (profile.LegalPerson == null)
                    {
                        availableScope |= AvailableScope.BaseInfo;
                        missingDataScopeTxt += "هویتی ، ";
                    }
                    if (profile.LegalPersonStakeholders.Count(x => x.Type == StakeHolderType.TakeAccess) == 0)
                    {
                        availableScope |= AvailableScope.StakeholderTakeAccess;
                        missingDataScopeTxt += "اشخاص دارای حق برداشت ، ";
                    }

                    if (profile.LegalPersonStakeholders.Count(x => x.Type == StakeHolderType.Manager) < 2)
                    {
                        availableScope |= AvailableScope.StakeholderTakeManager;
                        missingDataScopeTxt += "هیئت مدیره ، ";
                    }

                    if (profile.LegalPersonStakeholders.Count(x => x.Type == StakeHolderType.OrderAccess) == 0)
                    {
                        availableScope |= AvailableScope.StakeholderTakeOrder;
                        missingDataScopeTxt += "افراد دارای حق سفارش ، ";
                    }

                    if (!profile.LegalPersonShareholders.Any())
                    {
                        availableScope |= AvailableScope.StakeholderShareholder;
                        missingDataScopeTxt += "سهامداران ، ";
                    }

                    if (!profile.Addresses.Any())
                    {
                        missingDataScopeTxt += "ارتباطی ، ";
                        availableScope |= AvailableScope.Address;
                    }

                    if (profile.FinancialInfo == null)
                    {
                        missingDataScopeTxt += "مالی ، ";
                        availableScope |= AvailableScope.FinancialInfo;
                    }
                    if (!profile.Accounts.Any() || !profile.Accounts.Any(x => x.IsDefault))
                    {
                        missingDataScopeTxt += "بانکی ، ";
                        availableScope |= AvailableScope.BankingAccount;
                    }
                    break;
                case ProfileOwnerType.ForeignPrivatePerson:
                    break;
                case ProfileOwnerType.ForeignLegalPerson:
                    break;
            }

            return (availableScope, missingDataScopeTxt);

        }


    }
}