﻿using Microsoft.AspNetCore.Mvc;

namespace Cerberus.Web.WebLogic.Delegate
{
    public delegate RedirectToActionResult RedirectToNextStep();
}