﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Cerberus.Domain;
using Cerberus.Domain.Enum;
using Cerberus.Web.Configuration;
using Cerberus.Web.Model;
using Gatekeeper.Core.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Rewrite.Internal.UrlActions;
using Microsoft.Extensions.Options;

namespace Cerberus.Web.WebLogic
{


    public class CookieHelper
    {
        private readonly IJwtTokenService<UserSessionInfo> _jwtTokenService;
        private readonly WebConfiguration _webConfiguration;
        private readonly IHttpContextAccessor _contextAccessor;
        public CookieHelper(IJwtTokenService<UserSessionInfo> jwtTokenService, IOptions<WebConfiguration> webConfiguration, IHttpContextAccessor contextAccessor)
        {

            _jwtTokenService = jwtTokenService;
            _contextAccessor = contextAccessor;
            _webConfiguration = webConfiguration.Value;
        }

        public async Task CreateCookie(UserSessionInfo sessionInfo)
        {
            await KillCookie();

            var token = await _jwtTokenService.CreateAsync(sessionInfo, _webConfiguration.SessionTtl);
            //set access token into user cookie
          
            _contextAccessor.HttpContext.Response.Cookies.Append(ConventionalHelper.SessionCookieName, token, new CookieOptions
            {
                Secure = _webConfiguration.SecureCookie,
                SameSite =  SameSiteMode.Strict,
                Expires = DateTimeOffset.Now.AddHours(2)
            });
            _contextAccessor.HttpContext.Request.HttpContext.Items["CurrentUser"] = sessionInfo;
            _contextAccessor.HttpContext.Request.HttpContext.Items["UserId"] = sessionInfo.ProfileId;
        }

        public async Task KillCookie()
        {
            _contextAccessor.HttpContext.Request.HttpContext.Items["CurrentUser"] = null;
            if (!_contextAccessor.HttpContext.Request.Cookies.TryGetValue(ConventionalHelper.SessionCookieName,
                out var token))
                return;

            await _jwtTokenService.KillAsync(token);
            _contextAccessor.HttpContext.Response.Cookies.Delete(ConventionalHelper.SessionCookieName);
        }
    }

}

