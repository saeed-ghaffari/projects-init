﻿#region Using

using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

#endregion

namespace Cerberus.Web.WebLogic.ModelBinder
{
    public class MultiSelectModelBinder : IModelBinder
    {
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            var valueResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);

            if (!string.IsNullOrEmpty(valueResult.FirstValue))
            {
                var parts = valueResult.FirstValue.Split(',');
                var actualValue = new List<long>(parts.Length);

                foreach (var part in parts.Where(row => row.Length > 0).ToList())
                    if (long.TryParse(part, out var value))
                        actualValue.Add(value);

                bindingContext.ModelState.SetModelValue(bindingContext.ModelName, actualValue.ToArray(), "");
                bindingContext.Result = ModelBindingResult.Success(actualValue);
            }
            else
                bindingContext.Result = ModelBindingResult.Failed();
            return Task.CompletedTask;
        }
    }
}