﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Web.Model;
using Microsoft.AspNetCore.Http;
using StackExchange.Redis;

namespace Cerberus.Web.WebLogic
{
    public static class CaptchaHelper
    {
        public static CaptchaResult GenerateCaptchaImage(int width, int height, string captchaCode)
        {
            using (Bitmap baseMap = new Bitmap(width, height))
            using (Graphics graph = Graphics.FromImage(baseMap))
            {
                Random rand = new Random();

                graph.Clear(GetRandomLightColor());

                DrawCaptchaCode();

                MemoryStream ms = new MemoryStream();

                baseMap.Save(ms, ImageFormat.Png);

                return new CaptchaResult
                {
                    CaptchaCode = captchaCode,
                    CaptchaByteData = ms.ToArray(),
                    Timestamp = DateTime.Now
                };

                int GetFontSize(int imageWidth, int captchCodeCount)
                {
                    var averageSize = imageWidth / captchCodeCount;

                    return Convert.ToInt32(averageSize);
                }


                Color GetRandomLightColor()
                {
                    int low = 180, high = 255;

                    int nRend = rand.Next(high) % (high - low) + low;
                    int nGreen = rand.Next(high) % (high - low) + low;
                    int nBlue = rand.Next(high) % (high - low) + low;

                    return Color.FromArgb(nRend, nGreen, nBlue);
                }

                void DrawCaptchaCode()
                {
                    HatchStyle[] hatchStyles =
                    {
                        HatchStyle.BackwardDiagonal, HatchStyle.Cross,
                        HatchStyle.DashedDownwardDiagonal, HatchStyle.DashedHorizontal,
                        HatchStyle.DashedUpwardDiagonal, HatchStyle.DashedVertical,
                        HatchStyle.DiagonalBrick, HatchStyle.DiagonalCross,
                        HatchStyle.Divot, HatchStyle.DottedDiamond, HatchStyle.DottedGrid,
                        HatchStyle.ForwardDiagonal, HatchStyle.Horizontal,
                        HatchStyle.HorizontalBrick, HatchStyle.LargeCheckerBoard,
                        HatchStyle.LargeConfetti, HatchStyle.LargeGrid,
                        HatchStyle.LightDownwardDiagonal, HatchStyle.LightHorizontal,
                        HatchStyle.LightUpwardDiagonal, HatchStyle.LightVertical,
                        HatchStyle.Max, HatchStyle.Min, HatchStyle.NarrowHorizontal,
                        HatchStyle.NarrowVertical, HatchStyle.OutlinedDiamond,
                        HatchStyle.Plaid, HatchStyle.Shingle, HatchStyle.SmallCheckerBoard,
                        HatchStyle.SmallConfetti, HatchStyle.SmallGrid,
                        HatchStyle.SolidDiamond, HatchStyle.Sphere, HatchStyle.Trellis,
                        HatchStyle.Vertical, HatchStyle.Wave, HatchStyle.Weave,
                        HatchStyle.WideDownwardDiagonal, HatchStyle.WideUpwardDiagonal, HatchStyle.ZigZag
                    };

                    //  SolidBrush fontBrush = new SolidBrush(Color.Black);
                    HatchBrush hatchBrush = new HatchBrush(hatchStyles[rand.Next
                        (hatchStyles.Length - 1)], Color.FromArgb((rand.Next(100, 255)),
                        (rand.Next(100, 255)), (rand.Next(100, 255))), Color.Black);

                    var fontEmSizes = new int[] { 15, 20, 25, 30, 35 };

                    var fontNames = new string[]
                    {
                        "Trebuchet MS",
                        "Arial",
                        "Times New Roman",
                        "Georgia",
                        "Verdana",
                        "Geneva"
                    };

                    FontStyle[] fontStyles =
                    {
                        FontStyle.Bold,
                        FontStyle.Italic,
                        FontStyle.Regular,
                        FontStyle.Strikeout,
                        FontStyle.Underline
                    };

                    int fontSize = GetFontSize(width, captchaCode.Length);
                    Font font = new Font(fontNames[rand.Next(fontNames.Length - 1)],
                        fontEmSizes[rand.Next(fontEmSizes.Length - 1)],
                        fontStyles[rand.Next(fontStyles.Length - 1)]);
                    for (int i = 0; i < captchaCode.Length; i++)
                    {
                        // hatchBrush.BackgroundColor = GetRandomDeepColor();

                        int shiftPx = fontSize / 6;

                        float x = i * fontSize + rand.Next(-shiftPx, shiftPx) + rand.Next(-shiftPx, shiftPx);
                        int maxY = height - fontSize;
                        if (maxY < 0) maxY = 0;
                        float y = rand.Next(0, maxY);

                        graph.DrawString(captchaCode[i].ToString(), font, hatchBrush, x, y);
                    }
                }



            }
        }
    }
}
