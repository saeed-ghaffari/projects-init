﻿using System;

namespace Cerberus.Web.Enum
{
    [Flags]
    public enum AvailableScope
    {
        Non = 0,
        Address = 1,
        Agent = 2,
        BankingAccount = 4,
        BaseInfo = 8,
        Confirm = 16,
        FinancialInfo = 32,
        JobInfo = 64,
        Payment = 128,
        TradingCode = 256,
        StakeholderTakeAccess=512,
        StakeholderTakeManager = 1024,
        StakeholderShareholder=2048,
        StakeholderTakeOrder=4096,
        Factor=8192,
        Dashboard=16384,
        EditBankingAccount= 32768,
        EditPrivatePerson=65536
    }
}