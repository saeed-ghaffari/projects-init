﻿namespace Cerberus.Web.Enum
{
    public enum NotificationType
    {
        Success = 1,
        Warning = 2,
        Error = 3,
        Info = 4
    }
}
