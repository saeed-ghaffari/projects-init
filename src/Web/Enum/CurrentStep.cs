﻿namespace Cerberus.Web.Enum
{
    public enum CurrentStep
    {
        Init = 1,

        Payment = 2,

        BaseInfo = 3,

        Agent = 4,

        AddressInfo = 5,

        TradingCode = 6,

        JobInfo = 7,

        AllowTake = 8,

        Manager = 9,

        Shareholder = 10,

        AllowOrder = 11,

        FinancialInfo = 12,

        InvestmentPurpose =13,

        BankingAccount = 14,

        PolicyAccepted = 15,

    }
}