﻿using System.ComponentModel.DataAnnotations;

namespace Cerberus.Web.Model
{
    public class InitSessionRequest
    {
        [Required]
        [MinLength(11)]
        public string Mobile { get; set; }
    }
}