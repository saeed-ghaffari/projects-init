﻿namespace Cerberus.Web.Model
{
    public class CreateSessionRequest
    {
        public string Otp { get; set; }

        public string Mobile { get; set; }
    }
}