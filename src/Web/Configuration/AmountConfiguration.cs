﻿using System;
using Cerberus.Domain.Enum;

namespace Cerberus.Web.Configuration
{
    public class AmountConfiguration
    {
        public long GetAmountByType(ProfileOwnerType type)
        {
            return  type == ProfileOwnerType.IranianPrivatePerson ? IranianPrivatePerson :
                type == ProfileOwnerType.ForeignPrivatePerson ? ForeignPrivatePerson :
                type == ProfileOwnerType.IranianLegalPerson ? IranianLegalPerson : ForeignLegalPerson;
        }
        public ProfileOwnerType Type { get; set; }
        public long IranianPrivatePerson { get; set; }
        public long ForeignPrivatePerson { get; set; }
        public long IranianLegalPerson { get; set; }
        public long ForeignLegalPerson { get; set; }

        public long GetEditAmountByType(PrimitivePackageType type)
        {
            switch (type)
            {
                case PrimitivePackageType.EditMobileNumber:
                    return EditMobileNumber;
                case PrimitivePackageType.EditMobileNumberWithShahkar:
                    return EditMobileNumberWithShahkar;
                case PrimitivePackageType.EditPrivatePersonInfo:
                    return EditPrivatePersonInfo;
                case PrimitivePackageType.EditBankingAccount:
                  return  EditBankingAccount;
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }
        public PrimitivePackageType PrimitivePackageType{ get; set; }
        public long EditMobileNumber { get; set; }
        public long EditPrivatePersonInfo { get; set; }
        public long EditBankingAccount { get; set; }
        public long EditMobileNumberWithShahkar { get; set; }

    }
}
