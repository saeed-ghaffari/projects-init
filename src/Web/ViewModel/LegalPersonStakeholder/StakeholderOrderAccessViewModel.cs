﻿using Cerberus.Domain.Enum;
using System;
using System.ComponentModel.DataAnnotations;

namespace Cerberus.Web.ViewModel.LegalPersonStakeholder
{
    public class StakeholderOrderAccessViewModel
    {
        public long Id { get; set; }

        [Display(Name = "کد ملی / کد فراگیر")]
        public string UniqueIdentifier { get; set; }

        [Display(Name = "نام")]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }

        [Display(Name = "شروع دوره تصدی")]
        public string StartAt { get; set; }

        [Display(Name = "پایان دوره تصدی")]
        public string EndAt { get; set; }

        public string SignatureFile { get; set; }

        [Display(Name = "سمت")]
        public StakHolderPositionType PositionType { get; set; }
    }
}