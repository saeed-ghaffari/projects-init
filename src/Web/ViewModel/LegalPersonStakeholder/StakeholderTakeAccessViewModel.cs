﻿using System;
using Cerberus.Domain.Enum;
using System.ComponentModel.DataAnnotations;
using Cerberus.Web.WebLogic.ModelBinder;
using Microsoft.AspNetCore.Mvc;

namespace Cerberus.Web.ViewModel.LegalPersonStakeholder
{
    public class StakeholderTakeAccessViewModel
    {
        public long Id { get; set; }

        [Display(Name = "کد ملی / کد فراگیر")]
        public string UniqueIdentifier { get; set; }

        [Display(Name = "نام")]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی")]
        public string LastName { get; set; }

        public  string SignatureFile { get; set; }
        [Display(Name = "سمت")]
        public StakHolderPositionType PositionType { get; set; }


    }
}