﻿using System;
using System.Collections.Generic;
using Cerberus.Domain.Enum;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using Cerberus.Web.WebLogic.ModelBinder;
using Microsoft.AspNetCore.Mvc;

namespace Cerberus.Web.ViewModel.LegalPersonStakeholder
{
    public class CreateStakeholderTakeAccessViewModel
    {
        //TODO:Check UniqueIdentifier Is true

        [Display(Name = "کد ملی / کد فراگیر")]
        [Required(ErrorMessage = " {0} اجباری است")]
        public string UniqueIdentifier { get; set; }

        [Display(Name = "نام")]
        [Required(ErrorMessage = " {0} اجباری است")]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی")]
        [Required(ErrorMessage = " {0} اجباری است")]
        public string LastName { get; set; }

        [Display(Name = "سمت")]
        [Required(ErrorMessage = " {0} اجباری است")]
        [Range((byte)(StakHolderPositionType.Chairman), (byte)(StakHolderPositionType.Auditor), ErrorMessage = "مقدار نامعتبر است")]
        public StakHolderPositionType PositionType { get; set; }

        [Display(Name = "امضاء")]
        [Required(ErrorMessage = "لطفا فایل امضا را بارگذاری نمایید")]
        public IFormFile SignatureFile { get; set; }

        [Display(Name = "تاریخ تولد")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public string BirthDate { get; set; } = null;

        public IEnumerable<StakeholderTakeAccessViewModel> Stakeholders { get; set; }

    }
}