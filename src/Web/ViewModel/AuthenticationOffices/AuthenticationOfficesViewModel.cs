﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;

namespace Cerberus.Web.ViewModel.AuthenticationOffices
{
    public class AuthenticationOfficesViewModel
    {
        [Display(Name = "استان")]
        [MaxLength(10, ErrorMessage = "{0} معتبر نیست")]
        public string ProvinceId { get; set; }

        [Display(Name = "شهر")]
        [MaxLength(10, ErrorMessage = "{0} معتبر نیست")]
        public string CityId { get; set; }



        [Display(Name = "تلفن")]
        [MaxLength(12, ErrorMessage = "{0} معتبر نیست")]
        public string Telephone { get; set; }

        [Display(Name = "آدرس")]
        [MaxLength(50, ErrorMessage = "{0} معتبر نیست")]
        public string Address { get; set; }

        [Display(Name = "نام مسئول")]
        [MaxLength(40, ErrorMessage = "{0} معتبر نیست")]
        public string ResponsibleName { get; set; }

        [Display(Name = "نوع دفتر")]
        [Range((byte)AuthenticationOfficesType.CounterOffice,(byte)AuthenticationOfficesType.AtisazCounterOffice, ErrorMessage = "{0} معتبر نیست")]
        public AuthenticationOfficesType? Type { get; set; }

        [Display(Name = "نام مرکز احراز هویت")]
        [MaxLength(20, ErrorMessage = "{0} معتبر نیست")]
        public string OfficeName { get; set; }

        public int PageNumber { get; set; }

        [Display(Name = "نام بانک")]
        public long? BankId { get; set; }
        public Bank Bank { get; set; }

        [Display(Name = "نوع احراز هویت")]
        [Range((byte)AuthenticationOfficesType.NotInPresence, (byte)AuthenticationOfficesType.Presence, ErrorMessage = "{0} معتبر نیست")]
        public AuthenticationOfficesType? AuthType { get; set; }

    }
}
