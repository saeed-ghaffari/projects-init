﻿using System.ComponentModel.DataAnnotations;
using Cerberus.Domain.CustomValidation;
using Cerberus.Domain.Enum;

namespace Cerberus.Web.ViewModel.PrivatePerson
{
    public class CreatePrivatePersonNativeViewModel
    {

        [Display(Name = "نام")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(40, ErrorMessage = "{0} نمی تواند بیشتر از 40 کارکتر باشد")]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(50, ErrorMessage = "{0} نمی تواند بیشتر از 50 کارکتر باشد")]
        public string LastName { get; set; }

        [Display(Name = "نام پدر")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(40, ErrorMessage = "{0} نمی تواند بیشتر از 40 کارکتر باشد")]
        public string FatherName { get; set; }

        [Display(Name = "جنسیت")]
        [Required(ErrorMessage = "{0} اجباری است")]

        public Gender Gender { get; set; }


        [Display(Name = "حرف سری شناسنامه")]
        [MaxLength(3, ErrorMessage = "حداکثر 3 رقم مجاز است")]
        [RegularExpression(@"^\s*[الفبلدر1234910\s]+\s*$", ErrorMessage = "معتبر نیست")]
        public string SeriShChar { get; set; }


        [Display(Name = "سری شناسنامه")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MaxLength(2, ErrorMessage = "حداکثر 2 رقم مجاز است")]
        public string SeriSh { get; set; }

        [Required(ErrorMessage = "اجباری است")]
        [Display(Name = "سریال شناسنامه")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MaxLength(6, ErrorMessage = "حداکثر 6 رقم مجاز است")]
        public string Serial { get; set; }

        [Display(Name = "شماره شناسنامه")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MaxLength(10, ErrorMessage = "حداکثر 10 رقم مجاز است")]
        public string ShNumber { get; set; }


        [Display(Name = "تاریخ تولد")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [PersianMinDatetimeValidation(ErrorMessage = "{0} معتبر نیست")]
        public string BirthDate { get; set; }

        [Display(Name = "محل صدور")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(30, ErrorMessage = "{0} نمی تواند بیشتر از 30 کارکتر باشد")]
        public string PlaceOfIssue { get; set; }

        [Display(Name = "محل تولد")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(30, ErrorMessage = "{0} نمی تواند بیشتر از 30 کارکتر باشد")]
        public string PlaceOfBirth { get; set; }

        [Display(Name = "کد ملی")]
        public string UniqueIdentifier { get; set; }
    }
}