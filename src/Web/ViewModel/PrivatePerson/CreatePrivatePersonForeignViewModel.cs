﻿using Cerberus.Domain.Enum;
using System.ComponentModel.DataAnnotations;

namespace Cerberus.Web.ViewModel.PrivatePerson
{
    public class CreatePrivatePersonForeignViewModel
    {

        [Display(Name = "نام")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public string LastName { get; set; }

        [Display(Name = "نام پدر")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public string FatherName { get; set; }

        [Display(Name = "جنسیت")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public Gender Gender { get; set; }

        [Display(Name = "تاریخ تولد")]
        [Required(ErrorMessage = "{0} اجباری است")]
      
        public string BirthDate { get; set; }

        [Display(Name = "کشور محل تولد")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public long BornCountryId { get; set; }

        [Display(Name = "کشور صادر کننده مدرک")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public long? EvidenceCountryId { get; set; }

        [Display(Name = "نوع مدرک")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public EvidenceType EvidenceType { get; set; }

        [Display(Name = "تاریخ انقضاء مدرک")]
        [Required(ErrorMessage = "{0} اجباری است")]
      
        public string EvidenceExpirationDate { get; set; }

        [Display(Name = "شماره مدرک ارائه شده")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public string EvidenceNumber { get; set; }

        [Display(Name = "شماره شناسایی(گذرنامه)")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public string PassportNumber { get; set; }

        [Display(Name = "تابعیت")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public long? CitizenCountryId { get; set; }

        [Display(Name = "شماره اختصاصی")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public string SpecialNumber { get; set; }

        [Display(Name = "شماره مجوز سرمایه گذاری صادره از سازمان")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public string LicenseNumber { get; set; }

        [Display(Name = "تاریخ صدور مجوز")]
       
        public string LicenseIssueDate { get; set; }

        [Display(Name = "تاریخ انقضاء مجوز")]
       
        public string LicenseExpirationDate { get; set; }

        [Display(Name = "توضیحات")]
        public string Description { get; set; }
    }
}