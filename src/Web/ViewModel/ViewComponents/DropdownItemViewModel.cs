﻿namespace Cerberus.Web.ViewModel.ViewComponents
{
    public class DropdownItemViewModel
    {
        public long Value { get; set; }
        public string Text { get; set; }
    }
}
