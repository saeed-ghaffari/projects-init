﻿using System.ComponentModel.DataAnnotations;
using Cerberus.Domain.Enum;
using Cerberus.Web.WebLogic.CustomAttribute;

namespace Cerberus.Web.ViewModel.Profile
{
    public class CreateProfileViewModel
    {
        public string Msisdn { get; set; }

        [Display(Name = "کد ملی /شناسه ملی")]
        [Required(ErrorMessage = "{0} اجباری است")]
        //[StringLength(11 ,ErrorMessage = " {0} باید حداقل ده رقمی باشد .", MinimumLength =10)]
       // [NationalCode(ErrorMessage = "{0} اشتباه وارد شده است")]
        public string UniqueIdentifier { get; set; }

        [Display(Name = "نوع مشتری")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [Range((int)(ProfileOwnerType.IranianPrivatePerson), (int)(ProfileOwnerType.IranianLegalPerson), ErrorMessage = "مقادیر ورودی نامعتبر است")]
        public ProfileOwnerType Type { get; set; }

        [Display(Name = "کد تایید")]
        [Required(ErrorMessage = "{0} اجباری است")]
        // todo: fix validation message on length of 5
        [RegularExpression(@"[0-9]{5}", ErrorMessage = " {0} خود را صحیح وارد نمایید")]

        public string Otp { get; set; }

    }
}