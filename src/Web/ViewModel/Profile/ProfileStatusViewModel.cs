﻿using Cerberus.Domain.Enum;

namespace Cerberus.Web.ViewModel.Profile
{
    public class ProfileStatusViewModel
    {
        public long ProfileId { get; set; }

        public ProfileStatusViewModel ProfileStatus { get; set; }

        public ProfileOwnerType ProfileOwnerType { get; set; }
    }
}