﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;

namespace Cerberus.Web.ViewModel.Profile
{
    public class FinalProfileViewModel
    {
        public long ProfilePackageId { get; set; }
        public PackageType PackageType { get; set; }

    }
}
