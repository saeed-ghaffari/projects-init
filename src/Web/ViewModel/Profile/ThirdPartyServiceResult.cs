﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Web.ViewModel.Profile
{
    public class ThirdPartyServiceResult
    {
        public long ServiceId { get; set; }
        public bool HasMembership { get; set; }
    }
}
