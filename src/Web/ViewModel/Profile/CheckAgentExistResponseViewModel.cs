﻿namespace Cerberus.Web.ViewModel.Profile
{
    public class CheckAgentExistResponseViewModel
    {
        public long AgentProfileId { get; set; }
        public long Mobile { get; set; }
        public bool IsExist { get; set; }
        public string SessionKey { get; set; }
    }
}