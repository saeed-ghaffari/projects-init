﻿using System;
using Cerberus.Domain.Enum;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cerberus.Web.ViewModel.LegalPersonShareholder
{
    public class CreateShareholderViewModel
    {
        //TODO:Check UniqueIdentifier Is true

        [Display(Name = "کد ملی / کد فراگیر")]
        [Required(ErrorMessage = " {0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MaxLength(12, ErrorMessage = "حداکثر 12 رقم مجاز است")]
        public string UniqueIdentifier { get; set; }

        [Display(Name = "نام")]
        [Required(ErrorMessage = " {0}  اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(50, ErrorMessage = "{0} نمی تواند بیشتر از 50 کارکتر باشد")]
        public string FirstName { get; set; }

        [Display(Name = "نام خانوادگی")]
        [Required(ErrorMessage = " {0} اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(50, ErrorMessage = "{0} نمی تواند بیشتر از 50 کارکتر باشد")]
        public string LastName { get; set; }

        //TODO:Check PostalCode Is true
        [Display(Name = "کد پستی")]
        [Required(ErrorMessage = " {0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MaxLength(10, ErrorMessage = "{0} مجاز نمی باشد")]
        public string PostalCode { get; set; }

        [Display(Name = "نشانی")]
        [Required(ErrorMessage = " {0} اجباری است")]
        [MinLength(10, ErrorMessage = "{0} مجاز نمی باشد")]
        [MaxLength (200, ErrorMessage = "{0} مجاز نمی باشد")]
        public string Address { get; set; }

        [Display(Name = "سمت")]
        [Required(ErrorMessage = " {0} اجباری است")]
        [Range((byte)(StakHolderPositionType.Chairman), (byte)(StakHolderPositionType.Auditor), ErrorMessage = "مقدار نامعتبر است")]
        public StakHolderPositionType PositionType { get; set; }

        [Display(Name = "درصد سهامداری یا حق رای")]
        [Required(ErrorMessage = " {0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [Range(0,100,ErrorMessage = "{0} مجاز نمی باشد")]
        public int? PercentageVotingRight { get; set; }

        public IEnumerable<ShareholderViewModel> Shareholders { get; set; }
    }
}