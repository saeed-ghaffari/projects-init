﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cerberus.Web.ViewModel.TraceCode
{
    public class RecoverTraceCodeViewModel
    {
        [Display(Name = "شماره تلفن همراه")]
        [Required(ErrorMessage = "{0} اجباری است ")]
        [RegularExpression(@"9(1[0-9]|3[1-9]|2[1-9])-?[0-9]{3}-?[0-9]{4}", ErrorMessage = "لطفا شماره تلفن همراه خود را صحیح وارد نمایید")]
        public long Mobile { get; set; }

        [Display(Name = "کد ملی /شناسه ملی")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public string UniqueIdentifier { get; set; }
    }
}
