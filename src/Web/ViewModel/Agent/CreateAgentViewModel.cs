﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cerberus.Domain.CustomValidation;
using Cerberus.Domain.Enum;
using Cerberus.Web.WebLogic.ModelBinder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Cerberus.Web.ViewModel.Agent
{
    public class CreateAgentViewModel
    {

        public long? Id { get; set; }

        [Display(Name = "کد تایید")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [MaxLength(5, ErrorMessage = "{0} معتبر نیست")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  معتبر نیست")]
        public string OtpCode { get; set; }
        [Display(Name = "کد ملی")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [MaxLength(10, ErrorMessage = "{0} معتبر نیست")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0}  معتبر نیست")]
        public string UniqueIdentifier { get; set; }
        [Required(ErrorMessage = "نوع نماینده اجباری می باشد .")]
        [Range((int)(AgentType.Attorney), (int)(AgentType.Prescriptive), ErrorMessage = "مقادیر ورودی نامعتبر است")]
        public AgentType Type { get; set; }
        [Display(Name = "تاریخ انقضاء نمایندگی")]
        [PersianMaxDatetimeValidation(ErrorMessage = "{0} معتبر نیست")]
        public string ExpirationDate { get; set; }

        [Required(ErrorMessage = "بارگذاری نوع مدرک اجباری است")]
        [Display(Name = "انتخاب فایل")]
        public IEnumerable<IFormFile> AgentFiles { get; set; }

        public string Description { get; set; }
        public bool? IsConfirmed { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}