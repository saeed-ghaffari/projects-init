﻿namespace Cerberus.Web.ViewModel.Payment
{
    public class BankResponseViewModel
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}