﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cerberus.Domain.Enum;
using Cerberus.Web.ViewModel.ViewComponents;
using Cerberus.Web.WebLogic.CustomAttribute;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Cerberus.Web.ViewModel.BankingAccount
{
    public class CreateBankingAccountViewModel
    {

        public long Id { get; set; }
        public long ProfileId { get; set; }

        [Display(Name = "نام صاحب حساب")]
        public string Owner { get; set; }

        [Display(Name = "شماره حساب")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MaxLength(20, ErrorMessage = "{0} مجاز نمی باشد")]
        public string AccountNumber { get; set; }

        [Display(Name = "نوع شماره حساب")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [Range((byte)(BankingAccountType.LongTermAccount), ((byte)BankingAccountType.SavingAccount), ErrorMessage = "مقدار نامعتبر است")]
        public BankingAccountType? Type { get; set; }

        [Display(Name = "شماره شبا")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [MaxLength(26, ErrorMessage = "{0} مجاز نمی باشد")]
        [MinLength(26, ErrorMessage = "{0} مجاز نمی باشد")]
        [RegularExpression("^[0-9 IR]*$", ErrorMessage = "فقط اعداد انگلیسی مجاز است")]
        [Sheba(ErrorMessage = "{0} معتبر نمی باشد")]
        public string Sheba { get; set; }

        [Display(Name = "نام بانک")]
        [BindRequired]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "{0} معتبر نمی باشد")]
        public long? BankId { get; set; }

        [Display(Name = "کد شعبه")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "فقط عدد مجاز است")]
        [MaxLength(8, ErrorMessage = "{0} مجاز نمی باشد")]
        public string BranchCode { get; set; }

        [Display(Name = "نام شعبه")]
        [Required(ErrorMessage = "{0} اجباری است")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(60, ErrorMessage = "{0} نمی تواند بیشتر از 60 کارکتر باشد")]
        public string BranchName { get; set; }

        [Display(Name = "شهر شعبه بانک")]
        [Required(ErrorMessage = "{0} اجباری است")]
        public long? BranchCityId { get; set; }

        [Display(Name = "حساب پیش فرض")]
        [Required(ErrorMessage = "{0}اجباری است")]
        public bool IsDefault { get; set; }

        public bool LockAccount { get; set; } = false;
        public IEnumerable<Domain.Model.BankingAccount> BankingAccounts { get; set; }

    }
}