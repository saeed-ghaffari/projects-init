﻿using System.ComponentModel.DataAnnotations;
using Cerberus.Domain.Enum;
using Cerberus.Utility;

namespace Cerberus.Web.ViewModel.BankingAccount
{
    public class BankingAccountViewModel
    {
        public long Id { get; set; }

        [Display(Name = "شماره حساب")]
        public string AccountNumber { get; set; }

        [Display(Name = "نام صاحب حساب")]
        public  string Owner { get; set; }

        [Display(Name = "نوع شماره حساب")]
        public string TypeName =>Type.GetEnumDescription();

        public BankingAccountType? Type { get; set; }

        [Display(Name = "شماره شبا")]
        public string Sheba { get; set; }

        [Display(Name = "نام بانک")]
        public string BankName { get; set; }

        [Display(Name = "کد شعبه")]
        public string BranchCode { get; set; }

        [Display(Name = "نام شعبه")]
        public string BranchName { get; set; }

        [Display(Name = "شهر شعبه بانک")]
        public long? BranchCityName { get; set; }

        [Display(Name = "حساب پیش فرض")]
        public bool IsDefault { get; set; }

    }
}
