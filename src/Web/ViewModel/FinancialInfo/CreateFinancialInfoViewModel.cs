﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Domain.CustomValidation;
using Cerberus.Domain.Enum;
using Cerberus.Web.WebLogic.ModelBinder;
using Microsoft.AspNetCore.Identity.UI.Pages;
using Microsoft.AspNetCore.Mvc;

namespace Cerberus.Web.ViewModel.FinancialInfo
{
    public class CreateFinancialInfoViewModel
    {
        [NotMapped]
        public ProfileOwnerType ProfileOwnerType { get; set; }
        [Display(Name = "ارزش روز دارایی های تحت مالکیت شما تقریبا چقدر است؟ ")]
        [RegularExpression(@"^\s*[,1234567890\s]+\s*$", ErrorMessage = "مقدار نامعتبر است")]
        public long? AssetsValue { get; set; }

        [Display(Name = "متوسط درآمد ماهیانه شما از مشاغل و منابع مختلف در حال حاضر چقدر است؟ ")]
        [RegularExpression(@"^\s*[,1234567890\s]+\s*$", ErrorMessage = "مقدار نامعتبر است")]
        public long? InComingAverage { get; set; }

        [Display(Name = "بورس اوراق بهادار و فرابورس ")]
        [RegularExpression(@"^\s*[,1234567890\s]+\s*$", ErrorMessage = "مقدار نامعتبر است")]
        public long? SExchangeTransaction { get; set; }


        [Display(Name = " بورس های کالایی ")]
        [RegularExpression(@"^\s*[,1234567890\s]+\s*$", ErrorMessage = "مقدار نامعتبر است")]
        public long? CExchangeTransaction { get; set; }

        [Display(Name = "بورس های خارج از کشور ")]
        [RegularExpression(@"^\s*[,1234567890\s]+\s*$", ErrorMessage = "مقدار نامعتبر است")]
        public long? OutExchangeTransaction { get; set; }

        [Required(ErrorMessage = "پیش بینی سطح ارزش ریالی معاملات شما در بازار سرمایه اجباری است.")]
        [Display(Name = "پیش بینی می کنید سطح معاملات شما در بازار سرمایه ایران طی یک سال چه مبلغی می باشد؟")]
        [Range((byte)(Domain.Enum.TransactionLevel.One), ((byte)(Domain.Enum.TransactionLevel.Fifteen)), ErrorMessage = "مقدار ورودی نامعتبر است")]
        //  [RegularExpression("@([1-5]|1[1-5])", ErrorMessage = "مقدار ورودی نامعتبر است")]
        public TransactionLevel? TransactionLevel { get; set; }


        [Display(Name = "میزان آشنایی شما با مفاهیم مالی و سرمایه گذاری در اوراق بهادار چه میزان است؟")]
        [Range((byte)(Domain.Enum.TradingKnowledgeLevel.Excellent), ((byte)(Domain.Enum.TradingKnowledgeLevel.VeryLow)), ErrorMessage = "مقدار ورودی نامعتبر است")]
        public TradingKnowledgeLevel? TradingKnowledgeLevel { get; set; }

        [Display(Name = "نام شرکت / شرکت های کارگزاری در ایران که شما از طریق آنان مبادرت به معامله نموده اید را وارد فرمایید:")]
        [ModelBinder(BinderType = typeof(MultiSelectModelBinder))]
        public IEnumerable<long> BrokerIds { get; set; }



        [Display(Name = "منبع تأمین وجه سرمایه گذاری و هدف مشتری از انجام معاملات در بازار سرمایه ایران یا بورس های کالای ایرانی را بطور کامل بیان کنید")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(30, ErrorMessage = "{0} نمی تواند بیشتر از 30 کارکتر باشد")]
        public string CompanyPurpose { get; set; }

        [Display(Name = "نام مرجع رتبه بندی کننده")]
        [RegularExpression(@"^\s*[چجحخهعغفقثصضگکمنتالبیسشوئدذؤةرژزطظءأي ك پۀآ,\s]+\s*$", ErrorMessage = "فقط حروف فارسی مجاز است")]
        [MaxLength(30, ErrorMessage = "{0} نمی تواند بیشتر از 30 کارکتر باشد")]
        public string ReferenceRateCompany { get; set; }

        [Display(Name = "تاریخ رتبه بندی")]
        [PersianMinDatetimeValidation(ErrorMessage = "{0} معتبر نیست")]
        public string RateDate { get; set; }
        [Display(Name = "رتبه اخذ شده")]
        [RegularExpression(@"^\s*[1234567890\s]+\s*$", ErrorMessage = "مقدار نامعتبر است")]
        [Range(Int32.MinValue, Int32.MaxValue, ErrorMessage = "{0} معتیر نیست")]
        public long? Rate { get; set; }

        public byte? CheckRate { get; set; }
    }
}