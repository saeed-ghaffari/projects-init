﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Model;

namespace Cerberus.Web.ViewModel.Factor
{
    public class FactorViewModel
    {
        public long Amount{ get; set; }
        public string Date { get; set; }
        public string SaleReferenceId { get; set; }

        public string Name { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string UniqueIdentifier { get; set; }

        public string PostalCode { get; set; }
        public string Province { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string Alley { get; set; }
        public string Plaque { get; set; }
        public long Discount { get; set; }

        public string ThirdPartyTitle { get; set; }
        public string InvoiceNumber { get; set; }

        public ProfilePackage ProfilePackage { get; set; }

    }
}
