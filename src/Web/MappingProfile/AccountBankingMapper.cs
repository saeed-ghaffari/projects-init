﻿using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Membership;

namespace Cerberus.Web.MappingProfile
{
    public class AccountBankingMapper : AutoMapper.Profile
    {
        public AccountBankingMapper()
        {
            CreateMap<BankingAccountMembership, BankingAccount>()
                .ForAllMembers(opts => opts.Condition(x => x != null));
        }
    }
}