﻿using Cerberus.Domain.Model;
using Cerberus.Utility;
using Cerberus.Web.ViewModel.FinancialInfo;

namespace Cerberus.Web.MappingProfile
{
    public class FinancialInfoProfile : AutoMapper.Profile
    {
        public FinancialInfoProfile()
        {
            CreateMap<CreateFinancialInfoViewModel, FinancialInfo>()
                .ForMember(dis => dis.RateDate, opt => opt.MapFrom(s => s.RateDate.ToGeorgianDate())); 
            CreateMap<FinancialInfo, CreateFinancialInfoViewModel>()
                .ForMember(dis => dis.RateDate, opt => opt.MapFrom(s => s.RateDate.ToPersianDate("yyyy/MM/dd"))); 
        }
    }
}