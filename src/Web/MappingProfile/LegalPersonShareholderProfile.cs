﻿using Cerberus.Domain.Model;
using Cerberus.Web.ViewModel.LegalPersonShareholder;

namespace Cerberus.Web.MappingProfile
{
    public class LegalPersonShareholderProfile : AutoMapper.Profile
    {
        public LegalPersonShareholderProfile()
        {
            CreateMap<LegalPersonShareholder, ShareholderViewModel>();
            CreateMap<ShareholderViewModel, LegalPersonShareholder>();
            CreateMap<CreateShareholderViewModel, LegalPersonShareholder>();
        }
    }
}