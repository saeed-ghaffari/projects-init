﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Model;
using Cerberus.Web.ViewModel.AuthenticationOffices;
using Cerberus.Web.ViewModel.BankingAccount;

namespace Cerberus.Web.MappingProfile
{
    public class AuthenticationOfficesMapper : AutoMapper.Profile
    {
        public AuthenticationOfficesMapper()
        {
            CreateMap<AuthenticationOffices, AuthenticationOfficesViewModel>()
             .ForMember(dis => dis.ProvinceId, opt => opt.MapFrom(s => s.Province.Name))
                .ForMember(dis => dis.CityId, opt => opt.MapFrom(s => s.City.Name));

            CreateMap<AuthenticationOfficesViewModel, AuthenticationOffices >()
                ;
        }
    }
}
