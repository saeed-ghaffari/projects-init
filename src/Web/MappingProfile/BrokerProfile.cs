﻿using Cerberus.Domain.Model;
using Cerberus.Web.ViewModel.ViewComponents;

namespace Cerberus.Web.MappingProfile
{
    public class BrokerProfile : AutoMapper.Profile
    {
        public BrokerProfile()
        {
            CreateMap<Broker, DropdownItemViewModel>()
                .ForMember(dis => dis.Text, opt => opt.MapFrom(s => s.Title))
                .ForMember(dis => dis.Value, opt => opt.MapFrom(s =>s.Id));
        }
    }
}