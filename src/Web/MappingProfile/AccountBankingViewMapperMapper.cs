﻿using Cerberus.Domain.Model;
using Cerberus.Web.ViewModel.BankingAccount;

namespace Cerberus.Web.MappingProfile
{
    public class AccountBankingViewMapperMapper : AutoMapper.Profile
    {
        public AccountBankingViewMapperMapper()
        {
            CreateMap<BankingAccount, CreateBankingAccountViewModel>()
                 .ForAllMembers(opts => opts.Condition(x => x != null)); ;
        }
    }
}