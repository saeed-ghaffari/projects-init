﻿using Cerberus.Domain.Model;
using Cerberus.Utility;
using Cerberus.Web.ViewModel.JobInfo;

namespace Cerberus.Web.MappingProfile
{
    public class JobInfoProfile : AutoMapper.Profile
    {
        public JobInfoProfile()
        {
            CreateMap<CreateJobInfoViewModel, JobInfo>()
                .ForMember(dis => dis.EmploymentDate, opt => opt.MapFrom(s => s.EmploymentDate.ToGeorgianDate()));



            CreateMap<JobInfo, CreateJobInfoViewModel>()
                .ForMember(dis => dis.EmploymentDate, opt => opt.MapFrom(s => s.EmploymentDate.ToPersianDate("yyyy/MM/dd")));
        }
    }
}