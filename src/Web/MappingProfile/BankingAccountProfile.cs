﻿using Cerberus.Domain.Model;
using Cerberus.Web.ViewModel.BankingAccount;

namespace Cerberus.Web.MappingProfile
{
    public class BankingAccountProfile : AutoMapper.Profile
    {
        public BankingAccountProfile()
        {
            CreateMap<CreateBankingAccountViewModel, BankingAccount>();

            CreateMap<BankingAccount, BankingAccountViewModel>()
                .ForMember(dis => dis.BranchCityName, opt => opt.MapFrom(s => s.BranchCity.Name))
                .ForMember(dis => dis.BankName, opt => opt.MapFrom(s => s.Bank.Name));
        }
    }
}