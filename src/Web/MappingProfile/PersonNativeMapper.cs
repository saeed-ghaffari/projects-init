﻿using System;
using AutoMapper;
using Cerberus.Domain.Model.Membership;
using Cerberus.Utility;
using Cerberus.Web.ViewModel.PrivatePerson;

namespace Cerberus.Web.MappingProfile
{
    public class PersonNativeMapper : AutoMapper.Profile
    {
        public PersonNativeMapper()
        {
            CreateMap<PrivatePersonMembership, CreatePrivatePersonNativeViewModel>()
                .ForMember(dis => dis.FirstName, opt => opt.MapFrom(s => s.FirstName.Unify()))
                .ForMember(dis => dis.LastName, x => x.MapFrom(s => s.LastName.Unify()))
                .ForMember(dis => dis.FatherName, opt => opt.MapFrom(s => s.FatherName.Unify()))
                .ForMember(dis => dis.PlaceOfBirth, x => x.MapFrom(s => s.PlaceOfBirth.Unify()))
                .ForMember(dis => dis.PlaceOfIssue, opt => opt.MapFrom(s => s.PlaceOfIssue.Unify()))
                .ForMember(dis => dis.BirthDate, opt => opt.MapFrom(s => s.BirthDate.ToPersianDate("yyyy/MM/dd")))

                .ForMember(dis => dis.ShNumber, x => x.MapFrom(s => s.ShNumber.Unify()));
        }
    }
}