﻿using Cerberus.Domain.Model;
using Cerberus.Web.ViewModel.TradingCode;

namespace Cerberus.Web.MappingProfile
{
    public class TradingCodeProfile : AutoMapper.Profile
    {
        public TradingCodeProfile()
        {
            CreateMap<CreateTradingCodeViewModel, TradingCode>();

            CreateMap<TradingCode, TradingCodeViewModel>();
        }
    }
}