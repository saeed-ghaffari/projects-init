﻿using Cerberus.Domain.Model;
using Cerberus.Web.ViewModel.ViewComponents;

namespace Cerberus.Web.MappingProfile
{
    public class ProvinceProfile : AutoMapper.Profile
    {
        public ProvinceProfile()
        {
            CreateMap<Province, DropdownItemViewModel>()
                .ForMember(dis => dis.Text, opt => opt.MapFrom(s => s.Name))
                .ForMember(dis => dis.Value, opt => opt.MapFrom(s =>s.Id));
        }
    }
}