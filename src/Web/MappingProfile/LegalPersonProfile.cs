﻿using Cerberus.Domain.Model;
using Cerberus.Utility;
using Cerberus.Web.ViewModel.LegalPerson;

namespace Cerberus.Web.MappingProfile
{
    public class LegalPersonProfile : AutoMapper.Profile
    {
        public LegalPersonProfile()
        {
            CreateMap<CreateLegalPersonViewModel, LegalPerson>()
                .ForMember(dis => dis.RegisterDate, opt => opt.MapFrom(s => s.RegisterDate.ToGeorgianDate()));
            CreateMap<LegalPerson, CreateLegalPersonViewModel>()
                .ForMember(dis => dis.RegisterDate, opt => opt.MapFrom(s => s.RegisterDate.ToPersianDate("yyyy/MM/dd")));
        }
    }
}