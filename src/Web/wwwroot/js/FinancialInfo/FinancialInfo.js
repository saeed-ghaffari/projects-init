﻿

$(function () {

    var assetsValue = $("#AssetsValue").val();
    var inComingAverage = $("#InComingAverage").val();
    var sExchangeTransaction = $("#SExchangeTransaction").val();
    var cExchangeTransaction = $("#CExchangeTransaction").val();
    var outExchangeTransaction = $("#OutExchangeTransaction").val();
    var valueTrade = $("#ValueTrade").val();
    var valueGoodsTrade = $("#ValueGoodsTrade").val();
    var valueOutsideIranTrade = $("#ValueOutsideIranTrade").val();
    



    $("#AssetsValue").val(addCommas(assetsValue));
    $("#InComingAverage").val(addCommas(inComingAverage));
    $("#SExchangeTransaction").val(addCommas(sExchangeTransaction));
    $("#CExchangeTransaction").val(addCommas(cExchangeTransaction));
    $("#OutExchangeTransaction").val(addCommas(outExchangeTransaction));
    $("#ValueTrade").val(addCommas(valueTrade));
    $("#ValueGoodsTrade").val(addCommas(valueGoodsTrade));
    $("#ValueOutsideIranTrade").val(addCommas(valueOutsideIranTrade));
    


    $(".btn-main").click(function () {
       
        if ($("#AssetsValue").val()!==undefined)  $("#AssetsValue").val($("#AssetsValue").val().split(',').join(''));
        if ($("#InComingAverage").val() !== undefined)  $("#InComingAverage").val($("#InComingAverage").val().split(',').join(''));
        if ($("#SExchangeTransaction").val() !== undefined)  $("#SExchangeTransaction").val($("#SExchangeTransaction").val().split(',').join(''));
        if ($("#CExchangeTransaction").val() !== undefined) $("#CExchangeTransaction").val($("#CExchangeTransaction").val().split(',').join(''));
        if ($("#OutExchangeTransaction").val() !== undefined) $("#OutExchangeTransaction").val($("#OutExchangeTransaction").val().split(',').join(''));
        if ($("#ValueTrade").val()!==undefined) $("#ValueTrade").val($("#ValueTrade").val().split(',').join(''));
        if ($("#ValueGoodsTrade").val()!==undefined) $("#ValueGoodsTrade").val($("#ValueGoodsTrade").val().split(',').join(''));
        if ($("#ValueOutsideIranTrade").val()!==undefined) $("#ValueOutsideIranTrade").val($("#ValueOutsideIranTrade").val().split(',').join(''));
      
    });

    $('#AssetsValue').keyup(function (event) {

        if (event.which >= 37 && event.which <= 40) return;
        $(this).val(function (index, value) {
            return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                ;
        });
    });
    $('#InComingAverage').keyup(function (event) {

        if (event.which >= 37 && event.which <= 40) return;
        $(this).val(function (index, value) {
            return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                ;
        });
    });
    $('#SExchangeTransaction').keyup(function (event) {

        if (event.which >= 37 && event.which <= 40) return;
        $(this).val(function (index, value) {
            return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                ;
        });
    });
    $('#CExchangeTransaction').keyup(function (event) {

        if (event.which >= 37 && event.which <= 40) return;
        $(this).val(function (index, value) {
            return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                ;
        });
    });
    $('#OutExchangeTransaction').keyup(function (event) {

        if (event.which >= 37 && event.which <= 40) return;
        $(this).val(function (index, value) {
            return value
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                ;
        });
    });

    $('#ValueTrade').keyup(function (event) {

        if (event.which >= 37 && event.which <= 40) return;
        $(this).val(function (index, value) {
            return value
                    .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                ;
        });
    });
    $('#ValueGoodsTrade').keyup(function (event) {

        if (event.which >= 37 && event.which <= 40) return;
        $(this).val(function (index, value) {
            return value
                    .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                ;
        });
    });

    $('#ValueOutsideIranTrade').keyup(function (event) {

        if (event.which >= 37 && event.which <= 40) return;
        $(this).val(function (index, value) {
            return value
                    .replace(/\D/g, "")
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                ;
        });
    });
  




        if ($("#TransactionLevel").val() !== "") {
            if ($("#ReferenceRateCompany").val() !== "" || $("#RateDate").val() !== "" || $("#Rate").val() !== "") {
                $("#CheckRate").val("1");
                $("#CheckRate").trigger("click");
            } else {
                $("#CheckRate").val("2");
                $("#CheckRate").trigger("click");
            }
        } else {

            $("#CheckRate").val("1");
            $("#CheckRate").trigger("click");
        }
        if ($("#CheckRate").val() === "1") {

            $("#1").removeClass("hidden");
            $("#2").removeClass("hidden");
            $("#3").removeClass("hidden");
            $('#ReferenceRateCompany').prop('disabled', false);
            $('#ReferenceRateCompany').removeClass('disabletext');
            $('#RateDate').prop('disabled', false);
            $('#RateDate').removeClass('disabletext');
            $('#Rate').prop('disabled', false);
            $('#Rate').removeClass('disabletext');
        } else {
            $("#ReferenceRateCompany").val('');
            $("#RateDate").val('');
            $("#Rate").val('');
            $("#1").addClass("hidden");
            $("#2").addClass("hidden");
            $("#3").addClass("hidden");
            $('#ReferenceRateCompany').prop('disabled', true);
            $('#ReferenceRateCompany').addClass('disabletext');
            $('#RateDate').prop('disabled', true);
            $('#RateDate').addClass('disabletext');
            $('#Rate').prop('disabled', true);
            $('#Rate').addClass('disabletext');
        }


        $("#CheckRate").change(function () {
            if ($("#CheckRate").val() === "1") {

                $("#1").removeClass("hidden");
                $("#2").removeClass("hidden");
                $("#3").removeClass("hidden");
                $('#ReferenceRateCompany').prop('disabled', false);
                $('#ReferenceRateCompany').removeClass('disabletext');
                $('#RateDate').prop('disabled', false);
                $('#RateDate').removeClass('disabletext');
                $('#Rate').prop('disabled', false);
                $('#Rate').removeClass('disabletext');
            } else {
                $("#ReferenceRateCompany").val('');
                $("#RateDate").val('');
                $("#Rate").val('');
                $("#1").addClass("hidden");
                $("#2").addClass("hidden");
                $("#3").addClass("hidden");
                $('#ReferenceRateCompany').prop('disabled', true);
                $('#ReferenceRateCompany').addClass('disabletext');
                $('#RateDate').prop('disabled', true);
                $('#RateDate').addClass('disabletext');
                $('#Rate').prop('disabled', true);
                $('#Rate').addClass('disabletext');
            }
    });



     $(".btn-main").click(function () {
 
       if ($("#CheckRate").val() === "1" &&
        ($("#ReferenceRateCompany").val().trim() === "" ||
            $("#RateDate").val().trim() === "" ||
            $("#Rate").val().trim() === "")) {
            event.preventDefault();
            swalMessage(
                "در صورتی که مشتری از طرف مرجعی مورد رتبه بندی قرار گرفته باشد، نام مرجع رتبه بندی، تاریخ رتبه بندی و رتبه اخذ شده اجباری می باشد",
                "error");
        }
    });  
});




function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}


