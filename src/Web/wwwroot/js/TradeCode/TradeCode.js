﻿$(function () {
    if ($("#tradingCode-table tr").length <= 1) {
        swal({
            title: "",
            text: "در صورتی که کد سهامداری ندارید، می توانید بر روی دکمه تایید/ادامه گام بعدی کلیک نمایید ",
            type: 'info',
            showConfirmButton: true,
            confirmButtonText: 'متوجه شدم',
            });
    }

    var $firstPart = $("#FirstPart");
    var $secondPart = $("#SecondPart");
    var $thirdPart = $("#ThirdPart");

    $firstPart.parent().hide();
    $secondPart.parent().hide();
    $thirdPart.parent().hide();

    $("#labelTradeCode").css("display", "none");

    $("#Type").change(function () {
        switch ($("#Type").val()) {
            case "":
                clearText();
                $firstPart.parent().hide();
                $secondPart.parent().hide();
                $thirdPart.parent().hide();

                $("#labelTradeCode").css("display", "none");
                break;

            case "1":
                clearText();
                $firstPart.parent().show();
                $secondPart.parent().show();
                $thirdPart.parent().show();
                $thirdPart.attr('readonly', true);
                $thirdPart.addClass('disabletext');
                $firstPart.on('input', function (event) {
                    this.value = this.value.replace(/[^مدصختک]/g, '');
                });
                $("#labelTradeCode").css("display", "block");
                break;
            case "2":
                clearText();
                $firstPart.parent().hide();
                $secondPart.parent().show();
                $thirdPart.parent().hide();
                $thirdPart.removeClass('disabletext');
                $("#labelTradeCode").css("display", "block");
                break;

            case "3":
                clearText();
                $firstPart.parent().hide();
                $secondPart.parent().show();
                $thirdPart.parent().show();

                $thirdPart.removeClass('disabletext');

                $thirdPart.attr('readonly', false);
                $("#labelTradeCode").css("display", "block");

                break;


            default:

                clearText();
                $firstPart.parent().show();
                $secondPart.parent().show();
                $thirdPart.parent().show();
                $("#ThirdPart").prop('disabled', false);
                $thirdPart.removeClass('disabletext');
                $("#labelTradeCode").css("display", "block");

        }

        $("#FirstPart").change(function () {
            switch ($("#FirstPart").val()) {
                case 'م':
                    $thirdPart.val('توز');
                    break;
                case 'د':
                    $thirdPart.val('برق');
                    break;
                case 'ص':
                    $thirdPart.val('مصر');
                    break;
                case 'خ':
                    $thirdPart.val('خرد');
                    break;
                case 'ت':
                    $thirdPart.val('شرک');
                    break;
                default:
                    $thirdPart.val('');
            }
        });

    });
    $("#addTradeCodeBtn").click(function (event) {
        if ($("#SecondPart").val().trim() === "") {
            swal({
                title: "",
                text: "لطفا کد سهامداری را وارد نمایید.",
                type: 'warning',
                showConfirmButton: true,
                confirmButtonText: 'متوجه شدم',
                timer: 3000
            });
            $("#myModal").modal('hide');
            return false;
        }
        var type = $("#Type").val();
        if (type === "1") {
            if ($("#FirstPart").val().trim().length !== 1 || $("#SecondPart").val().trim().length !== 6 || $thirdPart.val().trim().length !== 3) {
                event.preventDefault();

                swal({
                    title: "",
                    text: "کد سهامداری وارد شده نامعتبر است . ",
                    type: 'warning',
                    showConfirmButton: true,
                    confirmButtonText: 'متوجه شدم',
                    timer: 3000
                });
            }

        }
        else if (type === "3") {

            if ($("#SecondPart").val().trim().length !== 5 || $thirdPart.val().trim().length !== 3) {
                event.preventDefault();

                swal({
                    title: "",
                    text: "کد سهامداری وارد شده نامعتبر است . ",
                    type: 'warning',
                    showConfirmButton: true,
                    confirmButtonText: 'متوجه شدم',
                    timer: 3000
                });
            }
        }

        else if (type === "2") {
            if ($("#SecondPart").val().trim() === "") {
                event.preventDefault();

                swal({
                    title: "",
                    text: "لطفا کد سهامداری را وارد نمایید . ",
                    type: 'warning',
                    showConfirmButton: true,
                    confirmButtonText: 'متوجه شدم',
                    timer: 3000
                });
            }

            if ($("#SecondPart").val().trim().length > 10) {
                event.preventDefault();

                swal({
                    title: "",
                    text: "کد سهامداری وارد شده نامعتبر می متوجه شدم . ",
                    type: 'warning',
                    showConfirmButton: true,
                    confirmButtonText: 'متوجه شدم',
                    timer: 3000
                });
            }
        }
    });






    function clearText() {
        $("#FirstPart").val('');
        $("#SecondPart").val('');
        $thirdPart.val('');
    }


});
function deleteTradeCode(id) {

    swal({
        title: "آیا از حذف این کد سهامداری اطمینان دارید؟",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'بله',
        cancelButtonText: "خیر",
        closeOnConfirm: true,
        closeOnCancel: true
    },
        function (isConfirm) {


            if (isConfirm) {
                var rowid = "#row_" + id;
                var data = $("#form-tradingCode-delete").serialize();
                $.ajax({
                    type: "POST",
                    url: "/tradingCodes/" + id,
                    data: data,
                    dataType: "json",
                    success: function (result) {
                       if (result.isSuccess)
                            $(rowid).fadeOut(500);
                        else 
                            swalMessage(result.message, 'error');
                        
                    },
                    error: function (result) {
                        
                    }
                });

            }
        });


}