﻿using System;
using System.Collections.Generic;
using CommandLine;

namespace Cerberus.Cli
{
    public class CommandOptions
    {
        [Option('e',"entity",Required = true,HelpText = "set entity name to update")]
        public IEnumerable<string> Entities { get; set; }

        [Option('s',"pageSize",Default = 1000,Required = false,HelpText = "set page size to iterate entities")]
        public int PageSize { get; set; }

        [Option('u',"uniqueIdentifier",Required = false,HelpText = "set one uniqueIdentifier to process")]
        public string UniqueIdentifier { get; set; }
    }
}