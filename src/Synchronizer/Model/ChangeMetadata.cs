﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Cerberus.Synchronizer.Enum;
using Cerberus.Synchronizer.Implementations;

namespace Cerberus.Synchronizer.Model
{
    public class ChangeMetadata
    {
        [Column("__$start_lsn")]
        public byte[] StartLsn { get; set; }

        [Column("__$seqval")]
        public byte[] SeqVal { get; set; }

        [Column("__$operation")]
        public ChangeDataOperation Operation { get; set; }

        [Column("__$update_mask")]
        public byte[] UpdateMask { get; set; }

        public DateTime ModifyDate { get; set; }

        public override string ToString()
        {
            return
                $"StartLsn={StartLsn?.ToHexString()} SeqVal={SeqVal?.ToHexString()} Operation={Operation} UpdateMask={UpdateMask?.ToHexString()}";
        }
    }
}