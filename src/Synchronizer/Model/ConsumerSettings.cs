﻿namespace Cerberus.Synchronizer.Model
{
    public class ConsumerSettings
    {
        public string CaptureInstance { get; set; }

        public string ChangeTable { get; set; }

        public int FetchLimit { get; set; } = 100;
    }
}