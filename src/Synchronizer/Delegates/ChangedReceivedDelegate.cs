﻿using System.Threading.Tasks;
using Cerberus.Synchronizer.Interface;
using Cerberus.Synchronizer.Model;

namespace Cerberus.Synchronizer.Delegates
{
    public delegate Task ChangeReceivedAsyncEventHandler<TEntity>(IConsumer<TEntity> sender, ChangeData<TEntity> data);
}