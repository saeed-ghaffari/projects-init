﻿using System.Data;

namespace Cerberus.Synchronizer.Interface
{
    public interface IConnectionProvider
    {
        IDbConnection GetConnection();
    }
}