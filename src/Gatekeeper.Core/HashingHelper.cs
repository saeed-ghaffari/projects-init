﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace Gatekeeper.Core
{
    public static class HashingHelper
    {
        public static string GenerateSha256Hash(string value)
        {
            var sb = new StringBuilder();

            using (var hash = SHA256.Create())
            {
                var enc = Encoding.UTF8;
                var result = hash.ComputeHash(enc.GetBytes(value));

                foreach (Byte b in result)
                    sb.Append(b.ToString("x2"));
            }

            return sb.ToString();
        }

        public static string GenerateMD5Hash(string value)
        {
            var sb = new StringBuilder();

            using (var hash = MD5.Create())
            {
                var enc = Encoding.UTF8;
                var result = hash.ComputeHash(enc.GetBytes(value));

                foreach (Byte b in result)
                    sb.Append(b.ToString("x2"));
            }

            return sb.ToString();
        }
    }
}