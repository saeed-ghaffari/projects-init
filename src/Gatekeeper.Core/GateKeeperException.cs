﻿using System;
using Gatekeeper.Core.Enum;

namespace Gatekeeper.Core
{
    public class GateKeeperException : Exception
    {
        public GateKeeperException()
        {
        }

        public GateKeeperException(
            ErrorCode errorCode,
            string customMessage = null,
            object value = null,
            Exception innerException = null)
            : base(customMessage, innerException)
        {
            ErrorCode = errorCode;
            CustomMessage = customMessage;
            Value = value;
        }

        public ErrorCode ErrorCode { get; set; }

        public string CustomMessage { get; set; }

        public object Value { get; set; }

    }
}