﻿
using System;
using System.Threading.Tasks;

namespace Gatekeeper.Core.Interface
{
    public interface IAuthService
    {
        Task InitAuthorizationAsync(string key, string otp,TimeSpan ttl);

        Task<bool> ValidateOtpAsync(string key, string otp);

    }
}