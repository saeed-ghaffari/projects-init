﻿namespace Gatekeeper.Core.Configurations
{
    public class OtpConfiguration
    {
        public int OtpLength { get; set; }

        public string OtpChars { get; set; }
    }
}