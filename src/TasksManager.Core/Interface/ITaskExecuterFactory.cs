﻿using Cerberus.TasksManager.Core.Delegate;

namespace Cerberus.TasksManager.Core.Interface
{
    public interface ITaskExecuterFactory
    {
        void RegisterConstructor(string type, CreateTaskExecuter ctor);

        ITaskExecuter Create(string type);
    }
}