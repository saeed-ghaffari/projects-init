﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.TasksManager.Core.Model;

namespace Cerberus.TasksManager.Core.Interface
{
    public interface ITaskRepository
    {
        /// <summary>
        /// Sets the status of Queued and InProgress tasks to pending if they are out of the timeout window
        /// </summary>
        /// <param name="timeoutWindowSeconds"></param>
        void CleanupTasks(int timeoutWindowSeconds);

        /// <summary>
        /// Fetches pending tasks and sets task statuses to fetched
        /// </summary>
        /// <returns></returns>
        IEnumerable<ScheduledTask> GetPendingTasks(long groupId, int count);

        IEnumerable<TaskGroup> GetTaskGroups(int runnerId);

        void Update(ScheduledTask task);

        Task<ScheduledTask> AddAsync(ScheduledTask task, bool dontAddDuplicates = false);

        Task<ScheduledTask> GetAsync(long taskId);

        Task<IEnumerable<ScheduledTask>> GetAsync(string type, string param);

        Task DeleteAsync(long taskId);

        Task<ScheduledTask> RecoverTask(ScheduledTask task, TimeSpan recoverDelay);

        Task<IEnumerable<ScheduledTask>> GetTaskListAsync(long refId, long groupId, int status);
    }
}