﻿CREATE SCHEMA [tsk]
GO
CREATE TYPE [tsk].[RefId] AS TABLE(
	[Id] [bigint] NULL
)
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tsk].[RecurringIntervalPeriod](
	[Id] [tinyint] NOT NULL,
	[Title] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tsk].[RecurringTask](
	[Id] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[Status] [tinyint] NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[Params] [nvarchar](max) NULL,
	[Interval] [int] NOT NULL,
	[LastTaskId] [bigint] NULL,
	[LastTaskStatus] [tinyint] NULL,
	[LastExecution] [datetime] NULL,
	[NextExecution] [datetime] NULL,
	[RefId] [bigint] NULL,
	[CreationTime] [datetime] NOT NULL,
	[IntervalPeriod] [tinyint] NOT NULL,
 CONSTRAINT [PK_RecurringTask] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [tsk].[RecurringTaskStatus]    Script Date: 8/26/2018 12:14:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tsk].[RecurringTaskStatus](
	[Id] [tinyint] NOT NULL,
	[Title] [varchar](50) NOT NULL,
 CONSTRAINT [PK_RecurringTaskStatus] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tsk].[Task](
	[Id] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[Status] [tinyint] NOT NULL,
	[Params] [nvarchar](max) NULL,
	[DueTime] [datetime] NOT NULL,
	[ExecuteTime] [bigint] NOT NULL,
	[FinishTime] [datetime] NULL,
	[CreationTime] [datetime] NOT NULL,
	[LastRun] [datetime] NULL,
	[RefId] [bigint] NULL,
	[RecurringTaskId] [bigint] NULL,
	[OriginalTaskId] [bigint] NULL,
	[RecoverySequence] [int] NOT NULL,
 CONSTRAINT [PK_Task] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tsk].[TaskGroup](
	[Id] [bigint] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[Title] [varchar](127) NOT NULL,
	[ConcurrencyLevel] [int] NOT NULL,
	[CreationTime] [datetime] NOT NULL,
	[Enabled] [bit] NOT NULL,
	[RunnerId] [int] NULL,
 CONSTRAINT [PK_TaskGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tsk].[TaskStatus](
	[Id] [tinyint] NOT NULL,
	[Title] [varchar](50) NOT NULL,
 CONSTRAINT [PK_TaskStatus_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [tsk].[VersionInfo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Version] [bigint] NOT NULL,
	[AppliedOn] [datetime] NOT NULL,
	[Description] [nvarchar](256) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [tsk].[RecurringTask] ADD  CONSTRAINT [DF_RecurringTask_CreationTime]  DEFAULT (getdate()) FOR [CreationTime]
GO
ALTER TABLE [tsk].[RecurringTask] ADD  CONSTRAINT [DF_RecurringTask_IntervalPeriod]  DEFAULT ((4)) FOR [IntervalPeriod]
GO
ALTER TABLE [tsk].[Task] ADD  CONSTRAINT [DF_Task_ExecuteTime]  DEFAULT ((-1)) FOR [ExecuteTime]
GO
ALTER TABLE [tsk].[Task] ADD  CONSTRAINT [DF_Task_CreationTime]  DEFAULT (getdate()) FOR [CreationTime]
GO
ALTER TABLE [tsk].[Task] ADD  CONSTRAINT [DF_Task_RecoverySequence]  DEFAULT ((0)) FOR [RecoverySequence]
GO
ALTER TABLE [tsk].[TaskGroup] ADD  CONSTRAINT [DF_TaskGroup_ConcurrencyLevel]  DEFAULT ((1)) FOR [ConcurrencyLevel]
GO
ALTER TABLE [tsk].[TaskGroup] ADD  CONSTRAINT [DF_TaskGroup_CreationTime]  DEFAULT (getdate()) FOR [CreationTime]
GO
ALTER TABLE [tsk].[TaskGroup] ADD  CONSTRAINT [DF_TaskGroup_Enabled]  DEFAULT ((1)) FOR [Enabled]
GO
ALTER TABLE [tsk].[RecurringTask]  WITH CHECK ADD  CONSTRAINT [FK_RecurringTask_RecurringTaskStatus] FOREIGN KEY([Status])
REFERENCES [tsk].[RecurringTaskStatus] ([Id])
GO
ALTER TABLE [tsk].[RecurringTask] CHECK CONSTRAINT [FK_RecurringTask_RecurringTaskStatus]
GO
ALTER TABLE [tsk].[RecurringTask]  WITH CHECK ADD  CONSTRAINT [FK_RecurringTask_TaskGroup] FOREIGN KEY([GroupId])
REFERENCES [tsk].[TaskGroup] ([Id])
GO
ALTER TABLE [tsk].[RecurringTask] CHECK CONSTRAINT [FK_RecurringTask_TaskGroup]
GO
ALTER TABLE [tsk].[RecurringTask]  WITH CHECK ADD  CONSTRAINT [FK_RecurringTask_TaskStatus] FOREIGN KEY([LastTaskStatus])
REFERENCES [tsk].[TaskStatus] ([Id])
GO
ALTER TABLE [tsk].[RecurringTask] CHECK CONSTRAINT [FK_RecurringTask_TaskStatus]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [tsk].[SP_AddRecurringTask]
	@GroupId bigint,
	@Type varchar(50),
	@Params nvarchar(max),
	@Interval int,
	@RefId bigint,
	@NextExecution datetime,
	@IntervalPeriod tinyint = 4
AS
BEGIN
	insert into tsk.RecurringTask (GroupId,IsEnabled,Status,Type,Params,Interval,RefId,NextExecution,IntervalPeriod)
		output inserted.*
		Values (@GroupId,1,1,@Type,@Params,@Interval,@RefId,@NextExecution,@IntervalPeriod)
		
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE[tsk].[SP_AddTask]
        @GroupId bigint,
           @Type varchar(50),
	@Params nvarchar(max),
	@DueTime datetime,
    @NoDuplicates bit = 0,
	@RefId bigint = null,
    @OriginalTaskId bigint = null,
	@RecoverySequence int =0,
	@RecurringTaskId bigint = null
AS
BEGIN
	if @NoDuplicates =0 or not exists(select* from tsk.Task where GroupId= @GroupId and Type = @Type and Params = @Params and RefId = @RefId and status < 5)

    begin
        insert into tsk.Task(GroupId, Type, Params, DueTime, [Status], RefId, OriginalTaskId, RecoverySequence, RecurringTaskId)

        output inserted.*
		values (@GroupId, @Type, @Params, @DueTime,1, @RefId, @OriginalTaskId, @RecoverySequence, @RecurringTaskId)


        select* from tsk.TaskGroup where Id =@GroupId
   end
	else

    begin
        select top 0 * from tsk.Task
    end
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Milad Ghafari
-- ALTER date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [tsk].[SP_CleanupTasks]
	@Since datetime
AS
BEGIN
	update tsk.Task set [Status]=1 where [Status] between 2 and 4
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Milad ghafari
-- ALTER date: 
-- Description:	Delete task recurring
-- =============================================
CREATE PROCEDURE [tsk].[SP_DeleteRecurringTask]
	@Id bigint
AS
BEGIN
	delete tsk.TaskRecurring where Id=@Id
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Milad
-- ALTER date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [tsk].[SP_DeleteTask]
	@Id bigint
AS
BEGIN
	delete from tsk.Task where Id =@Id
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Milad
-- ALTER date: 
-- Description:	Enables or disables a recurring task
-- =============================================
CREATE PROCEDURE [tsk].[SP_EnableTask]
	@Id bigint,
	@Value bit
AS
BEGIN
	update tsk.RecurringTask set IsEnabled = @Value where Id = @Id
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Milad
-- ALTER date: 
-- Description:	returns recurring tasks that should schedule a task
-- =============================================
CREATE PROCEDURE [tsk].[SP_GetActiveRecurringTasks]
AS
BEGIN
	select * from tsk.RecurringTask
	where IsEnabled = 1
		and Status = 1 -- idle
		and NextExecution < getdate()
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Milad Ghafarinejad 
-- ALTER date: 
-- Description:	get ExecuteTime Averag
-- =============================================
CREATE PROCEDURE [tsk].[SP_GetExecuteTimeAvg]
	@Id bigint,
	@GroupId int,
	@TaskType varchar(50)
AS
BEGIN
	declare @MaxId bigint 
	declare @ExecuteTimeAvg bigint

	set @MaxId=(select MAX(id) as MaxId from tsk.Task)
	set @ExecuteTimeAvg=(select AVG(ExecuteTime)as ExecuteTimeAvg from tsk.Task with(nolock)
		where Id>@Id and Id<=@MaxId and Status=5 and GroupId=@GroupId and [Type]=@TaskType )

	select @MaxId as LastId,ISNULL(@ExecuteTimeAvg,0) as ExecuteTimeAvg
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Milad Ghafarinejad
-- ALTER date: 
-- Description:	Get count tasks which have faild or bad params Status
-- =============================================
CREATE PROCEDURE [tsk].[SP_GetFailedTaskCount]
	@GroupId int,
	@Type varchar(50)
AS
BEGIN
	select count(*) as FailedTaskCount from tsk.Task where GroupId=@GroupId and Type=@Type and Status in (11,13)
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [tsk].[SP_GetPendingTasks]
	@GroupId bigint,
	@Count int
AS
BEGIN
	declare @TaskIds table (Id bigint,GroupId bigint)
	
	insert into @TaskIds (Id, GroupId)
		select top(@Count) Id, GroupId 
		from Task with(index(IX_Task_Group_PendingStatus))
		where GroupId = @GroupId and [Status]=1 and DueTime < getdate()
		order by Id

	-- set task status to fetched
	update tsk.Task set [Status]=2 where Id in (select Id from @TaskIds)
	
	select * from Task where Id in (select Id from @TaskIds)
	select * from TaskGroup where Id in (select GroupId from @TaskIds)
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Milad Ghafarinejad 
-- ALTER date: 
-- Description:	get Pending Average 
-- =============================================
CREATE PROCEDURE [tsk].[SP_GetPendingTimeAvg]
	@GroupId int,
	@TaskType varchar(50)
AS
BEGIN
	declare @MaxId bigint 
	declare @PendingTimeAvg bigint
	
	set @MaxId=(select MAX(id) as MaxId from tsk.Task)
	select @PendingTimeAvg = AVG(CONVERT(bigint,DATEDIFF(MILLISECOND,DueTime,GETDATE())))
	from tsk.Task  
	where GroupId=@GroupId and Status<5 and [Type]=@TaskType and DueTime < GETDATE()

	select @MaxId as LastId,@PendingTimeAvg as PendingTimeAvg
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [tsk].[SP_GetRecurringTaskById]
	@Id bigint
AS
BEGIN
	select * from tsk.RecurringTask where Id=@Id
end
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Milad ghafari
-- ALTER date: 
-- Description:	get recurring task by Type and refid
-- =============================================
CREATE PROCEDURE [tsk].[SP_GetRecurringTaskByRef]
	@Type varchar(50),
	@RefId bigint,
	@Param varchar(1024) = null
AS
BEGIN
	select * from tsk.RecurringTask where [Type]=@Type and RefId=@RefId and (@Param is null or Params = @Param)
end
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Milad ghafari
-- ALTER date: 
-- Description:	get recurring task by Type and refid
-- =============================================
CREATE PROCEDURE [tsk].[SP_GetRecurringTasksByRefs]
	@Type varchar(50),
	@RefIds as tsk.RefId readonly
AS
BEGIN
	select * from tsk.RecurringTask where [Type]=@Type and RefId in (select Id from @RefIds)
end
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Milad ghafari
-- Create date: 
-- Description:	get recurring task by Type and refid
-- =============================================
CREATE PROCEDURE [tsk].[SP_GetRecurringTasskByRefs]
	@Type varchar(50),
	@RefIds tsk.RefId readonly
AS
BEGIN
	select * from tsk.RecurringTask where [Type]=@Type and RefId in (select Id from @RefIds)
end
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Milad
-- ALTER date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [tsk].[SP_GetTaskById]
	@Id bigint
AS
BEGIN
	select * from tsk.Task where Id =@Id

	select * from tsk.TaskGroup where Id = (select GroupId from tsk.Task where Id= @Id)
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [tsk].[SP_GetTaskGroups]
	@RunnerId int
AS
BEGIN
	select * from tsk.TaskGroup where Enabled = 1 and RunnerId = @RunnerId
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Milad
-- ALTER date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [tsk].[SP_GetTasksByParams]
	@Type varchar(50),
	@Params varchar(1024)
AS
BEGIN
	declare @TaskIds table (Id bigint,GroupId bigint)
	
	insert into @TaskIds (Id, GroupId)
		select Id, GroupId from Task where [Type]=@Type and Params=@Params

	
	select * from Task where Id in (select Id from @TaskIds)
	select * from TaskGroup where Id in (select GroupId from @TaskIds)
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Milad Ghafarinejad 
-- ALTER date: 
-- Description:	get WaitTime Averag
-- =============================================
CREATE PROCEDURE [tsk].[SP_GetWaitTimeAvg]
	@Id bigint,
	@GroupId int,
	@TaskType varchar(50)
AS
BEGIN
	declare @MaxId bigint 
	declare @WaitTimeAvg bigint
	
	set @MaxId=(select MAX(id) as MaxId from tsk.Task)
	set @WaitTimeAvg=(select AVG(convert(bigint,DateDiff(MILLISECOND,DueTime,LastRun)))as WaitTimeAvg from tsk.Task 
		where Id>@Id and id<@MaxId and GroupId=@GroupId and Status>=5 and [Type]=@TaskType and LastRun is not null)

	select @MaxId as LastId,@WaitTimeAvg as WaitTimeAvg
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [tsk].[SP_UpdateRecurringTask] 
	@Id bigint,
	@IsEnabled bit,
	@Status tinyint,
	@Interval bigint,
	@LastTaskId	bigint,
	@LastTaskStatus tinyint,
	@LastExecution datetime,
	@NextExecution datetime,
	@IntervalPeriod tinyint = 4
AS
BEGIN
	update tsk.RecurringTask set 
		IsEnabled=@IsEnabled,
		Status=@Status,
		Interval=@Interval,
		LastTaskId=@LastTaskId,
		LastTaskStatus=@LastTaskStatus,
		LastExecution=@LastExecution,
		NextExecution=@NextExecution,
		IntervalPeriod=@IntervalPeriod
	output inserted.*
	where  Id=@Id
END
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Milad
-- ALTER date:
-- Description:	
-- =============================================
CREATE PROCEDURE [tsk].[SP_UpdateTaskStatus]
	@TaskId bigint,
	@Status int,
	@ExecuteTime bigint,
	@LastRun datetime
AS
BEGIN
	update tsk.Task set [Status]=@Status, ExecuteTime=@ExecuteTime, LastRun=@LastRun where Id = @TaskId
END
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Condcutor_ItemId] ON [dbo].[Conductor]
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Conductor_ChannelId_StartTime] ON [dbo].[Conductor]
(
	[ChannelId] ASC,
	[StartTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Task_Group_PendingStatus] ON [tsk].[Task]
(
	[GroupId] ASC,
	[Status] ASC
)
WHERE ([Status]<(5))
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_Task_Group_Status] ON [tsk].[Task]
(
	[GroupId] ASC,
	[Status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

-- =============================================
-- Author:		Milad
-- ALTER date: 
-- Description:	
-- =============================================
ALTER PROCEDURE [tsk].[SP_GetActiveRecurringTasks]
@RunnerId int
AS
BEGIN
	select 
		tsk.RecurringTask.* 
	from 
		tsk.RecurringTask inner join tsk.TaskGroup on RecurringTask.GroupId=TaskGroup.Id
	where 
		IsEnabled = 1
		and Status = 1 -- idle
		and NextExecution < GetDate()
		and TaskGroup.RunnerId=@RunnerId
END