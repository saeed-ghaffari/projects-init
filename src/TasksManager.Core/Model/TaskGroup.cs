﻿namespace Cerberus.TasksManager.Core.Model
{
    public class TaskGroup
    {
        public long Id { get; set; }

        public string Title { get; set; }

        public int ConcurrencyLevel { get; set; }

        public bool Enabled { get; set; }

        public override string ToString()
        {
            return $"{Title}(Id={Id}, ConcurrenctyLevel={ConcurrencyLevel})";
        }
    }
}