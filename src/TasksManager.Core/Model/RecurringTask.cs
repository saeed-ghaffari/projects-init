﻿using System;
using Cerberus.TasksManager.Core.Enum;

namespace Cerberus.TasksManager.Core.Model
{
    public class RecurringTask
    {
        public long Id { get; set; }

        public long GroupId { get; set; }

        public bool IsEnabled { get; set; }

        public RecurringTaskStatus Status { get; set; }

        public string Type { get; set; }

        public string Params { get; set; }

        public int Interval { get; set; }

        public RecurringIntervalPeriod IntervalPeriod { get; set; }

        public long? LastTaskId { get; set; }

        public TaskStatus? LastTaskStatus { get; set; }

        public DateTime? LastExecution { get; set; }

        public DateTime NextExecution { get; set; }

        public long? RefId { get; set; }

        public DateTime CreationTime { get; set; }
    }
}