﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;

namespace Cerberus.TasksManager.Core.Repository
{
    public class NullTaskRepository : ITaskRepository
    {
        public void CleanupTasks(int timeoutWindowSeconds) { }

        public IEnumerable<ScheduledTask> GetPendingTasks(long groupId, int count)
        {
            return null;
        }
        public IEnumerable<TaskGroup> GetTaskGroups(int runnerId)
        {
            return null;
        }

        public void Update(ScheduledTask task) { }

        public Task<ScheduledTask> AddAsync(ScheduledTask task, bool dontAddDuplicates)
        {
            return Task.FromResult(new ScheduledTask());
        }

        public Task<ScheduledTask> GetAsync(long taskId)
        {
            return Task.FromResult(new ScheduledTask());
        }

        public Task<IEnumerable<ScheduledTask>> GetAsync(string type, string param)
        {
            return Task.FromResult(
                (IEnumerable<ScheduledTask>) new List<ScheduledTask>());
        }

        public Task DeleteAsync(long taskId)
        {
            return Task.FromResult(0);
        }

        public Task<ScheduledTask> RecoverTask(ScheduledTask task, TimeSpan recoverDelay)
        {
            return Task.FromResult(new ScheduledTask());
        }

        public Task<IEnumerable<ScheduledTask>> GetTaskListAsync(long refId, long groupId, int status)
        {
            return Task.FromResult(
                (IEnumerable<ScheduledTask>)new List<ScheduledTask>());
        }
    }
}