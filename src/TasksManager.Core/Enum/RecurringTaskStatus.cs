﻿namespace Cerberus.TasksManager.Core.Enum
{
    public enum RecurringTaskStatus
    {
        Idle = 1,
        Running = 2
    }
}