﻿using System.Collections.Generic;
using Cerberus.Domain.ViewModel.Api;

namespace Cerberus.OfflineTasks.Model
{
    public class GetBankingAccountsCallbackData
    {
        public IList<KeyValuePair<string, IEnumerable<BankingAccountViewModel>>> Success { get; set; }
        public IList<GetBankingAccountsCallbackError> Failed { get; set; }
        public long ReferenceId { get; set; }
    }
}