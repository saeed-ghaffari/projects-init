﻿using System.Collections.Generic;

namespace Cerberus.OfflineTasks.Model
{
    public class Group
    {
        public long Id { get; set; }

        public string GroupName { get; set; }
        public IList<GroupMember> GroupMembers { get; set; }

    }
}