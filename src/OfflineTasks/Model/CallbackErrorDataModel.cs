﻿namespace Cerberus.OfflineTasks.Model
{
    public class CallbackErrorDataModel
    {
        public string UniqueIdentifier { get; set; }
        public string[] Meta { get; set; }
    }
}