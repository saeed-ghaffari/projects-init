﻿namespace Cerberus.OfflineTasks.Model
{
    public class GetSejamStatusCallbackError
    {
        public string UniqueIdentifier { get; set; }
        public string[] Meta { get; set; }
    }
}