﻿using System.Collections.Generic;
using Cerberus.Domain.ViewModel.Api;

namespace Cerberus.OfflineTasks.Model
{
    public class GetTraceCodeCallbackData
    {
        public IList<KeyValuePair<string, TraceCodeViewModel>> Success { get; set; }
        public IList<CallbackErrorDataModel> Failed { get; set; }
        public long ReferenceId { get; set; }
    }
}