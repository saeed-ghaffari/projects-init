﻿using System.Collections.Generic;
using Cerberus.Domain.ViewModel.Api;

namespace Cerberus.OfflineTasks.Model
{
    public class GetSejamStatusCallbackData
    {
        public IList<KeyValuePair<string, SejamStatusViewModel>> Success { get; set; }
        public IList<GetSejamStatusCallbackError> Failed { get; set; }
        public long ReferenceId { get; set; }
    }
}