﻿using System.Data;
using System.Data.SqlClient;
using Cerberus.Domain.Interface.Repositories;
using Microsoft.Extensions.Options;

namespace Cerberus.OfflineTasks.Model
{
    public class SqlConnectionProvider : ISqlConnectionProvider
    {
        private readonly IOptions<ConnectionStringsConfiguration> _options;

        public SqlConnectionProvider(IOptions<ConnectionStringsConfiguration> options)
        {
            _options = options;
        }

        public IDbConnection GetConnection()
        {
            return new SqlConnection(_options.Value.MasterDbConnection);
        }

        public IDbConnection GetReadConnection()
        {
            return new SqlConnection(_options.Value.ReadConnection);
        }

        public IDbConnection GetRequestLogConnection()
        {
            return new SqlConnection(_options.Value.RequestLogDbConnection);
        }

        public IDbConnection GetStatusReportDbConnection()
        {
            return new SqlConnection(_options.Value.StatusReportDbConnection);
        }
    }
}