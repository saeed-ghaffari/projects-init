﻿using System.Collections.Generic;
using Cerberus.Domain.ViewModel;
using Cerberus.Domain.ViewModel.Api;

namespace Cerberus.OfflineTasks.Model
{
    public class GetCompactProfileCallbackData
    {
        public IList<KeyValuePair<string, CompactProfile>> Success { get; set; }
        public IList<CallbackErrorDataModel> Failed { get; set; }
        public long ReferenceId { get; set; }
    }
}