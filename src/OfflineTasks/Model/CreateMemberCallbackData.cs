﻿using System.Collections.Generic;

namespace Cerberus.OfflineTasks.Model
{
    public class CreateMemberCallbackData
    {
        public IList<string> Success { get; set; }
        public IList<CreateMemberCallbackError> Failed { get; set; }
        public long ReferenceId { get; set; }
    }
}