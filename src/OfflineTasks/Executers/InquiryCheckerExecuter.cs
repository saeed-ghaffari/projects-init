﻿using System;
using Cerberus.Domain.Interface.Services;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using System.Linq;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Model;
using Cerberus.Service;
using Cerberus.Utility;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    /// <summary>
    /// check all inquiries and generating trace code in happy day situation and send aggregated result as a sms to profile owner
    /// </summary>
    public class InquiryCheckerExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }

        public InquiryCheckerExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var bankingAccountService = scope.ServiceProvider.GetRequiredService<IBankingAccountService>();
                var privatePersonService = scope.ServiceProvider.GetRequiredService<IPrivatePersonService>();
                var legalPersonService = scope.ServiceProvider.GetRequiredService<ILegalPersonService>();
                var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                var agentService = scope.ServiceProvider.GetRequiredService<IAgentService>();
                IMessagingService messagingService = scope.ServiceProvider.GetRequiredService<IMessagingService>();
                ILegalPersonStakeholderService legalPersonStakeholderService = scope.ServiceProvider.GetRequiredService<ILegalPersonStakeholderService>();
                IRecurringTaskRepository recurringTaskRepository = scope.ServiceProvider.GetRequiredService<IRecurringTaskRepository>();
                ILogger<InquiryCheckerExecuter> logger = scope.ServiceProvider.GetRequiredService<ILogger<InquiryCheckerExecuter>>();
                IOptions<InquiryConfiguration> config = scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();
                var msisdnService = scope.ServiceProvider.GetRequiredService<IMsisdnService>();
                //  _logger.LogInformation("InquiryCheckerExecuter start ...");



                var profile = await profileService.GetWithNoCacheAsync(task.RefId.GetValueOrDefault());


                if (profile == null)
                    return TaskStatus.Failed;

                var mobile = profile.Mobile;
                var name = await profileService.GetFullName(task.RefId.GetValueOrDefault());

                var isConfirmedAccount = false;
                var isConfirmedPrivatePerson = false;
                var isConfirmedLegalPerson = false;
                var isConfirmedAgent = false;
                DateTime privatePersonBirthDate = DateTime.Now;
                bool hasAgent;
                PrivatePerson person = null;

                //check agent status


                //check private person identity information
                if (profile.Type == ProfileOwnerType.IranianPrivatePerson ||
                    profile.Type == ProfileOwnerType.ForeignPrivatePerson)
                {
                    person =
                        await privatePersonService.GetByProfileIdAsync(task.RefId.GetValueOrDefault());

                    if (person == null)
                    {
                        if (profile.Status != ProfileStatus.Sejami)
                        {
                            profile.Status = ProfileStatus.InvalidInformation;
                            await profileService.UpdateStatusAsync(profile);
                        }

                        await recurringTaskRepository.EnableTaskAsync(task.RecurringTaskId.GetValueOrDefault(), false);
                        await messagingService.SendSmsAsync(task.RefId.GetValueOrDefault(), mobile.ToString(),
                           profile.Carrier,
                            string.Format(config.Value.InquiryNotConfirmedMessage, name, config.Value.NocrInquiryTitle,
                                Environment.NewLine)
                        );
                        return TaskStatus.Done;
                    }

                    privatePersonBirthDate = person.BirthDate;

                    if (person.IsConfirmed == null)
                        return TaskStatus.Canceled;

                    if (person.IsConfirmed == false)
                    {
                        profile.Status = ProfileStatus.InvalidInformation;
                        await profileService.UpdateStatusAsync(profile);
                        await recurringTaskRepository.EnableTaskAsync(task.RecurringTaskId.GetValueOrDefault(),
                            false);
                        await messagingService.SendSmsAsync(task.RefId.GetValueOrDefault(), profile.Mobile.ToString(),
                            profile.Carrier,
                            string.Format(config.Value.InquiryNotConfirmedMessage, name, config.Value.NocrInquiryTitle,
                                Environment.NewLine)
                        );
                        return TaskStatus.Done;
                    }

                    isConfirmedPrivatePerson = true;

                }


                if (profile.Type == ProfileOwnerType.IranianLegalPerson)
                {
                    var legalPerson =
                        await legalPersonService.GetByProfileIdAsync(task.RefId.GetValueOrDefault());

                    if (legalPerson == null)
                    {
                        profile.Status = ProfileStatus.InvalidInformation;
                        await profileService.UpdateStatusAsync(profile);
                        await recurringTaskRepository.EnableTaskAsync(task.RecurringTaskId.GetValueOrDefault(), false);
                        await messagingService.SendSmsAsync(task.RefId.GetValueOrDefault(), profile.Mobile.ToString(),
                            profile.Carrier,
                            string.Format(config.Value.InquiryNotConfirmedMessage, name, config.Value.IlencInquiryTitle,
                                Environment.NewLine)
                        );
                        return TaskStatus.Done;
                    }

                    if (legalPerson.IsConfirmed == null)
                        return TaskStatus.Canceled;

                    if (legalPerson.IsConfirmed == false)
                    {
                        profile.Status = ProfileStatus.InvalidInformation;
                        await profileService.UpdateStatusAsync(profile);
                        await recurringTaskRepository.EnableTaskAsync(task.RecurringTaskId.GetValueOrDefault(),
                            false);
                        await messagingService.SendSmsAsync(task.RefId.GetValueOrDefault(), profile.Mobile.ToString(),
                            profile.Carrier,
                            string.Format(config.Value.InquiryNotConfirmedMessage, name, config.Value.IlencInquiryTitle,
                                Environment.NewLine)
                        );
                        return TaskStatus.Done;
                    }

                    isConfirmedLegalPerson = true;


                }

                //check accounts is valid
                var accounts = await bankingAccountService.GetListAsync(task.RefId.GetValueOrDefault());

                var accountList = accounts.ToList();

                if (accountList.Count == 0 || !accountList.Any(x => x.IsDefault)) // person must have an isDefault account
                {
                    if (profile.Status != ProfileStatus.Sejami)
                    {
                        profile.Status = ProfileStatus.InvalidInformation;
                        await profileService.UpdateStatusAsync(profile);
                    }

                    await messagingService.SendSmsAsync(task.RefId.GetValueOrDefault(), profile.Mobile.ToString(),
                        profile.Carrier,
                        string.Format(config.Value.InquiryNotConfirmedMessage, name, config.Value.IbanInquiryTitle,
                            Environment.NewLine));

                    await recurringTaskRepository.EnableTaskAsync(task.RecurringTaskId.GetValueOrDefault(), false);
                    return TaskStatus.Done;
                }


                if (accountList.Any(x => x.IsConfirmed == null))
                    return TaskStatus.Canceled;

                if (accountList.Any(x => x.IsConfirmed == false))
                {
                    if (profile.Status != ProfileStatus.Sejami)
                    {
                        profile.Status = ProfileStatus.InvalidInformation;
                        await profileService.UpdateStatusAsync(profile);
                    }

                    await recurringTaskRepository.EnableTaskAsync(task.RecurringTaskId.GetValueOrDefault(),
                        false);

                    await messagingService.SendSmsAsync(task.RefId.GetValueOrDefault(), profile.Mobile.ToString(),
                        profile.Carrier,
                        string.Format(config.Value.InquiryNotConfirmedMessage, name, config.Value.IbanInquiryTitle,
                            Environment.NewLine)
                    );
                    return TaskStatus.Done;
                }
                else
                    isConfirmedAccount = true;


                var agent = await agentService.GetByProfileIdAsync(task.RefId.GetValueOrDefault());

                hasAgent = agent != null;
                if (hasAgent)
                {

                    if (agent.IsConfirmed == null && isConfirmedPrivatePerson)
                    {
                        await recurringTaskRepository.EnableTaskAsync(task.RecurringTaskId.GetValueOrDefault(), false);
                        return TaskStatus.Canceled;
                    }
                    isConfirmedAgent = agent.IsConfirmed == true;
                    if (agent.IsConfirmed == false)
                    {
                        profile.Status = ProfileStatus.InvalidInformation;
                        await profileService.UpdateStatusAsync(profile);
                        await recurringTaskRepository.EnableTaskAsync(task.RecurringTaskId.GetValueOrDefault(), false);
                    }
                }


                if (profile.Type == ProfileOwnerType.IranianPrivatePerson && !hasAgent)
                {
                    if (!Tools.IsUnder18Years(person?.BirthDate))
                    {
                        var msisdn = await msisdnService.GetByProfileIdAsync(profile.Id);
                        if (msisdn?.IsConfirmed == null)
                            return TaskStatus.Canceled;
                        if (msisdn.IsConfirmed == false)
                        {
                            if (profile.Status != ProfileStatus.Sejami)
                            {
                                profile.Status = ProfileStatus.InvalidInformation;
                                await profileService.UpdateStatusAsync(profile);
                            }

                            await messagingService.SendSmsAsync(task.RefId.GetValueOrDefault(),
                                msisdn.Mobile.ToString(),
                                profile.Carrier,
                                string.Format(config.Value.ShahkarInquiryNotConfirmedMessage, name,
                                    Environment.NewLine));

                            await recurringTaskRepository.EnableTaskAsync(task.RecurringTaskId.GetValueOrDefault(),
                                false);
                            return TaskStatus.Done;
                        }
                    }
                }




                if (isConfirmedPrivatePerson && isConfirmedAccount && (!hasAgent || isConfirmedAgent))
                {
                    if (Tools.IsUnder18Years(privatePersonBirthDate) && !hasAgent) //if person has under 18 years old and don't choose an agent
                    {
                        profile.Status = ProfileStatus.InvalidInformation;
                        await profileService.UpdateStatusAsync(profile);
                        await messagingService.SendSmsAsync(task.RefId, profile.Mobile.ToString(), profile.Carrier,
                            string.Format(config.Value.UnderEighteenYearsOldMessage, name, Environment.NewLine));
                        await recurringTaskRepository.EnableTaskAsync(task.RecurringTaskId.GetValueOrDefault(), false);
                        return TaskStatus.Done;
                    }

                    if (Tools.IsUnder18Years(privatePersonBirthDate) && hasAgent && agent.Type == AgentType.Attorney) //if person had under 18 years old then agent type must not be attorney
                    {
                        agent.IsConfirmed = false;
                        agent.Locked = false;
                        agent.Description = config.Value.NotChooseProvinceAgentDescription;

                        await agentService.UpdateAsync(agent);
                        profile.Status = ProfileStatus.InvalidInformation;
                        await profileService.UpdateStatusAsync(profile);
                        await messagingService.SendSmsAsync(task.RefId, profile.Mobile.ToString(), profile.Carrier,
                            string.Format(config.Value.NotChooseProvinceAgent, name, Environment.NewLine));
                        await recurringTaskRepository.EnableTaskAsync(task.RecurringTaskId.GetValueOrDefault(), false);
                        return TaskStatus.Done;
                    }

                    if (Tools.IsUnder18Years(privatePersonBirthDate) && hasAgent && agent.Type == AgentType.Province)
                    {

                        var father = await privatePersonService.GetByProfileIdAsync(agent.AgentProfileId);

                        if (father.Gender != Gender.Male)//(PersianChars.GetClearChars(fatherName) != PersianChars.GetClearChars(father?.FirstName + father?.LastName))
                        {
                            agent.IsConfirmed = false;
                            agent.Locked = false;
                            agent.Description = config.Value.RejectAgentDescription;

                            await agentService.UpdateAsync(agent);
                            profile.Status = ProfileStatus.InvalidInformation;
                            await profileService.UpdateStatusAsync(profile);

                            await messagingService.SendSmsAsync(task.RefId, profile.Mobile.ToString(), profile.Carrier,
                                string.Format(config.Value.CheckAgentParentMessage, name, Environment.NewLine));
                            await recurringTaskRepository.EnableTaskAsync(task.RecurringTaskId.GetValueOrDefault(), false);
                            return TaskStatus.Done;
                        }

                    }
                    if (string.IsNullOrEmpty(profile.TraceCode))
                    {
                        profile.TraceCode = Tools.GenerateRandomNumber(config.Value.TraceCodeLength);
                        profile.Status = ProfileStatus.TraceCode;
                        await profileService.UpdateAsync(profile);
                        await messagingService.SendSmsAsync(task.RefId, profile.Mobile.ToString(), profile.Carrier, string.Format(config.Value.RealTraceCodeMessage, name, profile.TraceCode, Environment.NewLine));

                    }
                    else if (profile.Status < ProfileStatus.TraceCode && !string.IsNullOrEmpty(profile.TraceCode))
                    {
                        profile.Status = ProfileStatus.TraceCode;
                        await profileService.UpdateStatusAsync(profile);
                    }


                    await recurringTaskRepository.EnableTaskAsync(task.RecurringTaskId.GetValueOrDefault(), false);


                    return TaskStatus.Done;
                }

                if (isConfirmedLegalPerson && isConfirmedAccount)
                {
                    if (!string.IsNullOrEmpty(profile.TraceCode))
                    {
                        profile.Status = ProfileStatus.TraceCode;
                        await profileService.UpdateAsync(profile);

                        return TaskStatus.Done;
                    }

                    profile.TraceCode = Tools.GenerateRandomNumber(config.Value.TraceCodeLength);
                    profile.Status = ProfileStatus.TraceCode;
                    await profileService.UpdateAsync(profile);
                    await recurringTaskRepository.EnableTaskAsync(task.RecurringTaskId.GetValueOrDefault(), false);
                    await messagingService.SendSmsAsync(task.RefId, profile.Mobile.ToString(), profile.Carrier,
                        string.Format(config.Value.LegalTraceCodeMessage, name, profile.TraceCode, Environment.NewLine));

                    if (profile.Type == ProfileOwnerType.IranianLegalPerson)
                    {
                        var stakeHolders = await legalPersonStakeholderService.GetListAsync(profile.Id);

                        //send sms to all stakholders
                        var legalPersonStakeholders = stakeHolders.ToList();

                        var managers = legalPersonStakeholders.Where(x => x.Type == StakeHolderType.Manager).ToList();
                        foreach (var manager in managers)
                        {
                            var managerName = await profileService.GetFullName(manager.StakeholderProfileId);
                            var managerMobile = manager.StakeholderProfile.Mobile.ToString();
                            var isValid = ValidationHelper.IsMsisdnValid(ref managerMobile, out var carr);
                            if (!isValid)
                                return TaskStatus.BadParams;
                            await messagingService.SendSmsAsync(task.RefId, managerMobile, carr.GetValueOrDefault(),
                                string.Format(config.Value.DirectorateNotificationMessage, managerName, config.Value.ManagerTitle,
                                    name, Environment.NewLine));

                        }

                        var takeAccess = legalPersonStakeholders.Where(x => x.Type == StakeHolderType.TakeAccess).ToList();
                        foreach (var stakeholder in takeAccess)
                        {
                            var stakeHolderName =
                                await profileService.GetFullName(stakeholder.StakeholderProfileId);

                            var stakeholderMobile = stakeholder.StakeholderProfile.Mobile.ToString();
                            var isValid = ValidationHelper.IsMsisdnValid(ref stakeholderMobile, out var carr);
                            if (!isValid)
                                return TaskStatus.BadParams;
                            await messagingService.SendSmsAsync(task.RefId, stakeholderMobile, carr.GetValueOrDefault(),
                                string.Format(config.Value.DirectorateNotificationMessage, stakeHolderName,
                                    config.Value.TakeAccessTitle, name, Environment.NewLine));

                        }

                        var orderAccess = legalPersonStakeholders.Where(x => x.Type == StakeHolderType.OrderAccess)
                            .ToList();

                        foreach (var stakeholder in orderAccess)
                        {
                            var stakeHolderName =
                                await profileService.GetFullName(stakeholder.StakeholderProfileId);

                            var stakeholderMobile = stakeholder.StakeholderProfile.Mobile.ToString();
                            var isValid = ValidationHelper.IsMsisdnValid(ref stakeholderMobile, out var carr);
                            if (!isValid)
                                return TaskStatus.BadParams;

                            await messagingService.SendSmsAsync(task.RefId, stakeholderMobile, carr.GetValueOrDefault(),
                                string.Format(config.Value.DirectorateNotificationMessage, stakeHolderName,
                                    config.Value.OrderAccessTitle, name, Environment.NewLine));
                        }
                    }
                }

                return TaskStatus.Done;

            }

        }
    }
}