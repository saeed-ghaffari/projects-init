﻿using System;
using System.Collections.Generic;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.OfflineTasks.Model;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.Utility;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class GetPrivatePersonWithStatusExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public GetPrivatePersonWithStatusExecuter(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                IPrivatePersonApiService privatePersonApiService = scope.ServiceProvider.GetRequiredService<IPrivatePersonApiService>();
                IProfileApiService profileApiService = scope.ServiceProvider.GetRequiredService<IProfileApiService>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                INocrInquiryService nocrInquiryService = scope.ServiceProvider.GetRequiredService<INocrInquiryService>();


                GetProfileInfoTaskParam taskParam;

                try
                {
                    taskParam = JsonConvert.DeserializeObject<GetProfileInfoTaskParam>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                var callBackResponse = new GetPrivatePersonWithStatusCallbackData()
                {
                    Success = new List<KeyValuePair<string, PrivatePersonWithStatusViewModel>>(),
                    Failed = new List<CallbackErrorDataModel>(),
                    ReferenceId = task.Id
                };

                foreach (var info in taskParam.ProfileInfos)
                {
                    var uniqueIdentifier = info.UniqueIdentifier;
                    if (!uniqueIdentifier.IsPrivatePersonUniqueIdentifierValid() && !uniqueIdentifier.IsLegalPersonUniqueIdentifierValid())
                    {
                        callBackResponse.Failed.Add(new CallbackErrorDataModel()
                        {
                            UniqueIdentifier = uniqueIdentifier,
                            Meta = new[] { "invalid uniqueIdentifier" }
                        });

                        continue;
                    }
                    var privatePerson =
                        await privatePersonApiService.GetByProfileUniqueIdentifierAsync(uniqueIdentifier);

                    var profile =await profileApiService.GetByUniqueIdentifierAsync(uniqueIdentifier);
                    
                    if (profile?.Status<ProfileStatus.TraceCode || privatePerson==null)
                    {
                        callBackResponse.Failed.Add(new CallbackErrorDataModel()
                        {
                            UniqueIdentifier = uniqueIdentifier,
                            Meta = new[] { "profile not found" }
                        });

                        continue;
                    }
                    if (!privatePerson.BirthDate.Date.Equals(info.BirthDate.Date))
                    {
                        callBackResponse.Failed.Add(new CallbackErrorDataModel()
                        {
                            UniqueIdentifier = uniqueIdentifier,
                            Meta = new[] { "BirthDate is not correct" }
                        });

                        continue;
                    }

                    var inquiry = await nocrInquiryService.GetExistInquiryAsync(uniqueIdentifier, privatePerson.BirthDate.ToPersianDate());

                    if (inquiry != null)
                        ChangePrivatePersonInfo(privatePerson, inquiry);

                    if (profile != null)
                    {
                        var privatePersonWithStatus=new PrivatePersonWithStatusViewModel()
                        {
                            Mobile = profile?.Mobile.ToString(),
                            PrivatePerson = privatePerson,
                            Status = profile.Status
                        };

                        callBackResponse.Success.Add(
                            new KeyValuePair<string, PrivatePersonWithStatusViewModel>(uniqueIdentifier, privatePersonWithStatus));
                    }
                }

                //create callback task
                await taskRepository.AddAsync(new ScheduledTask()
                {
                    Type = CerberusTasksIdentifierHelper.CallbackThirdPartyServices.Value,
                    DueTime = DateTime.Now,
                    GroupId = CerberusTasksIdentifierHelper.CallbackThirdPartyServices.Key,
                    RefId = taskParam.ServiceId,
                    OriginalTaskId = task.Id,
                    Params = JsonConvert.SerializeObject(new CallbackThirdPartyServicesTaskParams()
                    {
                        CallbackUrl = taskParam.CallbackUrl,
                        Payload = callBackResponse,
                        ServiceId = taskParam.ServiceId
                    }),
                });

                return TaskStatus.Done;
            }
        }

        private void ChangePrivatePersonInfo(PrivatePersonViewModel privatePerson, NocrInquiry inquiry)
        {
            privatePerson.FirstName = inquiry.FirstName.Unify();
            privatePerson.LastName = inquiry.LastName.Unify();
            privatePerson.FatherName = inquiry.FatherName.Unify();

            privatePerson.Serial = inquiry.SerialNumber;

            var seriNumber = Tools.GetDigitsFromString(inquiry.SeriesNumber ?? string.Empty);

            privatePerson.SeriSh = seriNumber;

            if (seriNumber.Length > 0)
            {
                privatePerson.SeriShChar = inquiry.SeriesNumber == null
                    ? string.Empty
                    : inquiry.SeriesNumber.Replace(seriNumber, ""); //get characters from seriesNumber
            }
            else
            {
                privatePerson.SeriShChar = inquiry.SeriesNumber;
            }

            privatePerson.ShNumber = inquiry.IdentificationNumber;
        }
    }
}