﻿using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;
using System;
using System.Collections.Generic;
using System.Linq;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Service;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Cerberus.OfflineTasks.Executers
{
    public class DeadDownloadInquiryExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public DeadDownloadInquiryExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;
        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var deadListInquiryService = scope.ServiceProvider.GetRequiredService<IDeadInquiryService>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();
                var profileHistoryService = scope.ServiceProvider.GetRequiredService<IProfileHistoryService>();
                IOptions<InquiryConfiguration> config = scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();
                var messageConfig = scope.ServiceProvider.GetRequiredService<IOptions<IssuanceStockCodeConfiguration>>();
                var messageService = scope.ServiceProvider.GetRequiredService<IMessagingService>();
                if (string.IsNullOrWhiteSpace(task.Params))
                    return TaskStatus.BadParams;

                var param = task.Params.ToGuid();

                if (task.RecoverySequence > config.Value.MaxRecoverySequence)
                {
                    var mobiles = messageConfig.Value.SendErrorMobiles;

                    foreach (var m in mobiles)
                    {
                        await messageService.SendSmsAsync(
                            null,
                            m,
                            null,
                            string.Format(
                                $"دیشب نخواستم بیدارت کنم ، سرویس فوتی های ثبت احوال خطا داد،یادت نره یه بررسی بکن "), DateTime.Now.AddHours(6));
                    }
                    return TaskStatus.Canceled;
                }

                var inquiry = await deadListInquiryService.GetAsync(param);

                if (inquiry == null || inquiry.HasException || inquiry.DataNotReady)
                {
                    task.DueTime = DateTime.Now.AddMinutes(config.Value.DeadAddTimeToDownloadRequest);
                    task.RecoverySequence++;
                    task.Status = TaskStatus.Pending;
                    await taskRepository.AddAsync(task);
                    return TaskStatus.Recovered;
                }

                foreach (var item in inquiry.DeadInquiries)
                {

                    if (!string.IsNullOrWhiteSpace(item.UniqueIdentifier) && item.UniqueIdentifier.Length == 10)
                    {
                        if (item.UniqueIdentifier.IsAllDigits())
                        {
                            var profile =
                                await profileService.GetByUniqueIdentifierWithNoCacheAsync(item.UniqueIdentifier, false, false);
                            if (profile != null && profile.Status != ProfileStatus.Dead)
                            {

                                var oldStatus = profile.Status;
                                profile.Status = ProfileStatus.Dead;
                                profile.StatusReasonType = StatusReasonType.DeadInquiry;
                                await profileService.UpdateStatusAndReasonAsync(profile);


                                await profileHistoryService.CreateAsync(new ProfileHistory
                                {
                                    ProfileId = profile.Id,
                                    ChangeFrom = oldStatus,
                                    ReferenceId = item.Id.ToString(),

                                }, ProfileStatus.Dead);

                            }

                        }
                        else
                        {
                            var firstUniq = item.UniqueIdentifier.Substring(0, 3);

                            var profiles = await profileService.GetForDeadAsync(firstUniq, item.BirthDate);
                            if (profiles.Any())
                            {
                                var inquirySide = PersianChars.GetClearChars((item.LastName).Trim());
                                var lastUnique = item.UniqueIdentifier.Substring(7, 3);

                                var p = profiles.FirstOrDefault(x =>
                                    x.UniqueIdentifier.EndsWith(lastUnique) &&
                                    PersianChars.GetClearChars((x.PrivatePerson.LastName).Trim()) == inquirySide);
                                if (p != null && p.Status != ProfileStatus.Dead)
                                {
                                    var oldStatus = p.Status;
                                    p.Status = ProfileStatus.Dead;
                                    p.StatusReasonType = StatusReasonType.DeadInquiry;
                                    await profileService.UpdateStatusAndReasonAsync(p);

                                    await profileHistoryService.CreateAsync(new ProfileHistory
                                    {
                                        ProfileId = p.Id,
                                        ChangeFrom = oldStatus,
                                        ReferenceId = item.Id.ToString(),

                                    }, ProfileStatus.Dead);
                                }
                            }
                        }

                    }
                }

                return TaskStatus.Done;
            }
        }
    }
}
