﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.Interface.ReadRepositories.ReadInquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.Model;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.Service;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class IssuanceStockCodeOneExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;
        private static readonly NLog.ILogger Logger = NLog.LogManager.GetCurrentClassLogger();

        public IssuanceStockCodeOneExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;
        public bool CanHandleTask(string param) => true;

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var issuanceStockCodeService = scope.ServiceProvider.GetRequiredService<IIssuanceStockCodeService>();
                var issuanceStockCodeReadRepository = scope.ServiceProvider.GetRequiredService<IIssuanceStockCodeReadRepository>();
                var issuanceStockCodeResponseRepository = scope.ServiceProvider.GetRequiredService<IIssuanceStockCodeResponseRepository>();
                var config = scope.ServiceProvider.GetRequiredService<IOptions<IssuanceStockCodeConfiguration>>();
                var messageService = scope.ServiceProvider.GetRequiredService<IMessagingService>();
                var calendarRepository = scope.ServiceProvider.GetRequiredService<ICalendarRepository>();



                if (DateTime.Now.TimeOfDay < config.Value.FromTime || DateTime.Now.TimeOfDay >= config.Value.ToTime)
                {
                    await AddNewTaskAsync(task, config.Value.FromTime);
                    return TaskStatus.Done;
                }

                if (await calendarRepository.TodayIsHoliday())
                {
                    await AddNewTaskAsync(task, config.Value.FromTime);
                    return TaskStatus.Done;
                }

                if (DateTime.Now.DayOfWeek == DayOfWeek.Thursday)
                {
                    await AddNewTaskAsync(task, config.Value.FromTime);
                    return TaskStatus.Done;
                }

                Console.WriteLine("try to get process queue...");
                var counter = 0;
                while (true)
                {

                    if (DateTime.Now.TimeOfDay >= config.Value.ToTime)
                        break;


                    var profiles =(await issuanceStockCodeReadRepository.GetBatchAsync(config.Value.GetCurrentDay,
                           config.Value.Mode, config.Value.Divisor)).ToList();

                    Console.WriteLine($"_fetched {profiles.Count()} records from DB");

                    var take = config.Value.RecordCountToSend;
                    var skip = 0;
                    var recordToSend = profiles.Skip(skip).Take(take).ToList();

                    while (recordToSend.Any())
                    {

                        //var badDataList = null;
                        var removeList = recordToSend.Where(x =>
                            string.IsNullOrWhiteSpace(x.SerialNumber) || string.IsNullOrWhiteSpace(x.SeriesNumber) ||
                            string.IsNullOrWhiteSpace(x.IssuePlace)
                            || string.IsNullOrEmpty(x.SerialNumber) || string.IsNullOrEmpty(x.SeriesNumber) ||
                            string.IsNullOrEmpty(x.IssuePlace)).ToList();
                        if (removeList.Any())
                        {
                            var param = new UpdateIssuanceCodeTaskParams
                            {
                                Id = removeList.Select(x => x.IssuanceStockCodeId).ToList(),
                                Status = IssuanceStockCodeStatus.InvalidData,
                                Successful = false
                            };
                            await UpdateIssuanceCodeTask(param, task.Id, DateTime.Now.AddSeconds(3));
                        }

                        var profileCount = recordToSend.Count(x => !removeList.Contains(x));
                        counter += profileCount;

                        Console.WriteLine($"fetched {profileCount} records from queue");

                        try
                        {

                            var watcher = new Stopwatch();
                            watcher.Start();

                            var response = await issuanceStockCodeService.GetStockCodeAsync(recordToSend.Where(x => !removeList.Contains(x)));

                            watcher.Stop();

                            Console.WriteLine(
                                $"AS400 Response Time For {profileCount} Records(In Seconds): {watcher.Elapsed.Seconds}");
                            Console.WriteLine($"Total sent record in today until now: {counter}");


                            if (response.StatusCode == HttpStatusCode.OK)
                            {
                                var responseListToCreate = new List<IssuanceStockCodeResponse>();
                                var iscodeWrongList = new List<long>();
                                var iscodeRetryList = new List<long>();

                                foreach (var code in response.codeResponseModel)
                                {
                                    var profile = recordToSend.FirstOrDefault(x => x.Nin == code.NIN);

                                    if (profile != null)
                                    {
                                        responseListToCreate.Add(new IssuanceStockCodeResponse
                                        {
                                            Message = code.Message,
                                            InvestorCode = code.InvestorCode,
                                            IssuanceStockCodeId = profile.IssuanceStockCodeId,
                                            LegacyCode = code.LegacyCode,
                                            IsActive = true
                                        });

                                        if (string.IsNullOrEmpty(code.LegacyCode))
                                        {
                                            if ((code.Message.Contains("Bad") && code.Message.Contains("Update")) || (code.Message.Contains("Bad") && code.Message.ToLower().Contains("INSERT")))//conflict on issuance code, person must go to the his broker for update legacy code
                                            {
                                                if (profile.TryCount >= config.Value.BadResponseTryCount)
                                                {
                                                    iscodeWrongList.Add(profile.IssuanceStockCodeId);

                                                    await messageService.SendSmsAsync(
                                                        profile.ProfileId, profile.Mobile, null,
                                                        string.Format(config.Value.UnSuccessIssuanceMessage,
                                                            Environment.NewLine));
                                                }
                                                else
                                                {
                                                    iscodeRetryList.Add(profile.IssuanceStockCodeId);
                                                }
                                            }
                                        }
                                        else
                                            await messageService.SendSmsAsync(
                                                profile.ProfileId, profile.Mobile, null,
                                                string.Format(config.Value.SuccessIssuanceMessage, code.LegacyCode, Environment.NewLine));
                                    }
                                }

                                if (response.codeResponseModel.Any())
                                {
                                    // --- For success responses 
                                    var codeTaskParams = new UpdateIssuanceCodeTaskParams
                                    {
                                        Id = recordToSend.Where(x => !removeList.Contains(x)).Where(x=> !iscodeWrongList.Contains(x.IssuanceStockCodeId) && !iscodeRetryList.Contains(x.IssuanceStockCodeId)).Select(x => x.IssuanceStockCodeId)
                                                  .ToList(),
                                        Status = IssuanceStockCodeStatus.Done,
                                        Successful = true
                                    };
                                    await UpdateIssuanceCodeTask(codeTaskParams, task.Id, DateTime.Now.AddSeconds(3));
                                    // --- For wrong responses
                                    codeTaskParams = new UpdateIssuanceCodeTaskParams
                                    {
                                        Id = iscodeWrongList,
                                        Status = IssuanceStockCodeStatus.InvalidData,
                                        Successful = false
                                    };
                                    await UpdateIssuanceCodeTask(codeTaskParams, task.Id, DateTime.Now.AddSeconds(3));
                                    // --- For retry list
                                    codeTaskParams = new UpdateIssuanceCodeTaskParams
                                    {
                                        Id = iscodeRetryList,
                                        Status = IssuanceStockCodeStatus.None,
                                        Successful = false
                                    };
                                    await UpdateIssuanceCodeTask(codeTaskParams, task.Id, DateTime.Now.AddSeconds(3));
                                    await issuanceStockCodeResponseRepository.CreateRangeAsync(responseListToCreate);
                                }

                            }
                            else
                            {
                                Logger.Error(response.codeResponseModel);
                            }

                            if (DateTime.Now.TimeOfDay >= config.Value.ToTime)
                                break;
                        }
                        catch (Exception e)
                        {
                            if (config.Value.EnableSendErrorMessage)
                            {
                                var mobiles = config.Value.SendErrorMobiles;
                                Console.WriteLine(e);
                                Logger.Error(e);
                                Logger.Error(JsonConvert.SerializeObject(recordToSend));
                                foreach (var mobile in mobiles)
                                {
                                    await messageService.SendSmsAsync(
                                        null,
                                        mobile,
                                        null,
                                        string.Format(
                                            $"خطا در سرویس صدور کد بورسی در ساعت: {DateTime.Now.Hour}:{DateTime.Now.Minute}:{DateTime.Now.Second} . \r\nتعداد {profileCount} نفر در لیست انتظار رفتند. "));
                                }
                            }

                            var codeTaskParams = new UpdateIssuanceCodeTaskParams
                            {
                                Id = recordToSend.Where(x => !removeList.Contains(x)).Select(x => x.IssuanceStockCodeId).ToList(),
                                Status = IssuanceStockCodeStatus.Exception,
                                Successful = false
                            };
                            await UpdateIssuanceCodeTask(codeTaskParams, task.Id, DateTime.Now.AddSeconds(5));
                            await Task.Delay(1000 * 60 * 1);
                        }


                        skip += take;
                        recordToSend = profiles.Skip(skip).Take(take).ToList();
                    }

                    Console.WriteLine($"<======================================>{Environment.NewLine}");

                    await Task.Delay(config.Value.Delay);



                }

                Console.WriteLine($"Task was done at {DateTime.Now}");
                Console.WriteLine($"<**************************************>{Environment.NewLine}");

                await AddNewTaskAsync(task, config.Value.FromTime);

                return TaskStatus.Done;

            }
        }

        /// <summary>
        /// add new task for run on tomorrow
        /// </summary>
        /// <param name="task"></param>
        /// <param name="nextTime"></param>
        /// <returns></returns>
        private async Task AddNewTaskAsync(ScheduledTask task, TimeSpan nextTime)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();

                task.Status = TaskStatus.Pending;
                var nextDate = task.DueTime.AddDays(1).Date;
                task.DueTime = nextDate + nextTime;
                task.RecurringTaskId = null;
                await taskRepository.AddAsync(task);
            }
        }


        private async Task UpdateIssuanceCodeTask(UpdateIssuanceCodeTaskParams p, long taskId, DateTime dueTime)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                await taskRepository.AddAsync(new ScheduledTask
                {

                    Type = CerberusTasksIdentifierHelper.UpdateIssuanceStockCode.Value,
                    DueTime = dueTime,
                    GroupId = CerberusTasksIdentifierHelper.UpdateIssuanceStockCode.Key,
                    RefId = null,
                    OriginalTaskId = taskId,
                    Params = JsonConvert.SerializeObject(p)

                });
            }
        }
    }

}