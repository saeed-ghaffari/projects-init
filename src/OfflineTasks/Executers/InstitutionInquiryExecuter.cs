﻿using Cerberus.Domain.Interface.Inquiries;
using Cerberus.Domain.ViewModel;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using System;
using System.Threading.Tasks;
using Cerberus.Service;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class InstitutionInquiryExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }
        public InstitutionInquiryExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task<TasksManager.Core.Enum.TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                IInstitutionInquiryService institutionInquiry = scope.ServiceProvider.GetRequiredService<IInstitutionInquiryService>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();
                IOptions<InquiryConfiguration> config = scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();

                InstitutionInquiryVM param;

                try
                {
                    param = JsonConvert.DeserializeObject<InstitutionInquiryVM>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                if (task.RefId.GetValueOrDefault() <= 0)
                    return TaskStatus.BadParams;
                var inquiry = await institutionInquiry.GetAsync(param.PrivateCode);

                if (inquiry == null || inquiry.HasException)
                {
                    task.DueTime = DateTime.Now.AddMinutes(config.Value.InquiryCheckNextTime);
                    task.RecoverySequence++;
                    task.Status = TaskStatus.Pending;
                    await taskRepository.AddAsync(task);
                    return TaskStatus.Recovered;

                }

                //Todo: check integrity data
                return TaskStatus.Done;

            }
        }
    }
}
