﻿using System;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using Cerberus.Utility.Enum;
using Microsoft.Extensions.DependencyInjection;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;

namespace Cerberus.OfflineTasks.Executers
{
    public class ChangeProfileDataExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public ChangeProfileDataExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;
        public bool CanHandleTask(string param) => true;

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var profileService = scope.ServiceProvider.GetRequiredService<IProfileService>();

                ChangeProfileDataTaskParam param;
                try
                {
                    param = Newtonsoft.Json.JsonConvert.DeserializeObject<ChangeProfileDataTaskParam>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                if (task.RefId.GetValueOrDefault() <= 0)
                    return TaskStatus.BadParams;

                var profile = await profileService.GetAsync(param.ProfileId);

                if (profile == null)
                    return TaskStatus.BadParams;

                if (param.ProfileField == ProfileField.UniqueIdentifier)
                {
                    switch (profile.Type)
                    {
                        case ProfileOwnerType.IranianPrivatePerson when !param.Value.IsPrivatePersonUniqueIdentifierValid():
                        case ProfileOwnerType.IranianLegalPerson when !param.Value.IsLegalPersonUniqueIdentifierValid():
                            return TaskStatus.BadParams;
                        default:

                            profile.UniqueIdentifier = param.Value;
                            await profileService.UpdateAsync(profile);
                            return TaskStatus.Done;
                    }
                }

                if (param.ProfileField == ProfileField.Mobile)
                {
                    string msisdn = param.Value;
                    if (!ValidationHelper.IsMsisdnValid(ref msisdn, out Carrier? carrier) || carrier == null)
                        return TaskStatus.BadParams;
                    profile.Mobile = msisdn.ToLong();
                    await profileService.UpdateAsync(profile);
                    return TaskStatus.Done;
                }

                return TaskStatus.Canceled;
            }
        }
    }
}