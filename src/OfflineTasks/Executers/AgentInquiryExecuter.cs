﻿using System;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Services;
using Cerberus.Domain.ViewModel;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using TaskStatus = Cerberus.TasksManager.Core.Enum.TaskStatus;
using System.Linq;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Service;
using Cerberus.Utility;
using Cerberus.Utility.Enum;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Cerberus.OfflineTasks.Executers
{
    public class AgentInquiryExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;


        public bool IsReusable => false;
        public bool CanHandleTask(string param)
        {
            return true;
        }

        public AgentInquiryExecuter(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public async Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {

                var messagingService = scope.ServiceProvider.GetRequiredService<IMessagingService>();
                var agentRepository = scope.ServiceProvider.GetRequiredService<IAgentRepository>();
                var agentReadRepository = scope.ServiceProvider.GetRequiredService<IAgentReadRepository>();
                var privatePersonService = scope.ServiceProvider.GetRequiredService<IPrivatePersonService>();
                var bankingAccountService = scope.ServiceProvider.GetRequiredService<IBankingAccountService>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();

                var config = scope.ServiceProvider.GetRequiredService<IOptions<InquiryConfiguration>>();

                AgentInquiryVm param;
                try
                {
                    param = Newtonsoft.Json.JsonConvert.DeserializeObject<AgentInquiryVm>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                if (task.RefId.GetValueOrDefault() <= 0)
                    return TaskStatus.BadParams;

                var result = await agentReadRepository.GetConditionalListAsync(x =>
                     x.AgentProfileId == param.AgentProfileId.GetValueOrDefault() && x.ProfileId == task.RefId.GetValueOrDefault());


                var agent = result.FirstOrDefault();
                if (agent != null)
                {
                    var privatePerson = await privatePersonService.GetByProfileIdAsync(task.RefId.GetValueOrDefault());

                    if (privatePerson.IsConfirmed == false)
                    {
                        agent.Locked = false;
                        await agentRepository.UpdateAsync(agent);
                        return TaskStatus.Canceled;
                    }
                    if (privatePerson.IsConfirmed == null)
                    {
                        task.DueTime = DateTime.Now.AddMinutes(config.Value.AgentVarificationNextTime);
                        task.RecoverySequence++;
                        task.Status = TaskStatus.Pending;
                        await taskRepository.AddAsync(task);
                        return TaskStatus.Recovered;
                    }
                    if (agent.Type == AgentType.Province && agent.IsConfirmed != true)
                    {
                        if (Tools.IsUnder18Years(privatePerson.BirthDate))
                        {
                            var father = await privatePersonService.GetByProfileIdAsync(agent.AgentProfileId);

                            if (PersianChars.GetClearChars(privatePerson.LastName) ==
                                PersianChars.GetClearChars(father.LastName) &&
                                PersianChars.GetClearChars(privatePerson.FatherName) ==
                                PersianChars.GetClearChars(father.FirstName))
                            {
                                agent.Locked = true;
                                agent.IsConfirmed = true;
                                await agentRepository.UpdateAsync(agent);
                                return TaskStatus.Done;
                            }
                        }
                    }

                    var bankingAccount = await bankingAccountService.GetListAsync(task.RefId.GetValueOrDefault());
                    if (bankingAccount.Any(x => x.IsConfirmed != true))
                    {
                        agent.Locked = false;
                        await agentRepository.UpdateAsync(agent);
                        return TaskStatus.Canceled;
                    }

                    if (agent.IsConfirmed == null)
                    {
                        if (!param.SendSmsVerificationAgent)
                        {
                            param.SendSmsVerificationAgent = true;
                            task.Params = Newtonsoft.Json.JsonConvert.SerializeObject(param);
                            await messagingService.SendSmsAsync(task.RefId, param.Mobile, Carrier.Mtn, string.Format(
                                config.Value.VerificationAgentInquiryMessage, $"{privatePerson.FirstName} {privatePerson.LastName}", Environment.NewLine));

                        }

                        return TaskStatus.Done;
                    }

                    if (agent.IsConfirmed == false)
                    {
                        agent.Locked = false;
                        await agentRepository.UpdateAsync(agent);
                        await messagingService.SendSmsAsync(task.RefId, param.Mobile, Carrier.Mtn, string.Format(
                            config.Value.InquiryNotConfirmedMessage, $"{privatePerson.FirstName} {privatePerson.LastName}", config.Value.AgentInquiryTitle, Environment.NewLine));
                        return TaskStatus.Done;
                    }

                    agent.Locked = true;
                    await agentRepository.UpdateAsync(agent);
                    return TaskStatus.Done;
                }

                return TaskStatus.Done;
            }

        }
    }
}
