﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cerberus.Domain.Extensions;
using Cerberus.Domain.Interface.ApiServices;
using Cerberus.Domain.ViewModel.Api;
using Cerberus.Domain.ViewModel.OfflineTasks;
using Cerberus.Domain.ViewModel.OfflineTasks.TaskParams;
using Cerberus.OfflineTasks.Model;
using Cerberus.TasksManager.Core.Enum;
using Cerberus.TasksManager.Core.Interface;
using Cerberus.TasksManager.Core.Model;
using Cerberus.Utility;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace Cerberus.OfflineTasks.Executers
{
    public class GetBankingAccountsExecuter : ITaskExecuter
    {
        private readonly IServiceProvider _serviceProvider;

        public GetBankingAccountsExecuter(
            IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public bool IsReusable => false;

        public bool CanHandleTask(string param)
        {
            return true;
        }

        public async System.Threading.Tasks.Task<TaskStatus> ExecuteTask(ScheduledTask task)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                IBankingAccountApiService bankingAccountService = scope.ServiceProvider.GetRequiredService<IBankingAccountApiService>();
                IProfileApiService profileApiService = scope.ServiceProvider.GetRequiredService<IProfileApiService>();
                ITaskRepository taskRepository = scope.ServiceProvider.GetRequiredService<ITaskRepository>();



                GetBankingAccountsTaskParams taskParam;

                try
                {
                    taskParam = JsonConvert.DeserializeObject<GetBankingAccountsTaskParams>(task.Params);
                }
                catch
                {
                    return TaskStatus.BadParams;
                }

                var callBackResponse = new GetBankingAccountsCallbackData()
                {
                    Success = new List<KeyValuePair<string, IEnumerable<BankingAccountViewModel>>>(),
                    Failed = new List<GetBankingAccountsCallbackError>(),
                    ReferenceId = task.Id
                };

                foreach (var uniqueIdentifier in taskParam.UniqueIdentifiers)
                {
                    if (!uniqueIdentifier.IsPrivatePersonUniqueIdentifierValid() && !uniqueIdentifier.IsLegalPersonUniqueIdentifierValid())
                    {
                        callBackResponse.Failed.Add(new GetBankingAccountsCallbackError()
                        {
                            UniqueIdentifier = uniqueIdentifier,
                            Meta = new[] { "invalid uniqueIdentifier" }
                        });

                        continue;
                    }

                    var profile = await profileApiService.GetByUniqueIdentifierAsync(uniqueIdentifier);
                    if (profile == null || !profile.Status.IsConfirmed())
                    {
                        callBackResponse.Failed.Add(new GetBankingAccountsCallbackError()
                        {
                            UniqueIdentifier = uniqueIdentifier,
                            Meta = new[] { "profile not found" }
                        });

                        continue;
                    }

                    var accounts = await bankingAccountService.GetByProfileUniqueIdentifierAsync(profile.UniqueIdentifier);

                    callBackResponse.Success.Add(
                        new KeyValuePair<string, IEnumerable<BankingAccountViewModel>>(uniqueIdentifier,
                            accounts.Where(x => x.IsDefault)));
                }

                //create callback task
                await taskRepository.AddAsync(new ScheduledTask()
                {
                    Type = CerberusTasksIdentifierHelper.CallbackThirdPartyServices.Value,
                    DueTime = DateTime.Now,
                    GroupId = CerberusTasksIdentifierHelper.CallbackThirdPartyServices.Key,
                    RefId = taskParam.ServiceId,
                    OriginalTaskId = task.Id,
                    Params = JsonConvert.SerializeObject(new CallbackThirdPartyServicesTaskParams()
                    {
                        CallbackUrl = taskParam.CallbackUrl,
                        Payload = callBackResponse,
                        ServiceId = taskParam.ServiceId
                    }),
                });

                return TaskStatus.Done;
            }
        }
    }
}