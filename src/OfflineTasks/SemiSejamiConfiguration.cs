﻿namespace Cerberus.OfflineTasks
{
    public class SemiSejamiConfiguration
    {
        public string FirstTierPishkhanMessageFormat { get; set; }
        public string FirstTierKaraMessageFormat { get; set; }

        public string SecondTierMessageFormat { get; set; }
        public int SecondTier { get; set; }

        public string ThirdTierMessageFormat { get; set; }
        public int ThirdTier { get; set; }
    }

}