﻿using System.Threading.Tasks;

namespace Gatekeeper.AspModules.Interface
{
    public interface IRateLimitService
    {
        Task<bool> HasAccessAsync(string resourceKey, int periodInSec, int limit);
    }
}