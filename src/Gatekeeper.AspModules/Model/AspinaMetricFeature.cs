﻿using System.Collections.Generic;
using Gatekeeper.AspModules.Interface;

namespace Gatekeeper.AspModules.Model
{
    public class AspinaMetricFeature : IAspinaMetricFeatures
    {
        private readonly List<KeyValuePair<string, string>> _tags;

        public AspinaMetricFeature()
        {
            this._tags = new List<KeyValuePair<string, string>>(); ;
        }

        public void AddTag(string key, string value)
        {
            _tags.Add(new KeyValuePair<string, string>(key, value));
        }

        public IEnumerable<KeyValuePair<string, string>> GetAllTags()
        {
            return _tags;
        }
    }
}