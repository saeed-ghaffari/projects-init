﻿using System;
using System.Collections.Generic;

namespace Gatekeeper.AspModules.Model
{
    public class RequestLog
    {
        public string Method { get; set; }
        public int? HttpResponseCode { get; set; }
        public string Query { get; set; }
        public IEnumerable<KeyValuePair<string, string>> Headers { get; set; }
        public long ExecutionTime { get; set; }
        public DateTime CreationDate { get; set; }
        public List<KeyValuePair<string, string>> Tags { get; set; }
        public string Ip { get; set; }
    }
}