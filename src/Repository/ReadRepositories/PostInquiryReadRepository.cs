﻿using Cerberus.Domain.Interface.ReadRepositories.ReadInquiries;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.Repository.MsSql;

namespace Cerberus.Repository.ReadRepositories
{
    public class PostInquiryReadRepository : BaseReadRepository<PostInquiry>, IPostInquiryReadRepository
    {
        public PostInquiryReadRepository(ReadCerberusDbContext db) : base(db)
        {
        }
    }
}