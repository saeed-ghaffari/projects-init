﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Model;
using Cerberus.Repository.MsSql;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories
{
    public class AgentDocumentReadRepository : BaseReadRepository<AgentDocument>, IAgentDocumentReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;

        public AgentDocumentReadRepository(ReadCerberusDbContext db) : base(db)
        {
            _dbContext = db;
        }

        public async Task<List<AgentDocument>> GetListDocumentByAgentId(long id)
        {
            return await _dbContext.AgentDocuments.Where(x => x.AgentId == id).ToListAsync();
        }

       
    }
}