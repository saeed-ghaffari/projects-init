﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Model;
using Cerberus.Repository.MsSql;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories
{
    public class JobReadRepository : BaseReadRepository<Job>, IJobReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;

        public JobReadRepository(ReadCerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }

        public async Task<List<Job>> GetListAsync()
        {
            return await _dbContext.Jobs.AsNoTracking().ToListAsync();
        }
    }
}