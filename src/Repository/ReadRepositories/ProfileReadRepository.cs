﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.Repository.MsSql;
using Dapper;
using Microsoft.EntityFrameworkCore;
using Cerberus.Domain.Model.StatusData;
using Cerberus.Domain.ViewModel.StatusData;
using System.Data;
using System.Data.SqlClient;


namespace Cerberus.Repository.ReadRepositories
{
    public class ProfileReadRepository : BaseReadRepository<Profile>, IProfileReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;
        private readonly ISqlConnectionProvider _sqlConnectionProvider;

        public ProfileReadRepository(ReadCerberusDbContext dbContext,
            ISqlConnectionProvider sqlConnectionProvider) : base(dbContext)
        {
            _dbContext = dbContext;
            _sqlConnectionProvider = sqlConnectionProvider;
        }


        public async Task<Profile> GetByUniqueIdentifierAsync(string uniqueIdentifier, bool getAllRelations,
            bool includeAllChildRelations, ProfileOwnerType? type = null)
        {
            if (!getAllRelations)
                return await _dbContext.Profiles.FirstOrDefaultAsync(x => x.UniqueIdentifier == uniqueIdentifier);

            return await GetAllProfileDataAsync(uniqueIdentifier);
        }

        private async Task<Profile> GetAllProfileDataAsync(string uniqueIdentifier)
        {
            using (var con = _sqlConnectionProvider.GetConnection())
            {
                var result = await con.QueryMultipleAsync("[SP_GetProfileByUniqueIdentifier] @UniqueIdentifier",
                    new { uniqueIdentifier });

                var profile = await result.ReadFirstOrDefaultAsync<Profile>();
                if (profile == null)
                    return null;


               

                var dictFinancial = new Dictionary<long, FinancialInfo>();
                profile.FinancialInfo = result.Read<FinancialInfo, FinancialBroker, Broker, FinancialInfo>(
                    (info, infoBrokers, brokers) =>
                    {
                        if (info == null)
                            return null;

                        if (infoBrokers == null)
                            return info;

                        if (!dictFinancial.TryGetValue(info.Id, out var infoEntry))
                        {
                            infoEntry = info;
                            infoEntry.FinancialBrokers = new List<FinancialBroker>();
                            dictFinancial.Add(infoEntry.Id, infoEntry);
                        }

                        infoBrokers.Broker = brokers;
                        infoEntry.FinancialBrokers.Add(infoBrokers);

                        return infoEntry;
                    }).FirstOrDefault();



                profile.Accounts = result.Read<BankingAccount, Bank, City, BankingAccount>((account, bank, city) =>
                {
                    if (account == null)
                        return null;
                    account.Bank = bank;
                    account.BranchCity = city;

                    return account;
                })?.ToList();

                profile.Addresses = result.Read<Address, Country, Province, City, AddressSection, Address>(
                    (address, country, province, city, section) =>
                    {
                        if (address == null)
                            return null;
                        address.Country = country;
                        address.Province = province;
                        address.City = city;
                        address.Section = section;

                        return address;
                    })?.ToList();


                profile.IssuanceStockCodes = result.Read<IssuanceStockCode, IssuanceStockCodeResponse, IssuanceStockCode>((issuanceStockCode, issuanceStockCodeResponse) =>
                {
                    if (issuanceStockCode == null)
                        return null;
                    issuanceStockCode.IssuanceStockCodeResponses = new List<IssuanceStockCodeResponse> { issuanceStockCodeResponse };
                    return issuanceStockCode;
                }).ToList();

                // profile.IssuanceStockCodes = (await result.ReadAsync<IssuanceStockCode>())?.ToList();
      


                if (profile.Type == ProfileOwnerType.IranianPrivatePerson)
                {
                    var privatePersonResult =
                        await con.QueryMultipleAsync(
                            "SP_GetProfilePrivatePersonInfoByProfileId @ProfileId", new { ProfileId = profile.Id });

                    profile.PrivatePerson = await privatePersonResult.ReadFirstOrDefaultAsync<PrivatePerson>();


                    profile.Agent = privatePersonResult.Read<Agent, Profile, PrivatePerson, Agent>((agent, agentProfile, privatePerson) =>
                    {
                        agent.AgentProfile = agentProfile;
                        agent.AgentProfile.PrivatePerson = privatePerson;

                        return agent;
                    }).FirstOrDefault();

                    profile.JobInfo = privatePersonResult.Read<JobInfo, Job, JobInfo>((info, job) =>
                    {
                        if (info == null)
                            return null;
                        info.Job = job;
                        return info;
                    }).FirstOrDefault();

                }

                if (profile.Type == ProfileOwnerType.IranianLegalPerson)
                {

                    var legalPersonResult =
                        await con.QueryMultipleAsync("SP_GetProfileLegalPersonInfoByProfileId @ProfileId",
                            new { ProfileId = profile.Id });

                    profile.LegalPerson = await legalPersonResult.ReadFirstOrDefaultAsync<LegalPerson>();

                    profile.LegalPersonShareholders = (await legalPersonResult.ReadAsync<LegalPersonShareholder>())?.ToList();


                    profile.LegalPersonStakeholders = legalPersonResult
                        .Read<LegalPersonStakeholder, Cerberus.Domain.Model.Profile,
                            LegalPersonStakeholder>(
                            (stake, stakeProfile) =>
                            {
                                if (stake == null)
                                    return null;

                                if (stakeProfile == null)
                                    return stake;

                                stake.StakeholderProfile = stakeProfile;
                                stake.UniqueIdentifier = stakeProfile.UniqueIdentifier;
                                return stake;
                            })?.ToList();


                }

                if (profile?.Agent?.AgentProfile?.PrivatePerson != null)
                {
                    profile.Agent.UniqueIdentifier = profile?.Agent?.AgentProfile?.UniqueIdentifier;
                    profile.Agent.FirstName = profile?.Agent?.AgentProfile?.PrivatePerson?.FirstName;
                    profile.Agent.LastName = profile?.Agent?.AgentProfile?.PrivatePerson?.LastName;

                }

                return profile;
            }

        }




        public async Task<Profile> GetAsync(long id, bool getAllRelations)
        {
            if (!getAllRelations)
                return await GetAsync(id);

            using (var con = _sqlConnectionProvider.GetReadConnection())
            {
                var result = await con.QueryMultipleAsync($" select top 1 * from Profile (nolock) where Id={id}" +
                                                          $" select top 1 * from Agent (nolock)  where Agent.ProfileId={id}" +
                                                          $" select top 1 * from FinancialInfo (nolock) where FinancialInfo.ProfileId={id}" +
                                                          $" select * from TradingCode (nolock) where ProfileId={id}" +
                                                          $" select top 2 * from BankingAccount (nolock) where BankingAccount.ProfileId={id}" +
                                                          $" select top 2 * from Address (nolock) where ProfileId={id}" +
                                                          $" select top 1 * from PrivatePerson (nolock) where PrivatePerson.ProfileId={id}" +
                                                          $" select top 1 * from JobInfo (nolock)  where ProfileId={id}" +
                                                          $" select top 1 * from payment (nolock)  where ProfileId={id} and status=4" +
                                                          $" select top 1 * from legalPerson (nolock)  where ProfileId={id}" +
                                                          $" select * from LegalPersonStakeholder (nolock)  where ProfileId={id} " +
                                                          $" select * from LegalPersonShareholder (nolock)  where ProfileId={id} ");



                var profile = await result.ReadFirstOrDefaultAsync<Profile>();


                if (profile == null)
                    return null;

                profile.Agent = await result.ReadFirstOrDefaultAsync<Agent>();
                profile.FinancialInfo = await result.ReadFirstOrDefaultAsync<FinancialInfo>();
                profile.TradingCodes = (await result.ReadAsync<TradingCode>())?.ToList();
                profile.Accounts = (await result.ReadAsync<BankingAccount>())?.ToList();
                profile.Addresses = (await result.ReadAsync<Address>())?.ToList();
                profile.PrivatePerson = await result.ReadFirstOrDefaultAsync<PrivatePerson>();
                profile.JobInfo = await result.ReadFirstOrDefaultAsync<JobInfo>();
                profile.Payments = (await result.ReadAsync<Payment>())?.ToList();
                profile.LegalPerson = await result.ReadFirstOrDefaultAsync<LegalPerson>();
                profile.LegalPersonStakeholders = (await result.ReadAsync<LegalPersonStakeholder>())?.ToList();
                profile.LegalPersonShareholders = (await result.ReadAsync<LegalPersonShareholder>())?.ToList();
                return profile;
            }
        }

        public async Task<string> GetFullName(long profileId)
        {

            string fullName = string.Empty;
            var profile = await _dbContext
                .Profiles
                .Include(x => x.PrivatePerson)
                .Include(x => x.LegalPerson)
                .FirstOrDefaultAsync(x => x.Id == profileId);

            if (profile == null)
                return string.Empty;
            if (profile.Type == ProfileOwnerType.IranianPrivatePerson ||
                profile.Type == ProfileOwnerType.ForeignPrivatePerson)
            {
                if (profile.PrivatePerson != null)
                    fullName = profile.PrivatePerson?.FirstName + " " + profile.PrivatePerson?.LastName;
            }
            else
            {
                if (profile.LegalPerson != null)
                    fullName = profile.LegalPerson?.CompanyName;
            }

            return fullName;

        }
        public async Task<Profile> GetDataForFactorAsync(long profileId)
        {
            using (var con = _sqlConnectionProvider.GetReadConnection())
            {
                var result = await con.QueryMultipleAsync($" select top 1 * from Profile (nolock) where Id={profileId}" +
                                                          $"select ad.*,ad.Address as RemnantAddress,Province.*, City.* from Address ad (nolock) left outer join Province (nolock)" +
                                                          $" on ad.ProvinceId=Province.Id left outer join City (nolock) on ad.CityId=City.Id where ProfileId={profileId}" +
                                                          $" select top 1 * from PrivatePerson (nolock) where PrivatePerson.ProfileId={profileId}" +
                                                          $" select * from payment (nolock)  where ProfileId={profileId}" +
                                                          $" select top 1 * from legalPerson (nolock)  where ProfileId={profileId}");




                var profile = await result.ReadFirstOrDefaultAsync<Profile>();


                if (profile == null)
                    return null;


                profile.Addresses = result.Read<Address, Province, City, Address>(
                    (address, province, city) =>
                    {
                        if (address == null)
                            return null;
                        address.Province = province;
                        address.City = city;
                        return address;
                    })?.ToList();
                profile.PrivatePerson = await result.ReadFirstOrDefaultAsync<PrivatePerson>();

                profile.Payments = (await result.ReadAsync<Payment>())?.ToList();
                profile.LegalPerson = await result.ReadFirstOrDefaultAsync<LegalPerson>();

                return profile;
            }
        }

        public async Task<Profile> GetDataForFgmFactorAsync(long profileId)
        {

            using (var con = _sqlConnectionProvider.GetReadConnection())
            {
                var result = await con.QueryMultipleAsync($" select top 1 * from Profile (nolock) where Id={profileId}" +
                                                          $"select ad.*,ad.Address as RemnantAddress,Province.*, City.* from Address ad (nolock) left outer join Province (nolock)" +
                                                          $" on ad.ProvinceId=Province.Id left outer join City (nolock) on ad.CityId=City.Id where ProfileId={profileId}" +
                                                          $" select top 1 * from PrivatePerson (nolock) where PrivatePerson.ProfileId={profileId}" +
                                                          $" select top 1 * from legalPerson (nolock)  where ProfileId={profileId}");




                var profile = await result.ReadFirstOrDefaultAsync<Profile>();


                if (profile == null)
                    return null;


                profile.Addresses = result.Read<Address, Province, City, Address>(
                    (address, province, city) =>
                    {
                        if (address == null)
                            return null;
                        address.Province = province;
                        address.City = city;
                        return address;
                    })?.ToList();
                profile.PrivatePerson = await result.ReadFirstOrDefaultAsync<PrivatePerson>();
                profile.LegalPerson = await result.ReadFirstOrDefaultAsync<LegalPerson>();

                return profile;
            }
        }


        public async Task<IEnumerable<Profile>> GetByMobileAsync(long mobile, ProfileStatus status)
        {
            using (var con = _sqlConnectionProvider.GetReadConnection())
            {
                var result = await con.QueryMultipleAsync(
                    $"select mobile from Profile (nolock) where mobile={mobile} and status >={(int)status}");
                return (await result.ReadAsync<Profile>())?.ToList();

            }

            //return await _dbContext.Profiles.Where(x => x.Mobile == mobile && x.Status >= status).ToListAsync();
        }

        public async Task<Profile> GetRelatedDataForDashboardAsync(long profileId, ProfileOwnerType type)
        {

            if (type == ProfileOwnerType.IranianPrivatePerson)
            {
                using (var con = _sqlConnectionProvider.GetReadConnection())
                {
                    var result = await con.QueryMultipleAsync($"select top 1 * from Profile (nolock) where Id={profileId}" +
                                                              $" select top 1 * from Agent (nolock) left outer join Profile (nolock) AgentProfile on Agent.AgentProfileId=AgentProfile.Id" +
                                                              $" left outer join PrivatePerson (nolock) on PrivatePerson.ProfileId=AgentProfile.Id where Agent.ProfileId={profileId}" +
                                                              $" select top 1 * from FinancialInfo (nolock) left outer join FinancialBroker (nolock)  on FinancialInfoId=FinancialInfo.Id" +
                                                              $" left join [Broker] (nolock) on FinancialBroker.BrokerId=Broker.Id where FinancialInfo.ProfileId={profileId}" +
                                                              $" select * from TradingCode (nolock) where ProfileId={profileId}" +
                                                              $" select top 2 * from BankingAccount (nolock) left join Bank (nolock) on BankingAccount.BankId=Bank.Id left join City (nolock) on BankingAccount.BranchCityId=City.Id where BankingAccount.ProfileId={profileId}" +
                                                              $" select ad.*,ad.Address as RemnantAddress,Country.*,Province.*,City.*,AddressSection.*" +
                                                              $" from Address ad (nolock) left outer join Country (nolock) on ad.CountryId=Country.Id left outer join Province (nolock) on ad.ProvinceId=Province.Id left outer join City (nolock) on ad.CityId=City.Id " +
                                                              $" left outer join AddressSection (nolock) on ad.SectionId=AddressSection.Id where ProfileId={profileId}" +
                                                              $" select top 1 * from PrivatePerson (nolock) where PrivatePerson.ProfileId={profileId}" +
                                                              $" select top 1 * from JobInfo (nolock) left join Job (nolock) on JobInfo.JobId=Job.Id where ProfileId={profileId}" +
                                                              $" select top 1 * from IssuanceStockCode (nolock) join IssuanceStockCodeResponse (nolock) on IssuanceStockCode.Id=IssuanceStockCodeResponse.IssuanceStockCodeId where IssuanceStockCode.ProfileId={profileId} and IssuanceStockCodeResponse.IsActive = 1 order by IssuanceStockCodeResponse.Id desc ");

                    var profile = await result.ReadFirstOrDefaultAsync<Profile>();


                    if (profile == null)
                        return null;

                    profile.Agent = result.Read<Agent, Profile, PrivatePerson, Agent>(
                        (agent, agentProfile, privatePerson) =>
                        {
                            agent.AgentProfile = agentProfile;
                            agent.AgentProfile.PrivatePerson = privatePerson;

                            return agent;
                        }).FirstOrDefault();

                    var dictFinancial = new Dictionary<long, FinancialInfo>();
                    profile.FinancialInfo = result.Read<FinancialInfo, FinancialBroker, Broker, FinancialInfo>(
                        (info, infoBrokers, brokers) =>
                        {
                            if (info == null)
                                return null;

                            if (infoBrokers == null)
                                return info;

                            if (!dictFinancial.TryGetValue(info.Id, out var infoEntry))
                            {
                                infoEntry = info;
                                infoEntry.FinancialBrokers = new List<FinancialBroker>();
                                dictFinancial.Add(infoEntry.Id, infoEntry);
                            }

                            infoBrokers.Broker = brokers;
                            infoEntry.FinancialBrokers.Add(infoBrokers);

                            return infoEntry;
                        }).FirstOrDefault();

                    profile.TradingCodes = (await result.ReadAsync<TradingCode>())?.ToList();


                    profile.Accounts = result.Read<BankingAccount, Bank, City, BankingAccount>((account, bank, city) =>
                    {
                        if (account == null)
                            return null;
                        account.Bank = bank;
                        account.BranchCity = city;

                        return account;
                    })?.ToList();

                    profile.Addresses = result.Read<Address, Country, Province, City, AddressSection, Address>(
                        (address, country, province, city, section) =>
                        {
                            if (address == null)
                                return null;
                            address.Country = country;
                            address.Province = province;
                            address.City = city;
                            address.Section = section;

                            return address;
                        })?.ToList();


                    profile.PrivatePerson = await result.ReadFirstOrDefaultAsync<PrivatePerson>();


                    profile.JobInfo = result.Read<JobInfo, Job, JobInfo>((info, job) =>
                    {
                        if (info == null)
                            return null;
                        info.Job = job;
                        return info;
                    }).FirstOrDefault();

                    var dictIssuance = new Dictionary<long, IssuanceStockCode>();
                    profile.IssuanceStockCodes = result.Read<IssuanceStockCode, IssuanceStockCodeResponse, IssuanceStockCode>(
                        (iss, response) =>
                        {
                            if (iss == null)
                                return null;

                            if (response == null)
                                return iss;

                            if (!dictIssuance.TryGetValue(iss.Id, out var issuanceEntry))
                            {
                                issuanceEntry = iss;
                                issuanceEntry.IssuanceStockCodeResponses = new List<IssuanceStockCodeResponse>();
                                dictIssuance.Add(issuanceEntry.Id, issuanceEntry);
                            }

                            var stockCodeResponses = new List<IssuanceStockCodeResponse>();

                            stockCodeResponses.AddRange(issuanceEntry.IssuanceStockCodeResponses.ToList());

                            response.IssuanceStockCode = issuanceEntry;

                            stockCodeResponses.Add(response);

                            issuanceEntry.IssuanceStockCodeResponses = stockCodeResponses;

                            return issuanceEntry;
                        }).ToList();

                    return profile;

                }
            }

            if (type == ProfileOwnerType.IranianLegalPerson)
            {

                return await _dbContext.Profiles.Include(x => x.LegalPerson)
                    .Include(x => x.Addresses).ThenInclude(x => x.Country).Include(x => x.Addresses)
                    .ThenInclude(x => x.Province)
                    .Include(x => x.Addresses).ThenInclude(x => x.City).Include(x => x.Addresses)
                    .ThenInclude(x => x.Section)
                    .Include(x => x.FinancialInfo).ThenInclude(x => x.FinancialBrokers)
                    .ThenInclude(x => x.Broker)
                    .Include(x => x.TradingCodes)
                    .Include(x => x.Accounts).ThenInclude(x => x.Bank).Include(x => x.Accounts)
                    .ThenInclude(x => x.BranchCity)
                    .Include(x => x.LegalPersonStakeholders)
                    .ThenInclude(x => x.StakeholderProfile).ThenInclude(x => x.PrivatePerson)
                    .Include(x => x.LegalPersonShareholders).Where(x => x.Id == profileId).AsNoTracking().FirstOrDefaultAsync();

            }

            return new Profile();
        }

        public async Task<Profile> GetRelatedDataForScopeAsync(long profileId, ProfileOwnerType type)
        {

            switch (type)
            {
                case ProfileOwnerType.IranianPrivatePerson:
                    {
                        using (var con = _sqlConnectionProvider.GetConnection())
                        {
                            var result = await con.QueryMultipleAsync(
                                                                      $" select top 1 * from Profile (nolock) where Id={profileId}" +
                                                                      $" select top 1 * from Agent (nolock)  where Agent.ProfileId={profileId}" +
                                                                      $" select top 1 * from FinancialInfo (nolock) where FinancialInfo.ProfileId={profileId}" +
                                                                      $" select top 1 * from TradingCode (nolock) where ProfileId={profileId}" +
                                                                      $" select top 2 * from BankingAccount (nolock) where BankingAccount.ProfileId={profileId}" +
                                                                      $" select top 1 * from Address (nolock) where ProfileId={profileId}" +
                                                                      $" select top 1 * from PrivatePerson (nolock) where PrivatePerson.ProfileId={profileId}" +
                                                                      $" select top 1 * from JobInfo (nolock)  where ProfileId={profileId}" +
                                                                      $" select top 1 * from payment (nolock)  where ProfileId={profileId} and status = 4");


                            var profile = await result.ReadFirstOrDefaultAsync<Profile>();


                            if (profile == null)
                                return null;

                            profile.Agent = await result.ReadFirstOrDefaultAsync<Agent>();
                            profile.FinancialInfo = await result.ReadFirstOrDefaultAsync<FinancialInfo>();
                            profile.TradingCodes = (await result.ReadAsync<TradingCode>())?.ToList();
                            profile.Accounts = (await result.ReadAsync<BankingAccount>())?.ToList();
                            profile.Addresses = (await result.ReadAsync<Address>())?.ToList();

                            profile.PrivatePerson = await result.ReadFirstOrDefaultAsync<PrivatePerson>();
                            profile.JobInfo = await result.ReadFirstOrDefaultAsync<JobInfo>();
                            profile.Payments = (await result.ReadAsync<Payment>())?.ToList();

                            return profile;

                        }
                    }

                case ProfileOwnerType.IranianLegalPerson:
                    {
                        using (var con = _sqlConnectionProvider.GetReadConnection())
                        {
                            var result = await con.QueryMultipleAsync($" select top 1 * from Profile (nolock) where Id={profileId}" +
                                                                      $" select top 1 * from FinancialInfo (nolock) where FinancialInfo.ProfileId={profileId}" +
                                                                      $" select top 1 * from TradingCode (nolock) where ProfileId={profileId}" +
                                                                      $" select top 2 * from BankingAccount (nolock) where BankingAccount.ProfileId={profileId}" +
                                                                      $" select top 1 * from Address (nolock) where ProfileId={profileId}" +
                                                                      $" select top 1 * from LegalPerson (nolock) where ProfileId={profileId}" +
                                                                      $" select top 1 * from payment (nolock)  where ProfileId={profileId} and status = 4 " +
                                                                      $" select * from LegalPersonStakeholder (nolock)  where ProfileId={profileId}" +
                                                                      $" select * from LegalPersonShareholder (nolock)  where ProfileId={profileId}");


                            var profile = await result.ReadFirstOrDefaultAsync<Profile>();


                            if (profile == null)
                                return null;


                            profile.FinancialInfo = await result.ReadFirstOrDefaultAsync<FinancialInfo>();
                            profile.TradingCodes = (await result.ReadAsync<TradingCode>())?.ToList();
                            profile.Accounts = (await result.ReadAsync<BankingAccount>())?.ToList();
                            profile.Addresses = (await result.ReadAsync<Address>())?.ToList();
                            profile.LegalPerson = await result.ReadFirstOrDefaultAsync<LegalPerson>();
                            profile.Payments = (await result.ReadAsync<Payment>())?.ToList();
                            profile.LegalPersonStakeholders = (await result.ReadAsync<LegalPersonStakeholder>())?.ToList();
                            profile.LegalPersonShareholders = (await result.ReadAsync<LegalPersonShareholder>())?.ToList();


                            return profile;

                        }
                    }

                default:
                    return new Profile();
            }
        }


        public async Task<Profile> GetRelatedDataWithEntityAsync(long? profileId, string uniqueIdentifier, EntityType entity)
        {
            if (profileId != null)
            {
                return await
              _dbContext
                  .Profiles
                  .Include(entity.ToString())
                 .Where(x => x.Id == profileId).AsNoTracking().FirstOrDefaultAsync();
            }
            return await
                _dbContext
                    .Profiles
                    .Include(entity.ToString())
                    .Where(x => x.UniqueIdentifier == uniqueIdentifier).AsNoTracking().FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Profile>> GetProfileListAsync(long fromRow, long toRow, string uniqueIdentifier, ProfileOwnerType? type = null, ProfileStatus status = ProfileStatus.SuccessPayment)
        {
            using (var con = _sqlConnectionProvider.GetReadConnection())
            {
                var query =
                    "SELECT x.Id,x.Mobile,x.Carrier,x.UniqueIdentifier,x.Type,x.Status,x.TraceCode,x.[StatusReasonType] FROM ("
                    + $"SELECT ROW_NUMBER() OVER(ORDER BY Id) rw, * FROM dbo.Profile WITH(NOLOCK) where Status>={(int)status} ";

                if (type.HasValue)
                    query += $" and type={(int)type.Value}";

                if (uniqueIdentifier != null)
                    query += $" and UniqueIdentifier='{uniqueIdentifier}'";

                query += $" ) x WHERE x.rw>= {fromRow} AND x.rw<= {toRow}";

                return await con.QueryAsync<Profile>(query);
            }
        }

        public async Task<int> CountAsync(ProfileOwnerType? type = null, ProfileStatus? status = null)
        {
            var query = _dbContext.Profiles.AsQueryable();

            if (type.HasValue)
                query = query.Where(x => x.Type == type.Value);

            if (status.HasValue)
                query = query.Where(x => x.Status >= status.Value);

            return await query.CountAsync();
        }

        public async Task<Profile> GetRelatedDataForAgentAsync(long profileId)
        {
            return await _dbContext.Profiles.Include(x => x.PrivatePerson).Include(x => x.Agent)
                 .FirstOrDefaultAsync(x => x.Id == profileId);
        }

        public async Task<List<Profile>> GetForDeadAsync(string uniqueIdentifier, DateTime birthDate, ProfileStatus status = ProfileStatus.PolicyAccepted)
        {
            using (var con = _sqlConnectionProvider.GetReadConnection())
            {
                con.Open();
                var result = await con.QueryAsync<Profile, PrivatePerson, Profile>
                                                    ($@"SELECT *
                                                          FROM Profile p
                                                                INNER JOIN PrivatePerson pp ON p.Id = pp.ProfileId
                                                          WHERE p.UniqueIdentifier LIKE '{uniqueIdentifier}%'
                                                                AND pp.Birthdate = '{birthDate}'",
                    (profile, privatePerson) =>
                    {
                        profile.PrivatePerson = privatePerson;
                        return profile;
                    });

                return result.Where(x => x.Status >= status).ToList();
            }

            //return await _dbContext.Profiles.Include(x => x.PrivatePerson).Where(x =>
            //         x.UniqueIdentifier.StartsWith(uniqueIdentifier) && x.Status >= status &&
            //         x.PrivatePerson.BirthDate == birthDate).AsNoTracking().ToListAsync();
        }


        public async Task<Profile> GetSignatureFileByUniqueIdentifierAsync(string uniqueIdentifier)
        {
            using (var con = _sqlConnectionProvider.GetReadConnection())
            {
                var result = await con.QueryMultipleAsync($" select * from Profile (nolock) join  PrivatePerson (nolock) on profile.id=privatePerson.profileId " +
                                                          $"left join [file] (nolock) on PrivatePerson.SignatureFileId=[File].Id where profile.UniqueIdentifier='{uniqueIdentifier}' ");

                return result.Read<Profile, PrivatePerson, File, Profile>(
                    (profile, privatePerson, file) =>
                    {
                        if (profile == null)
                            return null;
                        profile.PrivatePerson = privatePerson;
                        profile.PrivatePerson.SignatureFile = file;
                        return profile;
                    })?.FirstOrDefault();



            }
        }
        public async Task<IEnumerable<BatchSejamStatusResultViewModel>> GetSejamStatus(string referenceId, List<StatusData> validUniqueIdentifiers)
        {
            List<BatchSejamStatusResultViewModel> result = new List<BatchSejamStatusResultViewModel>();

            //1: Bulkinsert ValidUniqueIdentifiers in StatusReport

            var data = new DataReaderAdapter<StatusData>(validUniqueIdentifiers);

            using (var connection = _sqlConnectionProvider.GetStatusReportDbConnection())
            {
                connection.Open();
                var bulkCopy = new SqlBulkCopy((SqlConnection)connection)
                {
                    DestinationTableName = "[dbo].[StatusData]",
                    BatchSize = 1_000_000
                };
                bulkCopy.WriteToServer(data);
                bulkCopy.Close();
            }

            //2: Query for get sejam status 
            var query = @"SELECT DISTINCT c.NationalCode,
                                        p.[UniqueIdentifier],
                                        p.[Status]
                        FROM   StatusReport.[dbo].[StatusData] c with(nolock) 
                               LEFT JOIN Cerberus.[dbo].[Profile] p with(nolock) ON p.[UniqueIdentifier] = c.NationalCode AND p.[status] > 1
                        WHERE c.RefrenceNumber = @RefId
                        ORDER  BY status DESC ";

            //*: It must be GetReadConnection but StatusReport DB not exist in read BDs, because 1: The StatusReport DB isn't on AlwaysOn , 2: Write DB is freer than read DB's 
            using (var connection = _sqlConnectionProvider.GetConnection()) //*
            {
                var ret = await connection.QueryAsync<BatchSejamStatusResultViewModel>(query, new
                {
                    RefId = referenceId
                });

                result = ret.ToList();
            }

            //3 : Delete data from StatusData table per RefrenceNumber
            using (var connection = _sqlConnectionProvider.GetStatusReportDbConnection())
            {
                await connection.ExecuteAsync($"Delete [dbo].[StatusData] Where RefrenceNumber = '{referenceId}'");
            }
            return result;
        }
        public async Task<IEnumerable<BatchPrivatePersonResultViewModel>> GetBatchPrivatePerson(string referenceId, List<StatusData> validUniqueIdentifiers)
        {
            List<BatchPrivatePersonResultViewModel> result = new List<BatchPrivatePersonResultViewModel>();

            //1: Bulkinsert ValidUniqueIdentifiers in StatusReport

            var data = new DataReaderAdapter<StatusData>(validUniqueIdentifiers);

            using (var connection = _sqlConnectionProvider.GetStatusReportDbConnection())
            {
                connection.Open();
                var bulkCopy = new SqlBulkCopy((SqlConnection)connection)
                {
                    DestinationTableName = "[dbo].[StatusData]",
                    BatchSize = 1_000_000
                };
                bulkCopy.WriteToServer(data);
                bulkCopy.Close();
            }

            //2: Query for get sejam status 
            var query = @"SELECT DISTINCT c.nationalCode,
                                         p.[UniqueIdentifier],
                                         p.[Status],
			                             pp.FirstName,
			                             pp.LastName,
			                             pp.FatherName,
			                             pp.Gender,
			                             pp.SeriShChar,
			                             pp.SeriSh,
			                             pp.Serial,
			                             pp.ShNumber,
			                             pp.BirthDate,
			                             pp.PlaceOfIssue,
			                             pp.PlaceOfBirth,
			                             pp.IsConfirmed
                            FROM   StatusReport.[dbo].[StatusData] c with(nolock) 
                                   left JOIN Cerberus.[dbo].[Profile] p with(nolock) ON p.[UniqueIdentifier] = c.NationalCode 
                                   left JOIN Cerberus.[dbo].[PrivatePerson] pp with(nolock) ON p.ID = pp.ProfileId
                            where c.RefrenceNumber= @RefId ";

            //*: It must be GetReadConnection but StatusReport DB not exist in read BDs, because 1: The StatusReport DB isn't on AlwaysOn , 2: Write DB is freer than read DB's 
            using (var connection = _sqlConnectionProvider.GetConnection()) //*
            {
                var ret = await connection.QueryAsync<BatchPrivatePersonResultViewModel>(query, new
                {
                    RefId = referenceId
                });

                result = ret.ToList();
            }

            //3 : Delete data from StatusData table per RefrenceNumber
            using (var connection = _sqlConnectionProvider.GetStatusReportDbConnection())
            {
                await connection.ExecuteAsync($"Delete [dbo].[StatusData] Where RefrenceNumber = '{referenceId}'");
            }
            return result;
        }

        public async Task<Profile> GetAuthenticatorProfileByUniqueIdentifierAsync(string uniqueIdentifier)
        {
            using (var con = _sqlConnectionProvider.GetConnection())
            {
                var result = await con.QueryMultipleAsync("[SP_GetAuthenticatorProfileByUniqueIdentifier] @UniqueIdentifier",
                    new { uniqueIdentifier });

                var profile = await result.ReadFirstOrDefaultAsync<Profile>();
                if (profile == null)
                    return null;

                profile.PrivatePerson = await result.ReadFirstOrDefaultAsync<PrivatePerson>();


                profile.Agent = result.Read<Agent, Profile, PrivatePerson, Agent>((agent, agentProfile, privatePerson) =>
                {
                    agent.AgentProfile = agentProfile;
                    agent.AgentProfile.PrivatePerson = privatePerson;

                    return agent;
                }).FirstOrDefault();

                profile.JobInfo = result.Read<JobInfo, Job, JobInfo>((info, job) =>
                {
                    if (info == null)
                        return null;
                    info.Job = job;
                    return info;
                }).FirstOrDefault();

                var dictFinancial = new Dictionary<long, FinancialInfo>();
                profile.FinancialInfo = result.Read<FinancialInfo, FinancialBroker, Broker, FinancialInfo>(
                    (info, infoBrokers, brokers) =>
                    {
                        if (info == null)
                            return null;

                        if (infoBrokers == null)
                            return info;

                        if (!dictFinancial.TryGetValue(info.Id, out var infoEntry))
                        {
                            infoEntry = info;
                            infoEntry.FinancialBrokers = new List<FinancialBroker>();
                            dictFinancial.Add(infoEntry.Id, infoEntry);
                        }

                        infoBrokers.Broker = brokers;
                        infoEntry.FinancialBrokers.Add(infoBrokers);

                        return infoEntry;
                    }).FirstOrDefault();

                profile.TradingCodes = (await result.ReadAsync<TradingCode>())?.ToList();


                profile.Accounts = result.Read<BankingAccount, Bank, City, BankingAccount>((account, bank, city) =>
                {
                    if (account == null)
                        return null;
                    account.Bank = bank;
                    account.BranchCity = city;

                    return account;
                })?.ToList();

                profile.Addresses = result.Read<Address, Country, Province, City, AddressSection, Address>(
                    (address, country, province, city, section) =>
                    {
                        if (address == null)
                            return null;
                        address.Country = country;
                        address.Province = province;
                        address.City = city;
                        address.Section = section;

                        return address;
                    })?.ToList();




                if (profile?.Agent?.AgentProfile?.PrivatePerson != null)
                {
                    profile.Agent.UniqueIdentifier = profile?.Agent?.AgentProfile?.UniqueIdentifier;
                    profile.Agent.FirstName = profile?.Agent?.AgentProfile?.PrivatePerson?.FirstName;
                    profile.Agent.LastName = profile?.Agent?.AgentProfile?.PrivatePerson?.LastName;

                }

                return profile;
            }
        }
    }
}