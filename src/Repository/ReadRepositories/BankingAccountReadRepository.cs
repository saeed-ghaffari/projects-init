﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Model;
using Cerberus.Repository.MsSql;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories
{
    public class BankingAccountReadRepository : BaseReadRepository<BankingAccount>, IBankingAccountReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;

        public BankingAccountReadRepository(ReadCerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }

        public async Task<IEnumerable<BankingAccount>> GetListAsync(long profileId, bool includeRelatedData = false)
        {
            if (!includeRelatedData)
                return await _dbContext
                    .BankingAccounts
                    .Where(x => x.ProfileId == profileId)
                    .ToListAsync();

            return await _dbContext
                .BankingAccounts
                .Include(x => x.Bank)
                .Include(x => x.ProfileOwner).ThenInclude(x => x.PrivatePerson)
                .Include(x => x.ProfileOwner).ThenInclude(x => x.LegalPerson)
                .Include(x => x.BranchCity)
                .Where(x => x.ProfileId == profileId)
                .AsNoTracking().ToListAsync();
        }

        public async Task<IEnumerable<BankingAccount>> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier)
        {

            return await
                _dbContext.BankingAccounts
                    .AsNoTracking()
                    .Include(x => x.BranchCity)
                    .Include(x => x.Bank)
                    .Where(x => x.ProfileOwner.UniqueIdentifier.Equals(uniqueIdentifier))
                    .ToListAsync();

        }


    }

}