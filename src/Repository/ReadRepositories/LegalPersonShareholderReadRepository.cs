﻿using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Repository.MsSql;

namespace Cerberus.Repository.ReadRepositories
{
    public class LegalPersonShareholderReadRepository : BaseReadRepository<LegalPersonShareholder>, ILegalPersonShareholderReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;

        public LegalPersonShareholderReadRepository(ReadCerberusDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<LegalPersonShareholder>> GetListAsync(long profileId)
        {
            return await _dbContext.LegalPersonShareholders.AsNoTracking().Where(row => row.ProfileId == profileId).ToListAsync();
        }

    }
}