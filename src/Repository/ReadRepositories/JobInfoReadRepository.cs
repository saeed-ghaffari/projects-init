﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Cerberus.Repository.MsSql;
using Dapper;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories
{
    public class JobInfoReadRepository : BaseReadRepository<JobInfo>, IJobInfoReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;
        private readonly ISqlConnectionProvider _sqlConnectionProvider;
        public JobInfoReadRepository(ReadCerberusDbContext db, ISqlConnectionProvider sqlConnectionProvider) : base(db)
        {
            this._dbContext = db;
            _sqlConnectionProvider = sqlConnectionProvider;
        }

        public async Task<JobInfo> GetByProfileIdAsync(long profileId, bool getAllRelated)
        {
            if (!getAllRelated)
                return await _dbContext.JobInfo.AsNoTracking().FirstOrDefaultAsync(x => x.ProfileId == profileId);

            return await _dbContext
                .JobInfo
                .AsNoTracking()
                .Include(x => x.Job)
                .FirstOrDefaultAsync(x => x.ProfileId == profileId);
        }

        public async Task<JobInfo> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier)
        {
            using (var db=_sqlConnectionProvider.GetReadConnection())
            {
                var cmd =
                    $"declare @profileId bigint select @profileId=Id from Profile where UniqueIdentifier='{uniqueIdentifier}'"
                    + "SELECT* FROM dbo.JobInfo j LEFT JOIN dbo.Job b ON b.Id = j.JobId WHERE j.ProfileId = @profileId";
                return (await db.QueryAsync<JobInfo, Job, JobInfo>(cmd, (info, job) => {

                    info.Job = job;
                    return info;
                })).FirstOrDefault();
            }

            
        }
    }
}