﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Cerberus.Repository.MsSql;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories
{
    public class BrokerReadRepository : BaseReadRepository<Broker>, IBrokerReadRepository
    {
        private readonly ReadCerberusDbContext _dbContext;

        public BrokerReadRepository(ReadCerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }

        public async Task<IEnumerable<Broker>> GetListAsync()
        {
            return await _dbContext.Brokers.AsNoTracking().ToListAsync();
        }
    }
}