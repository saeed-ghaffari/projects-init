﻿using Cerberus.Domain.Interface.ReadRepositories.ReadInquiries;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.Repository.MsSql;

namespace Cerberus.Repository.ReadRepositories
{
    public class IbanInquiryReadRepository : BaseReadRepository<IbanInquiry>, IIbanInquiryReadRepository
    {
        public IbanInquiryReadRepository(ReadCerberusDbContext dbContext) : base(dbContext)
        {
        }

    }
}
