﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.ReadRepositories;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Cerberus.Repository.MsSql;
using Dapper;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.ReadRepositories
{
    public class PermanentOtpReadRepository : BaseReadRepository<PermanentOtp>, IPermanentOtpReadRepository
    {
        private readonly ISqlConnectionProvider _sqlConnectionProvider;
        private readonly ReadCerberusDbContext _dbContext;

        public PermanentOtpReadRepository(ReadCerberusDbContext db, ISqlConnectionProvider sqlConnectionProvider) : base(db)
        {
            _sqlConnectionProvider = sqlConnectionProvider;
            _dbContext = db;
        }

        public async Task<PermanentOtp> GetAsync(long serviceId, string uniqueIdentifier, OtpTypes type)
        {
            using (var db = _sqlConnectionProvider.GetReadConnection())
            {
                return (await db.QueryAsync<PermanentOtp>(
                        $"select top 1 * from  PermanentOtp where serviceId={serviceId} and uniqueIdentifier='{uniqueIdentifier}' order by Id desc ")
                    ).FirstOrDefault();
            }
        }

        public Task<bool> IsExistAsync(long serviceId, string uniqueIdentifier)
        {
            return _dbContext.PermanentOtps.AsNoTracking()
                .AnyAsync(x =>
                    x.ServiceId == serviceId && uniqueIdentifier == x.UniqueIdentifier &&
                    x.ExpirationDate > DateTime.Now);
        }

    }
}
