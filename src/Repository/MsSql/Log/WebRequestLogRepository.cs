﻿using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Interface.Repositories.Log;
using Cerberus.Domain.Model.Log;
using Dapper.FastCrud;

namespace Cerberus.Repository.MsSql.Log
{
  public  class WebRequestLogRepository:IWebRequestLogRepository
    {
        private readonly ISqlConnectionProvider _connectionProvider;

        public WebRequestLogRepository(ISqlConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
        }

        public async Task AddAsync(WebRequestLog log)
        {
            using (var con = _connectionProvider.GetRequestLogConnection())
            {
                await con.InsertAsync(log);
            }
        }
    }
}
