﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class TradingCodeRepository : BaseRepository<TradingCode>, ITradingCodeRepository
    {
        private readonly CerberusDbContext _dbContext;

        public TradingCodeRepository(CerberusDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<TradingCode>> GetListAsync(long profileId)
        {
            return await _dbContext.TradingCodes.Where(x => x.ProfileId == profileId).ToListAsync();
        }

        public async Task<int> CountAsync(long profileId)
        {
            return await _dbContext.TradingCodes.AsNoTracking().CountAsync(x => x.ProfileId == profileId);
        }

        public async Task<IEnumerable<TradingCode>> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier)
        {
            return await _dbContext
                .TradingCodes
                .Where(x => x.ProfileOwner.UniqueIdentifier.Equals(uniqueIdentifier))
                .AsNoTracking().ToListAsync();
        }
    }
}