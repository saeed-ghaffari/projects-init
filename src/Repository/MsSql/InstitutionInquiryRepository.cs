﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Repository.MsSql
{
    public class InstitutionInquiryRepository : BaseRepository<InstitutionInquiry>, IInstitutionInquiryRepository
    {
        public InstitutionInquiryRepository(CerberusDbContext dbContext) : base(dbContext)
        {
        }
    }
}
