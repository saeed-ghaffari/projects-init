﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class AuthenticationOfficesRepository : BaseRepository<AuthenticationOffices>, IAuthenticationOfficesRepository
    {
       
        private readonly CerberusDbContext _dbContext;

        public AuthenticationOfficesRepository(CerberusDbContext context) : base(context)
        {
            this._dbContext = context;
        }

   
        public async Task<(List<AuthenticationOffices> authenticationOfficeses, long recordCount)> GetListAsync(AuthenticationOffices offices, int skip, int take)
        {
            var result = _dbContext.AuthenticationOfficeses.AsQueryable();

            if (offices.ProvinceId != null)
                result = result.Where(x => x.ProvinceId.Equals(offices.ProvinceId));

            if (offices.CityId != null)
                result = result.Where(x => x.CityId.Equals(offices.CityId));

            if (offices.Type!=null)
                result = result.Where(x => x.Type.Equals(offices.Type));

            if (offices.BankId!=null)
                result = result.Where(x => x.BankId.Equals(offices.BankId));

            if (!string.IsNullOrEmpty(offices.Address))
                result = result.Where(x => x.Address.Contains(offices.Address));

            if (!string.IsNullOrEmpty(offices.OfficeName))
                result = result.Where(x => x.OfficeName.Contains(offices.OfficeName));

            return (await result.Include(x => x.Province).Include(x => x.City).OrderBy(x => x.Province.Name).ThenBy(x => x.City.Name).Include(x=>x.Bank).Skip(skip).Take(take).AsNoTracking().ToListAsync(), await result.CountAsync());
        }

        public async Task<List<AuthenticationOffices>> GetAllAsync()
        {
            return await _dbContext.AuthenticationOfficeses.Include(x => x.Province).Include(c => c.City).AsNoTracking().ToListAsync();
        }
    }
}
