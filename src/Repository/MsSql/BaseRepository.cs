﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Cerberus.Domain;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseModel
    {
        private readonly CerberusDbContext _dbContext;

        public BaseRepository(CerberusDbContext db)
        {
            this._dbContext = db;
        }

        public async Task CreateAsync(T entity)
        {
            entity.ModifiedDate = DateTime.Now;
            await _dbContext.AddAsync<T>(entity);
            await _dbContext.SaveChangesAsync();
        }

        public virtual async Task UpdateAsync(T entity)
        {
            if (entity.Id == 0)
                throw new CerberusException(ErrorCode.BadRequest);

            entity.ModifiedDate = DateTime.Now;
            _dbContext.Update(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            _dbContext.Remove(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<T> GetAsync(long id)
        {
            var response = await _dbContext.FindAsync<T>(id);
            if (response == null || response.IsDeleted)
                return null;

            return response;

        }

        public async Task CreateRangeAsync(IEnumerable<T> entity)
        {
            await _dbContext.AddRangeAsync(entity);
            await _dbContext.SaveChangesAsync();

        }

        public async Task UpdateRangeAsync(IEnumerable<T> entity)
        {
            _dbContext.UpdateRange(entity);
            await _dbContext.SaveChangesAsync();

        }
        public async Task<IEnumerable<T>> GetListAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbContext.Set<T>().Where(predicate).ToListAsync();

        }
        public async Task<IEnumerable<T>> GetConditionalListAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbContext.Set<T>().Where(predicate).Where(x => !x.IsDeleted).ToListAsync();

        }
        public async Task<IEnumerable<T>> GetGenericAsync(Expression<Func<T, bool>> predicate,
            Func<IQueryable<T>, IQueryable<T>> include = null, Func<IQueryable<T>, IOrderedQueryable<T>> order = null)
        {
            IQueryable<T> query = _dbContext.Set<T>();
            if (predicate == null)
                return null;

            query = query.Where(predicate);
            if (include != null)
                query = include(query);
            if (order != null)
                query = order(query);

            return await query.ToListAsync();

        }
    }
}