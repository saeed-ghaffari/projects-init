﻿using Cerberus.Domain;
﻿using System;
using Cerberus.Domain.Model;
using Cerberus.Domain.Model.Inquiries;
using Cerberus.Domain.Model.Log;
using Cerberus.Domain.Model.Membership;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class CerberusDbContext : BaseCerberusDbContext
    {
        public CerberusDbContext(DbContextOptions<CerberusDbContext> options) : base(options)
        {

        }

    }

    public class ReadCerberusDbContext : BaseCerberusDbContext
    {
        public ReadCerberusDbContext(DbContextOptions<ReadCerberusDbContext> options) : base(options)
        {

        }

        public override int SaveChanges()
        {
            // Throw if they try to call this
            throw new InvalidOperationException("This context is read-only.");
        }
    }

    public class BaseCerberusDbContext : DbContext
    {
        public BaseCerberusDbContext(DbContextOptions<CerberusDbContext> options) : base(options)
        {

        }

        public BaseCerberusDbContext(DbContextOptions<ReadCerberusDbContext> options) : base(options)
        {
            
        }
      
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<IbanInquiry>().HasIndex(x => new { x.Iban, x.SuccessInquiry, x.CreationDate }).HasName("IX_Iban_SuccessInquiry_CreationDate");
            modelBuilder.Entity<NocrInquiry>().HasIndex(x => new { x.Ssn, x.SuccessInquiry, x.CreationDate }).HasName("IX_Ssn_SuccessInquiry_CreationDate");
            modelBuilder.Entity<IlencInquiry>().HasIndex(x => new { x.NationalCode, x.SuccessInquiry, x.CreationDate }).HasName("IX_NationalCode_SuccessInquiry_CreationDate");
            modelBuilder.Entity<Profile>().HasIndex(x => new { x.Mobile, x.Status }).HasName("IX_Profile_Mobile_Status");
            modelBuilder.Entity<Payment>().HasIndex(x => new { x.SaleReferenceId, x.Gateway }).HasName("IX_Payment_SaleReferenceId_Gateway");

            modelBuilder.Entity<PostInquiry>().HasIndex(x => new { x.Ssn, x.SuccessInquiry, x.CreationDate }).HasName("IX_Ssn_SuccessInquiry_CreationDate");
            modelBuilder.Entity<DeadInquiry>().HasIndex(x => new { x.UniqueIdentifier, x.BirthDate, x.LastName, x.FromDate, x.ToDate, x.SuccessInquiry })
                .HasName("IX_Dead_UniqueIdentifier_BirthDate_LastName_FromDate_ToDate_SuccessInquiry");
            modelBuilder.Entity<PrivatePerson>().HasIndex(x => new { x.BirthDate }).HasName("IX_PrivatePerson_BirthDate");
            modelBuilder.Entity<IssuanceStockCode>().HasIndex(x => new { x.Successful, x.Status }).HasName("IX_IssuanceStockCode_Successful_Status");
            modelBuilder.Entity<Msisdn>().HasIndex(x => new { x.ProfileId}).IsUnique().HasName("IX_Msisdn__ProfileId");
            modelBuilder.Entity<ShahkarInquiry>().HasIndex(x => new { x.Ssn,x.SuccessInquiry}).HasName("IX_ShahkarInquiry_Ssn_SuccessInquiry");

            base.OnModelCreating(modelBuilder);
        }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<PrivatePerson> PrivatePeople { get; set; }
        public DbSet<LegalPerson> LegalPeople { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<AddressSection> AddressSections { get; set; }
        public DbSet<Agent> Agents { get; set; }
        public DbSet<AgentDocument> AgentDocuments { get; set; }
        public DbSet<Bank> Banks { get; set; }
        public DbSet<BankingAccount> BankingAccounts { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<LegalPersonStakeholder> LegalPersonStakeholders { get; set; }
        public DbSet<LegalPersonShareholder> LegalPersonShareholders { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<TradingCode> TradingCodes { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<JobInfo> JobInfo { get; set; }
        public DbSet<FinancialInfo> FinancialInfos { get; set; }
        public DbSet<Broker> Brokers { get; set; }
        public DbSet<IbanInquiry> IbanInquiries { get; set; }
        public DbSet<IlencInquiry> IlencInquiries { get; set; }
        public DbSet<IndividualInquiry> IndividualInquiries { get; set; }
        public DbSet<InstitutionInquiry> InstitutionInquiries { get; set; }
        public DbSet<NocrInquiry> NocrInquiries { get; set; }
        public DbSet<LegacyCodeInquiry> LegacyCodeInquiries { get; set; }
        public DbSet<LegacyCodeResponse> LegacyCodeResponses { get; set; }
        public DbSet<FinancialBroker> FinancialBrokers { get; set; }
        public DbSet<ThirdPartyService> ThirdPartyServices { get; set; }
        public DbSet<AuthenticationOffices> AuthenticationOfficeses { get; set; }
        public DbSet<ProfileHistory> ProfileHistories { get; set; }
        public DbSet<ProfilePackage> ProfilePackages { get; set; }
        public DbSet<AddressMembership> AddressMembership { get; set; }
        public DbSet<BankingAccountMembership> BankingAccountMembership { get; set; }
        public DbSet<PrivatePersonMembership> PrivatePersonMembership { get; set; }
        public DbSet<SmsLog> SmsLogs { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<AgentDocumentArchive> AgentDocumentArchives { get; set; }
        public DbSet<AgentArchive> AgentArchives { get; set; }
        public DbSet<PermanentOtp> PermanentOtps { get; set; }
        public DbSet<IssuanceStockCode> IssuanceStockCodes { get; set; }
        public DbSet<IssuanceStockCodeResponse> IssuanceStockCodeResponses { get; set; }
        public DbSet<Calendar> Calendars { get; set; }
        public DbSet<PostInquiry> PostInquiries { get; set; }
        public DbSet<DeadInquiry> DeadInquiries { get; set; }
        public DbSet<ProfilingRequestLog> ProfilingRequestLogs { get; set; }
        public DbSet<WebRequestLog> WebRequestLogs { get; set; }
        public DbSet<EtfMembership> EtfMembership { get; set; }
        public DbSet<Msisdn> Msisdn { get; set; }
        public DbSet<ShahkarInquiry> ShahkarInquiries{ get; set; }

    }
}