﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class AgentDocumentRepository : BaseRepository<AgentDocument>, IAgentDocumentRepository
    {
        private readonly CerberusDbContext _dbContext;

        public AgentDocumentRepository(CerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }

        public async Task<List<AgentDocument>> GetListDocumentByAgentId(long id)
        {
            return await _dbContext.AgentDocuments.Where(x => x.AgentId == id).ToListAsync();
        }

        public async Task DeleteAsync(long id)
        {
            await _dbContext.Database.ExecuteSqlCommandAsync($"delete AgentDocument where id=@id", new SqlParameter("@id", id));
        }
    }
}