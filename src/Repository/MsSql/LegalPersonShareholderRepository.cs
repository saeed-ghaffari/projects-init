﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;

namespace Cerberus.Repository.MsSql
{
    public class LegalPersonShareholderRepository : BaseRepository<LegalPersonShareholder>, ILegalPersonShareholderRepository
    {
        private readonly CerberusDbContext _dbContext;

        public LegalPersonShareholderRepository(CerberusDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<LegalPersonShareholder>> GetListAsync(long profileId)
        {
            return await _dbContext.LegalPersonShareholders.AsNoTracking().Where(row => row.ProfileId == profileId).ToListAsync();
        }

    }
}