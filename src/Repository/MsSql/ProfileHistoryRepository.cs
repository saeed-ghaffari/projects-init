﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class ProfileHistoryRepository : BaseRepository<ProfileHistory>, IProfileHistoryRepository
    {
        private readonly CerberusDbContext _dbContext;

        public ProfileHistoryRepository(CerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }

        public async Task<ProfileHistory> GetByUniqueIdentifierAsync(string uniqueIdentifier)
        {
            return await _dbContext.ProfileHistories
                .AsNoTracking()
                .Where(x => x.Profile.UniqueIdentifier.Equals(uniqueIdentifier))
                .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<ProfileHistory>> GetByReferenceIdAsync(string referenceId, DateTime from, DateTime to)
        {
            return await _dbContext.ProfileHistories
                .AsNoTracking()
                .Include(x => x.Profile)
                .Where(x => x.ReferenceId.StartsWith(referenceId) && x.CreationDate >= from && x.CreationDate <= to)
                .ToListAsync();
        }

        public async Task<ProfileHistory> GetTheLastHistoryAsync(long profileId)
        {
            return await _dbContext.ProfileHistories
                .AsNoTracking()
                .LastOrDefaultAsync(x => x.ProfileId == profileId && x.ChangedTo == ProfileStatus.Sejami && x.ImageId!=null);

        }
    }
}