﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories.Inquiries;
using Cerberus.Domain.Model.Inquiries;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
     public  class ShahkarInquiryRepository:BaseRepository<ShahkarInquiry>,IShahkarInquiryRepository
    {
        private readonly CerberusDbContext _dbContext;
        public ShahkarInquiryRepository(CerberusDbContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<List<ShahkarInquiry>> GetBySsnAsync(string uniqueIdentifier)
        {
            return await _dbContext.ShahkarInquiries.Where(x => x.Ssn == uniqueIdentifier && x.SuccessInquiry).AsNoTracking().ToListAsync();
        }
    }
}
