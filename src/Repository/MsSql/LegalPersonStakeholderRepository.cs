﻿using System;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Utility;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class LegalPersonStakeholderRepository : BaseRepository<LegalPersonStakeholder>, ILegalPersonStakeholderRepository
    {
        private readonly CerberusDbContext _dbContext;

        public LegalPersonStakeholderRepository(CerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }

        public async Task<List<LegalPersonStakeholder>> GetListAsync(long profileId, StakeHolderType type)
        {
            return
                await _dbContext.LegalPersonStakeholders
                            .AsNoTracking()
                            .Include(x => x.StakeholderProfile)
                                .ThenInclude(x => x.PrivatePerson)
                            .Include(x => x.SignatureFile)
                            .Where(row =>  row.ProfileId == profileId && row.Type == type)
                            .ToListAsync();
        }

    }
}