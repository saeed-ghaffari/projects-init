﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class JobRepository : BaseRepository<Job>, IJobRepository
    {
        private readonly CerberusDbContext _dbContext;

        public JobRepository(CerberusDbContext db) : base(db)
        {
            this._dbContext = db;
        }

        public async Task<List<Job>> GetListAsync()
        {
            return await _dbContext.Jobs.AsNoTracking().ToListAsync();
        }
    }
}