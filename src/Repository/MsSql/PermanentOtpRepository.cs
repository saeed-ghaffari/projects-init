﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Dapper;
using Dapper.FastCrud;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class PermanentOtpRepository : BaseRepository<PermanentOtp>, IPermanentOtpRepository
    {
        private readonly ISqlConnectionProvider _sqlConnectionProvider;
        private readonly CerberusDbContext _dbContext;

        public PermanentOtpRepository(CerberusDbContext db, ISqlConnectionProvider sqlConnectionProvider) : base(db)
        {
            _sqlConnectionProvider = sqlConnectionProvider;
            _dbContext = db;
        }

        public async Task<PermanentOtp> GetAsync(long serviceId, string uniqueIdentifier, OtpTypes type)
        {
            using (var db = _sqlConnectionProvider.GetConnection())
            {
                return (await db.QueryAsync<PermanentOtp>(
                        $"select top 1 * from  PermanentOtp where serviceId={serviceId} and uniqueIdentifier='{uniqueIdentifier}' order by Id desc ")
                    ).FirstOrDefault();
            }
        }

     

        public async Task AddAsync(PermanentOtp permanentOtp)
        {
            using (var con = _sqlConnectionProvider.GetConnection())
            {
                await con.InsertAsync(permanentOtp);
            }
        }
    }
}
