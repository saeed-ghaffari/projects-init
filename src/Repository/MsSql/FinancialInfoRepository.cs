﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cerberus.Domain;
using Cerberus.Domain.Enum;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using Dapper;
using Microsoft.EntityFrameworkCore;

namespace Cerberus.Repository.MsSql
{
    public class FinancialInfoRepository : BaseRepository<FinancialInfo>, IFinancialInfoRepository
    {
        private readonly CerberusDbContext _dbContext;
        private readonly ISqlConnectionProvider _sqlConnectionProvider;
        public FinancialInfoRepository(CerberusDbContext db, ISqlConnectionProvider sqlConnectionProvider) : base(db)
        {
            this._dbContext = db;
            _sqlConnectionProvider = sqlConnectionProvider;
        }

        public async Task<FinancialInfo> GetByProfileIdAsync(long profileId, bool includeBrokers)
        {
            if (includeBrokers)

            {
                using (var con = _sqlConnectionProvider.GetConnection())
                {
                    var result = await con.QueryMultipleAsync("select * from FinancialInfo (nolock) " +
                                                              "left outer join FinancialBroker (nolock)  on FinancialInfoId=FinancialInfo.Id" +
                                                              " left join [Broker] (nolock) on FinancialBroker.BrokerId=Broker.Id " +
                                                              $"where FinancialInfo.ProfileId={profileId}");


                    var dictFinancial = new Dictionary<long, FinancialInfo>();
                    return result.Read<FinancialInfo, FinancialBroker, Broker, FinancialInfo>(
                        (info, infoBrokers, brokers) =>
                        {
                            if (info == null)
                                return null;

                            if (infoBrokers == null)
                                return info;

                            if (!dictFinancial.TryGetValue(info.Id, out var infoEntry))
                            {
                                infoEntry = info;
                                infoEntry.FinancialBrokers = new List<FinancialBroker>();
                                dictFinancial.Add(infoEntry.Id, infoEntry);
                            }

                            infoBrokers.Broker = brokers;
                            infoEntry.FinancialBrokers.Add(infoBrokers);

                            return infoEntry;
                        }).FirstOrDefault();
                }
            }

            return await _dbContext
                    .FinancialInfos
                    .FirstOrDefaultAsync(x => x.ProfileId == profileId);
        }


        public async Task UpdateAsync(FinancialInfo financialInfoModel, IEnumerable<Broker> brokers)
        {
            if (financialInfoModel.Id == 0)
                throw new CerberusException(ErrorCode.BadRequest);
            financialInfoModel.ModifiedDate = DateTime.Now;
            _dbContext.FinancialInfos.Update(financialInfoModel);


            await _dbContext.SaveChangesAsync();

            var oldBrokers = _dbContext.FinancialBrokers.Where(x => x.FinancialInfoId == financialInfoModel.Id).AsNoTracking().ToList();
            _dbContext.FinancialBrokers.RemoveRange(oldBrokers);

            foreach (var broker in brokers)
            {
                await _dbContext.FinancialBrokers.AddAsync(new FinancialBroker()
                {
                    BrokerId = broker.Id,
                    FinancialInfoId = financialInfoModel.Id,
                    ModifiedDate = DateTime.Now
                });
            }
            await _dbContext.SaveChangesAsync();
        }

        public async Task CreateAsync(FinancialInfo financialInfoModel, IEnumerable<Broker> brokers)
        {
            financialInfoModel.ModifiedDate = DateTime.Now;
            await _dbContext.FinancialInfos.AddAsync(financialInfoModel);

            await _dbContext.SaveChangesAsync();


            foreach (var broker in brokers)
            {
                await _dbContext.FinancialBrokers.AddAsync(new FinancialBroker()
                {
                    BrokerId = broker.Id,
                    FinancialInfoId = financialInfoModel.Id
                    ,
                    ModifiedDate = DateTime.Now
                });
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task<FinancialInfo> GetByProfileUniqueIdentifierAsync(string uniqueIdentifier)
        {
            using (var db = _sqlConnectionProvider.GetConnection())
            {
                var cmd = "DECLARE @profileId BIGINT " +
                          $"select @profileId = Id from Profile where UniqueIdentifier = '{uniqueIdentifier}' " +
                          "SELECT * from FinancialInfo left join FinancialBroker  on FinancialInfoId = FinancialInfo.Id left join [Broker] on FinancialBroker.BrokerId = Broker.Id where FinancialInfo.ProfileId = @profileId ";

                var dictFinancial = new Dictionary<long, FinancialInfo>();
                var financialInfo = (await db.QueryAsync<FinancialInfo, FinancialBroker, Broker, FinancialInfo>(cmd,
                    (info, infoBrokers, brokers) =>
                    {
                        if (info == null)
                            return null;

                        if (infoBrokers == null)
                            return info;

                        if (!dictFinancial.TryGetValue(info.Id, out var infoEntry))
                        {
                            infoEntry = info;
                            infoEntry.FinancialBrokers = new List<FinancialBroker>();
                            dictFinancial.Add(infoEntry.Id, infoEntry);
                        }

                        infoBrokers.Broker = brokers;

                        infoEntry.FinancialBrokers.Add(infoBrokers);

                        return infoEntry;
                    })).FirstOrDefault();

                return financialInfo;
            }

        }
    }
}