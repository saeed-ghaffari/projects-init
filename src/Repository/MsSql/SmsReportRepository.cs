﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model.Report;
using Dapper;
using Microsoft.Extensions.DependencyInjection;

namespace Cerberus.Repository.MsSql
{
    public class SmsReportRepository : ISmsReportRepository
    {
        private readonly IServiceProvider _serviceProvider;

        public SmsReportRepository(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public async Task<ReportModel> GetReportAsync(DateTime fromDate, DateTime toDate)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var sqlConnectionProvider = scope.ServiceProvider.GetService<ISqlConnectionProvider>();
                using (var con = sqlConnectionProvider.GetReadConnection())
                {
                    var msgList = await con.QueryAsync<ReportModel>($"exec [admin].[DailyReport] '{fromDate}','{toDate}'");
                    return msgList.FirstOrDefault();
                }
            }
        }


        public async Task<IEnumerable<GroupMember>> GetGroupMemberAsync(int? groupId=null)
        {
            using (var scope = _serviceProvider.CreateScope())
            {
                var sqlConnectionProvider = scope.ServiceProvider.GetService<ISqlConnectionProvider>();
                using (var con = sqlConnectionProvider.GetReadConnection())
                {
                    string command =
                        $"SELECT * FROM adm.[Group] g JOIN adm.GroupMember gm ON gm.GroupId = g.Id WHERE IsActive = 1";
                    if (groupId.HasValue)
                        command += $" and g.id={groupId.Value}";

                    var groups = await con.QueryAsync<Group, GroupMember, Group>(command,
                        (g, gp) =>
                        {
                            if (g.GroupMembers == null)
                                g.GroupMembers = new List<GroupMember>();
                            g.GroupMembers.Add(gp);
                            return g;
                        }, splitOn: "GroupId");
                    return groups.SelectMany(x => x.GroupMembers);
                }
            }
        }
    }
}
