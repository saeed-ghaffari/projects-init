﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;

namespace Cerberus.Repository.MsSql
{
    public class MessageRepository : BaseRepository<Message>, IMessageRepository
    {
        public MessageRepository(CerberusDbContext db) : base(db)
        {
        }
    }
}