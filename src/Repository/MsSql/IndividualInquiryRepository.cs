﻿using Cerberus.Domain.Interface.Repositories;
using Cerberus.Domain.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cerberus.Repository.MsSql
{
    public class IndividualInquiryRepository : BaseRepository<IndividualInquiry>, IIndividualInquiryRepository
    {
        public IndividualInquiryRepository(CerberusDbContext dbContext) : base(dbContext)
        {
        }
    }
}
