﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Moq;
using StackExchange.Redis;

namespace Komodo.Tests.Common
{
    public class MockDbFixture
    {
        private readonly Dictionary<RedisKey, CacheData<RedisValue>> _store
            = new Dictionary<RedisKey, CacheData<RedisValue>>();

        private readonly Dictionary<RedisKey, CacheData<HashEntry[]>> _hashStore
            = new Dictionary<RedisKey, CacheData<HashEntry[]>>();

        private class CacheData<T>
        {
            public T Value { get; set; }
            public DateTime Expiration { get; set; }
        }

        public IDatabase Database { get; private set; }

        public MockDbFixture()
        {
            var mock = new Mock<IDatabase>();

            // StringSetAsync
            mock.Setup(database =>
                database.StringSetAsync(
                    It.IsAny<RedisKey>(),
                    It.IsAny<RedisValue>(),
                    It.IsAny<TimeSpan?>(),
                    It.IsAny<When>(),
                    It.IsAny<CommandFlags>()
                )
            ).ReturnsAsync(
                (RedisKey key, RedisValue value,
                    TimeSpan? expiry, When when, CommandFlags flags) =>
                {
                    if (_store.ContainsKey(key))
                        _store[key] = new CacheData<RedisValue>()
                        {
                            Value = value,
                            Expiration = expiry.HasValue
                                ? DateTime.Now.Add(expiry.Value)
                                : _store[key].Expiration < DateTime.Now
                                    ? DateTime.Now.AddDays(1)
                                    : _store[key].Expiration
                        };
                    else
                        _store.Add(key, new CacheData<RedisValue>()
                        {
                            Value = value,
                            Expiration = DateTime.Now.Add(expiry ?? TimeSpan.FromDays(1))
                        });
                    return true;
                });


            // StringGetAsync
            mock.Setup(database =>
                database.StringGetAsync(
                    It.IsAny<RedisKey>(),
                    CommandFlags.None
                )
            ).ReturnsAsync((RedisKey key, CommandFlags flags) =>
            {
                var cache = _store.ContainsKey(key)
                    ? new CacheData<RedisValue>
                    {
                        Expiration = _store[key].Expiration,
                        Value = _store[key].Value
                    }
                    : null;

                if (cache == null)
                    return RedisValue.Null;

                if (cache.Expiration < DateTime.Now)
                    return RedisValue.Null;

                return cache.Value;
            });


            // KeyDeleteAsync
            mock.Setup(database =>
                database.KeyDeleteAsync(
                    It.IsAny<RedisKey>(),
                    It.IsAny<CommandFlags>()
                )).ReturnsAsync((RedisKey key, CommandFlags flag) =>
            {
                if (_store.ContainsKey(key))
                {
                    _store.Remove(key);
                    return true;
                }
                
                if (_hashStore.ContainsKey(key))
                {
                    _hashStore.Remove(key);
                    return true;
                }

                return false;
            });

            // KeyExpireAsync
            mock.Setup(database =>
                database.KeyExpireAsync(
                    It.IsAny<RedisKey>(),
                    It.IsAny<TimeSpan?>(),
                    It.IsAny<CommandFlags>()
                )).ReturnsAsync((RedisKey key, TimeSpan? timeSpan, CommandFlags flag) =>
            {
                if (_store.ContainsKey(key))
                {
                    if (timeSpan.HasValue)
                        _store[key].Expiration = DateTime.Now.Add(timeSpan.Value);
                    else
                        _store.Remove(key);

                    return true;
                }
                if (_hashStore.ContainsKey(key))
                {
                    if (timeSpan.HasValue)
                        _hashStore[key].Expiration = DateTime.Now.Add(timeSpan.Value);
                    else
                        _hashStore.Remove(key);

                    return true;
                }

                return false;
            });

            // HashSetAsync
            mock.Setup(database =>
                    database.HashSetAsync(
                        It.IsAny<RedisKey>(),
                        It.IsAny<HashEntry[]>(),
                        It.IsAny<CommandFlags>()
                    ))
                .Returns(
                    (RedisKey key, HashEntry[] entries, CommandFlags flags) =>
                    {
                        var value = new CacheData<HashEntry[]>()
                        {
                            Value = entries,
                            Expiration = DateTime.Now.AddDays(1)
                        };

                        if (_hashStore.ContainsKey(key))
                            _hashStore[key] = value;
                        else
                            _hashStore.Add(key, value);

                        return Task.CompletedTask;
                    }
                );

            // HashGetAsync
            mock.Setup(database =>
                database.HashGetAllAsync(
                    It.IsAny<RedisKey>(),
                    CommandFlags.None
                )
            ).ReturnsAsync((RedisKey key, CommandFlags flags) =>
            {
                CacheData<HashEntry[]> cache = _hashStore.ContainsKey(key)
                    ? new CacheData<HashEntry[]>
                    {
                        Expiration = _hashStore[key].Expiration,
                        Value = _hashStore[key].Value
                    }
                    : null;

                if (cache == null)
                    return null;

                return cache.Expiration < DateTime.Now
                    ? null
                    : cache.Value;
            });

            // HashGetAsync field
            mock.Setup(database =>
                database.HashSetAsync(
                    It.IsAny<RedisKey>(),
                    It.IsAny<RedisValue>(),
                    It.IsAny<RedisValue>(),
                    It.IsAny<When>(),
                    CommandFlags.None
                )
            ).ReturnsAsync((RedisKey key, RedisValue hashField, RedisValue value,
                When when, CommandFlags flags) =>
            {
                if (!_hashStore.ContainsKey(key))
                    return false;
                ref HashEntry hashEntry
                    = ref Find(_hashStore[key].Value, entry => entry.Name == hashField.ToString());

                hashEntry = new HashEntry(hashEntry.Name, value);

                return true;
            });

            Database = mock.Object;
        }

        private ref T Find<T>(T[] items, Func<T, bool> predicate)
        {
            for (int i = 0; i < items.Length; i++)
            {
                if (predicate(items[i]))
                    return ref items[i];
            }

            throw new Exception("Value not found");
        }
    }
}