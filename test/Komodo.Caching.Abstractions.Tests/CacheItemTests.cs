using System;
using Xunit;

namespace Komodo.Caching.Abstractions.Tests
{
    public class CacheItemTests
    {
        [Fact]
        public void ConstructorAndExpiration()
        {
            var item = new CacheItem<long>(5, TimeSpan.FromSeconds(5));

            Assert.False(item.IsExpired);
        }
    }
}
