﻿using System;
using System.Collections.Generic;
using System.Linq;
using ProtoBuf;

namespace Gatekeeper.Core.Test
{
    [ProtoContract]
    public class ContractModel
    {
        [ProtoMember(1)]
        public int PublicInt;
        [ProtoMember(2)]
        public long Long { get; set; }
        [ProtoMember(3)]
        public string String { get; set; }
        [ProtoMember(4)]
        public DateTime DateTime { get; set; }
        [ProtoMember(5)]
        public TimeSpan TimeSpan { get; set; }
        [ProtoMember(6)]
        public ContractModel Model { get; set; }
        [ProtoMember(7)]
        public List<ContractModel> TestModels { get; set; }


        public int PrivateTestInt { get; private set; } = 10;

        public override bool Equals(object obj)
        {
            return obj is ContractModel model &&
                   PublicInt == model.PublicInt &&
                   Long == model.Long &&
                   String == model.String &&
                   DateTime == model.DateTime &&
                   TimeSpan.Equals(model.TimeSpan) &&
                   EqualityComparer<ContractModel>.Default.Equals(Model, model.Model) &&
                   (!(TestModels == null ^ model.TestModels == null)
                    && (TestModels == null || TestModels.SequenceEqual(model.TestModels)));
        }

        public override int GetHashCode()
        {
            var hashCode = -470141322;
            hashCode = hashCode * -1521134295 + PublicInt.GetHashCode();
            hashCode = hashCode * -1521134295 + Long.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(String);
            hashCode = hashCode * -1521134295 + DateTime.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<TimeSpan>.Default.GetHashCode(TimeSpan);
            hashCode = hashCode * -1521134295 + EqualityComparer<ContractModel>.Default.GetHashCode(Model);
            hashCode = hashCode * -1521134295 + EqualityComparer<List<ContractModel>>.Default.GetHashCode(TestModels);
            hashCode = hashCode * -1521134295 + PrivateTestInt.GetHashCode();
            return hashCode;
        }
    }
}