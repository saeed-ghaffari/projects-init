﻿using System;
using System.Diagnostics;
using Gatekeeper.Core.Enum;
using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using Test.Common;
using Xunit;

namespace Gatekeeper.Core.Test
{
    public class JwtHelperTests
    {
        [Fact]
        public void EncodeAndDecodeTest()
        {
            var payload = new TestModel()
            {
                DateTime = DateTime.Now,
                String = "test",
                Long = 3232

            };

            var token = JwtHelper.Encode(payload, "test", JwtHashAlgorithm.HS256);

            //decode without validate
            Assert.Equal(payload, JwtHelper.Decode<TestModel>(token, "test", false));

            //decode with validation
            Assert.Equal(payload, JwtHelper.Decode<TestModel>(token, "test", true));

            //decode with incorrect key
            Assert.Throws<GateKeeperException>(() => JwtHelper.Decode<TestModel>(token, "test2", true));

            //invalid signature
            Assert.Throws<GateKeeperException>(() => JwtHelper.Decode<TestModel>(token + "m", "test", true));
        }
    }
}