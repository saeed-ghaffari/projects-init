﻿using System.Threading.Tasks;
using Komodo.Redis.StackExchange;
using Komodo.Tests.Common;
using StackExchange.Redis;
using Xunit;

namespace Komodo.Redis.Exchange.Tests
{
    [Collection(DbFixtureCollection.Name)]
    public class DatabaseStringExtensionsTests
    {
        private readonly IDatabase _db;

        public DatabaseStringExtensionsTests(MockDbFixture mockDb)
        {
            _db = mockDb.Database;
        }


        [Fact]
        public async Task StringDatabaseTest()
        {
            await _db.StringSetAsync<string>("key", "value");

            string fetchedValue = await _db.StringGetAsync<string>("key");

            Assert.Equal("value", fetchedValue);
        }


    }
}