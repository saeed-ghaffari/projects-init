﻿using System;
using System.Threading.Tasks;
using Komodo.Redis.StackExchange;
using Komodo.Tests.Common;
using StackExchange.Redis;
using Xunit;

namespace Komodo.Redis.Exchange.Tests
{
    [Collection(DbFixtureCollection.Name)]
    public class DatabaseHashExtensionsTests
    {
        private readonly IDatabase _db;

        public DatabaseHashExtensionsTests(MockDbFixture mockDb)
        {
            _db = mockDb.Database;
        }


        [Fact]
        public async Task HashDatabaseSetAndGet()
        {
            var model = new TestModel()
            {
                PublicInt = 1,
                TimeSpan = TimeSpan.FromDays(1),
                DateTime = DateTime.Now,
                Long = 123,
                Model = new TestModel(),
                String = "test"
            };

            await _db.HashSetAsync<TestModel>("key", model);

            TestModel fetchedValue = await _db.HashGetAsync<TestModel>("key");

            Assert.Equal(model, fetchedValue);
        }

        [Fact]
        public async Task HashDatabaseSetAndGetOnStruct()
        {
            var model = new TestStruct()
            {
                TimeSpan = TimeSpan.FromDays(1),
                DateTime = DateTime.Now,
                Long = 123,
                Model = new TestModel(),
                String = "test"
            };

            await _db.HashSetAsync<TestStruct>("key", model);

            TestStruct fetchedValue = await _db.HashGetAsync<TestStruct>("key");

            Assert.Equal(model, fetchedValue);
        }
    }
}